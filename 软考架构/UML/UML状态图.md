# 目录

- [目录](#目录)
- [概述](#概述)
- [组成结构](#组成结构)
  - [状态](#状态)
  - [转移](#转移)
  - [进入&退出节点](#进入退出节点)
  - [历史状态](#历史状态)
  - [并发区域](#并发区域)
  - [事件](#事件)
- [建模方法](#建模方法)
- [参考引用](#参考引用)

# 概述

状态图是对实体的状态建模，这个实体可以是类、角色、子系统、或组件等任意对象

状态图重点在于描述对象的状态及其状态之间的转移

状态图主要是为了模拟系统响应

# 组成结构

状态图重点在于描述对象的状态及其状态之间的转移，状态图的基本元素主要有：状态、转移、动作、自身转移、组合状态、进入节点、退出节点、历史状态、并发区域等，

1、状态（States）

2、转移（Transitions）

3、动作（State Actions）

4、自身转移（Self-Transitions）

5、组合状态（Compound States）

6、进入节点（Entry Point）

7、退出节点（Exit Point）

8、历史状态（History States）

9、并发区域（Concurrent Regions）

## 状态

**状态的组成部分**：

- 名字（name）：由一个字符串组成，用以识别不同状态。可匿名，一般放置在状态图符顶部
- 入口/出口动作（entry/exit action）：该动作表示进入/退出这个状态所执行的动作。动作可以是原子动作，也可是动作序列
- 内部转换（Internal Transaction） 内部转换不会引起状态变化的转换，此转换的触发不会导致状态的入口/出口动作被执行。语法：事件/动作表达式
- 子状态（Substate） UML中，子状态被定义为状态的嵌套结构，即包含在某状态内部的状态。包含子状态的状态被称为复合状态，不包含子状态的状态被称为简单状态。
- 延迟事件（Deferred Event） 延迟事件是事件的一个列表，此列表内的事件当前状态下不再处理，在系统进入其他状态时再处理。

---
**简单状态**：
状态用圆角矩形表示
初态用实心圆点表示，终态用圆形内嵌圆点表示

```plantuml
@startuml
[*] --> State
State --> [*]
@enduml

```

---

**组合状态/子状态**：
嵌套在另外一个状态中的状态称之为子状态（sub-state）,一个含有子状态的状态被称作组合状态（Compound States），如下图，NotShooting是组合状态，Idle是子状态

```plantuml
@startuml
[*] --> NotShooting

state NotShooting {
  [*] --> Idle
  Idle --> Configuring : EvConfig
  Configuring --> Idle : EvConfig
}

state Configuring {
  [*] --> NewValueSelection
  NewValueSelection --> NewValuePreview : EvNewValue
  NewValuePreview --> NewValueSelection : EvNewValueRejected
  NewValuePreview --> NewValueSelection : EvNewValueSaved

  state NewValuePreview {
     State1 -> State2
  }

}
@enduml

```

---
**入口/出口动作**：

```plantuml
@startuml
[*] --> State
State --> [*]
State: + On Entry/pickup
State: + On Exit/disconnect
@enduml

```

## 转移

转移（Transitions）是两个状态之间的一种关系，表示对象将在源状态（Source State）中执行一定的动作，并在某个特定事件发生而且某个特定的警界条件满足时进入目标状态（Target State）

转换由5个部分组成，分别为源状态、目标状态、触发事件、监护条件和动作。

触发事件（Trigger）：是转移的诱因，可以是一个信号，事件、条件变化（a change in some condition）和时间表达式。

监护条件（Guard Condition）：是一个方括号括起来的布尔表达式，当监护条件满足时，事件才会引发转移（Transition）。

动作（Action）：动作是一个可执行的原子计算，它可以包括操作、调用、另一个对象的创建或撤销、向一个对象发送信号。也可以是一个动作序列。

```plantuml
@startuml
left to right direction
[*] --> 源状态
源状态 --> 目标状态 : Trigger[Condition]/Action
目标状态 --> [*]
@enduml
```

**自身转移**：
状态可以有返回自身状态的转移，称之为自身转移（Self-Transitions）

```plantuml
@startuml
[*] --> state
state --> state : after 2s/poll input
state --> [*]
@enduml
```

## 进入&退出节点

如下图所示，由于一些原因并不会执行初始化（initialization），而是直接通过一个节点进入状态【Ready】，则此节点称之为进入节点（Entry Point）

```plantuml
@startuml
state Somp {
  state entry1 <<entryPoint>>
  state entry2 <<entryPoint>>
  state sin
  entry1 --> sin
  entry2 -> sin
  sin -> sin2
  sin2 --> exitA <<exitPoint>>
}

[*] --> entry1
exitA --> Foo
Foo1 -> entry2
@enduml

```

## 历史状态

历史状态（History States）

历史状态是一个伪状态（Pseudostate）,其目的是记住从组合状态中退出时所处的子状态，当再次进入组合状态，可直接进入这个子状态，而不是再次从组合状态的初态开始。

```plantuml
@startuml
[*] -> State1
State1 --> State2 : Succeeded
State1 --> [*] : Aborted
State2 --> State3 : Succeeded
State2 --> [*] : Aborted
state State3 {
  state "Accumulate Enough Data" as long1
  long1 : Just a test
  [*] --> long1
  long1 --> long1 : New Data
  long1 --> ProcessData : Enough Data
  State2 --> [H]: Resume
}
State3 --> State2 : Pause
State2 --> State3[H*]: DeepResume
State3 --> State3 : Failed
State3 --> [*] : Succeeded / Save Result
State3 --> [*] : Aborted
@enduml

```

## 并发区域

状态图可以分为区域，而区域又包括退出或者当前执行的子状态。说明组合状态在某一时刻可以同时达到多个子状态。如下图刹车系统，同时进入前刹车【Applying Front Brakes】状态和后刹车【Applying Rear Brakes】状态。

```plantuml
@startuml

state fork_state <<fork>>
[*] --> fork_state
fork_state --> State2
fork_state --> State3

state join_state <<join>>
State2 --> join_state
State3 --> join_state
join_state --> State4
State4 --> [*]

@enduml


```

## 事件

# 建模方法

绘制状态图之前，我们必须明确以下几点：

识别对象，以进行分析。

识别状态。

识别的事件

# 参考引用

[状态图](http://www.uml.org.cn/UMLTool/201409282.asp?artid=15053)

<https://blog.csdn.net/mumuxi709/article/details/106937462>
