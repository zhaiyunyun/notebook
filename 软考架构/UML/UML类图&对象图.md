# 目录

- [目录](#目录)
- [类图的组成](#类图的组成)
- [类图的6大关系](#类图的6大关系)
- [对象图](#对象图)
- [mermaid方法画类图](#mermaid方法画类图)
- [参考资料](#参考资料)

# 类图的组成

类图主要是用来显示系统中的类、接口以及它们之间的静态结构和关系的一种静态模型

可视化方法：用一个划分为三个格子的长方形（下面两个格子可省略）

1. 顶层：类名，应用领域的名字
2. 中层：属性，类的性质，表示方法为："可见性 属性名：类型 = 缺省值 {约束特性}"
   1. 可见性：包括 Public、Private 和 Protected，分别用+、-、#号表示
   2. 类型：它可以是基本数据类型，例如整数、实数、布尔型等，也可以是用户自定义的类型
   3. 缺省值：是一个可选项，即属性的初始值
   4. 约束特性则是用户对该属性性质的一个约束说明。例如“{只读}”说明该属性是只读属性
3. 底层：方法，类提供的功能，表示方法为："可见性：操作名（参数表）：返回类型 {约束特性}"

类图中具体类、抽象类、接口区别：
抽象类：类名和方法用*斜体*
接口：接口名变为：`<<interface>>` 换行 接口名

```plantuml
@startuml
class ConcreteClass {
 +field1
 -field2
 #field1

 +method1()
 -method2()
 #method3()
}
note top : 具体类
abstract abstractClass {
 +field1
 -field2
 #field1

 +method1()
 -method2()
 #method3()
}
note top : 抽象类
interface interface {
 +method1()
 -method2()
 #method3()
}
note top : 接口
@enduml
```

# 类图的6大关系

类图中的关系就是UML中的四种基本关系，请参考：[UML关系](./UML基本概念.md#UML关系)

```plantuml
@startuml
class father
interface iter
father <|-- child : 继承
iter <|.. child2 : 实现
Class1 <.. Class2 : 依赖
Class3 *-- Class4 : 组合
Class5 o-- Class6 : 聚合
Class7 <-- Class8 : 单向关联
Class9 -- Class10 : 双向关联
@enduml
```

关联关系中，可以通过关联类进一步关联的属性、操作以及其他信息
关联类的画法:

```plantuml
@startuml
class Student {
  Name
}
Student "0..*" - "1..*" Course
(Student, Course) .. Enrollment

class Enrollment {
  drop()
  cancel()
}
@enduml
```

# 对象图

对象图中的建模元素主要有对象和链，对象是类的实例，链是类之间的关联关系的实例

类图和对象图异同：

1. 对象图只有2层，名称和属性
2. 对象图的名称形式为："对象名：类名"
3. 对象图没有多重性

```plantuml
@startuml
left to right direction

object "Ethel:Writer" as ethel
ethel : name = "Ethel . Albro"
ethel : age = 34

object "Draft:Doument" as draft
draft : name = yun's document
draft : pageNumber = 43
ethel -- draft

class Writer {
 -name : String
 -age : int
}
class Document {
 -name : String
 -pageNumer : int
}
Writer -- Document
@enduml
```

# mermaid方法画类图

```mermaid
classDiagram

%% 泛化，继承关系，空心三角形的直线
classA <|-- classB : Generalization
%% 实现，对接口的实现，空心三角形的虚线
classM <|.. classN : Realization
%% 组合，整体与部分（不可分割），人与心脏，代码体现：成员变量
%% 实心菱形的直线
classC *-- classD : Composition
%% 聚合，整体与部分（可分割），车与发动机，代码体现：成员变量
%% 空心菱形的直线
classE o-- classF : Aggregation
%% 关联，整体与部分，
%% 实线+箭头（可选），带箭头
classG <-- classH : Association
%% 实线+箭头（可选），不带箭头
classI -- classJ : Association2
%% 依赖，使用关系，代码体现：局部变量、方法的参数和静态方法的调用
%% 虚线+箭头（可选），带箭头
classK <.. classL : Dependency
%% 虚线+箭头（可选），不带箭头
classO .. classP : Dependency2

```

# 参考资料

[mermaid类图关系](https://mermaid-js.github.io/mermaid/#/classDiagram?id=defining-relationship)

[UML类图有哪几种关系呢？](https://www.zhihu.com/question/419192424/answer/1471808645)
