# 目录

# 介绍

组件定义：组件是定义了良好接口的物理实现单元，是系统中可替换的物理部件。比如代码文件、动态链接库，脚本，web页面等，都属于组件。
**组件与类的区别**：

1. 类表示的是逻辑的抽象，而组件是存在于计算机中的物理抽象。其表现为组件是可以部署的，而类是不可以被部署的。
2. 组件表示的是物理模块，与类处于不同的抽象级别。（组件的粒度粗，类的粒度细。可以认为组件是由一组类与协作组成的。）

3. 类可以直接拥有操作和属性，而组件仅仅拥有可以通过接口访问的操作。

# 组成元素

基本元素
构件图的主要元素包括构件、接口和依赖关系。

## 组件

在UML中，构件用一个左侧带有突出两个小矩形的矩形来表示。

```plantuml
@startuml
[First component]
@enduml
```

## 接口

```plantuml
@startuml
() "First Interface"
interface Interf3
@enduml

```

## 组件和接口的关系

对于一个构件而言，它有两类接口：提供（provided）接口和所需（required）的接口。
对应的关系分为两种
提供：实现关系（实线连接）
需要：依赖关系（虚线箭头）

```plantuml
@startuml

DataAccess - [First Component]
[First Component] ..> HTTP : use

@enduml
```

## 组件之间的关系

构件间的关系以依赖的形式表达。把提供服务的构件称为提供者，把使用服务的构件称为客户。

在UML中，构件图中依赖关系的表示方法与类图中依赖关系相同，都是一个由客户指向提供者的虚线箭头。

```plantuml
@startuml
left to right direction
[find.exe] --> [net.dll]

@enduml
```

# 参考引用

<https://zhuanlan.zhihu.com/p/149284131>
