# 目录

# 介绍

部署图(deploymentdiagram，配置图)是用来显示系统中软件和硬件的物理架构。从部署图中，您可以了解到软件和硬件组件之间的物理关系以及处理节点的组件分布情况。

主要是对节点及其关系的建模

**节点**代表一个运行时计算机系统中的硬件资源，拥有一定的内存，并具有处理能力。例如一台计算机、一个工作站等其它计算设备都属于节点

一般用立方体表示，如下图：

```plantuml
@startuml
node node
@enduml
```

**节点与组件的区别**：

|   |  节点  |  组件 |
|  -- |  --    | --   |
| 表示   |硬件部件  |软件部件 |
| 相同点 | 1.参与依赖、泛化和关联关系，2.可以被嵌套，3.都可以有实例，4.都可以参与交互  | |
| 不同点 |执行构件的平台  |软件系统执行的主体 |
| 关系 |在节点上部署构件 |

**节点中的构件**：

```plantuml
@startuml
node Foo1 {
    database db
    [First component] ..> db
}
@enduml
```

**连接**

表示两个节点之间的物理连接（关系），用一根实线表示，通过约束对连接进行描述。

```plantuml
@startuml

node node1
node node2
node node3
node node4
node node5
node1 -- node2 : http
node1 .. node3
node1 ~~ node4
node1 == node5

@enduml
```

# 参考引用

<https://blog.csdn.net/CYL_happygirl/article/details/20036827>
