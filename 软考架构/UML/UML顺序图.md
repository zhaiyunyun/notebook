# 目录

- [目录](#目录)
- [概述](#概述)
- [序列图组成](#序列图组成)
  - [对象和对象状态](#对象和对象状态)
  - [消息](#消息)
  - [消息控制](#消息控制)
  - [分支与从属流](#分支与从属流)
- [参考引用](#参考引用)

# 概述

顺序图是交互图的一种，描述对象之间按照时间顺序交互的过程

序列图是对对象之间传送消息的时间顺序的可视化表示。序列图的主要用途是把用例表达的需求，转化为进一步、更加正式层次的精细表达。用例常常被细化为一个或者更多的序列图。同时序列图更有效地描述如何分配各个类的职责以及各类具有相应职责的原因。

# 序列图组成

序列图（Sequence Diagram）是由对象（Object）、生命线（Lifeline）、激活（Activation）、消息（Messages）、分支与从属流等元素构成的。

## 对象和对象状态

对象有：参与者（小人表示）、对象、边界类、实体类、控制类

```plantuml
@startuml
participant "：对象" as Foo
actor "：参与者" as Foo1
boundary "：边界类"  as Foo2
control "：控制类" as Foo3
entity "：实体类" as Foo4
Foo -> Foo1 : To actor 
Foo -> Foo2 : To boundary
Foo -> Foo3 : To control
Foo -> Foo4 : To entity
@enduml
```

对象有三种状态：

- 存在（生命线）：对象下面的虚线表示
- 激活：指对象处于执行状态。矩形条表示
- 销毁：用叉号表示

```plantuml
@startuml
A -> B: DoWork
activate B
destroy B
@enduml

```

## 消息

在UML中消息分为5类：简单消息，同步消息，异步消息，返回消息
消息格式：[条件][消息序号][返回值：=]消息名（[参数列表]）

```plantuml
@startuml
autoactivate on
Bob ->> Alice : 简单消息
Bob -\\ Alice : 异步消息
Bob -> Alice : 同步消息
Bob <<-- Alice : 返回消息
@enduml
```

## 消息控制

消息可以根据不同条件以限制它们只在满足条件时才能被发送。这里的条件分为多种，如if类型的条件，if…else类型的条件，switch…case类型的条件

条件控制用矩形方框表示。有下列几种：

alt：选择性片段。
loop：条件为真的循环片段。
opt：可选片段。
par：并行执行片段。
region：只能执行一个线段的临界片段。

```plantuml
@startuml
Alice -> Bob: 认证请求

alt 成功情况

    Bob -> Alice: 认证接受

else 某种失败情况

    Bob -> Alice: 认证失败
    group 我自己的标签
    Alice -> Log : 开始记录攻击日志
        loop 1000次
            Alice -> Bob: DNS 攻击
        end
    Alice -> Log : 结束记录攻击日志
    end

else 另一种失败

   Bob -> Alice: 请重复

end
@enduml

```

## 分支与从属流

有两种方式根据不同条件，选择发去那里

1. 分支是指从同一点发出的多个消息并指向不同的对象，根据条件是否互斥，可以有条件和并行两种结构
2. 从属流指的是从同一点发出多个消息指向同一个对象的不同生命线。

```plantuml
@startuml
autoactivate on

actor "：用户" as user
participant "：系统" as system
participant "：打印机" as printer

user -> system: 打印文件
system -> system: 验证权限
deactivate system
deactivate system

system -> printer: 打印文件
system -> user: 无权打印
@enduml
```

**TODO**: 打印文件和无权打印，是同时发送给不同对象的，puml无法实现
puml，无法实现分支和从属流

**消息控制和分支从属流区别**：
消息控制，根据不同条件决定是否发送消息
分支从属流，根据不同条件，是把消息发到哪里

# 参考引用

[分支从属流+消息条件控制](https://deepinout.com/uml-tutorials/uml-sequence-diagram/uml-sequence-diagram-constitute-elements.html)
