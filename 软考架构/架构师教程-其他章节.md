# 目录

# 第1章 计算机组成与体系结构

1.3 流水线

**流水线周期（T）**：最耗时的那一段所消耗的时间为流水线周期。如：使用流水线技术执行 100 条指令，每条指令取指 2ms，分析 4ms，执行 1ms，则流水线周期为 4ms。
**流水线执行时间**：第 1 条指令的执行时间+（n-1）*流水线周期
流水线吞吐率（TP）：是指在单位时间内流水线所完成的任务数量或输出的结果数量
**实际吞吐率** = n / Tk；n为指令数，Tk为流水线执行时间
**最大吞吐率** = 1 / T；T为流水线周期
**流水线的加速比**：不使用流水线所用的时间与使用流水线所用的时间之比称为流水线的加速比（speedup ratio）
