# 目录

- [目录](#目录)
- [设计模式分类](#设计模式分类)
- [23种设计模式](#23种设计模式)
  - [创建型模式](#创建型模式)
    - [简单工厂模式](#简单工厂模式)
    - [工厂方法模式(Factory Method)](#工厂方法模式factory-method)
    - [抽象工厂模式(Abstract Factory)](#抽象工厂模式abstract-factory)
    - [建造者模式（Builder）](#建造者模式builder)
    - [原型模式(Prototype)](#原型模式prototype)
    - [单例模式(Singleton)](#单例模式singleton)
  - [结构型模式](#结构型模式)
    - [代理模式](#代理模式)
    - [适配器模式](#适配器模式)
    - [桥接模式](#桥接模式)
    - [装饰器模式](#装饰器模式)
    - [外观模式](#外观模式)
    - [享元模式](#享元模式)
    - [组合模式](#组合模式)
  - [行为型模式](#行为型模式)
    - [模板方法模式](#模板方法模式)
    - [策略模式](#策略模式)
    - [命令模式](#命令模式)
    - [责任链模式](#责任链模式)
    - [状态模式](#状态模式)
    - [观察者模式](#观察者模式)
    - [中介者模式](#中介者模式)
    - [迭代器模式](#迭代器模式)
    - [访问者模式](#访问者模式)
    - [备忘录模式](#备忘录模式)
    - [解释器模式](#解释器模式)
- [设计模式的六大原则](#设计模式的六大原则)
- [参考引用](#参考引用)

# 设计模式分类

![设计模式分类](../images/设计模式分类.png)

按设计模式的目的划分为三类：

- 创建型：创建型模式将创建对象的过程进行了抽象，也可以理解为将创建对象的过程进行了封装，作为客户程序仅仅需要去使用对象，而不再关心创建对象过程中的逻辑
- 结构型：结构型模式是为解决怎样组装现有的类，设计他们的组合方式，从而达到实现一定的功能的目的。结构型模式包容了对很多问题的解决。例如：扩展性（外观、组成、代理、装饰）封装性（适配器，桥接）
- 行为型：行为型模式用于对象之间的职责划分，它不仅描述对象提供的服务，还描述它们之间的通信。处理类和对象间的交互方式和任务分布

# 23种设计模式

## 创建型模式

创建型模式的主要关注点是“怎样创建对象？”，它的**主要特点是“将对象的创建与使用分离”**。这样可以降低系统的耦合度，使用者不需要关注对象的创建细节，对象的创建由相关的工厂来完成。就像我们去商场购买商品时，不需要知道商品是怎么生产出来一样，因为它们由专门的厂商生产。

使用new直接创建对象的**坏处**：实例化对象和对象配置的代码在客户代码中到处都是，不好维护

**解决方法**：创建对象和使用对象分开，单一职责原则
**具体步骤**：工厂类封装创建对象过程包括参数设置等，抽象产品类提供产品使用接口

### 简单工厂模式

也叫静态工厂模式，分为三部分：
**IProduct：抽象产品类**，简单工厂模式所创建的所有对象的父类，它负责描述所有实例所共有的公共接口。
**Product：具体产品类**，是简单工厂模式的创建目标
**Factory：工厂类**，简单工厂模式的核心，它负责实现创建所有实例的内部逻辑。工厂类的创建产品类的方法可以被外界直接调用，创建所需的产品对象

```java
// 抽象产品类
public interface Shape {
   void draw();
}
// 具体产品类
public class Rectangle implements Shape {
 
   @Override
   public void draw() {
      System.out.println("Inside Rectangle::draw() method.");
   }
}
public class Square implements Shape {
 
   @Override
   public void draw() {
      System.out.println("Inside Square::draw() method.");
   }
}
// 工厂类
public class ShapeFactory {
    
   //使用 getShape 方法获取形状类型的对象
   public Shape getShape(String shapeType){
      if(shapeType == null){
         return null;
      }        
      if(shapeType.equalsIgnoreCase("CIRCLE")){
         return new Circle();
      } else if(shapeType.equalsIgnoreCase("RECTANGLE")){
         return new Rectangle();
      } else if(shapeType.equalsIgnoreCase("SQUARE")){
         return new Square();
      }
      return null;
   }
}
// 客户类
public class FactoryPatternDemo {
 
   public static void main(String[] args) {
      ShapeFactory shapeFactory = new ShapeFactory();
 
      //获取 Circle 的对象，并调用它的 draw 方法
      Shape shape1 = shapeFactory.getShape("CIRCLE");
 
      //调用 Circle 的 draw 方法
      shape1.draw();
 
      //获取 Rectangle 的对象，并调用它的 draw 方法
      Shape shape2 = shapeFactory.getShape("RECTANGLE");
 
      //调用 Rectangle 的 draw 方法
      shape2.draw();
 
      //获取 Square 的对象，并调用它的 draw 方法
      Shape shape3 = shapeFactory.getShape("SQUARE");
 
      //调用 Square 的 draw 方法
      shape3.draw();
   }
}
```

**存在的问题**：创建对象的逻辑全部在工厂类中，当新增对象时，需要修改工厂类，违反了开闭原则

### 工厂方法模式(Factory Method)

工厂方法模式，通过增加抽象工厂类，解决了简单工厂的问题，符合开闭原则
分为四部分：

- Product：抽象产品类。
- ConcreteProduct：具体产品类，实现Product接口。
- Factory：抽象工厂类，该方法返回一个Product类型的对象。
- ConcreteFactory：具体工厂类，返回ConcreteProduct实例。

```c++
#include <iostream>
using namespace std;

class Product
{
public:
    virtual void Show() = 0;
};

class ProductA : public Product
{
public:
    void Show()
    {
        cout<< "I'm ProductA"<<endl;
    }
};

class ProductB : public Product
{
public:
    void Show()
    {
        cout<< "I'm ProductB"<<endl;
    }
};

class Factory
{
public:
    virtual Product *CreateProduct() = 0;
};

class FactoryA : public Factory
{
public:
    Product *CreateProduct()
    {
        return new ProductA ();
    }
};

class FactoryB : public Factory
{
public:
    Product *CreateProduct()
    {
        return new ProductB ();
    }
};

int main(int argc , char *argv [])
{
    Factory *factoryA = new FactoryA();
    Product *productA = factoryA->CreateProduct();
    productA->Show();

    Factory *factoryB = new FactoryB ();
    Product *productB = factoryB->CreateProduct();
    productB->Show();
    return 0;
}
```

### 抽象工厂模式(Abstract Factory)

简单工厂和工厂方法的**局限**：只能创建同一类产品，也就是实现同一抽象产品接口的产品，如果要创建完全不同的产品，就需要完全复制和修改原产品创建的所有代码

抽象工厂的**解决办法**：在抽象工厂类中增加创建新产品的接口，并在具体工厂中实现新加产品的创建

在抽象工厂模式中有如下角色：

- AbstractFactory：抽象工厂，它声明了用来创建**多个不同产品**的方法
- ConcreteFactory：具体工厂，实现抽象工厂中定义的创建产品的方法
- AbstractProduct：抽象产品，提供**多个抽象产品**
- ConcreteProduct：具体产品，定义具体工厂生产的具体产品，并实现抽象产品中定义的业务方法。

```java
// 抽象产品：PC
public interface PC {
    void make();
}
// 具体产品：小米PC
public class MiPC implements PC {
    public MiPC() {
        this.make();
    }
    @Override
    public void make() {
        // TODO Auto-generated method stub
        System.out.println("make xiaomi PC!");
    }
}
// 具体产品：苹果PC
public class MAC implements PC {
    public MAC() {
        this.make();
    }
    @Override
    public void make() {
        // TODO Auto-generated method stub
        System.out.println("make MAC!");
    }
}
// 抽象产品：手机
public interface Phone {
    void make();
}
// 具体产品：小米手机
public class MiPhone implements Phone {
    public MiPhone() {
        this.make();
    }
    @Override
    public void make() {
        // TODO Auto-generated method stub
        System.out.println("make xiaomi phone!");
    }
}
// 具体产品：苹果手机
public class IPhone implements Phone {
    public IPhone() {
        this.make();
    }
    @Override
    public void make() {
        // TODO Auto-generated method stub
        System.out.println("make iphone!");
    }
}

// 抽象工厂，可以生产多种产品
public interface AbstractFactory {
    Phone makePhone();
    PC makePC();
}
// 具体工厂：小米工厂，可生产小米手机和小米PC
public class XiaoMiFactory implements AbstractFactory{
    @Override
    public Phone makePhone() {
        return new MiPhone();
    }
    @Override
    public PC makePC() {
        return new MiPC();
    }
}
// 具体工厂：苹果工厂
public class AppleFactory implements AbstractFactory {
    @Override
    public Phone makePhone() {
        return new IPhone();
    }
    @Override
    public PC makePC() {
        return new MAC();
    }
}
// 客户代码
public class Demo {
    public static void main(String[] arg) {
       AbstractFactory miFactory = new XiaoMiFactory();
       AbstractFactory appleFactory = new AppleFactory();
       miFactory.makePhone();            // make xiaomi phone!
       miFactory.makePC();                // make xiaomi PC!
       appleFactory.makePhone();        // make iphone!
       appleFactory.makePC();            // make MAC!
    }
}
```

### 建造者模式（Builder）

在软件开发过程中有时需要创建一个**复杂的对象**，这个复杂对象通常由多个子部件**按一定的步骤组合而成**。例如，计算机是由 CPU、主板、内存、硬盘、显卡、机箱、显示器、键盘、鼠标等部件组装而成的，采购员不可能自己去组装计算机，而是将计算机的配置要求告诉计算机销售公司，计算机销售公司安排技术人员去组装计算机，然后再交给要买计算机的采购员。

**建造者模式和工厂模式的不同**：

1. 关注点不同：建造者模式注重零部件的组装过程，而工厂方法模式更注重零部件的创建过程，但两者可以结合使用。
2. 建造者的是针对复杂对象的创建。也就是说，如果创建简单对象，通常都是使用工厂模式进行创建，而如果创建复杂对象，就可以考虑使用建造者模式

建造者（Builder）模式的主要角色如下。

- **产品角色**（Product）：它是包含多个组成部件的复杂对象，由具体建造者来创建其各个零部件。
- **抽象建造者**（Builder）：它是一个包含创建产品各个子部件的抽象方法的接口，通常还包含一个返回复杂产品的方法 getResult()。
- **具体建造者**(Concrete Builder）：实现 Builder 接口，完成复杂产品的各个部件的具体创建方法。
- **指挥者**（Director）：决定如何构建最终产品的算法，其会包含一个负责组装的方法void Construct(Builder builder)，它调用抽象建造者的方法按组装算法生产零部件，最终返回产品

```java
public class ParlourDecorator {
    public static void main(String[] args) {
        try {
            Decorator d;
            d = (Decorator) ReadXML.getObject();
            ProjectManager m = new ProjectManager(d);
            Parlour p = m.decorate();
            p.show();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

//产品：客厅
class Parlour {
    private String wall;    //墙
    private String TV;    //电视
    private String sofa;    //沙发 

    public void setWall(String wall) {
        this.wall = wall;
    }

    public void setTV(String TV) {
        this.TV = TV;
    }

    public void setSofa(String sofa) {
        this.sofa = sofa;
    }

    public void show() {
    }
}

//抽象建造者：装修工人
abstract class Decorator {
    //创建产品对象
    protected Parlour product = new Parlour();

    public abstract void buildWall();

    public abstract void buildTV();

    public abstract void buildSofa();

    //返回产品对象
    public Parlour getResult() {
        return product;
    }
}

//具体建造者：具体装修工人1
class ConcreteDecorator1 extends Decorator {
    public void buildWall() {
        product.setWall("w1");
    }

    public void buildTV() {
        product.setTV("TV1");
    }

    public void buildSofa() {
        product.setSofa("sf1");
    }
}

//具体建造者：具体装修工人2
class ConcreteDecorator2 extends Decorator {
    public void buildWall() {
        product.setWall("w2");
    }

    public void buildTV() {
        product.setTV("TV2");
    }

    public void buildSofa() {
        product.setSofa("sf2");
    }
}

//指挥者：项目经理
class ProjectManager {
    private Decorator builder;

    public ProjectManager(Decorator builder) {
        this.builder = builder;
    }

    //产品构建与组装算法
    public Parlour decorate() {
        builder.buildWall();
        builder.buildTV();
        builder.buildSofa();
        return builder.getResult();
    }
}
// 客户代码
public class ParlourDecorator {
    public static void main(String[] args) {
        try {
            Decorator d = new ConcreteDecorator2();
            ProjectManager m = new ProjectManager(d);
            Parlour p = m.decorate();
            p.show();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
```

### 原型模式(Prototype)

需要创建相同或类似对象，并且**创建成本比较高**（耗时或繁琐）或者**不知道创建细节**，则可以利用原型模式，就是克隆模式

原型模式的克隆分为浅克隆和深克隆。
浅克隆：创建一个新对象，新对象的属性和原来对象完全相同，对于非基本类型属性，仍指向原有属性所指向的对象的内存地址。
深克隆：创建一个新对象，属性中引用的其他对象也会被克隆，不再指向原有对象地址。

原型模式包含以下主要角色。

- 抽象原型类：规定了具体原型对象必须实现的接口。
- 具体原型类：实现抽象原型类的 clone() 方法，它是可被复制的对象。
- 访问类：使用具体原型类中的 clone() 方法来复制新的对象。

```java
// 抽象原型类
public interface Prototype {
    Prototype clone();
}
// 具体原型类
public class Report implements Prototype {
    private List<String> parts;

    public Report() {
        this.parts = new ArrayList<>();
    }

    public Report(List<String> parts) {
        this.parts = parts;
    }
    //耗时的数据加载操作
    public void loadData() {
        pats.clear();
        parts.add("老夫聊发少年狂，左牵黄，右擎苍，锦帽貂裘，千骑卷平冈。");
        parts.add("为报倾城随太守，亲射虎，看孙郎。");
        parts.add("酒酣胸胆尚开张，鬓微霜，又何妨！持节云中，何日遣冯唐？");
        parts.add("会挽雕弓如满月，西北望，射天狼。");
    }

    public List<String> getContents() {
        return parts;
    }

    @Override
    public Prototype copy() {
        List<String> cloneList = new ArrayList<>(parts);
        return new Report(cloneList);
    }
}

// 客户端使用
public class PrototypeClient {
    public void getReport(){
        //创建原型
        Report reportPrototype = new Report();
        //耗费资源的操作
        reportPrototype.loadData();

        //使用原型对象构建新的对象
        Report reportWithTitle = (Report) reportPrototype.copy();
        List<String> reportContent= reportWithTitle.getContents();
        reportContent.add(0,"《江城子·密州出猎》");
        reportContent.add(1,"----------------------------------------------------------");

        for (String s : reportContent) {
            System.out.println(s);
        }
    }
}
```

### 单例模式(Singleton)

为了节省内存资源、保证数据内容的一致性，对某些类要求只能创建一个实例，这就是所谓的单例模式。
方法：构造函数私有化，提供静态方法getInstance()获取对象实例

```java
// 懒汉式单例
public class LazySingleton {
    private static volatile LazySingleton instance = null;    //保证 instance 在所有线程中同步

    private LazySingleton() {
    }    //private 避免类在外部被实例化

    public static synchronized LazySingleton getInstance() {
        //getInstance 方法前加同步
        if (instance == null) {
            instance = new LazySingleton();
        }
        return instance;
    }
}
// 饿汉式单例
public class HungrySingleton {
    private static final HungrySingleton instance = new HungrySingleton();

    private HungrySingleton() {
    }

    public static HungrySingleton getInstance() {
        return instance;
    }
}
```

## 结构型模式

### 代理模式

为啥需要代理？

1. 客户类不能直接访问委托类
2. 通过代理类扩展委托类，代理类主要负责为委托类预处理消息、过滤消息、把消息转发给委托类，以及事后对返回结果的处理等

代理模式组成元素：

1. 服务接口类，通过接口或抽象类声明委托类和代理类实现的业务方法。
2. 委托类：实现了服务接口的具体业务
3. 代理类：提供了与真实主题相同的接口，其内部含有对真实主题的引用，它可以访问、控制或扩展真实主题的功能

根据代理的创建时期，代理模式分为静态代理和动态代理。
静态：由程序员创建代理类或特定工具自动生成源代码再对其编译，在程序运行前代理类的 .class 文件就已经存在了。
动态：在程序运行时，运用反射机制动态创建而成

```java
// 服务接口类
public interface BuyHouse {
    void buyHosue();
}

// 委托类
public class BuyHouseImpl implements BuyHouse {

    @Override
    public void buyHosue() {
        System.out.println("我要买房");
    }
}
// 代理类
public class BuyHouseProxy implements BuyHouse {

    private BuyHouse buyHouse;

    public BuyHouseProxy(final BuyHouse buyHouse) {
        this.buyHouse = buyHouse;
    }

    @Override
    public void buyHosue() {
        System.out.println("买房前准备");
        buyHouse.buyHosue();
        System.out.println("买房后装修");

    }
}
// 客户类
public class ProxyTest {
    public static void main(String[] args) {
        BuyHouse buyHouse = new BuyHouseImpl();
        BuyHouseProxy buyHouseProxy = new BuyHouseProxy(buyHouse);
        buyHouseProxy.buyHosue();
    }
}
```

### 适配器模式

定义：将一个类的接口转换成客户希望的另外一个接口，使得原本由于接口不兼容而不能一起工作的那些类能一起工作。

适配器模式（Adapter）包含以下主要角色。
**目标（Target）接口**：适配为的内容，比如需要用的接口。
**适配者（Adaptee）类**：被适配的内容，比如不兼容的接口。
**适配器（Adapter）类**：适配器，把 Adeptee 适配成 Target。

有继承和组合两种实现方式：
**继承方式**：

```java
package adapter;
//目标接口
interface Target
{
    public void hello();
}
//适配者接口
class Adaptee
{
    public void sayHello()
    {       
        System.out.println("hello");
    }
}
//类适配器类
class ClassAdapter extends Adaptee implements Target
{
    public void hello()
    {
        sayHello();
    }
}
//客户端代码
public class ClassAdapterTest
{
    public static void main(String[] args)
    {
        Target target = new ClassAdapter();
        target.hello();
    }
}
```

**组合方式**：

```java
//目标接口
interface Target
{
    public void hello();
}
//适配者接口
class Adaptee
{
    public void sayHello()
    {       
        System.out.println("hello");
    }
}
//对象适配器类
class ObjectAdapter implements Target
{
    private Adaptee adaptee;
    public ObjectAdapter(Adaptee adaptee)
    {
        this.adaptee=adaptee;
    }
    public void hello()
    {
        adaptee.sayHello();
    }
}
//客户端代码
public class ObjectAdapterTest
{
    public static void main(String[] args)
    {
        Adaptee adaptee = new Adaptee();
        Target target = new ObjectAdapter(adaptee);
        target.hello();
    }
}
```

### 桥接模式

### 装饰器模式

使用场景： 1、扩展一个类的功能。 2、动态增加功能，动态撤销。

注意事项：可代替继承。
<https://www.runoob.com/design-pattern/decorator-pattern.html>

注意事项： 1、和适配器模式的区别：适配器模式主要改变所考虑对象的接口，而代理模式不能改变所代理类的接口。 2、和装饰器模式的区别：装饰器模式为了增强功能，而代理模式是为了加以控制。

### 外观模式

### 享元模式

### 组合模式

参考：<https://www.cnblogs.com/adamjwh/p/9033547.html>
<http://c.biancheng.net/view/1373.html>

有时又叫作整体-部分（Part-Whole）模式，它是一种将对象组合成树状的层次结构的模式，用来表示“整体-部分”的关系，使用户对单个对象和组合对象具有一致的访问性，属于结构型设计模式。

组合模式包含以下主要角色。
**抽象构件（Component）角色**：它的主要作用是为树叶构件和树枝构件声明**公共接口**，并实现它们的默认行为
**树叶构件（Leaf）角色**：是组合中的叶节点对象，它没有子节点，用于继承或实现抽象构件。
**树枝构件（Composite）角色**：是组合中的分支节点对象，它有子节点，用于继承和实现抽象构件。它的主要作用是存储和管理子部件，通常包含 Add()、Remove()、GetChild() 等方法。

**透明方式**：

```java
//抽象构件
interface Component {
    public void add(Component c);
    public void remove(Component c);
    public Component getChild(int i);
    public void operation();
}

//树叶构件
class Leaf implements Component {
    private String name;
    public Leaf(String name) {
        this.name = name;
    }
    public void add(Component c) {
    }
    public void remove(Component c) {
    }
    public Component getChild(int i) {
        return null;
    }
    public void operation() {
        System.out.println("树叶" + name + "：被访问！");
    }
}

//树枝构件
class Composite implements Component {
    private ArrayList<Component> children = new ArrayList<Component>();
    public void add(Component c) {
        children.add(c);
    }
    public void remove(Component c) {
        children.remove(c);
    }
    public Component getChild(int i) {
        return children.get(i);
    }
    public void operation() {
        for (Object obj : children) {
            ((Component) obj).operation();
        }
    }
}
// 客户代码
public class CompositePattern {
    public static void main(String[] args) {
        Component c0 = new Composite();
        Component c1 = new Composite();
        Component leaf1 = new Leaf("1");
        Component leaf2 = new Leaf("2");
        Component leaf3 = new Leaf("3");
        c0.add(leaf1);
        c0.add(c1);
        c1.add(leaf2);
        c1.add(leaf3);
        c0.operation();
    }
}
```

**安全方式**：

```java
// 修改抽象类接口
interface Component {
    public void operation();
}
public class CompositePattern {
    public static void main(String[] args) {
        Composite c0 = new Composite();
        Composite c1 = new Composite();
        Component leaf1 = new Leaf("1");
        Component leaf2 = new Leaf("2");
        Component leaf3 = new Leaf("3");
        c0.add(leaf1);
        c0.add(c1);
        c1.add(leaf2);
        c1.add(leaf3);
        c0.operation();
    }
}
```

**透明式和安全式区别**：
透明式优点：抽象构件声明了所有子类中的全部方法，所以客户端无须区别树叶对象和树枝对象，对客户端来说是透明的
透明式缺点：树叶构件本来没有 Add()、Remove() 及 GetChild() 方法，却要实现它们（空实现或抛异常），这样会带来一些安全性问题

安全式：将管理子构件的方法移到树枝构件中，抽象构件和树叶构件没有对子对象的管理方法，这样就避免了上一种方式的安全性问题，但由于叶子和分支有不同的接口，客户端在调用时要知道树叶对象和树枝对象的存在，所以失去了透明性。

## 行为型模式

### 模板方法模式

### 策略模式

### 命令模式

### 责任链模式

### 状态模式

### 观察者模式

别名：listener模式，observer模式，又称发布订阅模式
**应用场景**：定义对象间的一种一对多的依赖关系，当一个对象的状态发生改变时，所有依赖于它的对象都得到通知并被自动更新。

**角色**：

1. Subject：主题接口（**抽象被观察者**），保存观察者对象的列表，提供增加、删除观察者的方法，并提供notify方法，给观察者发消息
2. ConcreteSubject：具体主题（**具体被观察者**），保存状态信息，在状态发生改变时，调用notify方法通知观察者
3. Observer：**抽象观察者**，是观察者者的抽象类，定义update方法，状态更新时被调用
4. ConcrereObserver：**具体观察者**，实现update接口

**类图**：

```plantuml
@startuml

interface Subject {
    + attach(Observer o)
    + dettach(Observer o)
    + notify()
}

class ConcreteSubject implements Subject {
    - List<Observer> observer_list
    + setState(State s)
    + getState()
}

interface Observer {
    + update()
}

class ConcrereObserver implements Observer {
    + update()
}

Subject ..> Observer : uses

@enduml
```

示例代码：

**被观察者接口**

```java
public abstract class Subject {
    private List<Observer> observerList = new ArrayList<Observer>();

    /**
     * 增加订阅者
     * @param observer
     */
    public void attach(Observer observer){
        observerList.add(observer);
    }

    /**
     * 删除订阅者
     * @param observer
     */
    public void detach(Observer observer) {
        observerList.remove(observer);
    }
    /**
     * 通知订阅者更新消息
     */
    public void notify() {
        for (Observer observer : observerList) {
            observer.update();
        }
    }
}

```

**具体被观察者**

```java
public class SubscriptionSubject implements Subject {

    private int state;

    public void setState(int state) {
        this.state = state
        notify()
    }

    public int getState(){
        return this.state
    }
}
```

**观察者接口**

```java
public abstract class Observer {
   protected Subject subject;
   public abstract void update();
}
```

**具体观察者**

```java
public class HexaObserver extends Observer{
 
   public HexaObserver(Subject subject){
      this.subject = subject;
      this.subject.attach(this);
   }
 
   @Override
   public void update() {
      System.out.println( "Hex String: " 
      + Integer.toHexString( subject.getState() ).toUpperCase() ); 
   }
}

```

**客户端**

```java
public class Client {
    public static void main(String[] args) {
        Subject subject = new Subject();

        new HexaObserver(subject);
        new OctalObserver(subject);
        new BinaryObserver(subject);

        System.out.println("First state change: 15");   
        subject.setState(15);
        System.out.println("Second state change: 10");  
        subject.setState(10);
    }
}
```

### 中介者模式

### 迭代器模式

### 访问者模式

**总结**：
两种角色，访问者和元素，对象结构是元素的容器
访问者定义visit方法，参数是具体元素
元素定义accept方法，参数是访问者，实现：调用访问者的visit(this)方法
客户使用方法：调用容器的visit方法，内部循环调用所有元素的accept，把访问者传进去

应用场景：将数据结构和操作分离，适用于数据结构不变，经常需要添加新操作的场景
有如下**角色**：

(1) Vistor（**抽象访问者**）：为该对象结构中具体元素角色声明一个访问操作接口。

(2) ConcreteVisitor（**具体访问者**）：每个具体访问者都实现了Vistor中定义的操作。

(3) Element（**抽象元素**）：定义了一个accept操作，以Visitor作为参数。

(4) ConcreteElement（**具体元素**）：实现了Element中的accept()方法，调用Vistor的访问方法以便完成对一个元素的操作。

(5) ObjectStructure（**对象结构**）：可以是组合模式，也可以是集合；能够枚举它包含的元素；提供一个接口，允许Vistor访问它的元素。

**类图**

```plantuml
@startuml
interface Visitor {
    + visit(Student element);
}
class GradeSelection implements Visitor {
    + void visit(Student element)
}
interface Element {
    + accept(Visitor visitor);
}

class Student implements Element {
    + void accept(Visitor visitor)
}
class ObjectStructure {
    - ArrayList<Element> elements = new ArrayList<>();
    + void addElement(Element element)
    + void accept(Visitor visitor)
}
 
ObjectStructure "1" *-- "*" Element
ObjectStructure <.. Visitor

@enduml
```

**顺序图**

```plantuml
@startuml
autoactivate on
VisitorClient -> ObjectStructure : addElement(Student)
deactivate
VisitorClient -> Visitor : new gradeSelection()
deactivate
VisitorClient -> ObjectStructure : accept(gradeSelection)
loop elements.size()
    ObjectStructure -> Element: accept(visitor)
    Element -> gradeSelection:visitor.visit(this)
end
deactivate
@enduml
```

1. **抽象访问者**

```java
/**
 * 抽象访问者，为该对象结构中具体元素角色声明一个访问操作接口。
 */
public interface Visitor {

    void visit(Student element);

    void visit(Teacher element);

}
```

2. **具体访问者:GradeSelection和ResearcherSelection**

```java
/**
 * 具体访问者，实现了Vistor中定义的操作。
 */
public class GradeSelection implements Visitor {

    private String awardWords = "[%s]的分数是%d，荣获了成绩优秀奖。";

    @Override
    public void visit(Student element) {
        // 如果学生考试成绩超过90，则入围成绩优秀奖。
        if (element.getGrade() >= 90) {
            System.out.println(String.format(awardWords, 
                    element.getName(), element.getGrade()));
        }
    }

    @Override
    public void visit(Teacher element) {
        // 如果老师反馈得分超过85，则入围成绩优秀奖。
        if (element.getScore() >= 85) {
            System.out.println(String.format(awardWords, 
                    element.getName(), element.getScore()));
        }
    }
}

/**
 * 具体访问者，实现了Vistor中定义的操作。
 */
public class ResearcherSelection implements Visitor {

    private String awardWords = "[%s]的论文数是%d，荣获了科研优秀奖。";

    @Override
    public void visit(Student element) {
        // 如果学生发表论文数超过2，则入围科研优秀奖。
        if(element.getPaperCount() > 2){
            System.out.println(String.format(awardWords,
                    element.getName(),element.getPaperCount()));
        }
    }

    @Override
    public void visit(Teacher element) {
        // 如果老师发表论文数超过8，则入围科研优秀奖。
        if(element.getPaperCount() > 8){
            System.out.println(String.format(awardWords,
                    element.getName(),element.getPaperCount()));
        }
    }
}

```

3. **抽象元素**

```java
/**
 * 抽象元素角色，定义了一个accept操作，以Visitor作为参数。
 */
public interface Element {

    //接受一个抽象访问者访问
    void accept(Visitor visitor);

}
```

4. **具体元素**

```java
/**
 * 具体元素，允许visitor访问本对象的数据结构。
 */
public class Teacher implements Element {

    private String name; // 教师姓名
    private int score; // 评价分数
    private int paperCount; // 论文数

    // 构造器
    public Teacher(String name, int score, int paperCount) {
        this.name = name;
        this.score = score;
        this.paperCount = paperCount;
    }

    // visitor访问本对象的数据结构
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getPaperCount() {
        return paperCount;
    }

    public void setPaperCount(int paperCount) {
        this.paperCount = paperCount;
    }
}
/**
 * 具体元素，允许visitor访问本对象的数据结构。
 */
public class Student implements Element {

    private String name; // 学生姓名
    private int grade; // 成绩
    private int paperCount; // 论文数

    // 构造器
    public Student(String name, int grade, int paperCount) {
        this.name = name;
        this.grade = grade;
        this.paperCount = paperCount;
    }

    // visitor访问本对象的数据结构
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getPaperCount() {
        return paperCount;
    }

    public void setPaperCount(int paperCount) {
        this.paperCount = paperCount;
    }
}

```

5. **对象结构:ObjectStructure**

```java
/**
 * 对象结构，是元素的集合，提供元素的访问入口。
 */
public class ObjectStructure {

    // 使用集合保存Element元素，示例没有考虑多线程的问题。
    private ArrayList<Element> elements = new ArrayList<>();

    /**
     * 访问者访问元素的入口
     *
     * @param visitor 访问者
     */
    public void accept(Visitor visitor) {
        for (int i = 0; i < elements.size(); i++) {
            Element element = elements.get(i);
            element.accept(visitor);
        }
    }

    /**
     * 把元素加入到集合
     *
     * @param element 待添加的元素
     */
    public void addElement(Element element) {
        elements.add(element);
    }

    /**
     * 把元素从集合中移除
     *
     * @param element 要移除的元素
     */
    public void removeElement(Element element) {
        elements.remove(element);
    }
}

```

6. **VisitorClient 客户端**

```java
/**
 * 如果教师发表论文数超过8篇或者学生论文超过2篇可以评选科研优秀奖，
 * 如果教师教学反馈分大于等于85分或者学生成绩大于等于90分可以评选成绩优秀奖。
 */
public class VisitorClient {

    public static void main(String[] args) {
        // 初始化元素
        Element stu1 = new Student("Student Jim", 92, 3);
        Element stu2 = new Student("Student Ana", 89, 1);
        Element t1 = new Teacher("Teacher Mike", 83, 10);
        Element t2 = new Teacher("Teacher Lee", 88, 7);
        // 初始化对象结构
        ObjectStructure objectStructure = new ObjectStructure();
        objectStructure.addElement(stu1);
        objectStructure.addElement(stu2);
        objectStructure.addElement(t1);
        objectStructure.addElement(t2);
        // 定义具体访问者，选拔成绩优秀者
        Visitor gradeSelection = new GradeSelection();
        // 具体的访问操作，打印输出访问结果
        objectStructure.accept(gradeSelection);
        System.out.println("----结构不变，操作易变----");
        // 数据结构是没有变化的，如果我们还想增加选拔科研优秀者的操作，那么如下。
        Visitor researcherSelection = new ResearcherSelection();
        objectStructure.accept(researcherSelection);
    }
}
```

### 备忘录模式

### 解释器模式

解释器（Interpreter）模式的**定义**：给分析对象定义一个语言，并定义该语言的文法表示，再设计一个解析器来解释语言中的句子。也就是说，用编译语言的方式来分析应用中的实例。这种模式实现了文法表达式处理的接口，该接口解释一个**特定的上下文**。

解释器模式包含以下主要角色。

- 抽象表达式（Abstract Expression）角色：定义解释器的接口，约定解释器的解释操作，主要包含解释方法 interpret()。
- 终结符表达式（Terminal Expression）角色：是抽象表达式的子类，用来实现文法中与终结符相关的操作，文法中的每一个终结符都有一个具体终结表达式与之相对应。
- 非终结符表达式（Nonterminal Expression）角色：也是抽象表达式的子类，用来实现文法中与非终结符相关的操作，文法中的每条规则都对应于一个非终结符表达式。
- 环境（Context）角色：通常包含各个解释器需要的数据或是公共的功能，一般用来传递被所有解释器共享的数据，后面的解释器可以从这里获取这些值。
- 客户端（Client）：主要任务是将需要分析的句子或表达式转换成使用解释器对象描述的抽象语法树，然后调用解释器的解释方法，当然也可以通过环境角色间接访问解释器的解释方法。

```java
package net.biancheng.c.interpreter;

import java.util.*;

/*文法规则
  <expression> ::= <city>的<person>
  <city> ::= 韶关|广州
  <person> ::= 老人|妇女|儿童
*/
public class InterpreterPatternDemo {
    public static void main(String[] args) {
        Context bus = new Context();
        bus.freeRide("韶关的老人");
        bus.freeRide("韶关的年轻人");
        bus.freeRide("广州的妇女");
        bus.freeRide("广州的儿童");
        bus.freeRide("山东的儿童");
    }
}

//抽象表达式类
interface Expression {
    public boolean interpret(String info);
}

//终结符表达式类
class TerminalExpression implements Expression {
    private Set<String> set = new HashSet<String>();

    public TerminalExpression(String[] data) {
        for (int i = 0; i < data.length; i++) set.add(data[i]);
    }

    public boolean interpret(String info) {
        if (set.contains(info)) {
            return true;
        }
        return false;
    }
}

//非终结符表达式类
class AndExpression implements Expression {
    private Expression city = null;
    private Expression person = null;

    public AndExpression(Expression city, Expression person) {
        this.city = city;
        this.person = person;
    }

    public boolean interpret(String info) {
        String s[] = info.split("的");
        return city.interpret(s[0]) && person.interpret(s[1]);
    }
}

//环境类
class Context {
    private String[] citys = {"韶关", "广州"};
    private String[] persons = {"老人", "妇女", "儿童"};
    private Expression cityPerson;

    public Context() {
        Expression city = new TerminalExpression(citys);
        Expression person = new TerminalExpression(persons);
        cityPerson = new AndExpression(city, person);
    }

    public void freeRide(String info) {
        boolean ok = cityPerson.interpret(info);
        if (ok) System.out.println("您是" + info + "，您本次乘车免费！");
        else System.out.println(info + "，您不是免费人员，本次乘车扣费2元！");
    }
}
```

对上下文Context的理解：
用户代码的使用方法有2种：

```java
// 方法一：使用context类间接进行表达式解析
Context bus = new Context({"韶关", "广州"}, {"老人", "妇女", "儿童"});
bus.freeRide("韶关的老人");
bus.freeRide("韶关的年轻人");
// 如果更换一个城市到北京上海呢，就需要另外一个上下文
Context bus = new Context({"北京", "上海"}, {"老人", "妇女", "儿童"});
bus.freeRide("韶关的老人");
bus.freeRide("韶关的年轻人");
// 方法二：客户代码负责创建语法树，然后把上下文传进去，在节点中可以获取context中的数据
Context ctx = new Context({"韶关", "广州"}, {"老人", "妇女", "儿童"});
cityPerson = new AndExpression(city, person);
cityPerson.interpreter(ctx)
```

# 设计模式的六大原则

面向对象设计的六大原则
1、开闭原则（Open Close Principle）

开闭原则的意思是：对扩展开放，对修改关闭。在程序需要进行拓展的时候，不能去修改原有的代码，实现一个热插拔的效果。简言之，是为了使程序的扩展性好，易于维护和升级。想要达到这样的效果，我们需要使用接口和抽象类，后面的具体设计中我们会提到这点。

2、里氏代换原则（Liskov Substitution Principle）

里氏代换原则是面向对象设计的基本原则之一。 里氏代换原则中说，任何基类可以出现的地方，子类一定可以出现。LSP 是继承复用的基石，只有当派生类可以替换掉基类，且软件单位的功能不受到影响时，基类才能真正被复用，而派生类也能够在基类的基础上增加新的行为。里氏代换原则是对开闭原则的补充。实现开闭原则的关键步骤就是抽象化，而基类与子类的继承关系就是抽象化的具体实现，所以里氏代换原则是对实现抽象化的具体步骤的规范。

3、依赖倒转原则（Dependence Inversion Principle）

这个原则是开闭原则的基础，具体内容：针对接口编程，依赖于抽象而不依赖于具体。

4、接口隔离原则（Interface Segregation Principle）

这个原则的意思是：使用多个隔离的接口，比使用单个接口要好。它还有另外一个意思是：降低类之间的耦合度。由此可见，其实设计模式就是从大型软件架构出发、便于升级和维护的软件设计思想，它强调降低依赖，降低耦合。

5、迪米特法则，又称最少知道原则（Demeter Principle）

最少知道原则是指：一个实体应当尽量少地与其他实体之间发生相互作用，使得系统功能模块相对独立。

6、合成复用原则（Composite Reuse Principle）

合成复用原则是指：尽量使用合成/聚合的方式，而不是使用继承

# 参考引用

工厂模式参考：<https://www.cnblogs.com/yssjun/p/11102162.html>

设计模式经典：<http://c.biancheng.net/design_pattern/>
