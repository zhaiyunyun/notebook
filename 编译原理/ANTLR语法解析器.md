# 安装

ANTLR 官方网址 <http://www.antlr.org/>
ANTLR 官方 Github <https://github.com/antlr/antlr4>
大量语法文件例子 <https://github.com/antlr/grammars-v4>

中文版介绍：<https://github.com/greycode/technical-articles/issues/1>

语法规则：<https://www.cxymm.net/article/yangguosb/86007195>

[官方安装手册](https://github.com/antlr/antlr4/blob/master/doc/getting-started.md)

[官网](https://www.antlr.org/api/)
[ANTLR4语法说明](https://github.com/antlr/antlr4/blob/4.6/doc/index.md)
[Java API](https://www.antlr.org/api/Java/index.html)
[ANTLR4 简明教程](https://wizardforcel.gitbooks.io/antlr4-short-course/content/introduction.html)
Why Use ANTLR?:<http://bearcave.com/software/antlr/antlr_expr.html>

## java版本

java1.7 == java7
java1.8 == java8

java1.8之后，都是java se 9、10、11等，最新java18了

## java安装

+ [Oracle Java安装](https://www.java.com/zh-CN/download/)：只可用于个人学习和培训，不能商业应用
+ [Oracle openjava安装](http://jdk.java.net/)：Oracle的GPL开源版本，可以商业应用
+ [openjdk安装](http://openjdk.java.net/)：开源社区版，oracle有参与

[oracle openjava8下载](https://download.java.net/openjdk/jdk8u41/ri/openjdk-8u41-b04-windows-i586-14_jan_2020.zip)

配置

```bash
JAVA_HOME
C:\Program Files\java\openjdk-8u41-b04-windows-i586-14_jan_2020\java-se-8u41-ri

CLASSPATH
.;C:\Javalib\antlr-4.9.2-complete.jar;%JAVA_HOME%\lib;%JAVA_HOME%\lib\tools.jar;

Path
%Path%;%JAVA_HOME%\bin;%JAVA_HOME%\jre\bin;
```

# 使用方法

[博客参考](https://www.cnblogs.com/clonen/p/9083359.html)
Antlr4入门（三）如何编写语法文件:<https://blog.csdn.net/qq_37771475/article/details/106528661>

+ 语法文件通用结构：

```EBNF
/** 可根据需要撰写 javadoc 风格的注释，可以是单行、多行*/
grammar Name;
//注意以下options imports tokens actions指定顺序可以任意调换

options {name1=value1; ... nameN=valueN;} 
import ... ;
  
tokens { Token1, ..., TokenN }
channels {...} // 只能是词法分析时才能定义
@actionName {...}
   
rule1 // 语法和词法分析规则定义，也有可能是混合在一起的规则定义
...
ruleN
```

+ 也可以将词法文件和语法文件定义分来

```EBNF
// 定义词法文件
lexer grammar testlexer; 定义词法文件
```

词法规则作用是将一个个字符按照定义的规则聚集成字符，主要有以下四类：

标识符： 即各类编程语言中的变量名；
常量值： 英文叫Literal，如数字、单引号字符串、双引号字符串、各个进制写法等；
操作符： 单字符（!、~、=、>等）、双字符（>=、<=）等；
关键字： 即编程语言中的关键字，如Java中的class、package、import、public等；

```EBNF
// 定义语法文件
paser grammar testpaser; 
options {
    tokenVocab=testlexer; //引入词法文件
}
```

通配符
| 表示或

+ 表示出现0次或以上
? 表示出现0次或1次

+ 表示出现1次或以上
~ 表示取反

>**注意：**

1. 词法与语法分开时，所有单引号的字符串，必须在词法中定义，不然会报错
2. 当使用分离的词法分析器和解析器时，期望词法分析器以Lexer结尾，而解析器以Parser结尾。文件的共同部分是语法名称。
即TestLexer和TestParser-> Test是语法的名称。

# 源码结构

**类图**

```plantuml
@startuml
interface ParseTreeVisitor {
 +T visit(ParseTree tree);
 +T visitChildren(RuleNode node);
 +T visitTerminal(TerminalNode node);
 +T visitErrorNode(ErrorNode node);
}

class AbstractParseTreeVisitor implements ParseTreeVisitor {
  +T visit(ParseTree tree)
}

interface CartelParserVisitor extends ParseTreeVisitor {
  +T visitFile_input(CartelParser.File_inputContext ctx);
}

class CartelParserBaseVisitor extends AbstractParseTreeVisitor implements CartelParserVisitor {
  +T visitFile_input(CartelParser.File_inputContext ctx)
}


interface Tree {
  +Tree getParent();
  +Object getPayload();
  +Tree getChild(int i);
  +int getChildCount();
  +String toStringTree();
}

interface SyntaxTree extends Tree {
  +Interval getSourceInterval();
}

interface ParseTree extends SyntaxTree {
  +void setParent(RuleContext parent);
  +<T> T accept(ParseTreeVisitor<? extends T> visitor);
  +String getText();
  +String toStringTree(Parser parser);
}

interface RuleNode extends ParseTree {
  +RuleContext getRuleContext();
}

interface TerminalNode extends ParseTree {
 + Token getSymbol();
}

class RuleContext implements RuleNode {
  + T accept(ParseTreeVisitor<? extends T> visitor)
}

class ParserRuleContext extends RuleContext {
 +List<ParseTree> children;
 +void enterRule(ParseTreeListener listener) 
 +void exitRule(ParseTreeListener listener)
 +RuleContext addChild(RuleContext ruleInvocation)
 +TerminalNode addChild(TerminalNode t)
 +ErrorNode addErrorNode(ErrorNode errorNode)
 +ParserRuleContext getParent()
 +ParseTree getChild(int i)
 +<T extends ParserRuleContext> T getRuleContext(Class<? extends T> ctxType, int i)
 +int getChildCount() 
 +String toInfoString(Parser recognizer)
}

class File_inputContext extends ParserRuleContext {
 + void enterRule(ParseTreeListener listener)
 + void exitRule(ParseTreeListener listener)
 + <T> T accept(ParseTreeVisitor<? extends T> visitor)
}

ParseTree ..> ParseTreeVisitor

class CartelParser extends Parser {
 + File_inputContext file_input()
}
class Parser extends Recognizer {

}

File_inputContext +-- CartelParser : 内部类



@enduml

```

**时序图**

```plantuml
@startuml
autoactivate on
client ->> CartelLexer : init(src_file)
deactivate CartelLexer
client -> CartelParser: init(tokens)
deactivate CartelParser
client -> CartelParser: file_input()
deactivate CartelParser
client -> ASTVistor: init()
deactivate ASTVistor
client -> ASTVistor : visit(tree)
ASTVistor -> AbstractParseTreeVisitor: visit(tree)

AbstractParseTreeVisitor -> RuleContext : accept(this)

RuleContext -> ParseTreeVisitor : visitChildren(this)


note right of ParseTreeVisitor
public T visitChildren(RuleNode node) {
  T result = defaultResult();
  int n = node.getChildCount();
  for (int i=0; i<n; i++) {
    if (!shouldVisitNextChild(node, result)) {
      break;
    }

    ParseTree c = node.getChild(i);
    T childResult = c.accept(this);
    result = aggregateResult(result, childResult);
  }

  return result;
}
end note

deactivate ASTVistor
@enduml
```
