# 概述

参考：[BNF&EBNF区别](https://www.jianshu.com/p/15efcb0c06c8)

BNF 是上下文无关文法的描述格式

BNF （原始）是最简单的方法，易于阅读，不利于解析器，**BNF 没有确切的规范**
EBNF（扩展的BNF），使用**最为广泛**，但没有一个标准的 EBNF
**变体一：**,由标准 ISO-14977 所定义,似乎**有一些问题，不该使用**[1](https://en.wikipedia.org/wiki/Extended_Backus%E2%80%93Naur_form)
[2](https://dwheeler.com/essays/dont-use-iso-14977-ebnf.html)
**变体二:**，W3C使用的，[见](https://www.w3.org/TR/2008/REC-xml-20081126/#sec-notation)
ABNF（增强型BNF）更加标准化，利于解析器的翻译，但不利于阅读；

通常，BNF 更适合教学、解释和理论讨论。EBNF 和 ABNF 经常用于语法定义 和 解析器解析，其中 ABNF 更加利于解析器解析；

**BNF和EBNF表达能力相同**，EBNF 在能定义的语言的意义上不比 BNF 更强大。在原理上用 EBNF 定义的任何文法都可以用 BNF 表达。但是经常导致可观的更多规则的表示。

**转换工具**：
<https://github.com/GuntherRademacher/rr>
<https://bottlecaps.de/rex/>
<https://bottlecaps.de/rr/ui>

# BNF语法定义

BNF表示语法规则的方式为：

非终结符用尖括号括起。
每条规则的左部是一个非终结符，右部是由非终结符和终结符组成的一个符号串，中间一般以::=分开。
具有相同左部的规则可以共用一个左部，各右部之间以直竖“|”隔开。

**BNF只包含**："::="（被定义为）， "|"（选择），"<>"（非终结符）
非终结符：<>括起来的
终结符：只出现在右侧，并且没有<>的符号

括在方括号 <> 中的字符序列表示元语言变量，其值是符号序列。
标记 "::=" 和 "|" （后者的意思是“或”）是元语言连接词。
公式中的任何标记，不是变量或连接词，都表示它自己。公式中标记或变量的并列表示所表示的序列的并列。

示例：

```BNF
<while loop> ::= while ( <condition> ) <statement>
<assignment statement> ::= <variable> = <expression>
<statement list> ::= <statement> | <statement list> <statement>
<unsigned integer> ::= <digit> | <unsigned integer><digit>
```

# EBNF语法的定义

EBNF在BNF基础上增加一些标记，使用更加方便，主要有：“[ .. ]” 可选（0或1）“{ .. }” (0或多次)

[参考](https://condor.depaul.edu/ichu/csc447/notes/wk3/BNF.pdf)

**原始EBNF**:

```EBNF
生成式 = 生成式名 '=' [ 表达式 ] [';'] ;
表达式 = 选择项 { '|' 选择项 } ;
选择项 = 条目 { 条目 } ;
条目   = 生成式名 | 标记 [ '…' 标记 ] | 分组 | 可选项 | 重复项 ;
分组   = '(' 表达式 ')' ;
可选项 = '[' 表达式 ']' ;
重复项 = '{' 表达式 '}' ;

|   选择
()  分组
[]  可选（0 或 1 次）
{}  重复（0 到 n 次）

```

**w3c EBNF**:

antlr4似乎使用这个规则

**通配符**：

- `| 表示或`
- `* 表示出现0次或以上`
- `? 表示出现0次或1次   [] 类似效果`
- `+ 表示出现1次或以上`
- `~ 表示取反`
- `()用于分组`

**总结**：

1. 终结符必须在引号 ("..." 或 '...') 中，非终结符尖括号 ("<...>")可以省略
2. 分号; 结束一个规则
3. 语法糖：增加了**重复、可选，分组**机制

# BNF和EBNF区别

## 语法比较

|                               | BNF                           | ISO EBNF                      | ABNF                          | ANTLR                         |
|:-----------------------------:|:-----------------------------:|:-----------------------------:|:-----------------------------:|:-----------------------------:|
| rule definition               | `<name> ::= ...`              | `name = ... ;`                | `name = ...`                  | `name : ... ;`                |
| terminal items                | `...`                         | `'...'` or `"..."`            | integer or `"..."`            | `'...'`                       |
| non-terminal items            | `<...>`                       | `...`                         | `...` or `<...>`              | `...`                         |
| concatenation                 | (space)                       | `,`                           | (space)                       | (space)                       |
| choice                        | `|`                           | `|`                           | `/`                           | `|`                           |
| optional                      | requires choice syntax[^1]    | `[...]`                       | `*1...` or `[...]`            | `...?`                        |
| 0 or more repititions         | requires choice syntax[^2]    | `{...}`                       | `*...`                        | `...*`                        |
| 1 or more repititions         | requires choice syntax[^3]    | `{...}-`                      | `1*...`                       | `...+`                        |
| n repititions                 |                               | `n*...`                       | `n*n...`                      |                               |
| n to m repititions            |                               |                               | `n*m...`                      |                               |
| grouping                      |                               | `(...)`                       | `(...)`                       | `(...)`                       |
| comment                       |                               | `(*...*)`                     | `;...`                        | `// ...` or `/* ... */`       |

[^1]: `optionalb ::= a b c d | a c d`

[^2]: `list ::= | listitem list`

[^3]: `list ::= listitem | listitem list`

<https://zh.wikipedia.org/wiki/%E6%89%A9%E5%B1%95%E5%B7%B4%E7%A7%91%E6%96%AF%E8%8C%83%E5%BC%8F>

<https://zh.wikipedia.org/wiki/%E6%89%A9%E5%B1%95%E5%B7%B4%E7%A7%91%E6%96%AF%E8%8C%83%E5%BC%8F>

EBNF 排除了 BNF 的一些缺陷:

- BNF 为自身使用了符号 (<, >, |, ::=)。当它们出现在要定义的语言中的时候，BNF 不得不加以修改或解释的使用。
- BNF-语法在一行中只表示一个规则。

EBNF 解决了这些问题:

- 终结符被严格的包围在引号 ("..." 或 '...') 中。给非终结符的尖括号 ("<...>")可以省略。
- 通常使用终止字符分号结束一个规则。

并且：进一步还提供了定义**重复次数，排除法选择**(比如除了引号的所有字符)和注释等的增强机制。

# 语法分析方法

上下文无关文法（语法分析）分析方法有：

自底向上的，称为LR分析。核心是移进-归约，遇到一个单词，先放到栈里，然后栈里面构成一个语句了，就把它们从栈里取出来，然后把对应语句的产生式的左边非终结符推到栈里

自顶向下的，称为LL分析，核心是生成-匹配，意思是按当前选择的产生式，将产生式右边推到栈里，然后逐个匹配终结符，遇到非终结符时则再选择一个产生式，再把右边推到栈里

另类分析方法：PEG，即解析表达式语法（Parsing Expression Grammar）

详见：[上下文无关文法](./编译原理.md#上下文无关文法)

# 生成器

[Comparison of parser generators](https://en.wikipedia.org/wiki/Comparison_of_parser_generators)

除了手写词法分析器和语法分析器，有一些工具可以生产词法分析和语法分析器

1. Lex（词法生成器）&YACC（语法生成器），
GNU/Linux中对应的为：Flex&Bison，这里的Flex就是由Vern Paxon实现的一个Lex，Bison则是GNU版本的YACC

讲解：<https://www.cnblogs.com/hdk1993/p/4922866.html>

1. ANTLR：java写的包含词法分析语法分析的生成器，使用自上而下（top-down）的递归下降LL(*)算法。语法文件使用BNF，输出结果是抽象语法树

ANTLR 文法定义使用类似EBNF（Extended Backus-Naur Form）的定义方式，形象十分简洁直观。

[ANTLR使用说明](./ANTLR语法解析器.md)

2. 另类SLK

区别：
[相对于Lex/yacc/bison，Antlr的优势](https://cloud.tencent.com/developer/ask/67028)

python中的parser：<https://wiki.python.org/moin/LanguageParsing>

# python语法解析

python使用EBNF和PEG混合文法描述

[python语法规范](https://docs.python.org/zh-cn/3/reference/grammar.html)

词法分析阶段：
Python 程序可以拆分为多个 逻辑行。NEWLINE 形符表示结束逻辑行。
根据显式(反斜杠)或隐式(圆括号、方括号、花括号内表达式)行拼接规则，一个或多个物理行(回车符)可组成逻辑行。

除NEWLINE、INDENT、DEDENT 外，还有 标识符、关键字、字面值、运算符 、分隔符 等形符。 空白符不是形符，可用于分隔形符。

EBNF语法：
python语句分为：

1. 简单语句：由一个单独的逻辑行构成。多条简单语句可以存在于同一行内并以分号分隔

```EBNF
simple_stmt ::=  expression_stmt
                 | assert_stmt
                 | assignment_stmt
                 | augmented_assignment_stmt
                 | annotated_assignment_stmt
                 | pass_stmt
                 | del_stmt
                 | return_stmt
                 | yield_stmt
                 | raise_stmt
                 | break_stmt
                 | continue_stmt
                 | import_stmt
                 | future_stmt
                 | global_stmt
                 | nonlocal_stmt
```

2. 复合语句：包含其它语句（语句组）的语句；它们会以某种方式影响或控制所包含其它语句的执行

```EBNF
compound_stmt ::=  if_stmt
                   | while_stmt
                   | for_stmt
                   | try_stmt
                   | with_stmt
                   | funcdef
                   | classdef
                   | async_with_stmt
                   | async_for_stmt
                   | async_funcdef
suite         ::=  stmt_list NEWLINE | NEWLINE INDENT statement+ DEDENT
statement     ::=  stmt_list NEWLINE | compound_stmt
stmt_list     ::=  simple_stmt (";" simple_stmt)* [";"]
```

3. 语句由表达式组成

“原子”指表达式的最基本构成元素。 最简单的原子是标识符和字面值。 以圆括号、方括号或花括号包括的形式在语法上也被归类为原子。 原子的句法为:

```EBNF
atom      ::=  identifier（标识符） | literal（字面值） | enclosure（圆括号、方括号或花括号）
enclosure ::=  parenth_form | list_display | dict_display | set_display
               | generator_expression | yield_atom
```

赋值表达式：

```EBNF
assignment_expression ::=  [identifier ":="] expression
```

条件表达式

```EBNF
conditional_expression ::=  or_test ["if" or_test "else" expression]
expression             ::=  conditional_expression | lambda_expr
```

Python是自己写了一个tokenizer和自己的一个LL(1) Parser
相关模块是：
tokenize --- 对 Python 代码使用的标记解析器
ast --- 抽象语法树
dis --- Python 字节码反汇编器
