# 按照产品代数

分为6代，7代，ultrascale，ultrascale+，Versal，每代产品都有不同的系列

## 7代器件命名规则

7代：SPARTAN，ARTIX，KINTEX，VIRTEX
7代选型：<https://docs.xilinx.com/v/u/en-US/7-series-product-selection-guide>
![](../images/xilinx-7-series.png)

## Ultrascale器件命名规则

Ultrascale：KINTEX，VIRTEX
<https://china.xilinx.com/content/dam/xilinx/support/documents/selection-guides/ultrascale-fpga-product-selection-guide.pdf>
![](../images/xilinx-ultrascale-series.png)

## Ultrascale+器件命名规则

Ultrascale+：ARTIX，KINTEX，VIRTEX，ZYNQ，其中ZYNQ表示集成了arm芯片
<https://docs.xilinx.com/v/u/en-US/ultrascale-plus-fpga-product-selection-guide>
![](../images/xilinx-ultrascale-plus-series.png)

Versal：AI Core，AI Edge，Prime，Premium，由名称可见主要用于AI领域，并且使用了ACAP自适应加速平台，采用异构加速，在软件和硬件级别上进行动态自定义来适应各种应用场景。

**不同系列特点：**
Artix：最低功耗和最低成本
Kintex：业界最佳性价比
Virtex：最高系统性能和容量

# 按纳米制程分

16nm：ultrascale+下的ARTIX, KINTEX, VIRTEX
20nm: ultrascale下的KINTEX, VIRTEX
28nm: 7代下的SPARTAIN, ARTIX, KINTEX, VIRTEX
45nm: 6代下的SPARTAN

# 参考

<https://zhuanlan.zhihu.com/p/622334501>
<https://zhuanlan.zhihu.com/p/666315171>
<https://zhuanlan.zhihu.com/p/612817485>
