# nvme协议

trim或discard命令

文件系统删除文件，只是把对应block标记为可用，机械盘可以覆盖，所以没有问题
而SSD因为不支持数据覆盖，所以标记为可用的block，并不能别GC回收

TRIM和DISCARD的支持，需要SSD支持，而且在mount文件系统时，加上discard选项
