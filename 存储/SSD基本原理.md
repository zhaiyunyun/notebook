# SSD构成

![SSD组成](../images/ssd-architecture.jpg)

一块SSD，由主控芯片、内存、闪存颗粒等单元所组成

+ **主控芯片**提供了外部主机接口（SATA,PCIe）、内部闪存接口(闪存遵循ONFI或者Toggle协议)，并通过内嵌的 CPU 来运行 **SSD 固件**;SSD 固件管理着主机可见的存储地址空间、闪存物理空间、垃圾回收、磨损均衡等。
+ **内存**用于运行 SSD 固件，并保存在地址空间虚拟化所需要的各种表项。
+ **多颗闪存颗粒**分布在 SSD 的电路板上，共同为SSD 提供存储空间。

固件算法分为前端、中间、后端：
前端是接口和相关的协议模块（STAT/SAS/PCIe等）
中间是FTL（Flash Transhlation Layer）模块（垃圾回收、磨损平衡等算法）
后端是和闪存通信模块（闪存遵循ONFI或者Toggle协议）

## 闪存颗粒

根据制作工艺，闪存存储器可以分为NOR型和NAND型。
跟据闪存密度来分：SLC(单层存储单元) MLC(双层存单元)TLC(三层存储单元)QLC

![NAND颗粒组成](../images/NAND-flash.jpg)

+ 1个Chip/Device
  + 多个DIE或者LUN：接受和执行闪存命令的基本单元
    + 多个Planes：每个Plane有独立的register，一个Page register，一个Cache register
      + 上千个Blocks：擦除的基本单位
        + 上百个Pages：读或写的基本单位，一般是4KB 或者 8 KB + 几百个字节的隐藏空间
          + cells： flash存储信息的基本单位，根据每个cell可以保存1bit, 2bit, 3bit可以分为SLC, MLC, TLC

### 特性

**读写不平衡**：读最快，其次是写，最慢是擦除（ms级）
**块擦页写**：
一个Wordline对应着一个或若干个Page，具体是多少取决于是SLC、 MLC或者TLC。对SLC来说，一个Word line对应一个Page； MLC则对应2个Page，这两个Page是一对（ Lower Page和Upper Page）； TLC对应3个Page（ Lower Page、 Upper Page和Extra Page，不同闪存厂家叫法不一样）。一个Page有多大，那么Wordline上面就有多少个存储单元，就有多少个Bitline。写入以页为单位。

一个Block当中的所有这些存储单元都是共用一个衬底的。当对衬底施加强电压，上面所有浮栅级的电子都会被吸出来。所以擦除是以块为单位的。
**先擦后写**：
Nand Flash的写入以page为单位，擦除以block为单位。在Page页写入之前，必须要将page页所在的block块擦除。这个是由Nand Flash的工作原理决定的。

**寿命有限**：
个NAND Block都有擦写次数（Program/Erase，简称为 P/E）的限制，当超过这个次数时，该Block可能就不能用了：浮栅极充不进电子（写失败），或者浮栅极的电子很容易就跑出来（比特翻转，0->1)，或者浮栅极里面的电子跑不出来（擦除失败）。这个最大擦写次数按SLC,MLC,TLC依次递减：SLC的擦写次数可达十万次，MLC一般为几千到几万，TLC降到几百到几千。

## SSD的FTL

**地址映射**
HOST是通过LBA访问SSD的，每个LBA代表着一个Sector（一般为512B大小），操作系统一般以4K为单位访问SSD，叫用户页（Host Page）
SSD主控与FLASH之间是FLASH Page为基本单元访问FLASH的，我们称FLASH Page为物理页（Physical Page）
地址映射负责记录用户页和物理页的关系

**垃圾回收gc**

SSD 并不允许覆盖的操作，当原本 Page 中已有数据时，只能先删除再写入，为了加快写入速度，一般 SSD 修改数据时，会先写入其他空闲页，再将原始页标记为 stale ，直到最终被垃圾回收擦除。
带来的问题：**写放大**

**预留空间OP**

垃圾回收需要其他的空间来转移有效数据，所以SSD内部需要预留空间（需要有自己的小金库，不能工资全部上缴），这部分空间HOST是看不到的。这部分预留空间，不仅仅用以做垃圾回收，事实上，SSD内部的一些系统数据，也需要预留空间来存储，比如前面说到的映射表（Map Table），比如SSD固件，以及其它的一些SSD系统管理数据。

**磨损均衡**:
Wear Leveling 避免某一个Nand Block很快坏去，使所有Block的PE Cycle均衡发展。分为动态磨损均衡和静态磨损均衡

**SSD 并行处理**
SSD 有四种层次的并行处理方式：

Channel-level parallelism
Package-level parallelism
Chip-level parallelism
Plane-level parallelism

SSD 背后的奥秘 <https://blog.joway.io/posts/ssd-notes/>

SSD使用**多种冗余检查方法来**保护用户数据不受位翻转、操作或丢失的影响。

在SSD控制器内存中使用纠错码（ECC）和循环冗余校验（CRC）来防止数据更改或操作。
在闪存颗粒中使用低密度奇偶校验（LDPC）和CRC来防止闪存颗粒错误导致的数据丢失。
在闪存颗粒之间使用异或冗余来防止闪存故障导致的数据丢失。
