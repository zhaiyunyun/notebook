
# spdk核心概念

**reactor对应物理线程**
SPDK运行时指定了多少个core，就会起多少个reactor。Core和reactor是一一绑定的，poller是每个reactor上的周期性服务
Reactor与pthread和CPU核心的对应关系为1:1:1。

**spdk_thread是周期性被调用的函数**

spdk_thread需要被放到一个Reactor上去执行，具体到哪一个Reactor是由spdk_thread的CPUMASK决定。如图3，spdk_thread A, C, D的CPUMASK分别在Core0,1,2的bit位上置位，它们分别被放置在Core0,1,2上，spdk_thread B的CPUMASK对Core0,1,2的bit位上均有置位，代表它可以被放置到Core0,1,2任何一个上。在执行过程中，spdk_thread可以动态地从一个Reactor上迁移到另一个Reactor上，如果将spdk_thread A的CPUMASK更改为2，它将会被从Core0，调度到Core1上去执行

当Reactors进行轮询时，除了处理自己的事件消息之外，还会调用注册在该reactor下面的每一个线程进行轮询。不过通常一个reactor只有一个thread，在spdk应用中，更多的是注册多个poller而不是注册多个thread。具体的轮询方法为：

# spdk启动的reactor_thread_poller

默认启动的reator，thread和poller

- reactor_0:
  - app_thread
    - nvmf_tgt_accept
    - rpc_subsystem_poll
  - nvmf_tgt_poll_group_0
    - nvmf_poll_group_poll

# nvmf_tgt下重要的poller

**数据处理poller**
group->poller = SPDK_POLLER_REGISTER(nvmf_poll_group_poll, group, 0);

**请求accept的poller**
rtransport->accept_poller = SPDK_POLLER_REGISTER(nvmf_rdma_accept, &rtransport->transport,
        opts->acceptor_poll_rate);

# 资源创建(内存)

创建nvmf target


```puml

@startuml
User -> nvmf : main
activate nvmf
nvmf -> nvmf : nvmf_target_app_start
activate nvmf
nvmf -> nvmf : nvmf_target_advance_state
activate nvmf

nvmf -> nvmf : spdk_subsystem_init
nvmf -> nvmf : nvmf_create_nvmf_tgt
activate nvmf

nvmf -> nvmf : spdk_nvmf_tgt_create
activate nvmf
nvmf -> nvmf : nvmf_tgt_create_poll_group
nvmf -> nvmf : group->poller = SPDK_POLLER_REGISTER(nvmf_poll_group_poll)
nvmf -> nvmf : nvmf_poll_group_add_transport
activate nvmf

nvmf -> nvmf : nvmf_transport_poll_group_create

@enduml

```

```
main
  nvmf_target_app_start
    nvmf_target_advance_state
      spdk_subsystem_init
      nvmf_create_nvmf_tgt
        spdk_nvmf_tgt_create
          nvmf_tgt_create_poll_group
          group->poller = SPDK_POLLER_REGISTER(nvmf_poll_group_poll, group, 0);
          nvmf_poll_group_add_transport
            nvmf_transport_poll_group_create
            poll_group_create （nvmf_rdma_poll_group_create）
              nvmf_rdma_resources_create（资源创建）
```

创建rdma transport

```
rpc_nvmf_create_transport
  spdk_nvmf_transport_create
    nvmf_rdma_create
      rtransport->accept_poller = SPDK_POLLER_REGISTER(nvmf_rdma_accept
```

# 请求accept流程

请求创建入口：
nvmf_rdma_accept
 nvmf_process_cm_event
  nvmf_rdma_connect
   spdk_nvmf_tgt_new_qpair
    _nvmf_poll_group_add
     spdk_nvmf_poll_group_add
      poll_group_add（nvmf_rdma_poll_group_add）
       nvmf_rdma_qpair_initialize
        spdk_rdma_qp_create
         mlx5dv_create_qp(mlx5/rdma_verbs)
       nvmf_rdma_event_accept
        spdk_rdma_qp_accept
         rdma_accept(mlx5/rdma_verbs)

# 数据处理流程

请求处理：
nvmf_poll_group_poll
 nvmf_transport_poll_group_poll
  poll_group_poll( nvmf_rdma_poll_group_poll )
   nvmf_rdma_poller_poll
    nvmf_rdma_request_process
