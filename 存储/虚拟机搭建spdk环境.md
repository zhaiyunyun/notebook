# 环境准备

虚拟机网卡设置为桥接模式

虚拟机中配置代理为代理

# 配置iSCSI target环境

准备2台虚拟机：

server：10.1.225.65
client：10.1.225.75

## server端

### 源码编译spdk

```bash
git clone https://github.com/spdk/spdk
cd spdk
git submodule update --init
./scripts/pkgdep.sh
./configure
make
```

### 创建nvme虚拟盘

一：创建一块NVMe虚拟盘给server

参考[SPDK环境搭建](https://blog.csdn.net/freedom1523646952/article/details/118770672)

二：启动SPDK，指示操作系统放弃控制设备，让SPDK控制设备

```bash
$ cd spdk
# 运行脚本转让设备控制权给SPDK
$ sudo /scripts/setup.sh 510000:03:00.0 (15ad 07f0): nvme -> uio_pci_generic

# 查看设备状态

$ sudo scripts/setup.sh status 
Hugepages 56node     hugesize     free /  total 
node0   1048576kB        0 /      0 
node0      2048kB     1024 /   1024

Type     BDF             Vendor Device NUMA    Driver           Device     Block devices
NVMe     0000:03:00.0    15ad   07f0   0       uio_pci_generic  -          - 
# 此时设备将从/dev目录下消失 
$ ls /dev | grep nvme 
# 无输出信息
```

三：Configure iSCSI Target

参考：[Configure iSCSI Target](https://spdk.io/doc/iscsi.html)

## client端

一：安装iscsi客户端

Fedora: yum install -y iscsi-initiator-utils
Ubuntu: apt-get install -y open-iscsi

二：运行iscsi 80参考：[Configure initiator](https://spdk.io/doc/iscsi.html)

# 配置nvme target环境

nvme-of支持2种传输层协议：tcp和rdma
rdma，需要网卡支持rdma，可以用soft-RoCE软件模拟RDMA
服务端和客户端都需要配置Soft-RoCE

## Soft-RoCE安装

参考：RDMA之RoCE & Soft-RoCE
<https://zhuanlan.zhihu.com/p/361740115>

```bash
sudo apt-get install libibverbs1 ibverbs-utils librdmacm1 libibumad3 ibverbs-providers rdma-core
sudo modprobe rdma_rxe
sudo rdma link add rxe_0 type rxe netdev ens33
rdma link
ibv_devices #查看rdma设备信息
```

rdma测速：
server端：

```bash
sudo apt-get install perftest
ib_send_bw -d rxe_0
```

client端：

```bash
sudo apt-get install perftest
ib_send_bw -d rxe_0 <server_ip>
```

以下主要参考：[SPDK NVMf初体验](https://cyanwoods.com/posts/spdk-nvmf%E5%88%9D%E4%BD%93%E9%AA%8C/)
和官网：[NVMe-oF Target Getting Started Guide](https://spdk.io/doc/nvmf.html)

## server端

编译spdk

```bash
sudo scripts/pkgdep.sh --rdma
./configure --with-rdma <other config parameters>
make
```

启动nvme_tgt

```bash
HUGEMEM=2048 ./scripts/setup.sh
build/bin/nvmf_tgt
```

另起一个终端，通过rpc配置nvme系统

```bash
scripts/rpc.py nvmf_create_transport -t RDMA -u 8192 -i 131072 -c 8192
scripts/rpc.py nvmf_create_transport -t TCP -u 16384 -m 8 -c 8192

scripts/rpc.py bdev_malloc_create -b Malloc0 512 512
scripts/rpc.py nvmf_create_subsystem nqn.2016-06.io.spdk:cnode1 -a -s SPDK00000000000001 -d SPDK_Controller1
scripts/rpc.py nvmf_subsystem_add_ns nqn.2016-06.io.spdk:cnode1 Malloc0
scripts/rpc.py nvmf_subsystem_add_listener nqn.2016-06.io.spdk:cnode1 -t rdma -a 192.168.100.8 -s 4420
```

## client端

直接通过nvme-cli软件包测试

```bash
nvme discover -t rdma -a 192.168.100.8 -s 4420
nvme connect -t rdma -n "nqn.2016-06.io.spdk:cnode1" -a 192.168.100.8 -s 4420
nvme disconnect -n "nqn.2016-06.io.spdk:cnode1"
```

# 错误解决

你可能会遇到如下错误：

**编译spdk出错**
buildtools/meson.build:52:8: ERROR: Problem encountered: missing python module: elftools

**解决方法** <https://github.com/clearlinux/distribution/issues/1464>

apt-get install -y python3-pyelftools python-pyelftools

**启动iscsi_tgt报错** No available 2048 kB hugepages reported

**解决方法一：**
HUGEMEM=2048 ./scripts/setup.sh
**解决办法二：**
echo 1024 > /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages
echo 1024 > /sys/kernel/mm/hugepages/hugepages-1048576kB/nr_hugepages
