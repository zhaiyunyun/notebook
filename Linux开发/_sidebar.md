- **Linux内核**

  - [linux内核编译](Linux开发/linux内核编译.md)
  - [acpi-pci初始化](Linux开发/acpi-pci初始化.md)
  - [linux下trace机制](Linux开发/linux下trace机制.md)

- **Linux系统**

  - [linux系统编程](Linux开发/linux系统编程.md)
  - [linux进程和线程](Linux开发/linux进程和线程.md)
