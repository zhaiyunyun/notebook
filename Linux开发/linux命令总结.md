1. 查询pci设备PCI信息：`lspci -nn | grep Mellanox`
2. 查询网卡端口PCI信息：`eethtool -i et0`
3. 查询pci插槽PCI信息：`dmidecode -t slot`

通过PCI地址信息，可以将pci设备，网口，pci slot连接起来

1. 查询cpu和内存NUMA节点信息：`namuctl --hardware`
2. 查询pci设备NUMA节点信息：`lspci -n -d {venderid:devid} -vvv`

1. 查询pci slot的pci版本：`dmidecode -t slot`， 或者：`lspci -n -d venderid:devid -vvv grep --color Width`,通过LnkCap查看

查询内存条频率
dmidecode | grep -C 16 'Speed:'  | grep -A 16 "Memory Device" | grep 'Speed:'
root@l:~# dmidecode | grep -C 16 'Speed:'  | grep -A 16 "Memory Device" | grep 'Speed:'
        Speed: 2667 MT/s
        Speed: 2667 MT/s
root@l:~#

1MHz = 2MT/s

热拔插

1. 卡热拔插：/sys/bus/pci/slots/57/power，没有power文件表示卡不支持热拔插
2. 网口设备路径：/sys/bus/pci/drivers/mlx5_core
3. 网口down：echo 1 > /sys/devices/pci0000:00/0000:00:01.1/0000:01:00.0/remove
4. 网口up：echo 1 > /sys/devices/pci0000:00/0000:00:01.1/enable
