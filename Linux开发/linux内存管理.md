# 全景

![linux kernel map](../images/linux-kernel-map.png)

<https://linux-kernel-labs.github.io/refs/heads/master/index.html>

<https://zhuanlan.zhihu.com/p/460060762>
Linux内核内存相关问题，这一篇让你彻底了解

【纯干货】Linux内存管理（最透彻的一篇）
<https://zhuanlan.zhihu.com/p/503089287>

linux内存管理浅析
<https://blog.csdn.net/ctthuangcheng/article/details/8915146>

linux虚拟地址空间划分-32bit
https://zhuanlan.zhihu.com/p/342069369

# 内存管理

**用户空间内存分配函数**：
用户程序对内存的操作（分配、回收、映射、等）都是对mm的操作，具体来说是对mm上的vma（虚拟内存空间）的操作。这些vma代表着进程空间的各个区域，比如堆、栈、代码区、数据区、各种映射区、等等

linux内存分配系统调用：
brk：改变堆段空间的分配
mmap：映射虚拟内存页
<https://blog.csdn.net/weixin_42730667/article/details/119938889>

内核空间mmap函数具体实现步骤如下：

1. 通过kmalloc, get_free_pages, vmalloc等分配一段虚拟地址
2. 如果是使用kmalloc, get_free_pages分配的虚拟地址，那么使用virt_to_phys()将其转化为物理地址，再将得到的物理地址通过”phys>>PAGE_SHIFT”获取其对应的物理页面帧号。或者直接使用virt_to_page从虚拟地址获取得到对应的物理页面帧号。
如果是使用vmalloc分配的虚拟地址，那么使用vmalloc_to_pfn获取虚拟地址对应的物理页面的帧号。
3. 对每个页面调用SetPageReserved()标记为保留才可以。
4. 通过remap_pfn_range为物理页面的帧号建立页表，并映射到用户空间。
说明：kmalloc, get_free_pages, vmalloc分配的物理内存页面最好还是不要用remap_pfn_range，建议使用VMA的nopage方法。

————————————————
版权声明：本文为CSDN博主「luckywang1103」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：<https://blog.csdn.net/luckywang1103/article/details/50619251>

libc库函数
malloc： 利用堆动态分配，实际上是调用brk()系统调用，该调用的作用是扩大或缩小进程堆空间（它会修改进程的brk域）
alloca 是向栈申请内存,因此无需释放

参考：linux内存管理浅析 <https://blog.csdn.net/ctthuangcheng/article/details/8915146>

**内核空间内存分配函数：**
内核中的内存申请:kmalloc、vmalloc、kzalloc、kcalloc、get_free_pages【转】 <https://developer.aliyun.com/article/374905>

unsigned long __get_free_pages(gfp_t gfp_mask, unsigned int order)
申请大小：以页(4MB)为单位申请,
申请的连续物理内存在(PAGE_OFFSET,HIGH_MEMORY，约0~896M)之间
返回虚拟地址，虚拟地址和物理地址相差一个固定偏移(3G)
基于buddy机制实现

static inline void *kmalloc(size_t size, gfp_t flags);
kfree()
申请大小：最小可以申请的内存是32字节或64字节,最大可以申请的内存是128KB-16
申请的是物理内存，返回虚拟地址，同get_free_pages
基于slab机制实现

较常用的 flags（分配内存的方法）：

GFP_ATOMIC —— 分配内存的过程是一个原子过程，分配内存的过程不会被（高优先级进程或中断）打断；
GFP_KERNEL —— 正常分配内存；
GFP_DMA —— 给 DMA 控制器分配内存，需要使用该标志（DMA要求分配虚拟地址和物理地址连续）。
flags 的参考用法：
　|– 进程上下文，可以睡眠　　　　　GFP_KERNEL
　|– 进程上下文，不可以睡眠　　　　GFP_ATOMIC
　|　　|– 中断处理程序　　　　　　　GFP_ATOMIC
　|　　|– 软中断　　　　　　　　　　GFP_ATOMIC
　|　　|– Tasklet　　　　　　　　　GFP_ATOMIC
　|– 用于DMA的内存，可以睡眠　　　GFP_DMA | GFP_KERNEL
　|– 用于DMA的内存，不可以睡眠　　GFP_DMA |GFP_ATOMIC



void* vmalloc(unsigned long size)
vfree()
申请大小：适合大块内存
申请的内存是位于vmalloc_start到vmalloc_end之间的虚拟内存，返回的虚拟地址是连续的，但物理内存不一定连续
基于slab机制实现

**主要参考文章**：
Linux内存管理精华文章汇总: <https://zhuanlan.zhihu.com/p/40327018>

## 硬件基础

![cpu如何访问内存](../images/cpu-mmu.jpg)

**MMU地址转换**
![MMU地址转换](../images/MMU-address-translate.jpg)
输入虚拟地址，输出物理地址

**MMU分段机制**

主要有段寄存器，GDT,LDT数据结构

Linux内核探秘——内存寻址（一） <https://zhuanlan.zhihu.com/p/341570104>

保护模式的寻址方式不在使用寄存器分段的方式直接寻址方式了。而采用的是使用GDT（全局分段描述表）来寻址。从而使用更多的内存地址。
保护模式下，段寄存器，包含：CS、SS、DS、ES、FS、GS。段偏移量可以保存到IP、BX、SI、DI寄存器。段寄存器后13位相当于GDT表中某个描述符的索引，即段选择子，第2位存储了TI值（0代表GDT，1代表LDT），第0、1位存储了当前的特权级（CPL）。

Linux中的段 <https://cloud.tencent.com/developer/article/1740848>

**MMU分页机制**

![linux分页](../images/linux-page-table.jpg)

主要利用页表，页目录

## 物理地址空间布局

![物理地址空间布局](../images/linux-physical-ram-region.png)

内核将 0~896M 的物理地址空间一对一映射到自己的线性地址空间中，这样它便可以随时访问 ZONE_DMA 和 ZONE_NORMAL 里的物理页面
896M之上的ZONE_HIGH系统并不会建立映射关系，而是等实际使用的时候在分配，比如加载运行应用程序等
主要数据结构
mem_map: 所有的物理page对象构成一个mem_map数组
free_area: 空闲的page链表

## 内核虚拟地址布局

![内核虚拟地址布局](../images/kernel-space-area.png)

内核空间高端的 128M 地址主要由3部分组成
vmalloc area、
持久化内核映射区
临时内核映射区。

## 用户虚拟地址布局

![用户虚拟地址布局](../images/user-space-area.png)
代码区之上便是数据区，未初始化数据区，堆区，**mmap映射区（共享lib）**，栈区，以及参数、全局环境变量。

查看可执行程序与虚拟地址空间的映射关系，cat /proc/{pid}/maps
<https://blog.csdn.net/u012307248/article/details/13289423>

## linux虚拟地址与物理地址的关系

![虚拟地址与物理地址关系](../images/phy-page-vir-page.png)

总结
![总结](../images/linux-v-to-page.png)

# 用户空间与内核空间信息交互方法

**用户程序主动发起**
一：编写自己的系统调用
二：编写驱动，通过open,read,write,ioctl统一界面进行交互，不用添加系统调用
三：虚拟文件系统，proc，sysfs等
四：内存映射，mmap
**内核主动发起**：
一：调用用户程序，比如uevent，加载模块调用modprobe，netlink 等
二：内核中调用系统调用brk，在当前进程的堆中申请空间，把数据拷贝过去
三：发送信息，比如SIGKILL

**API总结**
用户程序是无法访问内核空间的，所以没有对应api接口
在内核中，是无法直接访问用户空间的，因为页表不一样，内核访问用户空间的API有
unsigned long copy_to_user(void __user *to, const void *from, unsigned long n)
unsigned long copy_from_user(void *to, const void__user *from, unsigned long n)
而get_user(),put_user()的作用：
复制的内存是简单类型，如char,int ,long等，则可以使用简单的put_user()和get_user()

# ioremap

编写驱动时，需要访问设备的io资源来与设备通信
比如nvme驱动，本质是一个pci驱动，nvme驱动probe时，系统会传进来pci_dev *pdev的设备
如果是platform驱动，可以用platform_get_resource获取设备io资源
然后就可以利用pci的函数获取设备的io地址空间，然后通过ioremap映射到内核虚拟地址
然后就可以对bar这个地址，
dev->bar = ioremap(pci_resource_start(pdev, 0), size);
通过以下接口进行io端口的访问
unsigned readb(address);
unsigned readw(address);
unsigned readl(address);
void writeb(unsigned value, address);
void writew(unsigned value, address);
void writel(unsigned value, address);

Writing a PCI device driver for Linux
<https://olegkutkov.me/2021/01/07/writing-a-pci-device-driver-for-linux/>

[ioremap()](https://www.cnblogs.com/ggzhangxiaochao/p/12893777.html)

# DMA接口

Documentation\core-api\dma-api-howto.rst
Documentation\core-api\dma-api.rst