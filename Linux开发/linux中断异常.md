# 官方API文档

\Documentation\core-api\genericirq.rst

Documentation\core-api\irq\index.rst

# 概念

中断：为了避免CPU轮询外设的低效率，采用中断机制将外部事件通知CPU

从中断来源分，cpu内部产生的中断，叫做异常(也叫同步中断)，异常又分为故障、陷阱、异常终止等
cpu外部产生的中断，叫做中断(也叫异常中断)，分为可屏蔽中断，不可屏蔽中断

中断与异常的不同便是中断是用来处理异步的外界事件。而异常则是用来处理被处理器发现的错误。

有两种外部中断源和两种异常：

1、 中断

可屏蔽中断，通过INTR引脚产生。

不可屏蔽中断，通过NMI引脚产生。

2、 异常

处理器检测到的。他们被进一步分为错误（faults），陷阱(traps)，和中止(aborts)。

被编程的。指令INTO，INT 3，INT n, 和BOUND 能够引发异常。这些指令通常被称为“软中断”，但处理器象普通中断一样处理它们。

# 术语

**IRQ**: Interrupt Request
**ISR**: Interrupt Service Routine
**PIC**: Programmable Interrupt Controller
**VIC**: Vectored Interrupt Controller 向量中断控制器
**GIC**: Generic Interrupt Controller 通用中断控制器 ARM平台
NMI(不可屏蔽中断线)和INTR(可屏蔽中断线)

# 硬件基础

一文读懂多架构的中断控制器 <https://zhuanlan.zhihu.com/p/494271647>

在计算机中，中断控制器是一种集成电路，可帮助微处理器或CPU处理来自多个不同源（如外部I / O设备）的**中断请求**（Interrupt Request，以下简称**IRQ**），这些中断请求可能同时发生。中断控制器有助于确定 IRQ 的优先级，以便CPU在得到 IRQ 的相对优先级评估结果后，切换到最合适的**中断处理程序** （Interrupt Service Routine，以下简称**ISR**）。
随着**SMP**（Symmetric Multiprocessing）架构的发展，外部中断源的数量持续增加。软中断（Software Interrupt，**SWI**）和核间中断（Interprocessor Interrupt，**IPI**），基于消息传递的中断（Message Signaled Interrupts，**MSI**），以及对虚拟化的支持，都需要中断控制器深度参与。传统的中断控制器已不能满足需求，各个架构也开始使用更成熟的的中断控制器。就是后来的APIC和GIC

x86平台： 8259A, APIC
ARM平台： VIC, NVIC, GIC
**PIC架构**
![PIC架构](../images/pic.jpg)
**APIC架构**
![APIC架构](../images/apic.jpg)

PIC全称Programmable Interrupt Controller，通常是指Intel 8259A双片级联构成的最多支持15个interrupts的中断控制系统。APIC全称Advanced Programmable Interrupt Controller，APIC是为了多核平台而设计的。它由两个部分组成IOAPIC和LAPIC，其中IOAPIC通常位于南桥中 用于处理桥上的设备所产生的各种中断，LAPIC则是每个CPU都会有一个。IOAPIC通过APICBUS(现在都是通过FSB/QPI)将中断信息分派给每颗CPU的LAPIC,CPU上的LAPIC能够智能的决定是否接受系统总线上传递过来的中断信息，而且它还可以处理Local端中断的pending、nesting、masking，以及IOAPIC于Local CPU的交互处理。
APIC相较于PIC来说，最大的优点是能适用于MP平台

# 中断子系统架构

Linux kernel的中断子系统之（一）：综述 <http://www.wowotech.net/linux_kenrel/interrupt_subsystem_architecture.html>

![中断子系统架构](../images/linux-interrupt-system-architecture.gif)

从下向上：
一层硬件层：cpu、中断控制器
二层驱动层：cpu和中断控制器驱动代码
三层通用中断处理模块：硬件无关的代码
四层普通外设的驱动：这些驱动将使用Linux kernel通用中断处理模块的API来实现自己的驱动逻辑。

<http://www.wowotech.net/linux_kenrel/interrupt_descriptor.html>

# Linux中断机制

Linux中断机制由三部分组成：
中断子系统初始化：内核自身初始化过程中对中断处理机制初始化，例如中断的数据结构以及中断请求等。
中断或异常处理：中断整体处理过程。
中断API：为设备驱动提供API，例如注册，释放和激活等。

<https://zhuanlan.zhihu.com/p/507340775>

<https://zhuanlan.zhihu.com/p/83709066>

## 中断初始化

中断描述符表初始化需要经过两个过程：
第一个过程在内核引导过程。由两个步骤组成，首先给分配IDT分配2KB空间（256中断向量，每个向量由8bit组成）并初始化；然后把IDT起始地址存储到IDTR寄存器中。
第二个过程内核在初始化自身的start_kernal函数中使用trap_init初始化系统保留中断向量，使用init_IRQ完成其余中断向量初始化。

中断描述符表最终初始化分为两部分：异常和中断，分别在函数 trap_init(void) 和 init_IRQ(void) 中实现，都由系统初始化入口函数 start_kernel() 所调用。对于特定异常，其处理异常函数预先已经设定好；但对于特定异步中断，由于需要捕获设备I/O中断的程序数目不定，所以得采用特定数据结构来对irq及其action进行描述。

### 中断描述符表（IDT）初始化

Linux内核中处理中断主要有三个数据结构，irq_desc,irq_chip和irqaction
irq_desc 用于描述IRQ线的属性与状态，被称为中断描述符。
irq_chip 用于描述不同类型的中断控制器。
irqaction 中断处理函数

![中断数据结构关系图](../images/irq-data-struct.gif)

在linux kernel中，对于每一个外设的IRQ都用struct irq_desc来描述，我们称之中断描述符（struct irq_desc）。linux kernel中会有一个数据结构保存了关于所有IRQ的中断描述符信息，我们称之中断描述符DB（上图中红色框图内）。当发生中断后，首先获取触发中断的HW interupt ID，然后通过irq domain翻译成IRQ nuber，然后通过IRQ number就可以获取对应的中断描述符。调用中断描述符中的highlevel irq-events handler来进行中断处理就OK了。而highlevel irq-events handler主要进行下面两个操作：

（1）调用中断描述符的底层irq chip driver进行mask，ack等callback函数，进行interrupt flow control。

（2）调用该中断描述符上的action list中的specific handler（我们用这个术语来区分具体中断handler和high level的handler）。这个步骤不一定会执行，这是和中断描述符的当前状态相关，实际上，interrupt flow control是软件（设定一些标志位，软件根据标志位进行处理）和硬件（mask或者unmask interrupt controller等）一起控制完成的。

```c
/**
 * struct irq_desc - interrupt descriptor
 * @irq:           interrupt number for this descriptor
 * @handle_irq:             highlevel irq-events handler [if NULL, __do_IRQ()]
 * @chip:         low level interrupt hardware access
 * @action:             the irq action chain
 * @name:             flow handler name for /proc/interrupts output
 */
struct irq_desc{
  unsigned int          irq;
  irq_flow_handler_t handle_irq;
  struct irq_chip              *chip;
  struct irqaction      *action;   /* IRQ action list */
  const char             *name;
}

struct irq_chip {
  const char      *name;
  unsigned int   (*startup)(unsigned int irq);
  void        (*shutdown)(unsigned int irq);
  void        (*enable)(unsigned int irq);
  void        (*disable)(unsigned int irq);
  void        (*ack)(unsigned int irq);
  void        (*mask)(unsigned int irq);
}

/**
 * struct irqaction - per interrupt action descriptor
 * @handler:   interrupt handler function
 * @name:      name of the device
 * @dev_id:     cookie to identify the device
 * @next:  pointer to the next irqaction for shared interrupts
 * @irq:    interrupt number
 * @dir:    pointer to the proc/irq/NN/name entry
 * @thread_fn: interupt handler function for threaded interrupts
 * @thread:     thread pointer for threaded interrupts
 * @thread_flags:    flags related to @thread
 */
 
struct irqaction {
  irq_handler_t handler;
  unsigned long flags;
  const char *name;
  void *dev_id;
  struct irqaction *next;
  int irq;
  struct proc_dir_entry *dir;
  irq_handler_t thread_fn;
  struct task_struct *thread;
  unsigned long thread_flags;
};

```

## 中断或异常处理过程

Linux的中断子系统是按照服务中断共享的模式设计的，因此ISR的安装需要分为两级，
第一级是针对这个IRQ线的，称为generic handler(或high level handler)，Generic handler在初始化的时候由 **irq_set_handler**(irq, handle) 添加，其中"irq"和"handle"分别是IRQ号和第一级的处理函数。
第二级是针对挂载在这个IRQ线上的不同设备的，称为specific handler。Specific handler的安装则是由**request_threaded_irq**()函数完成的。

当外设触发一次中断后，一个大概的处理过程是：

1、找虚拟中断号"irq", 通过irq_find_mapping()函数根据“hwirq”获取虚拟中断号
2、调用 do_IRQ(irq, ...)函数进行实际的中断处理
3、调用generic_handle_irq()进入回调IRQ之前安装的第一级处理函数(desc->handle_irq)。对于电平触发，注册的"handle_irq"是handle_level_irq()。对于边沿触发，注册的"handle_irq"是handle_edge_irq()(相关代码位于"/kernel/irq/chip.c")。
4、第二级处理函数，就是遍历IRQ线上的链表，依次执行通过前面讲的request_threaded_irq()安装的各个"irq_action"，也就是第二级的处理函数(action->handler)。这里需要判断是不是自己的设备产生的中断

## 中断API

内核提供的API主要用于驱动的开发。

（1）硬中断的开关

简单禁止和激活当前处理器上的本地中断：

local_irq_disable();

local_irq_enable();

保存本地中断系统状态下的禁止和激活：

unsigned long flags;

local_irq_save(flags);

local_irq_restore(flags);

（2）软中断的开关

禁止下半部，如softirq、tasklet和workqueue等：

local_bh_disable();

local_bh_enable();

需要注意的是，禁止下半部时仍然可以被硬中断抢占。

（3）判断中断状态

in_interrupt() (irq_count()) // 是否处于中断状态(硬中断或软中断)

in_irq() (hardirq_count()) // 是否处于硬中断

in_softirq() (softirq_count()) // 是否处于软中断

注册IRQ：
int request_irq(unsigned int irq, irq_handler_t handler, unsigned long flags, const char *name, void*dev);
释放IRQ：
request_threaded_irq 最新的api
void free_irq(unsigned int, void *);
激活当前CPU中断：
local_irq_enable()；
禁止当前CPU中断：
local_irq_disable();
激活指定中断线：
void enable_irq(unsigned int irq);
禁止指定中断线：
void disable_irq(unsigned int irq);
禁止指定中断线：
void disable_irq_nosync(unsigned int irq);
注：此函数调用irq_chip中disable禁止指定中断线，所以不会保证中断线上执行的中断服务程序已经退出。

**如何获取中断号**
linux irq 自动探测  <https://www.cnblogs.com/rongpmcu/p/7662223.html>
probe_irq_on 和 probe_irq_off
或者通过设备树，通过相关接口查询

# 中断机制划分

linux 中断顶/底半部机制
顶半部：关中断下执行，不可中断，一般只处理和硬件相关的。例如ack中断，read HW FIFO to ram等
底半部：开中断下执行，可中断，处理大多数任务，softirq, tasklet和work queue

为什么要有softirq？
降低系统反应时间，硬中断执行完，立马开中断，其他部分放在底半部执行
提高性能，softirq可以运行在多个cpu上，所以softirq函数必须是可重入的，考虑并发，要引入同步机制

从本质上将，bottom half机制的设计有两方面的需求，一个是性能，一个是易用性。设计一个通用的bottom half机制来满足这两个需求非常的困难，因此，内核提供了softirq和tasklet两种机制。softirq更倾向于性能，而tasklet更倾向于易用性。

workqueue：进程上下文 <--> tasklet/softirq：中断上下文

性能：softirq：多核运行，单核上串行<-->tasklet：串行执行(即使多核也只能一个运行)

易用行：softirq：需要考虑重入/并发/同步<-->tasklet：即使多核也串行，所以不用考虑

linux kernel的中断子系统之（八）：softirq
<http://www.wowotech.net/linux_kenrel/soft-irq.html>

## softirq

Linux内核硬中断 / 软中断的原理和实现
<https://cloud.tencent.com/developer/article/1518703>

**软中断数据结构**
softirq_action 用于描述一个软中断，action是软中断的处理函数。
softirq_vec 所有软中断数组，都是静态定义的。内核中使用“softirq_vec”表示所有支持的软中断

```c++
static struct softirq_action softirq_vec[NR_SOFTIRQS];
 
enum {
    HI_SOFTIRQ = 0, /* 优先级高的tasklets */
    TIMER_SOFTIRQ, /* 定时器的下半部 */
    NET_TX_SOFTIRQ, /* 发送网络数据包 */
    NET_RX_SOFTIRQ, /* 接收网络数据包 */
    BLOCK_SOFTIRQ, /* BLOCK装置 */
    BLOCK_IOPOLL_SOFTIRQ,
    TASKLET_SOFTIRQ, /* 正常优先级的tasklets */
    SCHED_SOFTIRQ, /* 调度程序 */
    HRTIMER_SOFTIRQ, /* 高分辨率定时器 */
    RCU_SOFTIRQ, /* RCU锁定 */
    NR_SOFTIRQS /* 10 */
};
```

**软中断执行** 核心API是“do_softirq”。

```c
asmlinkage void do_softirq(void)
{
    __u32 pending;
    unsigned long flags;
 
    /* 如果当前已处于硬中断或软中断中，直接返回 */
    if (in_interrupt()) 
        return;
 
    local_irq_save(flags);
    pending = local_softirq_pending();
    if (pending) /* 如果有激活的软中断 */
        __do_softirq(); /* 处理函数 */
    local_irq_restore(flags);
}
```

**相关api**：
注册： open_softirq(int nr, void (*action)(struct softirq_action*))
触发： raise_softirq rasie_softirq_irqsoff(unsigned int nr) wakeup_softirqd(void)
使能： local_bh_disable和local_bh_enable

softirq应用最多的是被动触发，触发时机
1） irq_exit
在硬中断退出时，会检查local_softirq_pending和preemt_count，如果都符合条件，则执行软中断。
2） local_bh_enable
使用此函数开启软中断时，会检查local_softirq_pending，如果都符合条件，则执行软中断。
3） raise_softirq
主动唤起一个软中断。内核提供了软中断处理线程“ksoftirq”，在适当时机会唤醒ksoftirq后台线程进行执行，处理软中断。

## tasklet

tasklet也是一种软中断，考虑到优先级问题，分别占用了向量表(softirq_vec)中的HI_SOFTIRQ和TASKLET_SOFTIRQ两类软中断
相比起其他普通的 softirq，tasklet 支持在运行过程中动态注册，其可扩展性更强，很适合 driver 模块使用。
相关api
创建：
静态创建：使用中定义的两个宏中的一个：DECLARE_TASKLET；DECLARE_TASKLET_DISABLED
动态创建：使用tasklet_init函数：
触发执行：
tasklet_schedule()函数和tasklet_hi_schedule()函数分别用来在当前CPU上触发软中断向量TASKLET_SOFTIRQ和HI_SOFTIRQ

## workqueue

workqueue是将中断处理中可以延后执行的部分作为任务(work item)放在一个队列(queue)中，由一个或多个单独的内核线程(worker)依次执行。
INIT_WORK(&mywork, mywork_handler); 加入到系统默认工作队列中
schedule_work(&mywork);

# 中断处理流程

<https://www.jianshu.com/p/ad946d9205f2>

<https://zhuanlan.zhihu.com/p/486982208>

<https://www.jianshu.com/p/7b2504349506>

<https://zhuanlan.zhihu.com/p/342614491>

一个合格的linux驱动工程师需要对kernel中的中断子系统有深刻的理解，只有这样，在写具体driver的时候才能：

1、正确的使用linux kernel提供的的API，例如最著名的request_threaded_irq（request_irq）接口

2、正确使用同步机制保护驱动代码中的临界区

3、正确的使用kernel提供的softirq、tasklet、workqueue等机制来完成具体的中断处理

# linux有哪些栈

Linux 中的各种栈：进程栈 线程栈 内核栈 中断栈
<https://blog.csdn.net/yangkuanqaz85988/article/details/52403726>
每一个进程，都有一个进程栈，内核栈，或者线程栈
arm，中断栈和内核栈共享，x86是独立的

# 进程上下文-中断上下文-原子上下文

在临界区里，不能睡眠

# 其他知识

intel 保护模式
一、不同任务之间的保护，不同任务有独立的地址空间
二、同一任务内的保护，在一个任务之内，定义有四种执行特权级别，用于限制对任务中的段进行访问

一句话解释，Intel中断和调用门很像，都通过门描述符间接实现代码转移，把任务门、中断门、陷阱门放在IDT中实现中断

重新认识Intel中断（一）
<https://blog.csdn.net/huang987246510/article/details/88954782>

中断描述符表是保护模式下用于存储中断处理程序的数据结构。CPU在接收到中断时，会根据中断向量在中断描述符表中检索对应的描述符。

中断描述符表中的描述符有哪些类型？

中断描述表中的主要包含以下类型：

任务门描述符
中断门描述符
陷阱门描述符
调用门描述符

任务门描述符：用于在发生中断时调度相应进程

中断门描述符：描述中断处理程序所在段选择子和段内偏移值。据此修改EIP及关闭中断Eflags的IT标识位

陷阱门描述符：与中断门描述符一样，但不关中断

处理器总是运行在下面三个状态之一：
内核态，进程上下文，内核代表用户执行程序
内核态，中断上下文，内核代表硬件执行程序
用户态，

门(调用、中断、任务或陷阱)用于跨段转移执行控制。权限级别检查的完成方式取决于所使用的目标类型和指令。
调用门使用 CALL 和 JMP 指令。调用门将控制从较低权限代码转移到较高权限代码。门 DPL 用于确定哪些权限级别可以访问门。调用门正在(或可能已经)逐渐被放弃，转而支持更快的 SYSENTER/SYSEXIT 机制。
任务门用于硬件多任务支持。硬件任务切换可以自动发生(CALL/JMP 到任务门描述符)，或者在设置 NT 标志时通过中断或 IRET。它的工作方式与中断门或陷阱门相同。据我所知，没有使用任务门，因为内核通常希望在任务切换时完成额外的工作。
中断和陷阱门与任务门一起被称为中断描述符表。除了参数从一个特权堆栈到另一个特权堆栈的传输之外，它们的工作方式与调用门相同。一个区别是中断门会清除 EFLAGS 中的 IF 位，而陷阱门则不会。这使它们成为处理硬件中断的理想选择。陷阱广泛用于硬件辅助虚拟化。
有关更多信息，请参阅有关您感兴趣的处理器的英特尔架构手册。
更新
要回答评论:
区分中断和陷阱的原因有很多。一是范围的不同:中断门指向内核空间(毕竟，管理硬件的是内核)而陷阱在用户空间中调用。响应硬件事件调用中断处理程序，而响应 CPU 指令执行陷阱。
举一个简单(但不切实际)的例子来更好地理解为什么中断门和陷阱门对 EFLAGS 的处理方式不同，请考虑如果我们在单处理器系统上为硬件事件编写中断处理程序并且在我们无法清除 IF 位时会发生什么情况正在服务一个。当我们忙于为第一个中断服务时，可能会出现第二个中断。然后我们的中断处理程序将在我们 IH 执行期间的某个随机点被处理器调用。这可能会导致数据损坏、死锁或其他不良魔法。实际上，中断禁用是确保一系列内核语句被视为临界区的机制之一。
不过，上面的示例假设了可屏蔽中断。无论如何，您不会想忽略 NMI。
今天，这在很大程度上也无关紧要。今天几乎没有区别 fast and slow interrupt handlers (搜索“快速和慢速处理程序”)，中断处理程序可以嵌套方式执行，SMP 处理器强制将本地中断禁用与自旋锁结合起来，等等。
现在，陷阱门确实用于服务软件中断、异常等。处理器中的缺页错误或除零异常可能是通过陷阱门处理的。使用陷阱门控制程序执行的最简单示例是 INT 3 指令，该指令用于在调试器中实现断点。在进行虚拟化时，发生的情况是管理程序在环 0 中运行，而 guest 内核通常在环 1 中运行 - 特权代码将因一般异常错误而失败。 Witchel 和 Rosenblum 开发 binary translation ，这基本上是重写指令来模拟它们的效果。发现关键指令并用陷阱替换。然后当陷阱执行时，控制权交给 VMM/管理程序，它负责模拟环 0 中的关键指令。
对于硬件辅助虚拟化，陷阱和模拟技术的使用受到了一定的限制(因为它非常昂贵，尤其是在它是动态的时)，但二进制转换的实践是 still widely used .
有关更多信息，我建议您查看:
