# 多进程和多线程

现代操作系统通过多进程实现多任务宏观的并发执行

进程是资源分配的基本单元
线程是处理器调度的基本单位

进程和线程，不同操作系统的实现方式可能不同

## 进程通信

linux 进程通信方式：
管道：命名管道（mkfifo）,父子进程间匿名管道 pipe，内核中利用文件实现
信号量：sem_init，sem_post 等，为了防止不同进程临界区资源产生问题，P 操作获取信号量，V 操作释放信号量
信号：sigaction，kill 等，Ctrl-C 产 SIGINT 信号,Ctrl-\产 SIGQUIT 信号,Ctrl-Z 产 SIGTSTP 信号
消息队列：msgget，存在与内核中，数据会从用户空间和内核空间相互复制，不适合数据量大操作频繁的场景
共享内存：shmget，地址空间映射，不需要复制数据，读写需要其他方式进行同步
套接字：socket，不同机器间的数据通信

## 线程同步

linux 内核没有提供线程，通过 pthread 库实现用户级多线程，c++11 后标准库开始支持多线程，实现原理是为每个创建的线程在当前进程空间生成运行栈
windows 系统提供系统线程，可通过 CreateThread，ExitThread 等系统函数操作，或 vc++运行库提供\_beginthreadex 对 CreateThread 的包装函数

pthread与std:thread关系：
标准库thread在linux上是利用ptread实现，所以需要添加 -lpthread 链接

以下主要以 linux 下 c++学习和总结

| 功能 | pthread | std::thread(c++11) | thrd_t(c11) |
| :----- | :---------- | :---------- |
|线程创建 | pthread_create | thread 类 | thrd_create(C11) |
|互斥 mutex | pthread_mutex_init | mutex 类 | mtx_init |
|条件变量 | pthread_cond_init | condition_variable | cnd_init |
|读写锁 | pthread_rwlock_init | shared_mutex | thrd_create(C11) |
|信号量 | sem_init | counting_semaphore(C++20) | sem_init |
|屏障 | pthread_barrier_init | barrier(C++20) | 无 |

进程通信的信号量和信号 ，线程同步也可以使用

互斥锁，只有加锁和不加锁两种状态，使用简单情况，相当于 0，1 状态的信号量
信号量，信号量的 count 代表多个可用的资源，比如多台打印机的情况
条件变量：可以以原子的方式阻塞进程，直到某个特定条件为真为止。对条件的测试是在互斥锁的保护下进行的。条件变量始终与互斥锁一起使用。

使用场景：
<https://www.cnblogs.com/lonelycatcher/archive/2011/12/20/2294161.html>

mutex，一句话：保护共享资源。限制对共享资源的访问
semaphore的用途，一句话：调度线程。不对吧？
条件锁，本质上还是锁，它的用途，还是围绕“共享资源”的。条件锁最典型的用途就是：**防止不停地循环去判断一个共享资源是否满足某个条件**。

虽然 Mutex和Semaphore 在一定程度上可以互相替代，比如你可以把 值最大为1 的Semaphore当Mutex用，也可以用Mutex＋计数器当Semaphore。但是对于设计理念上还是有不同的，Mutex管理的是资源的使用权，而Semaphore管理的是资源的数量，有那么一点微妙的小区别。

作者：姚冬
链接：<https://www.zhihu.com/question/47704079/answer/135960522>
来源：知乎
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

互斥锁（Mutex）保证了使用资源线程的唯一性和排他性，但是无法限制资源释放后其他线程申请的顺序问题，所以是无序的。而信号量（Semaphore）一般就是互斥的（少许情况读取是可以同时申请的），其保证了。程执行的有序性，可以理解为从一到多的进步，像比较有名的理发厅问题，我们就需要设一个Integer而非Boolean，因为理发厅里面的座椅不可能只有一张。

作者：沈小南
链接：<https://www.zhihu.com/question/47704079/answer/2312942796>
来源：知乎
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

semaphore和mutex的区别？ - 二律背反的回答 - 知乎
<https://www.zhihu.com/question/47704079/answer/135859188>
