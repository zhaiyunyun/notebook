# 参考

Linux内核模块编程——Hello World模块
<https://www.cnblogs.com/MartinLwx/p/10628530.html>

**hello.c代码**如下：

```c
#include<linux/module.h>   //每个模块都要包括的头文件
#include<linux/kernel.h>   //用到了printk()函数
#include<linux/init.h>

MODULE_LICENSE("GPL");  //没有指定license会出现error

static int hello_init(void)    //static使得该文件以外无法访问
{
 printk(KERN_ALERT "Hello World\n"); //只能使用内核里定义好的C函数，printk会根据日志级别将指定信息输出到控制台或日志文件中，KERN_ALERT会输出到控制台
 return 0;
}
static void hello_exit(void)
{
 printk(KERN_ALERT "Goodbye World\n");
}

module_init(hello_init);
module_exit(hello_exit);

```

**Makefile代码**如下：

```makefile
obj-m := hello.o

KERNEL_DIR := /lib/modules/$(shell uname -r)/build   #指定内核源码
PWD := $(shell pwd)  #指向当前目录

all:
 make -C $(KERNEL_DIR) V=2 M=$(PWD) modules
clean:
 make -C $(KERNEL_DIR) M=$(PWD) clean

```

**运行**

```bash
modinfo hello.ko    #modinfo用来查看模块信息
insmod hello.ko    #加载模块
lsmod   #查看已经载入的模块，看看有没有hello！
dmesg   #如果成功应该可以看到最后一行输出了hello world
rmmod hello   #卸载模块
```

# 几个疑问

一：编译命令解释
make -C $(KERNEL_DIR) M=$(PWD) modules
make -C 先进入到 KERNEL_DIR 目录
编译外部module的方法，内核Makefile提供的

```makefile
# The following are the only valid targets when building external
# modules.
# make M=dir clean     Delete all automatically generated files
# make M=dir modules   Make all modules in specified dir
# make M=dir        Same as 'make M=dir modules'
# make M=dir modules_install
```

二：头文件
系统当中有2个 kernel.h
/usr/src/linux-headers-5.4.0-109/include/linux/kernel.h # 编译内核驱动使用
/usr/include/linux/kernel.h # 编译应用程序时用的

使用V=2可以看编译过程

```bash
gcc -Wp,-MD,/home/zhaiyunyun/code/module_test/.hello.o.d  -nostdinc -isystem /usr/lib/gcc/x86_64-linux-gnu/9/include  -I./arch/x86/include -I./arch/x86/include/generated  -I./include -I./arch/x86/include/uapi
```

从输出可以看到，使用-nostdinc 关闭默认路径，并且试用-I指定了内核自己的头文件搜索目录
