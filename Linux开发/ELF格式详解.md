# 参考

[elf格式详解](https://blog.csdn.net/shuange3316/article/details/79156096?spm=1001.2014.3001.5502)

ELF文件解析（一）：Segment和Section: <https://www.cnblogs.com/jiqingwu/p/elf_format_research_01.html>

# 概念

linux elf是linux系统可执行文件格式

## section和segment关系

注意区分段(segment)和节(section)的概念
我们写汇编程序时，用.text，.bss，.data这些指示，都指的是**section**，比如.text，告诉汇编器后面的代码放入.text section中。
目标代码文件中的section和section header table中的条目是一一对应的。section的信息用于链接器对代码重定位。

**常见section**
.text 代码段
.data 包含初始化的全局变量和静态变量，可写
.rodata 包含只读数据，组成不可写的段
.bss 包含程序运行时未初始化的数据（全局变量和静态变量

[ 0]                   NULL            00000000 000000 000000 00      0   0  0
[ 1] .interp           PROGBITS        00008154 000154 000019 00   A  0   0  1
[ 2] .note.ABI-tag     NOTE            00008170 000170 000020 00   A  0   0  4
[ 3] .note.gnu.build-i NOTE            00008190 000190 000024 00   A  0   0  4
[ 4] .gnu.hash         GNU_HASH        000081b4 0001b4 000024 04   A  5   0  4
[ 5] .dynsym           DYNSYM          000081d8 0001d8 000040 10   A  6   1  4
[ 6] .dynstr           STRTAB          00008218 000218 00003c 00   A  0   0  1
[ 7] .gnu.version      VERSYM          00008254 000254 000008 02   A  5   0  2
[ 8] .gnu.version_r    VERNEED         0000825c 00025c 000020 00   A  6   1  4
[ 9] .rel.dyn          REL             0000827c 00027c 000008 08   A  5   0  4
[10] .rel.plt          REL             00008284 000284 000018 08   A  5  12  4
[11] .init             PROGBITS        0000829c 00029c 00000c 00  AX  0   0  4
[12] .plt              PROGBITS        000082a8 0002a8 000038 04  AX  0   0  4
[13] .text             PROGBITS        000082e0 0002e0 000130 00  AX  0   0  4
[14] .fini             PROGBITS        00008410 000410 000008 00  AX  0   0  4
[15] .rodata           PROGBITS        00008418 000418 000004 04  AM  0   0  4
[16] .ARM.exidx        ARM_EXIDX       0000841c 00041c 000008 00  AL 13   0  4
[17] .eh_frame         PROGBITS        00008424 000424 000004 00   A  0   0  4
[18] .init_array       INIT_ARRAY      00010f0c 000f0c 000004 00  WA  0   0  4
[19] .fini_array       FINI_ARRAY      00010f10 000f10 000004 00  WA  0   0  4
[20] .jcr              PROGBITS        00010f14 000f14 000004 00  WA  0   0  4
[21] .dynamic          DYNAMIC         00010f18 000f18 0000e8 08  WA  6   0  4
[22] .got              PROGBITS        00011000 001000 00001c 04  WA  0   0  4
[23] .data             PROGBITS        0001101c 00101c 00000c 00  WA  0   0  4
[24] .bss              NOBITS          00011028 001028 00000c 00  WA  0   0  4
[25] .comment          PROGBITS        00000000 001028 000032 01  MS  0   0  1
[26] .ARM.attributes   ARM_ATTRIBUTES  00000000 00105a 000035 00      0   0  1
[27] .shstrtab         STRTAB          00000000 00108f 00010a 00      0   0  1
[28] .symtab           SYMTAB          00000000 00164c 0006b0 10     29  80  4
[29] .strtab

而**文件载入内存执行**时，是以**segment组织**的，每个segment对应ELF文件中program header table中的一个条目，用来建立可执行文件的进程映像。
比如我们通常说的，**代码段、数据段**是segment，目标代码中的section会被链接器组织到可执行文件的各个segment中。
.text section的内容会组装到代码段中，.data, .bss等节的内容会包含在数据段中。

**常见segment**：
代码段：只读，包括：.text, .rodata
数据段：读写，包括：.data, .bss
堆和栈
