
# 参考

浅谈内核的Makefile、Kconfig和.config文件
<https://blog.csdn.net/thisway_diy/article/details/76981113>

[Linux] U-Boot 之构建过程（Kconfig 配置 + Kbuild 编译）详解
<https://bbs.21ic.com/icview-3202924-1-1.html>

探索 Linux 内核：Kconfig/kbuild 的秘密 <https://linux.cn/article-11227-1.html>

# 内核编译常用命令

下载安装当前内核的源码和头文件
sudo apt install linux-source
sudo apt-get install linux-headers-$(uname -r)

**内核源码和头文件**都安装在：/usr/src下， 注意与 /usr/include/应用程序默认头文件路径区分

```bash
make help
make menuconfig # 生成.config配置文件
make all -j 8 # 等于make vmlinux  modules bzImage 三个目标
make drivers/nvme # 单独编译某一驱动
make M=drives/nvme # 外部编译nvme驱动
sudo make modules_install # 驱动安装到 /lib/modules/5.16.10 
sudo make install # 在boot下生成config-5.16.10， initrd.img-5.16.10 System.map-5.16.10 vmlinuz-5.16.10，并更新grub
```

其他命令请看make help

# Linux内核编译系统

The Makefiles 包含下面5部分

Makefile                    the top Makefile. 普通makefile语法
.config                     `make *config`生成内核配置文件
arch/$(SRCARCH)/Makefile    the arch Makefile.
scripts/Makefile.*          common rules etc. for all
kbuild Makefiles            每个子目录中的kbuild文件，make扩展语法

**顶层Makefile结构**

```makefile
if 是多个目标（mixed-targets=1）
        一个一个处理
else if 配置（config-targets=1），即我们执行的 make xxxconfig 时的处理
      # makefile 527行
      config: scripts_basic outputmakefile FORCE
          $(Q)$(MAKE) $(build)=scripts/kconfig $@
      %config: scripts_basic outputmakefile FORCE
          $(Q)$(MAKE) $(build)=scripts/kconfig $@
else 这里就是 编译过程以及 非 config（例如 clean） 的处理了
      # Makefile：555行
      scripts: scripts_basic scripts_dtc include/config/auto.conf
          $(Q)$(MAKE) $(build)=$(@)
     if 需要 .confg
         检查  .confg 文件
      else
          include/config/auto.conf: ;
     endif
     # makefile:610行
     all 的定义（实际编译文件等等）

     非 config（例如 clean、check）等的定义
endif
```

## 依赖软件

需要安装：

```bash
sudo apt install libncurses5-dev
sudo apt install flex
sudo apt install bison
sudo apt install libssl-dev
sudo apt install libelf-dev
```

## Kconfig内核配置

Kconfig工具，实现了参数配置的语言，代码在scripts/Kconfig下面，当make %config时，会先编译生成 scripts/kconfig/mconf，紧接着使用刚生成的 scripts/kconfig/mconf 工具读取根目录的 Kconfig 文件，随之出现配置界面。
Kconfig语法文档：\Documentation\kbuild\kconfig.rst

配置完保存后，会生成
.config文件 不参与后续编译
include/config/auto.conf 会根据.config生成，这是最终的配置文件
include/generated/autoconf.h 用于 C 语言的源文件。

## Kconfig语法

kconfig语法整理 <https://www.jianshu.com/p/aba588d380c2>

每个config菜单项都要有类型定义

bool：布尔类型
tristate三态：内建、模块、移除
string：字符串
hex：十六进制
int：整型

## Kbuild编译

Kbuild文件，就是makefile文件，不过扩展了makefile语法，可以使用比如obj-m,obj-y等等
使用文档：Documentation/kbuild/makefiles.txt，Documentation/kbuild/modules.txt

Execute "make" or "make all" to build all targets marked with [*] 包括： vmlinux  modules bzImage
**vmlinux** 是ELF文件，即编译出来的最原始的文件。
KBUILD_VMLINUX_INIT： $(head-y)  init/
KBUILD_VMLINUX_MAIN  drivers-y := drivers/ sound/ firmware/  net/  lib/  usr/

**modules** 内核模块

**bzImage** vmlinuz经过gzip压缩后的文件，适用于大内核

## Kbuild语法

obj-y，静态编译进内核；
obj-m，动态编译进内核，即模块；

## 安装

sudo make modules_install
sudo make install # 会默认执行update-initramfs 和 update-grub

initramfs 在内核中的作用与实现
<https://blog.csdn.net/song_lee/article/details/106027410>
initramfs 的工作方式更加简单直接一些，启动的时候加载内核和 initramfs 到内存执行，内核初始化之后，切换到用户态执行 initramfs 的程序/脚本，加载需要的驱动模块、必要配置等，然后加载 rootfs 切换到真正的 rootfs 上去执行后续的 init 过程。

## 卸载内核

删除/lib/modules/目录下不需要的内核库文件
删除/usr/src/linux/目录下不需要的内核源码
删除/boot目录下启动的内核和内核映像文件
更改grub的配置文件，删除不需要的内核启动列表

# 编译错误

内核错误: No rule to make target ‘debian/canonical-certs.pem‘, needed by ‘certs/x509_certificate_list‘

<https://blog.csdn.net/qq_36393978/article/details/118157426>

# 链接过程

```
--------------------------------------------------------------------------------------------------                 
						     	usr/built-in.o 
								kernel/built-in.o
								mm/built-in.o
								fs/built-in.o
								ipc/built-in.o
								security/built-in.o
								crypto/built-in.o
								block/built-in.o 
								#平台相关
								arch/arm/nwfpe/built-in.o
								arch/arm/vfp/built-in.o
								arch/arm/kernel/built-in.o
								arch/arm/mm/built-in.o
							    arch/arm/common/built-in.o                            
								arch/arm/mach-s3c64xx/built-in.o                     
								arch/arm/plat-samsung/built-in.o                                             
								   		   ||       sound/built-in.o       arch/arm/lib/lib.a
arch/arm/kernel/init_task.o                ||       firmware/built-in.o    arch/arm/lib/built-in.o 
arch/arm/kernel/head.o  init/built-in.o    ||       drivers/built-in.o     lib/lib.a
         ||                  ||            ||             ||               lib/built-in.o    net/built-in.o 
         ||                  ||            ||             ||                   ||                 || 
      $(head-y)          $(init-y)      $(core-y)    $(drivers-y)           $(libs-y)          $(net-y)    
        |                    |             |              |                    |                  |
        |                    |             |              |                    |                 /
        \                   /               \             \                    /              /
          \               /                    \            \                 /            /
            \           /                         \           \             /          /
              \       /                               \         \         /        /
                \   /                                     \       \     /      /
           $(vmlinux-init)      .tmp_kallsyms2.o           $(vmlinux-main)           $(vmlinux-lds)
                   \                   |                        /                     /
                     \                 |                     /                     /
                       \               |                  /                     /
                         \             |               /                     /
                           \           |            /                     /
                             \         |          /        链接         /
                              ——  —— —— —— —— ——    ——  —— —— —— ——  /      
                                       vmlinux
 ----------------------------------------------------------------------------------------------------
```
vmlinux最终由 head， init， core， drivers， libs， net编译生成
链接过程由vmlinux-lds控制，vmlinux-lds由 include/asm-gerneric/vmlinux.lds.h 和 体系结构相关的 arch/x86/kernel/vmlinux.lds.S汇编文件生成

链接脚本中定义了各个section

```

 * A minimal linker scripts has following content:
 * [This is a sample, architectures may have special requiriements]
 *
 * OUTPUT_FORMAT(...)
 * OUTPUT_ARCH(...)
 * ENTRY(...)
 * SECTIONS
 * {
 *	. = START;
 *	__init_begin = .;
 *	HEAD_TEXT_SECTION
 *	INIT_TEXT_SECTION(PAGE_SIZE)
 *	INIT_DATA_SECTION(...)
 *	PERCPU_SECTION(CACHELINE_SIZE)
 *	__init_end = .;
 *
 *	_stext = .;
 *	TEXT_SECTION = 0
 *	_etext = .;
 *
 *      _sdata = .;
 *	RO_DATA(PAGE_SIZE)
 *	RW_DATA(...)
 *	_edata = .;
 *
 *	EXCEPTION_TABLE(...)
 *
 *	BSS_SECTION(0, 0, 0)
 *	_end = .;
 *
 *	STABS_DEBUG
 *	DWARF_DEBUG
 *	ELF_DETAILS
 *
 *	DISCARDS		// must be the last
 * }

```
参考：linux内核链接脚本vmlinux.lds分析续篇之 --- initcall机制（十三）
https://www.cnblogs.com/jianhua1992/p/16852793.html