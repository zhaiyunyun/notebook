# acpi

随手写：ACPI
https://zhuanlan.zhihu.com/p/164355285

arm平台一般使用设备树，x86使用acpi描述硬件信息

cpu 物理地址空间划分是硬件决定的，也就是主板确定之后，内存，外设的物理地址就已经确定了
cpu 通过对相应物理地址的读写跟外设通信

但是操作系统为了屏蔽硬件细节更加通用，在os内部会使用虚拟地址空间

比如对**内存的访问**，kmalloc，vmalloc都是从虚拟地址空间分配，然后os的内存管理从物理内存上真实的分配物理内存，然后建立映射关系，做到更加灵活

对**外设的访问**，也是同样的道理，通过ioremap将物理地址映射虚拟地址，也是先分配虚拟地址空间，然后建立页表进行物理地址和虚拟地址的映射
那物理地址怎么知道？对arm设备树来说，就是在设备树中写死的（设备树中的是根据芯片手册而来，也就是硬件决定），对apci方式来说，就是apci的table中写死的，pci设备的物理地址是在pci probe时确定

**中断**：物理中断连线是主板硬件确定，也就是说每个设备的物理中断号是硬件决定，当然可以通过bios配置（就是配置中断控制器）
request_irq是请求内核分配一个逻辑中断号（系统中唯一），os负责从中断池中分配，然后建立这个设备的物理中断号和逻辑中断号的映射关系


物理地址空间：cat /proc/iomem
物理IO端口空间：/proc/ioports
进程虚拟地址空间：cat /proc/<PID>/maps
内核虚拟地址空间：cat /proc/meminfo，cat /proc/vmstat，cat /proc/kallsyms

物理中断号：/dev/interrupts
虚拟中断号：/proc/interrupts

ACPI 全称是 Advanced Configuration Power Interface，定义了kernel和硬件之间统一接口

ACPI为OS提供两类数据结构：表格和定义块。通常由BIOS/UEFI收集系统各方面信息并创建的。表格当中定义数据，而定义块当中定义执行方法。

## 查看ACPI表格
```bash
# ls /sys/firmware/acpi/tables/
APIC  BOOT  data  DSDT  dynamic  FACP  FACS  HPET  MCFG  SRAT  WAET
```
用户态工具来自acpica
```bash
cat /sys/firmware/acpi/tables/DSDT ./dsdt.bin
iasl -d ./dsdt.bin
vi dsdt.dsl
```

ACPI SpecV3.0学习总结
https://blog.csdn.net/gaojy19881225/article/details/80018761

重要的表格
MCFG    : PCI-Memory Mapped Configuration table and sub-table  ('MCFG')

## 设备树

cat /sys/firmware/devicetree/base
官方文档：Documentation\devicetree\index.rst


# pcie根扫描

```c
static struct acpi_scan_handler pci_root_handler = {
	.ids = root_device_ids,
	.attach = acpi_pci_root_add,
	.detach = acpi_pci_root_remove,
	.hotplug = {
		.enabled = true,
		.scan_dependent = acpi_pci_root_scan_dependent,
	},
};
```
acpi_init->acpi_scan_init
               ->acpi_pci_root_init 注册 pci_root_handler
               ->acpi_bus_scan->acpi_bus_attach->acpi_scan_attach_handler 调用acpi_pci_root_add

在acpi_pci_root_add函数中，根据ACPI设备ID，判断是否为PCIE还是PCI ROOT
```c
	if (strcmp(acpi_hid, "PNP0A08") == 0)
		root->bridge_type = ACPI_BRIDGE_TYPE_PCIE;
	else if (strcmp(acpi_hid, "ACPI0016") == 0)
		root->bridge_type = ACPI_BRIDGE_TYPE_CXL;
```

枚举pcie总线上所有设备
acpi_pci_root_add->pci_acpi_scan_root->acpi_pci_root_create->pci_scan_child_bus->pci_scan_child_bus_extend

pci_scan_child_bus_extend

```c
	for (devfn = 0; devfn < 256; devfn += 8) {
		nr_devs = pci_scan_slot(bus, devfn);
```

# acpi-pci初始化

1. pci 初始化

```bash
memory@memory-153:~\$ cat /boot/System.map-3.16.0 | grep pci | grep **initcall
ffffffff81e5bba0 t**initcall_pcibus_class_init2
ffffffff81e5bba8 t **initcall_pci_driver_init2
ffffffff81e5bc58 t **initcall_acpi_pci_init3
ffffffff81e5bc78 t**initcall_register_xen_pci_notifier3
ffffffff81e5bc98 t **initcall_pci_arch_init3
ffffffff81e5bde0 t**initcall_pci_slot_init4
ffffffff81e5bfe8 t **initcall_pci_subsys_init4
ffffffff81e5c1a8 t **initcall_pcibios_assign_resources5
ffffffff81e5c1d8 t**initcall_pci_apply_final_quirks5s
ffffffff81e5c1e8 t **initcall_pci_iommu_initrootfs
ffffffff81e5c650 t**initcall_pci_proc_init6
ffffffff81e5c658 t **initcall_pcie_portdrv_init6
ffffffff81e5c668 t **initcall_pcie_pme_service_init6
ffffffff81e5c678 t**initcall_pci_hotplug_init6
ffffffff81e5c680 t **initcall_pcied_init6
ffffffff81e5c730 t **initcall_virtio_pci_driver_init6
ffffffff81e5c760 t **initcall_platform_pci_module_init6
ffffffff81e5c7a0 t **initcall_serial_pci_driver_init6
ffffffff81e5c878 t **initcall_sis_pci_driver_init6
ffffffff81e5c880 t **initcall_ata_generic_pci_driver_init6
ffffffff81e5c958 t **initcall_ehci_pci_init6
ffffffff81e5c970 t**initcall_ohci_pci_init6
ffffffff81e5cb90 t **initcall_pci_resource_alignment_sysfs_init7
ffffffff81e5cb98 t **initcall_pci_sysfs_init7
ffffffff81e5cc08 t**initcall_pci_mmcfg_late_insert_resources7
```

2. pci hotplug 流程
      1) 通过 acpi_install_notify_handler 向 acpi 模块注册消息处理函数
         消息处理函数为 handle_hotplug_event

handle_hotplug_event-->hotplug_event_work-->hotplug_event-->acpiphp_rescan_slot-->pci_scan_slot
   -->pci_scan_single_device-->pci_scan_device-->pci_setup_device
                            -->pci_device_add-->device_add
