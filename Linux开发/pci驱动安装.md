# 驱动安装流程

1. 查找设备名字和型号，[查找方法](#pci设备名称型号查找)
2. 根据名称和型号，网上搜索驱动下载
3. 验证驱动是否支持此设备，modinfo中alias指的是这个模块/驱动所支持的硬件的型号

modinfo命令可以单看指定的模块/驱动的信息，其中alias指的是这个模块/驱动所支持的硬件的型号
modalias格式：

```c
static ssize_t modalias_show(struct device *dev, struct device_attribute *attr,
                             char *buf)
{
    struct pci_dev *pci_dev = to_pci_dev(dev);

    return sprintf(buf, "pci:v%08Xd%08Xsv%08Xsd%08Xbc%02Xsc%02Xi%02X\n",
                   pci_dev->vendor, pci_dev->device,
                   pci_dev->subsystem_vendor, pci_dev->subsystem_device,
                   (u8)(pci_dev->class >> 16), (u8)(pci_dev->class >> 8),
                   (u8)(pci_dev->class));
}
static DEVICE_ATTR_RO(modalias);
```

参考：Linux Device Driver Part IV. PCI Device <https://tcbbd.moe/linux/device-driver/pci-device/>

# pci设备名称型号查找

一、设备vendor_id&device_id

venderID和deviceID标识一个设备，由PCI制造商分配，驱动使用这两个ID来查找设备；venderID有一个全球的注册，由供应商决定；

```bash
# lspci -nn | grep NVI
85:00.0 VGA compatible controller [0300]: NVIDIA Corporation GK104GL [GRID K2] [10de:11bf] (rev a1)
86:00.0 VGA compatible controller [0300]: NVIDIA Corporation GK104GL [GRID K2] [10de:11bf] (rev a1)
```

输出中各个值的说明：

**第一列**："85:00.0" 和 “86::00.0”，格式”bus:slot.func“
总线号（bus）：  从系统中的256条总线中选择一条，0--255。
设备号（slot）：  在一条给定的总线上选择32个设备中的一个。0--31。
功能号（func）：  选择多功能设备中的某一个功能，有八种功能，0--7。 PCI规范规定，功能0是必须实现的。

”0300“ PCI 设备类型 指 PCI 设备的类型，来自不同厂商的同一类设备的类型码可以是相同的。
“10de” 供应商识别字段（**Vendor ID**） 该字段用一标明设备的制造者。一个有效的供应商标识由 PCI SIG 来分配，以保证它的唯一性。Intel 的 ID 为 0x8086，Nvidia 的 ID 为 0x10de
“11bf” 设备识别字段（**Device ID**） 用以标明特定的设备，具体代码由供应商来分配。本例中表示的是 GPU 图形卡的设备 ID。
“a1”  版本识别字段（**Revision ID**） 用来指定一个设备特有的版本识别代码，其值由供应商提供
下图说明了 PCI 域、总线、设备等概念之间的联系（lspci 的输出没有标明域，但对于一台 PC 而言，一般只有一个域，即0号域。）：

二、找到了设备编号可以到<https://pci-ids.ucw.cz/>查找与该设备相关的信息，可以找到设备的名字

# 参考

linux查看设备信息和驱动安装信息 <https://blog.csdn.net/m1223853767/article/details/79615011>

# IB网卡型号 & 速率

IB网卡型号 & 速率
<https://blog.csdn.net/hezuijiudexiaobai/article/details/123153840>
