
<https://blog.csdn.net/gatieme/article/details/68948080>
Linux内核调试的方式以及工具集锦
<https://www.jianshu.com/p/285c91c97c28>

# linux下trace机制

一：tracing 目录文件以及创建的代码

```bash
root@memory-153:/sys/kernel/debug/tracing# ls
available_events            options              stack_trace_filter
available_filter_functions  per_cpu              trace
available_tracers           printk_formats       trace_clock
buffer_size_kb              README               trace_marker
buffer_total_size_kb        saved_cmdlines       trace_options
current_tracer              saved_cmdlines_size  trace_pipe
dyn_ftrace_total_info       set_event            trace_stat
enabled_functions           set_ftrace_filter    tracing_cpumask
events                      set_ftrace_notrace   tracing_max_latency
free_buffer                 set_ftrace_pid       tracing_on
function_profile_enabled    set_graph_function   tracing_thresh
instances                   set_graph_notrace    uprobe_events
kprobe_events               snapshot             uprobe_profile
kprobe_profile              stack_max_size
max_graph_depth             stack_trace

root@memory-153:/sys/kernel/debug/tracing# cat /boot/System.map-3.12.1 | grep trace | grep **initcall
ffffffff81e482d0 t**initcall_trace_init_flags_sys_exitearly
ffffffff81e482d8 t **initcall_trace_init_flags_sys_enterearly
ffffffff81e482e8 t **initcall_register_trigger_all_cpu_backtraceearly
ffffffff81e48348 t**initcall_tracer_alloc_buffersearly
ffffffff81e48358 t **initcall_init_trace_printkearly
ffffffff81e48360 t **initcall_event_trace_memsetupearly
ffffffff81e48368 t**initcall_init_ftrace_syscallsearly
ffffffff81e48410 t **initcall_ftrace_mod_cmd_init1
ffffffff81e48418 t **initcall_init_function_trace1
ffffffff81e48420 t **initcall_init_wakeup_tracer1
ffffffff81e48428 t**initcall_init_graph_trace1
ffffffff81e48430 t **initcall_event_trace_enable1
ffffffff81e48978 t **initcall_ftrace_init_debugfs5
ffffffff81e48980 t**initcall_tracer_init_debugfs5
ffffffff81e48988 t **initcall_init_trace_printk_function_export5
ffffffff81e48998 t **initcall_event_trace_init5
ffffffff81e489a0 t**initcall_init_kprobe_trace5
ffffffff81e489a8 t **initcall_init_uprobe_trace5
ffffffff81e48c10 t **initcall_init_tracepoints6
ffffffff81e48c20 t**initcall_stack_trace_init6
ffffffff81e48c28 t **initcall_init_mmio_trace6
ffffffff81e48c30 t **initcall_init_blk_tracer6
ffffffff81e494a8 t**initcall_clear_boot_tracer7
ffffffff81e494b0 t \_\_initcall_kdb_ftrace_register7

[root@localhost boot]# cat /boot/System.map | grep trace | grep __initcall
ffffffff83401b80 d str__initcall__trace_system_name
ffffffff853adc78 d __initcall_trace_init_perf_perm_irq_work_exitearly
ffffffff853adc7c d __initcall_register_nmi_cpu_backtrace_handlerearly
ffffffff853adc9c d __initcall_trace_init_flags_sys_enterearly
ffffffff853adca0 d __initcall_trace_init_flags_sys_exitearly
ffffffff853adcb0 d __initcall_init_trace_printkearly
ffffffff853adcb4 d __initcall_event_trace_enable_againearly
ffffffff853add30 d __initcall_ftrace_mod_cmd_init1
ffffffff853add34 d __initcall_init_wakeup_tracer1
ffffffff853add38 d __initcall_init_graph_trace1
ffffffff853add3c d __initcall_trace_events_synth_init_early1
ffffffff853add40 d __initcall_init_kprobe_trace_early1
ffffffff853add6c d __initcall_tracefs_init1
ffffffff853addd4 d __initcall_trace_boot_init1s
ffffffff853ae0dc d __initcall_tracer_init_tracefs5
ffffffff853ae0e0 d __initcall_init_trace_printk_function_export5
ffffffff853ae0e4 d __initcall_init_graph_tracefs5
ffffffff853ae0e8 d __initcall_trace_events_synth_init5
ffffffff853ae0f0 d __initcall_init_kprobe_trace5
ffffffff853ae0f8 d __initcall_init_uprobe_trace5
ffffffff853ae29c d __initcall_init_tracepoints6
ffffffff853ae2a0 d __initcall_stack_trace_init6
ffffffff853ae2a4 d __initcall_init_blk_tracer6
ffffffff853ae5fc d __initcall_init_hwlat_tracer7
ffffffff853ae600 d __initcall_init_osnoise_tracer7
ffffffff853ae604 d __initcall_kdb_ftrace_register7
ffffffff853ae6e0 d __initcall_clear_boot_tracer7s


```

ftrace_init_debugfs 7
available_filter_functions enabled_functions set_ftrace_filter
set_ftrace_notrace set_graph_function set_ftrace_pid trace_stat function_profile_enabled

tracer_init_debugfs 20

tracing_cpumask trace_options trace trace_pipe buffer_size_kb buffer_total_size_kb free_buffer trace_marker trace_clock tracing_on snapshot per_cpu
available_tracers current_tracer tracing_max_latency tracing_thresh README saved_cmdlines dyn_ftrace_total_info instances options

init_trace_printk_function_export 1 printk_formats

event_trace_init 3
available_events set_event events

init_kprobe_trace 2
kprobe_events kprobe_profile

init_uprobe_trace 2
uprobe_events uprobe_profile

stack_trace_init 3
stack_max_size stack_trace stack_trace_filter

not found:
saved_cmdlines_size set_graph_notrace max_graph_depth

二：tracepoint, ftrace event, syscall trace
tracepoint

```c
VMLINUX_SYMBOL(**start\_**tracepoints_ptrs) = .;  \
_(\_\_tracepoints_ptrs) /_Tracepoints: pointer array_/\
VMLINUX_SYMBOL(**stop\_**tracepoints_ptrs) = .;  \
_(\_\_tracepoints_strings)/_Tracepoints: strings_/ \

tracepoint.h

# define DEFINE*TRACE_FN(name, reg, unreg)     \

 static const char \_\_tpstrtab*##name[]     \
 **attribute**((section("**tracepoints_strings"))) = #name;  \
struct tracepoint**tracepoint*##name     \
 **attribute**((section("**tracepoints"))) =    \
  { **tpstrtab*##name, STATIC*KEY_INIT_FALSE, reg, unreg, NULL };\
 static struct tracepoint \* const \_\_tracepoint_ptr*##name **used  \
 **attribute**((section("**tracepoints*ptrs"))) =   \
  &\_\_tracepoint*##name;

ftrace event

trace_types  所有 tracer 链表
nop tracer
function_trace
wakeup_tracer
wakeup_rt_tracer
mmio_tracer
blk_tracer

vmlinux.lds.h

# define FTRACE_EVENTS() . = ALIGN(8);     \

   VMLINUX_SYMBOL(**start_ftrace_events) = .; \
   \*(\_ftrace_events)    \
   VMLINUX_SYMBOL(**stop_ftrace_events) = .;

ftrace.h

# undef DEFINE*EVENT_PRINT

# define DEFINE_EVENT_PRINT(template, call, proto, args, print)  \

         \
static const char print_fmt*##call[] = print;    \
         \
static struct ftrace*event_call \_\_used event*##call = {   \
 .name   = #call,    \
 .class   = &event*class*##template,  \
 .event.funcs  = &ftrace*event_type_funcs*##call, \
 .print*fmt  = print_fmt*##call,   \
};         \
static struct ftrace*event_call **used     \
**attribute**((section("\_ftrace_events"))) \***event*##call = &event\_##call

syscall trace

# define SYSCALL*TRACE_ENTER_EVENT(sname)    \

 static struct syscall_metadata \_\_syscall_meta*##sname;  \
 static struct ftrace*event_call \_\_used    \
   event_enter*##sname = {     \
  .name                   = "sys*enter"#sname,  \
  .class   = &event_class_syscall_enter, \
  .event.funcs            = &enter_syscall_print_funcs, \
  .data   = (void \*)&\_\_syscall_meta*##sname,\
  .flags   = TRACE*EVENT_FL_CAP_ANY, \
 };        \
 static struct ftrace_event_call **used    \
   **attribute**((section("\_ftrace_events")))   \
  \***event_enter*##sname = &event*enter*##sname;

# define SYSCALL*TRACE_EXIT_EVENT(sname)     \

 static struct syscall_metadata \_\_syscall_meta*##sname;  \
 static struct ftrace*event_call \_\_used    \
   event_exit*##sname = {     \
  .name                   = "sys*exit"#sname,  \
  .class   = &event_class_syscall_exit, \
  .event.funcs  = &exit_syscall_print_funcs, \
  .data   = (void \*)&\_\_syscall_meta*##sname,\
  .flags   = TRACE*EVENT_FL_CAP_ANY, \
 };        \
 static struct ftrace_event_call **used    \
   **attribute**((section("\_ftrace_events")))   \
 \***event_exit*##sname = &event*exit*##sname;

# define SYSCALL*METADATA(sname, nb, ...)   \

 static const char \*types*##sname[] = {   \
  **MAP(nb,**SC*STR_TDECL,**VA_ARGS**)  \
 };       \
 static const char \*args*##sname[] = {   \
  **MAP(nb,**SC*STR_ADECL,**VA_ARGS**)  \
 };       \
 SYSCALL_TRACE_ENTER_EVENT(sname);   \
 SYSCALL_TRACE_EXIT_EVENT(sname);   \
 static struct syscall_metadata **used   \
   **syscall_meta*##sname = {    \
  .name   = "sys"#sname,   \
  .syscall*nr = -1, /* Filled in at boot */ \
  .nb_args  = nb,    \
  .types  = nb ? types*##sname : NULL, \
  .args  = nb ? args*##sname : NULL, \
  .enter_event = &event_enter*##sname,  \
  .exit*event = &event_exit*##sname,  \
  .enter*fields = LIST_HEAD_INIT(\_\_syscall_meta*##sname.enter*fields), \
 };       \
 static struct syscall_metadata **used   \
   **attribute**((section("**syscalls_metadata"))) \
  \*\_\_p_syscall_meta*##sname = &\__syscall_meta_##sname;

# else

# define SYSCALL_METADATA(sname, nb, ...)

# endif

# define SYSCALL*DEFINE0(sname)     \

 SYSCALL_METADATA(*##sname, 0);    \
 asmlinkage long sys\_##sname(void)

# define SYSCALL*DEFINE1(name, ...) SYSCALL_DEFINEx(1,*##name, **VA_ARGS**)

# define SYSCALL*DEFINE2(name, ...) SYSCALL_DEFINEx(2,*##name, **VA_ARGS**)

# define SYSCALL*DEFINE3(name, ...) SYSCALL_DEFINEx(3,*##name, **VA_ARGS**)

# define SYSCALL*DEFINE4(name, ...) SYSCALL_DEFINEx(4,*##name, **VA_ARGS**)

# define SYSCALL*DEFINE5(name, ...) SYSCALL_DEFINEx(5,*##name, **VA_ARGS**)

# define SYSCALL*DEFINE6(name, ...) SYSCALL_DEFINEx(6,*##name, **VA_ARGS**)

# define SYSCALL_DEFINEx(x, sname, ...)    \

 SYSCALL_METADATA(sname, x, **VA_ARGS**)   \
 **SYSCALL_DEFINEx(x, sname,**VA_ARGS\_\_)
```

# trace

```
[root@localhost tracing]# cat available_tracers
timerlat osnoise hwlat blk function_graph wakeup_dl wakeup_rt wakeup function nop
```
## ftrace

function函数调用
function_graph，性能分析

trace系列1 - function trace学习笔记
https://blog.csdn.net/jasonactions/article/details/118353455

ftrace 实现原理
https://zhuanlan.zhihu.com/p/355246942


## tracepoints

trace event是基于tracepoints做的
比如nvme_setup_cmd这个trace event定义
在drivers/nvme/host/trace.h中定义
TRACE_EVENT(nvme_setup_cmd)

然后再需要插桩的地方调用 trace_nvme_setup_cmd(req, cmd);

trace_nvme_setup_cmd 在TRACE_EVENT宏定义，在tracepoint.h中
```c
#define __DECLARE_TRACE(name, proto, args, cond, data_proto)		\
	extern int __traceiter_##name(data_proto);			\
	DECLARE_STATIC_CALL(tp_func_##name, __traceiter_##name);	\
	extern struct tracepoint __tracepoint_##name;			\
	static inline void trace_##name(proto)				\
	{								\
		if (static_key_false(&__tracepoint_##name.key))		\
			__DO_TRACE(name,				\
				TP_ARGS(args),				\
				TP_CONDITION(cond), 0);			\
		if (IS_ENABLED(CONFIG_LOCKDEP) && (cond)) {		\
			rcu_read_lock_sched_notrace();			\
			rcu_dereference_sched(__tracepoint_##name.funcs);\
			rcu_read_unlock_sched_notrace();		\
		}							\
	}								\
	
```
通过分析vmlinux.lds可知
有关于tracepoints的section

基于 tracepoint 的静态事件跟踪
https://mp.weixin.qq.com/s?__biz=Mzg2MDc2OTYxMQ==&mid=2247484266&idx=1&sn=0627554f552e1ad1ee67488120c227a5&chksm=ce201f4ef957965845f13f3a9fbd3100fdbd744e94f5f9c8406235ca0e7d65186fbcbdf7fb99&scene=178&cur_album_id=2781934910456201220#rd