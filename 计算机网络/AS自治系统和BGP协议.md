# AS自治系统和BGP协议

AS：自治系统

BGP：各个自治系统通过 BGP 协议相互连接

查看各个运营商的网络情况：<https://lookinglass.org/>

<http://www.bgplookingglass.com/>

<https://www.robtex.com/>

联通：<http://lg.hkunicom.com/lg>

<https://www.de-cix.net/>

<https://lg.fra.de-cix.net/routeservers/rs1_fra_ipv4>

<https://lg.telia.net/>

<http://www.hkix.net/hkix/hkixlg.htm>

# 中国电信三张网络

1. 163 骨干网   ASN: AS4134  省级出国节点 ip 地址均为 202.97 开头

2. CN2 GT 普通 CN2    无 ASN，不对外宣告（AS4809 不确定）    省级节点为 202.97 开头，出国时走 59.43 的 CN2 线路以避开拥堵，回程全走 163 线路

测试工具：<https://tools.ipip.net/traceroute.php>

如搬瓦工普通 CN2 机房，测试 IP:23.252.103.101，

![tracerote1](../images/traceroute1.png)

3. CN2 GIA 高端线路 无 ASN，不对外宣告 省市全部 59.43 开头，回程也是 59.43，双向 CN2，真正的 CN2 线路

如搬瓦工高端 CN2 机房，测试 IP: 65.49.131.102

![tracerote2](../images/traceroute2.png)
