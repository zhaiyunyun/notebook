
# 流程

## 软件开发阶段和人员

![devops流程](../images/软件开发阶段和角色.jpg)

## devops改进

敏捷开发强调开发测试你自己一起搞
devops强调开发测试部署运维你自己一起搞

可以这样来理解： DevOps是CI/CD思想的延伸，CI/CD则是DevOps的技术核心 ，如果没有CI/CD，没有自动化测试，DevOps是没有任何意义的。 所以说DevOps是以CI/CD为基础来优化程序的开发、测试、运维等各个不同环节。 持续集成是一种开发实践，它倡导团队成员需要频繁的集成他们的工作，每次集成都通过自动化构建（包括编译、构建、自动化测试）来验证，从而尽快地发现集成中的错误。

![devops流程](../images/devops流程.jpg)

# Jenkins

Jenkins是一款开源 CI&CD 软件，用于自动化各种任务，包括构建、测试和部署软件。

参考文档： <http://52wiki.cn/project-14/>

## 安装启动

两种安装方式，war包和docker
war包，需要先安装java环境，
启动：java -jar jenkins.war
重启：<http://localhost:8080/restart>
关闭：<http://localhost:8080/exit>
或者通过 Jenkins-cli.jar 包进行管理

## 代理设置

配置国内镜像源，先到镜像源站点查看可用的镜像源：<http://mirrors.jenkins-ci.org/status.html>
在 Jenkins 中配置该镜像源

## 插件安装

**中文插件**：
安装中文插件Localization: Chinese (Simplified) ，系统管理->插件管理
中文配置，需要设置浏览器的优先语言为中文
重启Jenkins，<http://localhost:8080/restart>
**blueocean插件**：
<https://www.jenkins.io/zh/doc/book/blueocean/getting-started/#on-an-existing-jenkins-instance>

## 流水线

流水线是对CD流程的自动化抽象
定义流水线方式有：界面手动配置，和Jenkinsfile
推荐Jenkinsfile，可以用scm管理，方便协作
新建项目的时候，需要选择流水线类型

### Jenkinsfile语法

脚本式（Scripted）语法和声明式（Declar-ative）语法
脚本式，优点是灵活，声明式优点是清晰明了。推荐使用声明式

**脚本式**，是固定格式的groovy语言
按照格式编写，在其中可以加入groovy的脚本，例如循环、判断、添加变量等等。这样的好处是降低了学习成本，例如上面的下载代码的git指令，用groovy单纯实现就如下方式。

```groovy
node () {
    def branch = 'test' // 定义变量
    stage 'pull'
        sh " echo 拉取代码"
    
    stage 'build'
        sh " echo 构建代码"
}
"git clone http://代码".execute().text
```

**声明式**：

```jenkins
pipeline {
    agent any
    stages {
        stage('pull') {
            steps {
                echo '开始拉取代码'
            }
        }
    }
}
```

以下声明式语法中，每个步骤都要有，少一个都会报错

pipeline：固定语法，代表整条流水线
agent：指定流水线在哪执行，默认any即可，也可以指定在docker、虚拟机等等里执行
stages：流水线中多个stage的容器，至少包含一个stage
stage：流水线的阶段，每个阶段都必须有名称，stage必须有，且只能有一个steps
steps：阶段中的一个或多个具体步骤（step）的容器，steps部分至少包含一个步骤，echo就是一个步骤

## groovy语言

JVM支持哪些语言 <https://www.cnblogs.com/lujiahua/p/11404928.html>
将groovy编译之后的class文件用jvm运行 <https://blog.csdn.net/weixin_44264207/article/details/108639451>

groovy是一种jvm虚拟机语言，所谓jvm虚拟机语言，就是可以编译成字节码在虚拟机上运行的语言
目前可以在java虚拟机上运行的有：Kotlin，Groovy，Scala，Clojure，java等10种语言

## Jenkins与groovy关系

声明式Jenkinsfile就是用groovy实现的一种DSL
