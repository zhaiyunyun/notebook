<https://www.mianshigee.com/tutorial/CMake-Cookbook/readme.md>
CMake菜谱（CMake Cookbook中文版）

# cmake简介

CMake是一个跨平台的、开源的构建工具。
cmake是makefile的上层工具，它们的目的正是为了产生可移植的makefile
可以生成如 Unix 的 Makefile 或 Windows 的 Visual Studio 工程

旧版 CMake 2.0 主要是基于 directory 来构建，很多复用只能靠变量实现。
Modern CMake 最大的改进是引入了 target，支持了对构建的闭包性和传播性的控制，从而实现了构建可以模块化。
所以，在 Modern CMake 中强烈推荐抛弃旧的 directory 方式，使用 target 的方式构建整个工程。

一般流程
mkdir build
cmake ../
make
make test
make install

参考：
Modern CMake 最佳实践 <https://www.jianshu.com/p/8abf754654c4>
CMake最佳实践 <https://www.jianshu.com/p/3711361d10a5>

**定义target**
定义可执行程序： add_executable
定义库： add_library

**target依赖**
源文件列表，通过 target_sources 配置。
头文件目录，通过 target_include_directories 配置。
预编译宏，通过 target_compile_definitions 配置。
编译选项和特性，通过 target_compile_options，target_compile_features 配置。
链接选项，通过 target_link_options 配置。
链接库目录，通过 target_link_directories
链接库， target_link_libraries

**安装和测试**
安装：通过 install 配置安装规则，install (TARGETS MathFunctions DESTINATION bin)
测试：enable_testing，和 add_test (test_run Demo 5 2)添加测试用例

**环境变量**
<https://cmake.org/cmake/help/latest/manual/cmake-variables.7.html#variables-that-provide-information>
使用CMake构建C++项目 <https://zhuanlan.zhihu.com/p/92928820>

一般情况下，使用 `$ { }`进行变量的引用。在IF等语句中，是直接使用变量名而不通过${}取值。

隐式定义：使用PROJECT指令，会隐式的定义 PROJECT_BINARY_DIR 和 PROJECT_SOURCE_DIR
显示定义：set(xxx fdsa.c)

cmake中一些预定义变量
PROJECT_SOURCE_DIR this is the directory which contains the top-level CMakeLists.txt, i.e. the top level source directory
PROJECT_BINARY_DIR if you are building in-source, this is the same as CMAKE_SOURCE_DIR, otherwise this is the top level directory of your build tree
CMAKE_INCLUDE_PATH 环境变量,非cmake变量
CMAKE_LIBRARY_PATH 环境变量
CMAKE_CURRENT_SOURCE_DIR 当前处理的CMakeLists.txt所在的路径
CMAKE_CURRENT_BINARY_DIR target编译目录
使用ADD_SURDIRECTORY(src bin)可以更改此变量的值
SET(EXECUTABLE_OUTPUT_PATH <新路径>)并不会对此变量有影响,只是改变了最终目标文件的存储路径
CMAKE_CURRENT_LIST_FILE 输出调用这个变量的CMakeLists.txt的完整路径
CMAKE_CURRENT_LIST_LINE 输出这个变量所在的行
CMAKE_MODULE_PATH 定义自己的cmake模块所在的路径
SET(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake),然后可以用INCLUDE命令来调用自己的模块
EXECUTABLE_OUTPUT_PATH 重新定义目标二进制可执行文件的存放位置
LIBRARY_OUTPUT_PATH 重新定义目标链接库文件的存放位置
PROJECT_NAME 返回通过PROJECT指令定义的项目名称
CMAKE_ALLOW_LOOSE_LOOP_CONSTRUCTS 用来控制IF ELSE语句的书写方式

**其他命令**：
aux_source_directory(. DIR_SRCS) 查找当前目录下的所有源文件，并将名称保存到 DIR_SRCS 变量
add_subdirectory(math)
ADD_DEPENDENCIES(target-name depend-target1 depend-target2 ...)
INCLUDE 指令，用来载入 CMakeLists.txt 文件，也用于载入预定义的 cmake 模块.
INCLUDE(file1 [OPTIONAL])
INCLUDE(module [OPTIONAL])
MESSAGE( STATUS "CMAKE_BINARY_DIR:         " ${CMAKE_BINARY_DIR} )

# 包管理

## 导包

Modern CMake 目前提供两种方式使用第三方库，分别是 find_package, FetchContent_Declare
**find_package** 用于查找本地安装的第三方包，linux下安装目录: `~/.CMake/packages/<PackageName>`
cmake自带的库列表：<https://cmake.org/cmake/help/latest/manual/cmake-modules.7.html>
示例：导入gtest库
使用find_package(GTest REQUIRED)
或使用include(GoogleTest)，可使用一些特殊语法

```cmake
enable_testing()
find_package(GTest REQUIRED)

add_executable(foo foo.cc)
target_link_libraries(foo GTest::gtest GTest::gtest_main)

add_test(AllTestsInFoo foo)
```

**FetchContent_Declare**: 可以直接使用 FetchContent_Declare(libName) 和 FetchContent_MakeAvailable(libName) 来导入一个 git 上的库后，就可以像其他 target 一样使用了

```cmake
cmake_minimum_required(VERSION 3.14)

include(FetchContent)
FetchContent_Declare(
  mycom_toolchains
  GIT_REPOSITORY git@mycompany.com:git/projD.git
  GIT_TAG        20b415f9034bbd2a2e8216e9a5c9e632
)
FetchContent_MakeAvailable(mycom_toolchains)

project(CrossCompileExample)
```

可以考虑结合 concan 与 CMake 一起使用。concan 是业界目前 C/C++功能最完善的包管理器，具体请参考：concan 官网介绍
c++包管理工具
<https://www.cnblogs.com/xueweihan/p/11414263.html>

## 打包
