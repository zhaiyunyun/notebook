# Linux内核性能测试工具全景图

1.Linux性能监控工具及对应的内核层
![](../images/Linux性能观察工具.jpg)

2.Linux性能基础测试工具及对应内核层
![](../images/Linux性能benchmark工具.jpg)

3.Linux性能监控工具Sar及对应内核层
![](../images/Linux性能监控工具sar.jpg)

4.Linux性能调优工具及对应的内核层
![](../images/Linux性能调优工具.jpg)

# rdma编译

715 2022-08-12 22:21:02 yum install libnl3-devel
716 2022-08-12 22:21:17 yum install libnl3

mkdir build
cd build
cmake -DIN_PLACE=0 -DNO_MAN_PAGES=1 -DCMAKE_INSTALL_PREFIX=/usr/ ..
make -j8
make install

# 带宽测试

服务端： perf record -a -g ib_write_bw -R  -d rocep65s0f0 -D 100 -F

客户端： ib_write_bw -R  -d rocep65s0f1 -i 1 192.168.9.9  -D 100 -F

生成火焰图
perf script -i perf.data &> perf.unfold
./FlameGraph/stackcollapse-perf.pl perf.unfold &> perf.folded
./FlameGraph/flamegraph.pl perf.folded > perf.svg

perf report -g

或用火焰图脚本直接生成火焰图---出错

火焰图测试
gunzip -c example-perf-stacks.txt.gz | ./stackcollapse-perf.pl --all | ./flamegraph.pl --color=java --hash > example-perf.svg

ib_write_bw -R  -d rocep65s0f0 -D 100 -F | ./FlameGraph/stackcollapse-perf.pl --all | ./FlameGraph/flamegraph.pl --color=java --hash > rdma-perf.svg

ib_write_bw -R  -d rocep65s0f1 -i 1 192.168.9.9  -D 100 -F

# 添加符号表

perf buildid-list
perf buildid-cache --update=/usr/lib/debug/lib64/libibverbs.so.1.13.35.0-35.0-1.el8.x86_64.debug
perf buildid-cache --update=/usr/lib/debug/lib64/libibverbs.so.1.13.35.0-35.0-1.el8.x86_64.debug
perf buildid-cache --update=/usr/lib/debug/lib64/libmlx5.so.1.19.35.0-35.0-1.el8.x86_64.debug

perf buildid-cache --update=./rdma-core/build/lib/libmlx5.so.1.23.41.0

perf buildid-cache --update=./t1.debug
可以用，但是版本不对

注意：必须是同一次编译出来的elf才会有同一个buildid，然后buildid-cache --update才能生效
也就是我们必须自己编译rdma-core，release版本发布出去，debug版本用来添加符号表才能分析

是二进制文件的头部位和section内容计算出来的160位SHA-1算法值
查看elf可执行文件的buildid：readelf -n t1

perf record常用选项

-e record指定PMU事件
    --filter  event事件过滤器
-a  录取所有CPU的事件
-p  录取指定pid进程的事件
-g  使能函数调用图功能
--call-graph
