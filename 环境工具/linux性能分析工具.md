# linux性能分析工具

![性能分析工具汇总](../images/linux分析工具.jpg)

## 分析工具

1、CPU 性能分析工具：
vmstat
ps
sar
time
strace
pstree
top
2、Memory 性能分析工具：
vmstat
strace
top
ipcs
ipcrm
cat /proc/meminfo
cat /proc/slabinfo
cat /proc//maps
3、I/O 性能分析工具：
vmstat
ipstat
repquota
quotacheck
4、Network 性能分析工具：
ifconfig
ethereal
tethereal
iptraf
iwconfig
nfsstat
mrtg
ntop
netstat
cat /proc/sys/net

## Linux 性能调优工具

当通过上述工具及命令，我们发现了应用的性能瓶颈以后，我们可以通过以下工具或者命令来进行性能的调整
1、CPU 性能调优工具：
nice / renic
sysctl
2、Memory 性能调优工具：
swapon
ulimit
sysctl
3、I/O 性能调优工具：
edquota
quoton
sysctl
boot line:elevator=
4、Network 性能调优工具：
ifconfig
iwconfig
sysctl
三、性能调整

1、CPU 性能调整

当一个系统的 CPU 空闲时间或者等待时间小于 5%时，我们就可以认为系统的 CPU 资源耗尽，我们应该对 CPU 进行性能调整。

CPU 性能调整方法：
编辑/proc/sys/kernel/中的文件，修改内核参数。

```bash
# cd /proc/sys/kernel/

# ls /proc/sys/kernel/

acct hotplug panic real-root-dev
cad_pid modprobe panic_on_oops sem
cap-bound msgmax pid_max shmall
core_pattern msgmnb powersave-nap shmmax
core_uses_pid msgmni print-fatal-signals shmmni
ctrl-alt-del ngroups_max printk suid_dumpable
domainname osrelease printk_ratelimit sysrq
exec-shield ostype printk_ratelimit_burst tainted
exec-shield-randomize overflowgid pty threads-max
hostname overflowuid random version
一般可能需要编辑的是 pid_max 和 threads-max，如下：

# sysctl kernel.threads-max

kernel.threads-max = 8192

# sysctl kernel.threads-max=10000

kernel.threads-max = 10000
2、Memory 性能调整

当一个应用系统的内存资源出现下面的情况时，我们认为需要进行 Memory 性能调整：
页面频繁换进换出；
缺少非活动页。
例如在使用 vmstat 命令时发现，memory 的 cache 使用率非常低，而 swap 的 si 或者 so 则有比较高的数据值时，应该警惕内存的性能问题。
Memory 性能调整方法： 1)关闭非核心的服务进程。
相关的方法请见 CPU 性能调整部分。 2)修改/proc/sys/vm/下的系统参数。

# ls /proc/sys/vm/

block_dump laptop_mode nr_pdflush_threads
dirty_background_ratio legacy_va_layout overcommit_memory
dirty_expire_centisecs lower_zone_protection overcommit_ratio
dirty_ratio max_map_count page-cluster
dirty_writeback_centisecs min_free_kbytes swappiness
hugetlb_shm_group nr_hugepages vfs_cache_pressure

# sysctl vm.min_free_kbytes

vm.min_free_kbytes = 1024

# sysctl -w vm.min_free_kbytes=2508

vm.min_free_kbytes = 2508

# cat /etc/sysctl.conf

…
vm.min_free_kbytes=2058
… 3)配置系统的 swap 交换分区等于或者 2 倍于物理内存。

# free

total used free shared buffers cached
Mem: 987656 970240 17416 0 63324 742400
-/+ buffers/cache: 164516 823140
Swap: 1998840 150272 1848568
3、I/O 性能调整
系统出现以下情况时，我们认为该系统存在 I/O 性能问题：
系统等待 I/O 的时间超过 50%；
一个设备的平均队列长度大于 5。
我们可以通过诸如 vmstat 等命令，查看 CPU 的 wa 等待时间，以得到系统是否存在 I/O 性能问题的准确信息。
I/O 性能调整方法： 1)修改 I/O 调度算法。
Linux 已知的 I/O 调试算法有 4 种：
deadline - Deadline I/O scheduler
as - Anticipatory I/O scheduler
cfq - Complete Fair Queuing scheduler
noop - Noop I/O scheduler
可以编辑/etc/yaboot.conf 文件修改参数 elevator 得到。

# vi /etc/yaboot.conf

image=/vmlinuz-2.6.9-11.EL
label=linux
read-only
initrd=/initrd-2.6.9-11.EL.img
root=/dev/VolGroup00/LogVol00
append=”elevator=cfq rhgb quiet” 2)文件系统调整。
对于文件系统的调整，有几个公认的准则：
将 I/O 负载相对平均的分配到所有可用的磁盘上；
选择合适的文件系统，Linux 内核支持 reiserfs、ext2、ext3、jfs、xfs 等文件系统；

# mkfs -t reiserfs -j /dev/sdc1

文件系统即使在建立后，本身也可以通过命令调优；
tune2fs (ext2/ext3)
reiserfstune (reiserfs)
jfs_tune (jfs) 3)文件系统 Mount 时可加入选项 noatime、nodiratime。

# vi /etc/fstab

…
/dev/sdb1 /backup reiserfs acl, user_xattr, noatime, nodiratime 1 1 4)调整块设备的 READAHEAD，调大 RA 值。
[root@overflowuid ~]# blockdev –report
RO RA SSZ BSZ StartSec Size Device
…
rw 256 512 4096 0 71096640 /dev/sdb
rw 256 512 4096 32 71094240 /dev/sdb1
[root@overflowuid ~]# blockdev –setra 2048 /dev/sdb1
[root@overflowuid ~]# blockdev –report
RO RA SSZ BSZ StartSec Size Device
…
rw 2048 512 4096 0 71096640 /dev/sdb
rw 2048 512 4096 32 71094240 /dev/sdb1
4、Network 性能调整

一个应用系统出现如下情况时，我们认为该系统存在网络性能问题：
网络接口的吞吐量小于期望值；
出现大量的丢包现象；
出现大量的冲突现象。
Network 性能调整方法： 1)调整网卡的参数。

# ethtool eth0

Settings for eth0:
Supported ports: [ TP ]
Supported link modes: 10baseT/Half 10baseT/Full
100baseT/Half 100baseT/Full
1000baseT/Full
Supports auto-negotiation: Yes
Advertised link modes: 10baseT/Half 10baseT/Full
100baseT/Half 100baseT/Full
1000baseT/Full
Advertised auto-negotiation: Yes
Speed: 100Mb/s
Duplex: Half
Port: Twisted Pair
PHYAD: 0
Transceiver: internal
Auto-negotiation: on
Supports Wake-on: d
Wake-on: d
Current message level: 0×00000007 (7)
Link detected: yes
# ethtool -s eth0 duplex full
# ifconfig eth0 mtu 9000 up 2)增加网络缓冲区和包的队列。

# cat /proc/sys/net/ipv4/tcp_mem

196608 262144 393216

# cat /proc/sys/net/core/rmem_default

135168

# cat /proc/sys/net/core/rmem_max

131071

# cat /proc/sys/net/core/wmem_default

135168

# cat /proc/sys/net/core/wmem_max

131071

# cat /proc/sys/net/core/optmem_max

20480

# cat /proc/sys/net/core/netdev_max_backlog

300

# sysctl net.core.rmem_max

net.core.rmem_max = 131071

# sysctl -w net.core.rmem_max=135168

net.core.rmem_max = 135168 3)调整 Webserving。

# sysctl net.ipv4.tcp_tw_reuse

net.ipv4.tcp_tw_reuse = 0

# sysctl -w net.ipv4.tcp_tw_reuse=1

net.ipv4.tcp_tw_reuse = 1

# sysctl net.ipv4.tcp_tw_recycle

net.ipv4.tcp_tw_recycle = 0

# sysctl -w net.ipv4.tcp_tw_recycle=1

net.ipv4.tcp_tw_recycle = 1
```
