
# 目录

- [目录](#目录)
- [总结](#总结)
- [拖拽画图](#拖拽画图)
  - [draw.io](#drawio)
  - [processon](#processon)
  - [思维导图](#思维导图)
- [代码画图](#代码画图)
  - [GraphViz](#graphviz)
    - [vscode插件](#vscode插件)
    - [python调用](#python调用)
  - [plantuml](#plantuml)
  - [Mermaid](#mermaid)
- [示例](#示例)
  - [Mermaid流程图](#mermaid流程图)
  - [时序图](#时序图)
  - [plantuml用例图](#plantuml用例图)
  - [plantuml架构图](#plantuml架构图)
  - [plantuml思维导图](#plantuml思维导图)

# 总结

以下类型的图可使用[plantuml](#plantuml)来画
UML图：[用例图](#plantuml用例图)，类图，对象图，活动图，组件图，部署图，状态图，定时图
[架构图](#plantuml架构图)
[思维导图](#plantuml思维导图)

以下类型的图可使用[Mermaid](#mermaid)来画
[流程图](#mermaid流程图)
[时序图](#时序图)

必装markdown插件：
Markdown All in One： 基础markdown语法
Markdownlint Markdown： 语法格式检测和语法错误提示及修复建议
Markdown Preview Enhanced： 显示功能增强，比如各种图像，数学公式显示，内部支持 flow charts, sequence diagrams, mermaid, PlantUML, WaveDrom, GraphViz，Vega & Vega-lite，Ditaa 图像渲染。 你也可以通过使用 Code Chunk 来渲染 TikZ, Python Matplotlib, Plotly 等图像。

**Markdown Preview Enhanced** ： 这个插件基本支持了所有画图插件

# 拖拽画图

## draw.io

官网: <https://diagrams.net/>
在线使用: <https://app.diagrams.net/>
vscode插件: [hediet.vscode-drawio]([hediet.vscode-drawio](https://marketplace.visualstudio.com/items?itemName=hediet.vscode-drawio))
插件用法: 新建.drawio, .dio, .drawio.svg or .drawio.png后缀的文件自动打开，可以导出png，svg文件

## processon

官网： <https://www.processon.com/>

## 思维导图

百度脑图
vscode插件：souche.vscode-mindmap
新建.xm文件，运行mindmap可视化显示

# 代码画图

## GraphViz

dot 和 GraphViz
DOT语言是一种文本图形描述语言。DOT语言文件通常是具有.gv或是.dot的文件扩展名。
DOT语言定义了图，但没有提供渲染图的工具。这里列出了一些可以用来渲染，查看与修改DOT图的程序：

- Graphviz - 一组用来修改和渲染图的软件库和程序。
- OmniGraffle 可以导入DOT语言的一个子集，生成一个可编辑的文档，但转换结果不能被导出成为DOT文件。

官网：<http://www.graphviz.org/>
安装：<http://www.graphviz.org/download/>
教程：<http://graphs.grevian.org/>

### vscode插件

joaompinto.vscode-graphviz: [下载](https://marketplace.visualstudio.com/items?itemName=joaompinto.vscode-graphviz)
插件用法: 新建.dot，输入dot语言，右击在右侧显示就可

### python调用

python包： <https://pypi.org/project/graphviz/>
**注意**：需安装java，graphviz，配置dot可执行程序在系统path上

> Markdown Preview Enhanced 插件已经支持在md文件中嵌入dot图形了

## plantuml

[官网](https://plantuml.com/zh/), [教程](https://plantuml.com/zh/sitemap-language-specification), [在线使用](http://www.plantuml.com/plantuml/uml/SyfFKj2rKt3CoKnELR1Io4ZDoSa70000)

[本地server搭建](https://github.com/plantuml/plantuml-server)

vscode插件：[jebbs.plantuml](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml)
插件配置：

- PlantUMLServer render，在插件配置本地server地址或者使用官方server
- Local render，需安装java，Graphviz

> Markdown Preview Enhanced 插件已经支持在md文件中嵌入dot图形了

## Mermaid

[官网](https://mermaid-js.github.io/mermaid/#/), [在线使用](https://mermaid-js.github.io/mermaid-live-editor), [教程](https://mermaid-js.github.io/mermaid/#/n00b-syntaxReference?id=syntax-structure)
vscode插件：vstirbu.vscode-mermaid-preview
使用方法：新建.mmd或者在嵌入markdown的代码，Ctrl+shift+p，选择mermaid preview

# 示例

## Mermaid流程图

```flow

start=>start: 开始
input=>inputoutput: 输入
operation=>operation: 操作
condition=>condition: 操作出错？
output=>inputoutput: 输出
error=>operation: 请重新输入
end=>end: 结束

start->input
input->operation
operation->condition
condition(no,bottom)->output
condition(yes)->error(top)->input
output->end
```

## 时序图

```sequence
Title: Here is a title
A->B: Normal line
B-->C: Dashed line
C->>D: Open arrow
D-->>A: Dashed open arrow
```

## plantuml用例图

```plantuml
@startuml
left to right direction
skinparam packageStyle rectangle
actor customer
actor clerk
rectangle checkout {
  customer -- (checkout)
  (checkout) .> (payment) : include
  (help) .> (checkout) : extends
  (checkout) -- clerk
}
@enduml

```

## plantuml架构图

```plantuml
@startuml
skinparam rectangle<<behavior>> {
 roundCorner 25
}
sprite $bProcess jar:archimate/business-process
sprite $aService jar:archimate/application-service
sprite $aComponent jar:archimate/application-component

rectangle "Handle claim"  as HC <<$bProcess>><<behavior>> #Business
rectangle "Capture Information"  as CI <<$bProcess>><<behavior>> #Business
rectangle "Notify\nAdditional Stakeholders" as NAS <<$bProcess>><<behavior>> #Business
rectangle "Validate" as V <<$bProcess>><<behavior>> #Business
rectangle "Investigate" as I <<$bProcess>><<behavior>> #Business
rectangle "Pay" as P <<$bProcess>><<behavior>> #Business

HC *-down- CI
HC *-down- NAS
HC *-down- V
HC *-down- I
HC *-down- P

CI -right->> NAS
NAS -right->> V
V -right->> I
I -right->> P

rectangle "Scanning" as scanning <<$aService>><<behavior>> #Application
rectangle "Customer admnistration" as customerAdministration <<$aService>><<behavior>> #Application
rectangle "Claims admnistration" as claimsAdministration <<$aService>><<behavior>> #Application
rectangle Printing <<$aService>><<behavior>> #Application
rectangle Payment <<$aService>><<behavior>> #Application

scanning -up-> CI
customerAdministration  -up-> CI
claimsAdministration -up-> NAS
claimsAdministration -up-> V
claimsAdministration -up-> I
Payment -up-> P

Printing -up-> V
Printing -up-> P

rectangle "Document\nManagement\nSystem" as DMS <<$aComponent>> #Application
rectangle "General\nCRM\nSystem" as CRM <<$aComponent>>  #Application
rectangle "Home & Away\nPolicy\nAdministration" as HAPA <<$aComponent>> #Application
rectangle "Home & Away\nFinancial\nAdministration" as HFPA <<$aComponent>>  #Application

DMS .up.|> scanning
DMS .up.|> Printing
CRM .up.|> customerAdministration
HAPA .up.|> claimsAdministration
HFPA .up.|> Payment

legend left
Example from the "Archisurance case study" (OpenGroup).
See
====
<$bProcess> :business process
====
<$aService> : application service
====
<$aComponent> : application component
endlegend
@enduml

```

## plantuml思维导图

```plantuml
@startmindmap
+ OS
++ Ubuntu
+++ Linux Mint
+++ Kubuntu
+++ Lubuntu
+++ KDE Neon
++ LMDE
++ SolydXK
++ SteamOS
++ Raspbian
-- Windows 95
-- Windows 98
-- Windows NT
--- Windows 8
--- Windows 10
@endmindmap

```
