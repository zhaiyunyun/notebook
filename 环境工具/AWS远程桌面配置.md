# AWS图形界面与远程桌面配置流程

### 一·准备工作

#### 1.更换Ubuntu的默认软件源

#### 2.安装Ubuntu-desktop

```
sudo apt update
sudo apt install ubuntu-desktop
```

#### 3.安装LightDM（Ubuntu18.04）

```
sudo apt install lightdm
```

安装过程中，根据提示，选择LightDM作为默认桌面管理器

#### 4.更新软件包并重启Linux服务器

```
sudo apt upgrade
sudo reboot
```

#### 5.配置X服务器

###### 检查自动启动配置状况

```
sudo systemctl get-default
```

若上述命令返回graphical.target则表明 X 服务器已配置为自动启动。继续下一步。

如果命令返回 multi-user.target，则表明 X 服务器未配置为自动启动。运行以下命令：

```
sudo systemctl set-default graphical.target
```

###### 启动x服务器

```
sudo systemctl isolate graphical.target
```

###### 验证x服务器是否在运行

```
ps aux | grep X | grep -v grep
```

下面显示 X 服务器在运行时的示例输出。

```
root      1891  0.0  0.7 277528 30448 tty7     Ssl+ 10:59   0:00 /usr/bin/Xorg :0 -background none -verbose -auth /run/gdm/auth-for-gdm-wltseN/database -seat seat0 vt7
```

#### 6.安装Glxinfo实用程序

```
sudo apt install mesa-utils
```

#### 7.安装并配置NVIDIA驱动程序

安装流程不再叙述

###### 生成更新的xorg.conf

```
sudo nvidia-xconfig --preserve-busid --enable-all-gpus
sudo systemctl isolate multi-user.target
sudo systemctl isolate graphical.target
```

###### 验证OpenGL硬件渲染是否可用

```
sudo DISPLAY=:0 XAUTHORITY=$(ps aux | grep "X.*\-auth" | grep -v grep | sed -n 's/.*-auth \([^ ]\+\).*/\1/p') glxinfo | grep -i "opengl.*version"
```

下面显示了 OpenGL 硬件渲染可用时的示例输出。

```
OpenGL core profile version string: 4.4.0 NVIDIA 390.75
OpenGL core profile shading language version string: 4.40 NVIDIA via Cg compiler
OpenGL version string: 4.6.0 NVIDIA 390.75
OpenGL shading language version string: 4.60 NVIDIA
OpenGL ES profile version string: OpenGL ES 3.2 NVIDIA 390.75
OpenGL ES profile shading language version string: OpenGL ES GLSL ES 3.20
```

### 二·安装NICE DCV服务器（Ubuntu18.04 64位 x86）

###### NICE DCV 服务器程序包使用安全 GPG 签名进行数字签名。要允许程序包管理器验证程序包签名，您必须导入 NICE GPG 密钥。为此，打开一个终端窗口并导入 NICE GPG 密钥

```
wget https://d1uj6qtbmh3dt5.cloudfront.net/NICE-GPG-KEY
gpg --import NICE-GPG-KEY
```

###### 从 NICE 网站下载程序包

```
wget https://d1uj6qtbmh3dt5.cloudfront.net/2020.2/Servers/nice-dcv-2020.2-9662-ubuntu1804-x86_64.tgz
```

###### 提取 .tgz 存档的内容并导航到提取的目录

```
tar -xvzf nice-dcv-2020.2-9662-ubuntu1804-x86_64.tgz && cd nice-dcv-2020.2-9662-ubuntu1804-x86_64
```

###### 安装 NICE DCV 服务器

```
sudo apt install ./nice-dcv-server_2020.2.9662-1_amd64.ubuntu1804.deb
```

### 三·安装后配置

#### 1.设置NICE DCV服务器自动启动

```
sudo systemctl enable dcvserver
```

#### 2.更改NICE DCV服务器TCP端口

导航到 /etc/dcv/，并使用文本编辑器打开 dcv.conf。

在 web-port 部分中找到 [connectivity] 参数，并将现有 TCP 端口号替换为新的 TCP 端口号（本例中为8444）。

```
[connectivity]

web-port=8444
```

#### 3.在NICE DCV服务器上启用自动控制台会话

导航到 /etc/dcv/，并使用文本编辑器打开 dcv.conf。

按照以下格式将 create-session 和 owner 参数添加到对应段：

```
[session-management]
create-session = true

[session-management/automatic-console-session]
owner="session-owner"
```

保存文件并关闭后，重启NICE DCV服务器

```
sudo systemctl stop dcvserver
sudo systemctl start dcvserver
```

#### 4.配置身份验证

导航到 /etc/dcv/，并使用文本编辑器打开 dcv.conf。

找到 [authentication] 部分中的 security 参数，并将现有值替换为none。

#### 5.设置Ubuntu账户的密码

```
sudo passwd ubuntu
```

##### 此时应当可以使用NICE DCV客户端或网页连接配置好的服务器并用设置的密码登陆Ubuntu
