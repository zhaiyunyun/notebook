# 目录

- [目录](#目录)
- [CUDA](#cuda)
  - [CUDA Driver 和 NVIDIA Driver的区别](#cuda-driver-和-nvidia-driver的区别)
  - [CUDA 两种api](#cuda-两种api)
- [CUDA安装](#cuda安装)
- [OpenGL](#opengl)
  - [检查opengl信息](#检查opengl信息)
- [Vulkan](#vulkan)
- [NVIDIA驱动安装](#nvidia驱动安装)

# CUDA

**参考引用**：[CUDA、NVIDIA driver、多版本cuda](https://zhuanlan.zhihu.com/p/370673949)

CUDA包含三部分，CUDA toolkit、CUDA driver和NVIDIA GPU driver。

The CUDA software environment consists of three parts:

- CUDA Toolkit (libraries, CUDA runtime and developer tools) - **User-mode SDK** used to build CUDA applications
- CUDA driver - **User-mode driver** component used to run CUDA applications (such as libcuda.so on Linux systems)
- NVIDIA GPU device driver - **Kernel-mode driver** component for NVIDIA GPUs.

在linux系统中，CUDA driver和NVIDIA GPU device driver是统一在NVIDIA driver下的。

![CUDA组成结构](../images/CUDA组成结构.jpg)

## CUDA Driver 和 NVIDIA Driver的区别

如上所述，CUDA本身包含CUDA Driver和GPU kernel-mode Driver，而这两者在Linux系统中是统一在NVIDIA Driver中的。因此在安装好NVIDIA Driver好以后，只需要安装CUDA toolkit就可以保证CUDA相关的程序运行。

NVIDIA的显卡驱动程序和CUDA完全是两个不同的概念哦，CUDA是NVIDIA推出的用于自家GPU的并行计算框架，也就是说CUDA只能在NVIDIA的GPU上运行，而且只有当要解决的计算问题是可以大量并行计算的时候才能发挥CUDA的作用。

CUDA的本质是一个工具包（ToolKit）

## CUDA 两种api

CUDA有两种API，分别是**运行时API和驱动API（Runtime API与Driver API）**，
nvidia-smi的结果除了有GPU驱动版本型号，还有CUDA Driver API的版本号
而nvcc的结果是对应CUDA Runtime API，包含在CUDA toolkit里

# CUDA安装

**参考网址**：[Ubuntu 20.04 安装 CUDA Toolkit 的三种方式](https://www.cnblogs.com/klchang/p/14353384.html)

如上，安装NVIDIA Driver，自然就包含了CUDA Driver和GPU kernel-mode Driver
**NVIDIA Driver**安装方法：

1. **图形界面安装**，在 “Software & Update” 应用，在标签页 "Additional Drivers" 中选择 “nvidia-driver-460-server”
2. **命令行安装**，首先，需要卸载原有的 NVIDIA 驱动并**禁用自带的驱动** nouveau；然后，重启电脑，使用 lsmod | grep nouveau 命令检查禁用自带驱动是否成功；如果禁用成功，则安装从 NVIDIA 官方地址下载的 [CUDA  Toolkit](https://developer.nvidia.com/cuda-toolkit-archive)。其步骤则与方式二相同，差别在于这次需要安装 CUDA 驱动

**验证方法**：nvidia-smi查看驱动版本和CUDA驱动版本

**CUDA Toolkit**安装：

1. 采用 Ubuntu **软件源**中的 CUDA Tools 软件包
   这种方式安装简单，但安装的 CUDA Toolkit 版本往往**不是最新版本**。查询目前可安装的 CUDA Toolkit 版本的命令，如下所示

```bash
apt search nvidia-cuda-toolkit
sudo apt install nvidia-cuda-toolkit
```

2. **下载并安装** CUDA Toolkit
   下载cuda安装包：<https://developer.nvidia.com/cuda-downloads>
   sudo sh cuda_11.0.3_450.51.06_linux.run
   需要注意，安装时，选择**不安装 CUDA 驱动**
   安装结束后，添加环境变量到 ~/.bashrc 文件的末尾，具体添加内容如下：

```bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib64
export PATH=$PATH:/usr/local/cuda/bin
export CUDA_HOME=$CUDA_HOME:/usr/local/cuda
```

在 Terminal 中，激活环境变量命令为 **source ~/.bashrc** 。

测试 CUDA Toolkit 。 通过编译自带 Samples并执行， 以验证是否安装成功。具体命令如下所示：

```bash
cd /usr/local/cuda/samples/1_Utilities/deviceQuery
sudo make
./deviceQuery
```

CUDA有两种API，分别是运行时API和驱动API，即所谓的Runtime API与Driver API，nvidia-smi的结果除了有GPU驱动版本型号，还有CUDA Driver API的版本号，这里是10.0，而nvcc的结果是对应CUDA Runtime API

补充说明：在安装CUDA 时候会安装3大组件，分别是 NVIDIA 驱动、toolkit和samples。NVIDIA驱动是用来控制GPU硬件，toolkit里面包括nvcc编译器等，samples或者说SDK 里面包括很多样例程序包括查询设备、带宽测试等等。上面说的CUDADriver API是依赖于NVIDIA驱动安装的，而CUDA Runtime API 是通过CUDA toolkit安装的。

**验证方法**：nvcc -V

# OpenGL

**opengl规范**：是一个由Khronos组织制定并维护的规范(Specification)，定义了一系列API，包含了一系列可以操作图形、图像的函数

**opengl库实现**：OpenGL库的开发者通常是**显卡的生产商**，你购买的显卡所支持的OpenGL版本都为这个系列的显卡专门开发的

这些实现主要有：Windows平台下的WGL、Linux下的**Mesa/GLX**、Mac OS X下的Cocoa/NSGL，以及跨平台的GLUT、GLFW、SDL等等。

**Mesa**是Linux下的OpenGL实现。它提供了对AMD Radeon系列、Nvidia GPU、Intel i965, i945, i915以及VMWare虚拟GPU等多种硬件驱动的支持，同时也提供了对softpipe等多种软件驱动的支持。Mesa项目由Brian Paul于1993年8月创建，于1995年2月发布了第一个发行版，此后便受到越来越多的关注，如今Mesa已经是任何一个Linux版本首选的OpenGL实现。

**GLX**则是在Linux上用于提供GL与窗口交互、窗口管理等等的一组API。它的作用与Windows的WGL、Mac OS X的AGL以及针对OpenGL ES的EGL相似。在Linux上，窗口创建、管理等API遵循X Window接口，而GLX提供了OpenGL与X Window交互的办法。因此GLX也可以运用于其他使用X Window的平台，例如FreeBSD等。

对**NVIDIA显卡有2种方式**：

1. 使用NVIDIA专用闭源驱动，实现了OpenGL
2. 使用Nouveau作为硬件驱动，使用Mesa作为OpenGL实现

**intel完全开源**：直接推驱动代码到Mesa中，所以intel显卡驱动和opengl实现，只需要用mesa就可以

参考：[What is the relationship between the graphics card driver and OpenGL on Linux?](https://superuser.com/questions/1404273/what-is-the-relationship-between-the-graphics-card-driver-and-opengl-on-linux)

## 检查opengl信息

nvidia 驱动自带 openGL 的实现，默认随着驱动被安装。
**方式一**：

```bash
$ nvidia-settings -g | grep -i "opengl"
OpenGL vendor string: NVIDIA Corporation
OpenGL renderer string: GeForce GTX 1650/PCIe/SSE2
OpenGL version string: 4.6.0 NVIDIA 440.82
OpenGL extensions:
```

**方式二**：

```bash
$ sudo yum -y install mesa-utils
$ glxinfo | grep -i opengl
OpenGL vendor string: NVIDIA Corporation
OpenGL renderer string: GeForce GTX 1650/PCIe/SSE2
OpenGL core profile version string: 4.4.0 NVIDIA 440.82
OpenGL core profile shading language version string: 4.40 NVIDIA via Cg compiler
OpenGL core profile context flags: (none)
OpenGL core profile profile mask: core profile
OpenGL core profile extensions:
OpenGL version string: 4.6.0 NVIDIA 440.82
OpenGL shading language version string: 4.60 NVIDIA
```

英伟达显卡驱动版本选择：
<https://www.nvidia.com/download/index.aspx?lang=en-us>
GenForce RTX 3080：NVIDIA-Linux-x86_64-470.74.run

# Vulkan

vulkan是新一代替代opengl的图形接口
NVIDIA专用驱动自带vulkan支持

**查看vulkan版本**：

```bash
$ /usr/bin/vulkaninfo | head -n 5
===========
VULKAN INFO
===========

Vulkan Instance Version: 1.1.70
WARNING: radv is not a conformant vulkan implementation, testing use only.
```

# NVIDIA驱动安装

<http://www.360doc.com/content/20/1109/15/38520160_944893020.shtml>
