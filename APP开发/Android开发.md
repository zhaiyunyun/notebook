
# 安卓系统架构

官网介绍：<https://developer.android.google.cn/guide/platform>

1. linux 内核，主要是内存管理，进程管理，驱动管理等
2. HAL，硬件抽象层，由于 linux 开源许可的原因，各个厂家的驱动不希望开源，所以提供 HAL 各个厂家实现自己的驱动
3. android 运行层，包括 c/c++的各种库和 java 虚拟机 davlik 以及 java 标准库，ndk 接口：<https://developer.android.google.cn/ndk/reference>
4. java framework 层，app 框架，提供 activty 管理等，api 接口：<https://developer.android.google.cn/reference>

# Android 系统启动流程

- init 进程
  - Zygote 受精卵进程，所有 app 进程都从这里创建，监听 socket：666
    - ServiceManager：系统服务的注册查询，bindler 通信的基础，提供 app 和个系统服务的通信
    - SystemServer：启动各种系统服务线程 - 系统 ready 后，AMS 启动 home 桌面

SystemServer 启动的系统服务：

1. ActivityManagerService：四大组件的管理，app 的生命管理
2. WindowManagerService： 输入事件的分发和管理
3. PackageManagerService ：软件包的解包验证安装

# apk 安装流程

PackageManagerService 负责解析 apk 包，然后注册 AndroidManifest.xml 所声明的所有组件

# APP 启动

HOME 进程发送 startActivity 给 ActivityManagerService，AMS 发送 Zygote 消息创建进程，system_server 发送消息给 App 进程，主线程收到消息后回调 Activity.onCreate

# flutter 与 react native 跨平台技术

flutter 框架采用 dart 语言，底层是 dart 虚拟机，release 版本默认 AOT 编译，就是将 dart 代码编译成机器码，所以效率高

reactnative 框架，底层桥，中间是 js 解释器，上层是 jsx，桥接主要包装原生控件或其他功能给上层使用，性能差的原因是一有解释器，而是有桥接转换

参考：
<https://www.jianshu.com/p/e084414c7e1c>
<https://blog.csdn.net/freekiteyu/article/details/70082302>
<https://www.jianshu.com/p/2f95ab717078>
