
# 总结

双指针，指的是在遍历对象的过程中，不是普通的使用单个指针进行访问，而是使用**两个相同方向（快慢指针）**或者**相反方向（对撞指针）的指针**进行扫描，从而达到相应的目的。

快慢指针：两个指针以**不同的策略移动**，结束条件：快追上慢，或者快指针到边界
对撞指针：两个指针以**不同的策略移动**，结束条件：撞上

<https://zhuanlan.zhihu.com/p/71643340>
<https://cloud.tencent.com/developer/article/1861191>

# 快慢指针

## 27. 移除元素

[力扣题目链接](https://leetcode-cn.com/problems/remove-element/)

给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。

不要使用额外的数组空间，你必须仅使用 $O(1)$ 额外空间并**原地**修改输入数组。

元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。

示例 1:
给定 nums = [3,2,2,3], val = 3,
函数应该返回新的长度 2, 并且 nums 中的前两个元素均为 2。
你不需要考虑数组中超出新长度后面的元素。

示例 2:
给定 nums = [0,1,2,2,3,0,4,2], val = 2,
函数应该返回新的长度 5, 并且 nums 中的前五个元素为 0, 1, 3, 0, 4。

**你不需要考虑数组中超出新长度后面的元素。**

**思路**

快慢指针：关键是确定两个指针的指向
快指针：指向原始数组
慢指针：指向移除val元素之后的新数组，也就是新数组第一个就是跟val不同的元素

```c++
// 双指针法，快慢指针
// auto iter = nums.begin();
// auto inserter = nums.end();
// while(iter != nums.end()) {
//     if (inserter == nums.end()) {
//         if (*iter == val) {
//             inserter = iter;
//         }
//     } else {
//         if (*iter != val) {
//             *inserter = *iter;
//             ++inserter;
//         }
//     }
//     ++iter;
// }
// return (inserter-nums.begin());
int slow_iter = 0;
for (int fast_iter = 0; fast_iter < nums.size(); ++fast_iter) {
    if (val != nums[fast_iter]){
        nums[slow_iter++] = nums[fast_iter];
    }
}
return slow_iter;
```

# 对撞指针

## 881. 救生艇

[力扣题目链接](https://leetcode-cn.com/problems/boats-to-save-people/)

给定数组 people 。people[i]表示第 i 个人的体重 ，船的数量不限，每艘船可以承载的最大重量为 limit。

每艘船最多可同时载两人，但条件是这些人的重量之和最多为 limit。

返回 承载所有人所需的最小船数 。

示例 1：

输入：people = [1,2], limit = 3
输出：1
解释：1 艘船载 (1, 2)
示例 2：

输入：people = [3,2,2,1], limit = 3
输出：3
解释：3 艘船分别载 (1, 2), (2) 和 (3)
示例 3：

输入：people = [3,5,3,4], limit = 5
输出：4
解释：4 艘船分别载 (3), (3), (4), (5)
