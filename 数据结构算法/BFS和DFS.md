# 易错点

1. 要注意起点终点坐标，是从0开始还是从1开始

# 数据结构选择

BFS或DFS，一般需要用图来存储数据结构

注意使用邻接矩阵还是邻接表存储，邻接矩阵有时会超内存

请查看：[数据结构之图](数据结构之图.md#代码实现)

# BFS算法模板

应用场景：找最短路径
算法模板：
找到起点
起点入队，并标记visited
while(queue.empty())
{
    // 进入当前node，pop
    // 判断是否找到目标，check
    // 所有符合条件的子节点入队，push(node.next_steps())，并标记visited
}
关键点：

1. 队列中node，可以存储所需信息，比如存储层数，则可以知道最短路径的step，存储父节点，可以回溯回去得到答案的路径
2. next_steps中，在这个函数中添加搜索规则，哪些方向能走，哪些不能走，比如走过的被mark过的不能再走
3. 入队后**立马标记visited**，如果在while立马标记，则会多走重复的节点

**好习惯**：注意结构化设计和编程
根据思路，写出代码框架，然后实现各个小函数，然后分别单元测试

# DFS算法模板

DFS是回溯算法的一种

实现方式：递归和非递归，非递归方式使用栈实现，函数调用就是用栈实现
void dfs(pos)
{
    // 当前该怎么做
    // 1. 首先判断是否到达终点，到达则直接返回
    // 2. 标记当前pos已经遍历过
    // 下一步该怎么做
    // 1. 获取下一步可能走的pos，然后dfs(next_pos)下去
}

# BFS题目

## 牛客-AB19单源最短路

题目链接： <https://www.nowcoder.com/practice/f24504c9ff544f7c808f76693cc34af8>

**题目描述**
给你一个无向图，图中包含 5000 个点 m 个边，任意两个点之间的距离是 1 ，无重边或自环。请求出1号点到n号点的最短距离。

注意：图中可能存在孤立点，即存在点与任意点都没有边相连

如果1号点不能到达n号点，输出-1.
输入描述：
第一行两个整数n和m，表示图的点和边数。
接下来m行，每行两个整数u，v，表示u到v有一条无向边。
1≤n≤5000 500001≤m≤50000
输出描述：
输出一行，表示1到n的最短路，如不存在，输出-1.

输入：
4 4
1 2
2 4
3 4
3 1
输出：
2

输入：
4 3
1 2
2 3
3 1
输出：
-1

**代码实现**

```c++
#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include <queue>
#include <map>
using namespace std;
// 邻接表
class AdjGraph
{
private:
    // 邻接表
    // 顶点数组，元素是顶点类型，顶点类型包含顶点数据和邻接点的数组
    // 邻接点，边信息类型比如可以存权值等，邻接点就存储顶点数组的编号
    using VertexData = int; // 存储顶点数据
    using EdgeInfo = int; // 边信息，可以存权值
    // 邻接点类型
    typedef struct Adjacent {
        int vertex_index; // 顶点数组index
        EdgeInfo weight; // 边信息，权重啥的
    } Adjacent;
    typedef struct Vertex {
        VertexData data; // 顶点数据
        vector<Adjacent> adj_list; // 邻接点列表
    } Vertex;
    vector<Vertex> m_vertexs;
    int m_vertex_cnt = 0; // 顶点个数
    int m_edge_cnt = 0; // 边个数
    const int not_walk = 0;
    const int has_walked = 1;
    bool m_add_edge(int start, int end);
public:
    AdjGraph(int vertex_cnt, int edge_cnt): m_vertex_cnt(vertex_cnt), \
        m_edge_cnt(edge_cnt), m_vertexs(vertex_cnt){}
    bool add_edge(int start, int end);
    void print_graph();
    int bfs(int start, int end);
};

bool AdjGraph::add_edge(int start, int end)
{
    if (start >= m_vertex_cnt || end >= m_vertex_cnt) {
        return false;
    }
    m_add_edge(start, end);
    m_add_edge(end, start);
    return true;
}

bool AdjGraph::m_add_edge(int start, int end)
{
    if (start >= m_vertex_cnt || end >= m_vertex_cnt) {
        return false;
    }
    Adjacent adj;
    adj.vertex_index = end;
    m_vertexs[start].adj_list.push_back(adj);
    return true;
}

void AdjGraph::print_graph()
{
    cout << "vertex count = " << m_vertex_cnt << endl;
    cout << "edge count = " << m_edge_cnt << endl;
    for (size_t i = 0; i < m_vertexs.size(); ++i) {
        cout << "vertex " << i;
        for (size_t j = 0; j < m_vertexs[i].adj_list.size(); ++j) {
            cout << "-->> " << m_vertexs[i].adj_list[j].vertex_index;
        }
        cout << endl;
    }
}

int AdjGraph::bfs(int start, int end)
{
    int step = -1;
    queue<tuple<int, int>> q;
    // queue
    // 起点入队
    q.push({start, 0});
    m_vertexs[start].data = has_walked;
    while (!q.empty())
    // while 队非空
    {
        // 出队
        auto [curr_node, node_step] = q.front();
        // cout << parent_node <<" "<< curr_node<<" " << node_step << endl;
        q.pop();
        // check， 是否到达终点，到则，break，不到
        if (curr_node == end) {
            step = node_step;
            break;
        }
        // 标记当前已遍历
        m_vertexs[curr_node].data = has_walked;

        // 获取所有子节点
        auto edges = m_vertexs[curr_node].adj_list;
        for (size_t i = 0; i < edges.size(); ++i) {
            int next_node = edges[i].vertex_index;
            if (m_vertexs[next_node].data == not_walk) {
                q.push({next_node, node_step+1});
            }
        }
        // 所有子节点入队
    }

    return step;
}

int main()
{
    int point_cnt = 0, edge_cnt = 0;
    cin >> point_cnt >> edge_cnt;

    AdjGraph g(5001,edge_cnt);
    while (--edge_cnt >= 0) {
        int start = 0, end = 0;
        cin >> start >> end;
        bool ret = g.add_edge(start, end);
        if (!ret) {
            cout << "can't add edge " << start << " " << end << endl;
        }
    }
    cout << g.bfs(1, point_cnt) << endl;
    return 0;
}
```

## 牛客-AB20走迷宫

题目链接： <https://www.nowcoder.com/practice/e88b41dc6e764b2893bc4221777ffe64>
题目描述
给定一个 n*m 的网格，在网格中每次在不超过边界的情况下可以选择向上、向下、向左、向右移动一格。网格中的一些格子上放置有障碍物，放有障碍物的格子不能到达。求从(sx,sy) 到(ex,ey)最少的移动次数。若不能到达，输出−1。
输入描述：
第一行输入两个整数n, m(1≤n,m≤1000)，表示网格大小。
第二行输入四个整数sx,sy,ex,ey，表示起点和终点的坐标。
接下来的n行，每行输入一个长度为m的字符串。其中，第i行第j个字符表示第i行第j列的格子上的障碍物情况，若字符为'*'，则格子上有障碍物，若字符为'.'，则格子上没有障碍物。
保证起点不存在障碍物。
输出描述：
输出一行一个整数，表示从(sx,sy) 到(ex,ey)最少的移动次数

输入：
5 5
1 1 5 5
.....
****.
.....
**.**
.....

输出：
12
**代码实现**

```c++
#include <iostream>
#include <vector>
#include <queue>
#include <tuple>
#include <algorithm>
#include <fstream>
#include <stdio.h>     //freopen
using namespace std;

// 邻接矩阵
class MatrixGraph
{
private:
    int m_rows;
    int m_cols;
    vector<vector<char>> m_vertex_matrx;
    const char barrier = '*';
    const char no_barrier = '.';
    const char walked = '#';
public:
    MatrixGraph(int rows, int cols): m_rows(rows), m_cols(cols), \
        m_vertex_matrx(rows, vector<char>(cols)) {}
    void create_graph();
    void print();
    int bfs(int sx, int sy, int ex, int ey);
};

void MatrixGraph::create_graph()
{
    for (int r = 0; r < m_rows; ++r) {
        for (int c = 0; c < m_cols; ++c) {
            char input_c;
            cin >> input_c;
            m_vertex_matrx[r][c] = input_c;
        }
    }
}

void MatrixGraph::print()
{
    cout << "rows = " << m_rows << endl;
    cout << "cols = " << m_cols << endl;
    cout << m_vertex_matrx.capacity()<< endl;
    for (size_t i = 0; i < m_rows; ++i) {
        for (size_t j = 0; j < m_cols; ++j) {
            cout << m_vertex_matrx[i][j] << " ";
        }
    }
    cout << "print end" << endl;
}
int MatrixGraph::bfs(int sx, int sy, int ex, int ey)
{
    int step = -1;
    // 找起点
    tuple<int,int,int> start = make_tuple(sx, sy, 0);
    // 起点入队
    queue<tuple<int,int,int>> q;
    q.push(start);
    m_vertex_matrx[sx][sy] = walked;
    while (!q.empty())
    // while 队非空
    {
        // 出队
        auto [x, y, curr_step] = q.front();
        // cout << x <<" "<< y<<" " << curr_step << endl;
        q.pop();
        // check， 是否到达终点，到则，break，不到
        if (x == ex && y == ey) {
            step = curr_step;
            break;
        }
        // 标记当前已遍历
        // m_vertex_matrx[x][y] = walked;

        // 符合条件的子节点入队
        int next_step = curr_step + 1;
        int left = y-1;
        if (left >= 0 && m_vertex_matrx[x][left] == no_barrier) {
            q.push(make_tuple(x,left,next_step));
            m_vertex_matrx[x][left] = walked;
        }
        int right = y+1;
        if (right < m_cols && m_vertex_matrx[x][right] == no_barrier) {
            q.push(make_tuple(x,right,next_step));
            m_vertex_matrx[x][right] = walked;
        }
        int up = x-1;
        if (up >= 0 && m_vertex_matrx[up][y] == no_barrier) {
            q.push(make_tuple(up,y,next_step));
            m_vertex_matrx[up][y] = walked;
        }
        int down = x+1;
        if (down < m_rows && m_vertex_matrx[down][y] == no_barrier) {
            q.push(make_tuple(down,y,next_step));
            m_vertex_matrx[down][y] = walked;
        }
    }
    return step;
}

int main()
{
    freopen("data.txt", "r", stdin);
    int n = 0, m = 0;
    cin >> n >> m;
    int sx=0,sy=0,ex=0,ey=0;
    cin >> sx >> sy >> ex >> ey;
    --sx;
    --sy;
    --ex;
    --ey;
    MatrixGraph g(n, m);
    g.create_graph();
    cout << g.bfs(sx,sy,ex,ey) << endl;
    return 0;
}
```

# DFS题目

<https://www.nowcoder.com/exam/oj?tab=%E7%AE%97%E6%B3%95%E7%AF%87&topicId=308>

## luogo题目

题目背景
给定一个N*M方格的迷宫，迷宫里有T处障碍，障碍处不可通过。给定起点坐标和终点坐标，问: 每个方格最多经过1次，有多少种从起点坐标到终点坐标的方案。在迷宫中移动有上下左右四种方式，每次只能移动一个方格。数据保证起点上没有障碍。

题目描述
无

输入格式
第一行N、M和T，N为行，M为列，T为障碍总数。第二行起点坐标SX,SY，终点坐标FX,FY。接下来T行，每行为障碍点的坐标。

输出格式
给定起点坐标和终点坐标，问每个方格最多经过1次，从起点坐标到终点坐标的方案总数。

输入
2 2 1
1 1 2 2
1 2

输出
1

```c++
#include <iostream>
#include <vector>
using namespace std;

int path_cnts = 0;
int ex = -1, ey = -1;
vector<vector<int>> *pdata;
// 0 可走，1 障碍, 2 走过
void mark_walk_past(int x, int y)
{
    (*pdata)[x][y] = 2;
    return;
}
vector<pair<int,int>> get_childs(int x, int y)
{
    vector<pair<int,int>> childs;
    int left = y-1;
    if (left > 0 && (*pdata)[x][left] == 0) {
        childs.push_back(make_pair(x,left));
    }
    int right = y+1;
    if (right < (*pdata)[x].size() && (*pdata)[x][right] == 0) {
        childs.push_back(make_pair(x,right));
    }
    int up = x-1;
    if (up > 0 && (*pdata)[up][y] == 0) {
        childs.push_back(make_pair(up,y));
    }
    int down = x+1;
    if (down < (*pdata).size() && (*pdata)[down][y] == 0) {
        childs.push_back(make_pair(down,y));
    }
    return childs;
}
void dfs(int x, int y)
{
    if (x == ex && y == ey) {
        ++path_cnts;
        return;
    }
    mark_walk_past(x, y);
    vector<pair<int,int>> child_pos = get_childs(x, y);
    for (auto &p: child_pos) {
        dfs(p.first, p.second);
    }
    return;
}
int main()
{
    int rows, cols, t_cnts;
    cin >> rows >> cols >> t_cnts;
    pdata = new vector<vector<int>>(rows+1, vector<int>(cols+1, 0));
    int sx, sy;
    cin >> sx >> sy >> ex >> ey;
    while (--t_cnts >= 0) {
        int x, y;
        cin >> x >> y;
        (*pdata)[x][y] = 1;
    }

    for (auto &r: *pdata) {
        for (auto &c: r) {
            cout << c << " ";
        }
        cout << endl;
    }
    cout << endl;

    dfs(sx, sy);
    cout << path_cnts << endl;
    return 0;
}

```

## LeetCode题目

给定一个二叉树，找出其最大深度。

二叉树的深度为根节点到最远叶子节点的最长路径上的节点数。

说明: 叶子节点是指没有子节点的节点。

示例：
给定二叉树 [3,9,20,null,null,15,7]
返回它的最大深度 3 。

来源：力扣（LeetCode）
链接：<https://leetcode-cn.com/problems/maximum-depth-of-binary-tree>
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。

二：
在一个 106 x 106 的网格中，每个网格上方格的坐标为 (x, y) 。

现在从源方格 source = [sx, sy] 开始出发，意图赶往目标方格 target = [tx, ty] 。数组 blocked 是封锁的方格列表，其中每个 blocked[i] = [xi, yi] 表示坐标为 (xi, yi) 的方格是禁止通行的。

每次移动，都可以走到网格中在四个方向上相邻的方格，只要该方格 不 在给出的封锁列表 blocked 上。同时，不允许走出网格。

只有在可以通过一系列的移动从源方格 source 到达目标方格 target 时才返回 true。否则，返回 false。

输入：blocked = [[0,1],[1,0]], source = [0,0], target = [0,2]
输出：false
解释：
从源方格无法到达目标方格，因为我们无法在网格中移动。
无法向北或者向东移动是因为方格禁止通行。
无法向南或者向西移动是因为不能走出网格。

输入：blocked = [], source = [0,0], target = [999999,999999]
输出：true
解释：
因为没有方格被封锁，所以一定可以到达目标方格。

来源：力扣（LeetCode）
链接：<https://leetcode-cn.com/problems/escape-a-large-maze>
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。

## AB23 kotori和素因子

<https://www.nowcoder.com/practice/7b1c858a3e7a41ed8364178979eaae67?tpId=308&tqId=500564&ru=/exam/oj&qru=/ta/algorithm-start/question-ranking&sourceUrl=%2Fexam%2Foj%3Ftab%3D%25E7%25AE%2597%25E6%25B3%2595%25E7%25AF%2587%26topicId%3D308>

描述
kotori拿到了一些正整数。她决定从每个正整数取出一个素因子。但是，kotori有强迫症，她不允许两个不同的正整数取出相同的素因子。

她想知道，最终所有取出的数的和的最小值是多少？

注：若a%k==0，则称k是a的因子。若一个数有且仅有两个因子，则称其是素数。显然1只有一个因子，不是素数。

输入描述：
第一行一个正整数n，代表kotori拿到正整数的个数。

第二行共有n个数ai，表示每个正整数的值。

保证不存在两个相等的正整数。

1<=n<=10

2<=ai<=1000
输出描述：
一个正整数，代表取出的素因子之和的最小值。若不存在合法的取法，则输出-1。

输入：
4
12 15 28 22
输出：
17

5
4 5 6 7 8

-1

## AB22 岛屿数量

题目链接： <https://www.nowcoder.com/practice/0c9664d1554e466aa107d899418e814e>

描述
给一个01矩阵，1代表是陆地，0代表海洋， 如果两个1相邻，那么这两个1属于同一个岛。我们只考虑上下左右为相邻。
岛屿: 相邻陆地可以组成一个岛屿（相邻:上下左右） 判断岛屿个数。
例如：
输入
[
[1,1,0,0,0],
[0,1,0,1,1],
[0,0,0,1,1],
[0,0,0,0,0],
[0,0,1,1,1]
]
对应的输出为3
(注：存储的01数据其实是字符'0','1')

输入：
[[1,1,0,0,0],[0,1,0,1,1],[0,0,0,1,1],[0,0,0,0,0],[0,0,1,1,1]]
输出
3

```c++
#include <algorithm>
class Solution {
public:
    /**
     * 判断岛屿数量
     * @param grid char字符型vector<vector<>> 
     * @return int整型
     */
    const char sea = '0';
    const char land = '1';
    const char walked = '2';
    vector<pair<int,int>> get_childs(vector<vector<char> >& grid, int x, int y)
    {
        vector<pair<int,int>> childs;
        int left = y-1;
        if (left >= 0 && grid[x][left] == land) {
            childs.push_back(make_pair(x,left));
        }
        int right = y+1;
        if (right < grid[x].size() && grid[x][right] == land) {
            childs.push_back(make_pair(x,right));
        }
        int up = x-1;
        if (up >= 0 && grid[up][y] == land) {
            childs.push_back(make_pair(up,y));
        }
        int down = x+1;
        if (down < grid.size() && grid[down][y] == land) {
            childs.push_back(make_pair(down,y));
        }
        return childs;
    }
    void mark(vector<vector<char> >& grid, int i, int j)
    {
        grid[i][j] = walked;
    }
    void bfs(vector<vector<char> >& grid, int i, int j)
    {
        queue<pair<int,int>> q;
        q.push(make_pair(i,j));
        while (!q.empty()) {
            auto [x,y] = q.front();
            q.pop();
            mark(grid, x, y);
            auto child_nodes = get_childs(grid, x, y);
            for_each(begin(child_nodes), end(child_nodes), [&q](auto &p){q.push(p);});
        }
    }

    void dfs(vector<vector<char> >& grid, int x, int y)
    {
        mark(grid, x, y);
        auto child_nodes = get_childs(grid, x, y);
        for (auto &p: child_nodes) {
            dfs(grid, p.first, p.second);
        }
    }

    int solve(vector<vector<char> >& grid) {
        int cnt = 0;
        for (decltype(grid.size()) i = 0; i < grid.size(); ++i) {
            for (auto j = 0; j < grid[i].size(); ++j) {
                if (grid[i][j] == '1') {
                    // bfs(grid, i, j);
                    dfs(grid, i, j);
                    ++cnt;
                }
            }
        }
        return cnt;
    }
};
```

## AB21 N皇后问题

描述
N 皇后问题是指在 n * n 的棋盘上要摆 n 个皇后，
要求：任何两个皇后不同行，不同列也不在同一条斜线上，
求给一个整数 n ，返回 n 皇后的摆法数。

数据范围: 1 \le n \le 91≤n≤9
要求：空间复杂度 O(1)O(1) ，时间复杂度 O(n!)O(n!)

例如当输入4时，对应的返回值为2，
对应的两种四皇后摆位如下图所示：

输入输出
1 1
8 92

## 飞步笔试

```c++
#include <iostream>
#include <vector>
#include <string>

using namespace std;

class PathCheck
{
public:
    PathCheck(vector<vector<char>> world, string path) {
        this->world = world;
        this->path = path;
    }
    void mark(int x, int y, char c){
        world[x][y] = c;
    }
    vector<pair<int,int>> next_steps(int x, int y, int next_pos) {
        vector<pair<int,int>> steps;
        char next_char = path.at(next_pos);
        int left = y - 1;
        if (left >=0 && world[x][left] == next_char) {
            steps.push_back(make_pair(x, left));
        }
        int right = y + 1;
        if (right < world[x].size() && world[x][right] == next_char) {
            steps.push_back(make_pair(x, right));
        }
        int up = x - 1;
        if (up >=0 && world[up][y] == next_char) {
            steps.push_back(make_pair(up, y));
        }
        int down = x + 1;
        if (down < world.size() && world[down][y] == next_char) {
            steps.push_back(make_pair(down, y));
        }
        return steps;
    }
    bool dfs(int x, int y, int pos) {
        cout << pos << " " << x << " " << y << endl;
        // 是否到达终点
        char current_char = world[x][y];
        if ((pos == path.length()-1) && current_char == path.at(pos)) {
            return true;
        }
        
        // 标记走过的

        mark(x,y,walked);
        // 子节点，存在继续dfs，不存在，返回false
        vector<pair<int,int>> childs = next_steps(x,y,pos+1);
        if (childs.empty()) {
            // 恢复标记
            mark(x,y,current_char);
            return false;
        } else {
            for (auto &p : childs) {
                return dfs(p.first, p.second,pos+1);
            }
        }
    }
    bool is_path_exist() {
        char start_char = path.at(0);
        for (int i = 0; i < world.size(); ++i) {
            for (int j = 0; j < world[i].size(); ++j) {
                if (start_char == world[i][j]) {
                    if (dfs(i, j, 0)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
private:
    vector<vector<char>> world;
    string path;
    const char walked = '#';
};

int main()
{
    // 用bfs实现有问题，第一次就把两个a标记走过了
    // 用dfs实现
    vector<vector<char>> world{
        {'a','e','f','g'},
        {'c','d','a','m'},
        {'b','d','n','k'}
    };
    // string path("acda");
    string path("acdak");
    PathCheck path_check = PathCheck(world, path);
    cout << path_check.is_path_exist() << endl;
}
```
