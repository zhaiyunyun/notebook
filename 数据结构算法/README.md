数据结构，用来描述数据间的关系

算法，指解决某一特定问题的步骤

数据结构为算法服务，算法是特定问题下的解决办法，故没有通用性算法，应具体问题具体分析

# 一、数据结构

![数据结构](../images/数据结构.png)
以上为数据结构的逻辑结构，在计算机内部的物理存储结构有：顺序结构，链式结构、索引结构和散列结构。

# 二、算法

![算法](../images/算法.png)

# 三、学习目标

数据结构，熟悉各数据结构特点，掌握增删改查的操作方法。

算法学习，理解各算法所解决问题的定义，掌握算法复杂度分析方法，体会各算法的基本思想

# 四、学习途径

**算法刷题顺序**：
代码随想录 <https://gitee.com/programmercarl/leetcode-master>

1. 在线课程
   斯坦福大学算法，<https://www.coursera.org/specializations/algorithms#courses>

2. 可视化平台
   <https://algorithm-visualizer.org/>，特点：对算法有分类
   <https://visualgo.net/zh>，特点：有中文，有练习题目
   <https://www.cs.usfca.edu/~galles/visualization/Algorithms.html>，特点：可以输入可操作

3. 在线练题
   leetcode： <https://leetcode.com/>
   hihocoder： <http://hihocoder.com/hiho>
   lintcode： <https://www.lintcode.com/>
   <https://pintia.cn/problem-sets?tab=0>

4. 一些博客
   <https://www.cnblogs.com/skywang12345/p/3603935.html>

引用列表：

算法思想总结：
<https://zhuanlan.zhihu.com/p/36903717>
<https://blog.csdn.net/m0_37872090/article/details/80819788>

查找算法：
<https://blog.csdn.net/weixin_39241397/article/details/79344179>

数据结构和算法概述

<https://zhuanlan.zhihu.com/p/93928797>

<https://blog.csdn.net/EllieYaYa/article/details/82852743>

<https://www.zhihu.com/question/21318658>
