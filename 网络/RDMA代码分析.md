# RDMA架构概览

以下内容主要参考：mellanox公司资料
[rdma aware programming user manual](https://docs.nvidia.com/networking/display/RDMAAwareProgrammingv17)
RDMA Programming Basic，网上没搜到，mellanox的
[mellanox文档官网](https://docs.nvidia.com/networking/)

# Programming Overview

## 通信操作

send/revc
read/write
atomic fetch/add/compare/swap

## 传输模式

RC 可靠连接
UC 不可靠连接
UD 不可靠报文

## 核心概念

### SR

发送队列

### RR

接受队列

### CQ

完成队列

### MR

内存注册

### MW

内存窗口

### AH

地址handle

### GRH

全局路由头

### PD

保护域

### AE

异步事件

const char *ibv_event_type_str(enum ibv_event_type event)
{
static const char*const event_type_str[] = {
  [IBV_EVENT_CQ_ERR]  = "CQ error",
  [IBV_EVENT_QP_FATAL]  = "local work queue catastrophic error",
  [IBV_EVENT_QP_REQ_ERR]  = "invalid request local work queue error",
  [IBV_EVENT_QP_ACCESS_ERR] = "local access violation work queue error",
  [IBV_EVENT_COMM_EST]  = "communication established",
  [IBV_EVENT_SQ_DRAINED]  = "send queue drained",
  [IBV_EVENT_PATH_MIG]  = "path migrated",
  [IBV_EVENT_PATH_MIG_ERR] = "path migration request error",
  [IBV_EVENT_DEVICE_FATAL] = "local catastrophic error",
  [IBV_EVENT_PORT_ACTIVE]  = "port active",
  [IBV_EVENT_PORT_ERR]  = "port error",
  [IBV_EVENT_LID_CHANGE]  = "LID change",
  [IBV_EVENT_PKEY_CHANGE]  = "P_Key change",
  [IBV_EVENT_SM_CHANGE]  = "SM change",
  [IBV_EVENT_SRQ_ERR]  = "SRQ catastrophic error",
  [IBV_EVENT_SRQ_LIMIT_REACHED] = "SRQ limit reached",
  [IBV_EVENT_QP_LAST_WQE_REACHED] = "last WQE reached",
  [IBV_EVENT_CLIENT_REREGISTER] = "client reregistration",
  [IBV_EVENT_GID_CHANGE]  = "GID table change",
  [IBV_EVENT_WQ_FATAL]  = "WQ fatal"
 };

if (event < IBV_EVENT_CQ_ERR || event > IBV_EVENT_WQ_FATAL)
return "unknown";

return event_type_str[event];

用ibv_asyncwatch程序监控RNIC上的异步事件

### SG

散列表

### Polling

前文我们简单介绍了RDMA中最常见的一些资源，包括各种Queue，以及MR的概念等等。
MR用于控制和管理HCA对于本端和远端内存的访问权限，确保HCA只有拿到正确Key之后才能读写用户已经注册了的内存区域。为了更好的保障安全性，
IB协议又提出了Protection Domain（PD）的概念，用于保证RDMA资源间的相互隔离，本文就介绍一下PD的概念。

### QP-自己加的

QP Context存储，qp编号等信息

其实RDMA中不止有QP、MR这些资源，后文即将介绍的Address Handle，Memory Window等也是由PD进行隔离保护的。

在传统TCP-IP协议栈中，使用了家喻户晓的IP地址来标识网络层的每个节点。
而IB协议中的这个标识被称为GID（Global Identifier，全局ID），标识节点
QPN，类似socket，表示QP的编号

而对于UD来说，QP间没有连接关系，用户想发给谁，就在WQE中填好对端的地址信息就可以了。
用户不是直接把对端的地址信息填到WQE中的，而是提前准备了一个“地址薄”，每次通过一个索引来指定对端节点的地址信息，而这个索引就是AH。

虽然IB协议将QP称为“虚拟接口”，但是它是有实体的：

硬件上，QP是一段包含着若干个WQE的存储空间，IB网卡会从这段空间中读取WQE的内容，并按照用户的期望去内存中存取数据。至于这个存储空间是内存空间还是IB网卡的片内存储空间，IB协议并未做出限制，每个厂商有各自的实现
软件上，QP是一个由IB网卡的驱动程序所维护的数据结构，其中包含QP的地址指针以及一些相关的软件属性。

下期打算给大家讲解MR Buffer的实现原理和细节，它和QP Buffer在申请和使用流程上有诸多相似点，但是有一个明显的差异
——QP Buffer是驱动申请而用户不可见的，而MR Buffer却是用户自己申请的。

# Verbs API

VPI Verbs API

RDMA_CM API

RDMA Verbs API

接口API分类

![](../images/rdma-control-path.jpg)

控制面IO接口

RC QP建链
RC QP断链
UD QP建链
<https://zhuanlan.zhihu.com/p/494826608>

RDMA之基于Socket API的QP间建链
<https://zhuanlan.zhihu.com/p/476407641>
RDMA之基于CM API的QP间建链
<https://zhuanlan.zhihu.com/p/494826608>

Verbs API之后发生，代码解释
<https://blog.csdn.net/bandaoyu/article/details/113125473>

1）控制面：

设备管理：

device_list ibv_get_device_list()
用户获取可用的RDMA设备列表，会返回一组可用设备的指针。

device_context ibv_open_device(device)
打开一个可用的RDMA设备，返回其上下文指针（这个指针会在以后用来对这个设备进行各种操作）。

device_attr, errno ibv_query_device(device_context*)
查询一个设备的属性/能力，比如其支持的最大QP，CQ数量等。返回设备的属性结构体指针，以及错误码。

资源的创建，查询，修改和销毁：

pd ibv_alloc_pd(device_context)
申请PD。该函数会返回一个PD的指针。(PD(内存)保护域--见:【RDMA】技术详解（三）：理解RDMA Scatter Gather List|聚散表_bandaoyu的笔记-CSDN博客）

mr ibv_reg_mr(pd, addr, length, access_flag)
注册MR。用户传入要注册的内存的起始地址和长度，以及这个MR将要从属的PD和它的访问权限（本地读/写，远端读/写等），返回一个MR的指针给用户。

cq ibv_create_cq(device_context, cqe_depth, ...)
创建CQ。用户传入CQ的最小深度（驱动实际申请的可能比这个值大），然后该函数返回CQ的指针。

qp ibv_create_qp(pd, qp_init_attr)
创建QP。用户传入PD和一组属性（包括RQ和SQ绑定到的CQ、QP绑定的SRQ、QP的能力、QP类型等），向用户返回QP的指针。（SRQ=shared receive queue）

errno ibv_modiy_qp(qp, attr, attr_mask)
修改QP。用户传入QP的指针，以及表示要修改的属性的掩码和要修改值。修改的内容可以是QP状态、对端QPN(QP的序号)、QP能力、端口号和重传次数等等。如果失败，该函数会返回错误码。
Modify QP最重要的作用是让QP在不同的状态间迁移，完成RST-->INIT-->RTR-->RTS的状态机转移后才具备下发Send WR的能力。也可用来将QP切换到ERROR状态。

errno ibv_destroy_qp(qp)
销毁QP。即销毁QP相关的软件资源。其他的资源也都有类似的销毁接口。

中断处理：

event_info, errno ibv_get_async_event(device_context)
从事件队列中获取一个异步事件，返回异步事件的信息（事件来源，事件类型等）以及错误码。

连接管理

rdma_xxx()
用于CM建链，不在本文展开讲。

...
————————————————
版权声明：本文为CSDN博主「bandaoyu」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：<https://blog.csdn.net/bandaoyu/article/details/112860396>

数据面IO接口

2）数据面：

RDMA 基础
<https://zhuanlan.zhihu.com/p/567711325>
![OFED](../images/RDMA数据面流程.jpg)

下发WR

bad_wr, errno ibv_post_send(qp, wr)
向一个QP下发一个Send WR，参数wr是一个结构体，包含了WR的所有信息。包括wr_id、sge数量、操作码（SEND/WRITE/READ等以及更细分的类型）。

WR的结构会根据服务类型和操作类型有所差异，比如RC服务的WRITE和READ操作的WR会包含远端内存地址和R_Key，UD服务类型会包含AH，远端QPN和Q_Key等。

WR经由驱动进一步处理后，会转化成WQE下发给硬件。

出错之后，该接口会返回出错的WR的指针以及错误码。

bad_wr, errno ibv_post_recv(qp, wr)
同ibv_post_send，只不过是专门用来下发RECV操作WR的接口。

获取WC

num, wc ibv_poll_cq(cq, max_num)
从完成队列CQ中轮询CQE，用户需要提前准备好内存来存放WC，并传入可以接收多少个WC。该接口会返回一组WC结构体（其内容包括wr_id，状态，操作码，QPN等信息）以及WC的数量。
————————————————
版权声明：本文为CSDN博主「bandaoyu」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：<https://blog.csdn.net/bandaoyu/article/details/112860396>

# 事件

IBV 事件

IBV WC 事件

RDMA_CM 事件

# RDMA编程示例

RDMA编程流程
<https://blog.csdn.net/bandaoyu/article/details/112860396>

![rdma编程流程](../images/rdma-编程流程.jpg)

# 内核verbs编程和用户verbs编程

Linux内核rdma子系统

内核verbs接口头文件：include/rdma/ib_verbs.h
包含：ib_create_qp ib_post_send等

include/rdma/rdma_cm.h
包含：rdma_connect等接口

在内核中使用rdma子系统，需包含上面头文件，比如nvme host驱动，会调用上面接口

<https://www.kernel.org/doc/html/latest/translations/zh_CN/infiniband/user_verbs.html>

用户空间通过/dev/infiniband/uverbsN字符设备与内核进行慢速路径、资源管理 操作的通信。
快速路径操作通常是通过直接写入硬件寄存器mmap()到用户空间来完成 的，没有系统调用或上下文切换到内核。

命令是通过在这些设备文件上的write()s发送给内核的。ABI在 drivers/infiniband/include/ib_user_verbs.h中定义。
需要内核响应的命令的结 构包含一个64位字段，用来传递一个指向输出缓冲区的指针。状态作为write()系统调 用的返回值被返回到用户空间。

rdma-core的函数已经封装了跟内核交互的ioctl，所以上层perftest只需要调用接口就行

# 整体框架图

对比分析mellanox、intel e810（沐创）等驱动实现方式

## 硬件接口

数据交互DMA，控制接口PCIe bar空间定义

## 软件框架

用户态驱动

内核态RDMA驱动

设备驱动

各个驱动模块组成和模块间接口

# 流程分析

## 驱动初始化

probe流程分析，设备识别，核心数据结构初始化

## 建链断链

## 收发数据

## 内存管理

## 错误和异常事件处理

## 网卡管理

# 数据结构关系

rdma_device, ib_device, ib_cq, ib_qp ib_context等
