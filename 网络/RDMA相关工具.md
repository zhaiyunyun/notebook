
# 相关工具

root@localhost ~]# ibdev2netdev
mlx5_0 port 1 ==> ens2f0 (Up)
mlx5_1 port 1 ==> ens2f1 (Up)

[root@localhost ~]# ip addr

6: ens2f0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether b8:ce:f6:7f:3d:48 brd ff:ff:ff:ff:ff:ff
    inet 192.168.9.9/24 brd 192.168.9.255 scope global noprefixroute ens2f0
       valid_lft forever preferred_lft forever
    inet6 fe80::bace:f6ff:fe7f:3d48/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
7: ens2f1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether b8:ce:f6:7f:3d:49 brd ff:ff:ff:ff:ff:ff
    inet 192.168.8.9/24 brd 192.168.8.255 scope global noprefixroute ens2f1
       valid_lft forever preferred_lft forever
    inet6 fe80::bace:f6ff:fe7f:3d49/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

通过ip addr命令查看网卡对应的ip地址

在其中一台服务器（充当rping服务器）上运行以下命令

rping -s -C 10 -v

在其中一台服务器（充当rping客户端）上运行以下命令

```bash
rping -c -a 192.168.9.9 -C 10 -v

[root@localhost ~]# rping -c -a 192.168.9.9 -C 10 -v
ping data: rdma-ping-0: ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqr
ping data: rdma-ping-1: BCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrs
ping data: rdma-ping-2: CDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrst
ping data: rdma-ping-3: DEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstu
ping data: rdma-ping-4: EFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuv
ping data: rdma-ping-5: FGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvw
ping data: rdma-ping-6: GHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwx
ping data: rdma-ping-7: HIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxy
ping data: rdma-ping-8: IJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz
ping data: rdma-ping-9: JKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyzA
client DISCONNECT EVENT...
[root@localhost ~]#

[root@localhost ~]# ibv_rc_pingpong -d mlx5_0 -g 1
  local address:  LID 0x0000, QPN 0x000056, PSN 0x85ea3e, GID fe80::bace:f6ff:fe7f:3d48
  remote address: LID 0x0000, QPN 0x00014c, PSN 0x86ba3c, GID fe80::bace:f6ff:fe7f:3d49
8192000 bytes in 0.01 seconds = 11601.35 Mbit/sec
1000 iters in 0.01 seconds = 5.65 usec/iter
[root@localhost ~]#

[root@localhost ~]# ibv_rc_pingpong -d mlx5_1 -g 1 192.168.9.9
  local address:  LID 0x0000, QPN 0x00014c, PSN 0x86ba3c, GID fe80::bace:f6ff:fe7f:3d49
  remote address: LID 0x0000, QPN 0x000056, PSN 0x85ea3e, GID fe80::bace:f6ff:fe7f:3d48
8192000 bytes in 0.01 seconds = 12302.61 Mbit/sec
1000 iters in 0.01 seconds = 5.33 usec/iter
[root@localhost ~]#
```

<https://github.com/SoftRoCE/rxe-dev/issues/79>

ib规范定义的verbs IB Specification Release
RDMA之用户态与内核态交互 - Savir的文章 - 知乎
<https://zhuanlan.zhihu.com/p/346708569>

CM建立链接

<https://zhuanlan.zhihu.com/p/494826608>

Server端执行：

rdma_server
Client端执行：

rdma_client -s 192.168.217.128

Server端执行：

udaddy
Client端执行：

udaddy -s 192.168.217.128

# 抓包

RDMA抓包
ibdump
嗅探RDMA流量（抓包RDMA）非常棘手，因为一旦两端完成了初始握手，数据便会不经过内核协议栈通过网卡（HCA）直接到达内存。除了在网络上放置专用硬件嗅探器来抓包，剩下的唯一方法就是在网卡内放置有网卡商的hook接口，然后网卡商提供使用这些接口的 软件工具。

例如：Mellanox HCA（网卡）的ibdump，This tool is also a part of Mellanox OFED package.

查询链路模式
mlxconfig -d mlx5_0 -e q |grep -i link_type
修改ROCE V2模式
mlxconfig -d mlx5_3 s LINK_TYPE_P2=2

关闭防火墙
sudo firewall-cmd --state
systemctl stop firewalld

固件升级
mlxfwmanager --query 固件信息查询
mlxfwmanager -d mlx5_1 --online -u  固件在线升级

监听端口
netstat -nlp |grep LISTEN

H3CLinux安装ofed驱动
./mlnxofedinstall --skip-distro-check --add-kernel-support --distro openeuler22.03 --skip-repo --with-nvmf -vvv

tcpdump抓包
tcpdump -i ens16f0np0 -w test.cap

bw测试
131  ifconfig ens16f0np0 192.168.10.2
131: ib_write_bw -d mlx5_0 --duration=10 --rdma_cm --report_gbits --qp=1 -F

125   ifconfig ens16f0np0 192.168.10.3
125: ib_write_bw -d mlx5_0 --duration=10 --rdma_cm --report_gbits --qp=1 -F 192.168.10.2

spdk日志
./scripts/rpc.py -s /var/tmp/spdk_zyy.sock log_set_level "DEBUG"      # 这里最低只能设置"ERROR"
./scripts/rpc.py -s /var/tmp/spdk_zyy.sock log_get_level               # 显示当前log等级


./scripts/rpc.py -s /var/tmp/spdk_zyy.sock log_get_print_level

./scripts/rpc.py -s /var/tmp/spdk_zyy.sock log_set_print_level "DEBUG"
./scripts/rpc.py -s /var/tmp/spdk_zyy.sock log_set_flag nvmf
./scripts/rpc.py -s /var/tmp/spdk_zyy.sock log_set_flag nvme
./scripts/rpc.py -s /var/tmp/spdk_zyy.sock log_set_flag rdma


# 开启大页内存
./scripts/setup.sh status
cat /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages
echo 0 > /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages
echo 0 > /sys/kernel/mm/hugepages/hugepages-1048576kB/nr_hugepages


ulimit -l unlimited
mount -t hugetlbfs hugetlbfs /mnt/huge/huge-1048576kB -o pagesize=1048576kB
HUGEPGSZ=1048576 HUGENODE='nodes_hp[0]=25,nodes_hp[7]=25' ./scripts/setup.sh

HUGEPGSZ=1048576 HUGENODE='nodes_hp[3]=15,nodes_hp[4]=15' ./scripts/setup.sh
mount -t hugetlbfs hugetlbfs /mnt/huge/huge-1048576kB -o pagesize=1048576kB

rdma-core编译安装
https://runsisi.com/2021/03/07/rdma-core/

export ROOT="/root/zyy/rdma-core-42.1"
export BUILD_DIR=$ROOT/build
export BIN=$BUILD_DIR/bin
export LIB=$BUILD_DIR/lib
export INCLUDE=$BUILD_DIR/include
export PYTHONPATH=$BUILD_DIR/python:$BUILD_DIR/python/pyverbs
export LD_LIBRARY_PATH=$LIB
export LIBRARY_PATH=$LIB
export C_INCLUDE_PATH=$INCLUDE
# export PATH=$PATH:$BIN:$PATH


# spdk编译使用
./configure --with-rdma --without-isal --enable-debug --disable-unit-tests
make -j32


./build/bin/nvmf_tgt --rpc-socket=/var/tmp/spdk_zyy.sock --huge-dir=/mnt/huge/huge-1048576kB --logflag=all --cpumask=[40,41,42,43] -e 0xFFFF

./build/bin/nvmf_tgt --rpc-socket=/var/tmp/spdk_zyy.sock --huge-dir=/mnt/huge/huge-1048576kB --cpumask=[40,41,42,43]


./build/bin/nvmf_tgt_server --rpc-socket=/var/tmp/spdk_zyy_server.sock --huge-dir=/mnt/huge/huge-1048576kB --cpumask=[40,41,42,43]
./build/bin/nvmf_tgt --rpc-socket=/var/tmp/spdk_zyy_client.sock --huge-dir=/mnt/huge/huge-1048576kB --cpumask=[44,45,46,47]

./scripts/rpc.py -s /var/tmp/spdk_zyy.sock log_set_print_level "DEBUG"
./scripts/rpc.py -s /var/tmp/spdk_zyy.sock log_set_flag nvmf
./scripts/rpc.py -s /var/tmp/spdk_zyy.sock log_set_flag nvme
./scripts/rpc.py -s /var/tmp/spdk_zyy.sock log_set_flag rdma

./scripts/rpc.py -s /var/tmp/spdk_zyy_client.sock log_set_print_level "DEBUG"
./scripts/rpc.py -s /var/tmp/spdk_zyy_client.sock log_set_flag cache_mirror

scripts/rpc.py -s /var/tmp/spdk_zyy.sock nvmf_create_transport -t RDMA -u 8192 -i 131072 -c 8192
scripts/rpc.py -s /var/tmp/spdk_zyy.sock bdev_malloc_create -b Malloc0 512 512
scripts/rpc.py -s /var/tmp/spdk_zyy.sock nvmf_create_subsystem nqn.2016-06.io.spdk:cnode1 -a -s SPDK00000000000001 -d SPDK_Controller1
scripts/rpc.py -s /var/tmp/spdk_zyy.sock nvmf_subsystem_add_ns nqn.2016-06.io.spdk:cnode1 Malloc0
scripts/rpc.py -s /var/tmp/spdk_zyy.sock nvmf_subsystem_add_listener nqn.2016-06.io.spdk:cnode1 -t rdma -a 192.168.1.20 -s 4420

./scripts/rpc.py -s /var/tmp/spdk_zyy.sock cache_mirror_eable --cache_size=10 --server=1 --port=34567
./scripts/rpc.py -s /var/tmp/spdk_zyy_client.sock cache_mirror_eable --cache_size=10 --server=0 --server_ip=192.168.1.20 --port=34567

./scripts/rpc.py -s /var/tmp/spdk_zyy.sock cache_mirror_disable
./scripts/rpc.py -s /var/tmp/spdk_zyy.sock cache_mirror_get_stats

./scripts/rpc.py -s /var/tmp/spdk_zyy_client.sock cache_mirror_dump_mem --addr=35682224177152 --len=16

nvme disconnect -n "nqn.2016-06.io.spdk:cnode1"
nvme discover -t rdma -a 192.168.1.20 -s 4420
nvme connect -t rdma -n "nqn.2016-06.io.spdk:cnode1" -a 192.168.1.20 -s 4420
nvme disconnect -n "nqn.2016-06.io.spdk:cnode1"


ifconfig ens16f0np0 192.168.1.10
ifconfig ens16f1np1 192.168.1.11

git config --global http.proxy http://z29721:liuxu.0123@proxy.h3c.com:8080
git config --global https.proxy https://z29721:liuxu.0123@proxy.h3c.com:8080

