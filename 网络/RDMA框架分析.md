# 参考

RDMA概述 <https://zhuanlan.zhihu.com/p/138874738>  
智能网卡 <https://blog.csdn.net/qq_34667436/category_11742256.html>

# RDMA远程直接地址访问

![无DMA](../images/no-DMA.jpg)

用户内存-内核地址-IO设备
cpu全程参与数据拷贝

![有DMA](../images/DMA.jpg)

用户内存-DMA控制器-IO设备空间
CPU只参与开始和结束

![RDMA](../images/RDMA.jpg)

用户内存-RDMA网卡空间-对端用户空间

优点：

1. 0拷贝，不需要在用户空间和内核空间中来回复制数据
2. 内核Bypass：IO（数据）流程可以绕过内核，避免了系统调用和上下文切换的开销
3. CPU卸载：指的是可以在远端节点CPU不参与通信的情况下（当然要持有访问远端某段内存的“钥匙”才行）对内存进行读写，这实际上是把报文封装和解析放到硬件中做了

# 协议

RDMA本身指的是一种技术，具体协议层面，包含Infiniband（IB），RDMA over Converged Ethernet（RoCE）和internet Wide Area RDMA Protocol（iWARP）。三种协议都符合RDMA标准，使用相同的上层接口，在不同层次上有一些差别。

![RDMA-protocol](../images/RDMA-protocol.jpg)

## IB

IBTA（InfiniBand Trade Association）提出的IB协议,其规定了一整套完整的链路层到传输层（非传统OSI七层模型的传输层，而是位于其之上）规范，无法兼容以太网，需要独立IB网卡和专用交换设备

## RoCE

基于以太网链路层协议，实现了IB传输层协议

## iWARP

互联网广域RDMA协议

# 组织

**IBTA**：IBTA（InfiniBand Trade Association）负责制定和维护Infiniband协议标准

**OFA**：OFA（Open Fabric Alliance），负责开发、测试、认证、支持和分发独立于厂商的开源跨平台infiniband协议栈，其对用于支撑RDMA/Kernel bypass应用的OFED（OpenFabrics Enterprise Distribution）软件栈负责，保证其与主流软硬件的兼容性和易用性。OFED软件栈包括驱动、内核、中间件和API。
OFA提供了RDMA传输的一系列Verbs API。OFA开发出了OFED（Open Fabric Enterprise Distribution）协议栈，支持多种RDMA传输层协议。

上述两个组织是配合关系
IBTA主要负责开发、维护和增强Infiniband协议标准；
OFA负责开发和维护Infiniband协议和上层应用API。

# 开源包

RDMA之Verbs - Savir的文章 - 知乎
<https://zhuanlan.zhihu.com/p/329198771>

**rdma-core**

指开源RDMA用户态软件协议栈，包含用户态框架、各厂商用户态驱动、API帮助手册以及开发自测试工具等。
rdma-core在github上维护，我们的用户态Verbs API实际上就是它实现的。

代码仓库：<https://github.com/linux-rdma/rdma-core>

**源码编译安装**后生成的可执行程序和动态库

```bash
zyy@ubuntu:~/open-src/rdma-core-42.0/build/bin$ ls
check_lft_balance.pl  ibcacheedit          ibnodes        ibstatus         ibv_srq_pingpong   rcopy         rstream     udaddy
cmtime                ibccconfig           ibping         ibswitches       ibv_uc_pingpong    rdma_client   saquery     udpong
dump_fts              ibccquery            ibportstate    ibsysstat        ibv_ud_pingpong    rdma_rename   sminfo      umad_compile_test
dump_lfts.sh          ibfindnodesusing.pl  ibqueryerrors  ibtracert        ibv_xsrq_pingpong  rdma_server   smpdump     umad_reg2
dump_mfts.sh          ibhosts              ibroute        ibv_asyncwatch   iwpmd              rdma_xclient  smpquery    umad_register2
ibacm                 ibidsverify.pl       ibrouters      ibv_devices      mckey              rdma_xserver  srp_daemon  umad_sa_mcm_rereg_test
ib_acme               iblinkinfo           ibsendtrap     ibv_devinfo      mcm_rereg_test     riostream     testleaks   vendstat
ibaddr                ibnetdiscover        ibstat         ibv_rc_pingpong  perfquery          rping         ucmatose
zyy@ubuntu:~/open-src/rdma-core-42.0/build/bin$ 

zyy@ubuntu:~/open-src/rdma-core-42.0/build/lib$ ls
ibacm                    libibacmp.so              libibumad.so.3.2.42.0     libmlx4.so.1.0.42.0   librdmacm.so.1
libbnxt_re-rdmav34.so    libibmad.so               libibverbs.so             libmlx5-rdmav34.so    librdmacm.so.1.3.42.0
libcxgb4-rdmav34.so      libibmad.so.5             libibverbs.so.1           libmlx5.so            librspreload.so
libefa-rdmav34.so        libibmad.so.5.3.42.0      libibverbs.so.1.14.42.0   libmlx5.so.1          librxe-rdmav34.so
libefa.so                libibnetdisc.so           libipathverbs-rdmav34.so  libmlx5.so.1.24.42.0  libsiw-rdmav34.so
libefa.so.1              libibnetdisc.so.5         libirdma-rdmav34.so       libmthca-rdmav34.so   libvmw_pvrdma-rdmav34.so
libefa.so.1.1.42.0       libibnetdisc.so.5.0.42.0  libmlx4-rdmav34.so        libocrdma-rdmav34.so  pkgconfig
libhfi1verbs-rdmav34.so  libibumad.so              libmlx4.so                libqedr-rdmav34.so
libhns-rdmav34.so        libibumad.s

```

**apt安装**：sudo apt-get install libibverbs1 ibverbs-utils librdmacm1 libibumad3 ibverbs-providers rdma-core
通过dpkg -L rdma-core可以查看软件内容，比如
zyy@ubuntu:~$ dpkg -L ibverbs-utils
/.
/usr
/usr/bin
/usr/bin/ibv_asyncwatch
/usr/bin/ibv_devices
/usr/bin/ibv_devinfo
/usr/bin/ibv_rc_pingpong
/usr/bin/ibv_srq_pingpong
/usr/bin/ibv_uc_pingpong
/usr/bin/ibv_ud_pingpong
/usr/bin/ibv_xsrq_pingpong

**kernel RDMA subsystem**

指开源的Linux内核中的RDMA子系统，包含RDMA内核框架及各厂商的驱动。

RDMA子系统跟随Linux维护，是内核的的一部分。一方面提供内核态的Verbs API，一方面负责对接用户态的接口。

**开源OFED**

全称为OpenFabrics Enterprise Distribution，是一个开源软件包集合，
其中包含内核框架和驱动、用户框架和驱动、以及各种中间件、测试工具和API文档。
它会定期从rdma-core和内核的RDMA子系统取软件版本，并对各商用OS发行版进行适配。除了协议栈和驱动外，还包含了perftest等测试工具。
![OFED](../images/OFED.jpg)

下载地址：<https://www.openfabrics.org/downloads/OFED/>

**厂家OFED**
除了开源OFED之外，各厂商也会提供定制版本的OFED软件包，

比如华为的HW_OFED和Mellanox的MLNX_OFED。这些定制版本基于开源OFED开发，由厂商自己测试和维护，会在开源软件包基础上提供私有的增强特性，并附上自己的配置、测试工具等。

MLX OFED网卡驱动下载：<https://network.nvidia.com/products/infiniband-drivers/linux/mlnx_ofed/>
驱动文档：<https://docs.nvidia.com/networking/spaces/viewspace.action?key=MLNXOFEDv571020>
Ubuntu依赖：<https://docs.nvidia.com/networking/display/MLNXOFEDv571020/General+Support#GeneralSupport-HardwareandSoftwareRequirements>
通过 sudo ./mlnxofedinstall --check-deps-only 检查

```bash
# 查看将安装所有软件包列表：
zyy@ubuntu:~/open-src/MLNX_OFED_LINUX-5.7-1.0.2.0-ubuntu20.04-x86_64$ sudo ./mlnxofedinstall -p
MLNX_OFED packages: ofed-scripts mlnx-tools mlnx-ofed-kernel-utils 等等
# 或者看配置文件查看软件包列表：
zyy@ubuntu:~/open-src/MLNX_OFED_LINUX-5.7-1.0.2.0-ubuntu20.04-x86_64/docs/conf$ ls
ofed-all.conf  ofed-basic.conf  ofed-hpc.conf  ofed_net.conf-example  ofed-vma.conf  ofed-vmaeth.conf  ofed-vmavpi.conf
# 软件包信息：DEBS下的package文档
```

![MLX驱动Stack Architecture](../images/MLNX-OFED-stack.png)

包含的模块

- mlx5
  - mlx5_ib
  - mlx5_core (includes Ethernet)
- Mid-layer core
  - Verbs, MADs, SA, CM, CMA, uVerbs, uMADs
- Upper Layer Protocols (ULPs)
  - IPoIB, SRP Initiator and SRP
- MPI
  - Open MPI stack supporting the InfiniBand, RoCE and Ethernet interfaces
  - MPI benchmark tests (OSU BW/LAT, Intel MPI Benchmark, Presta)
- OpenSM: InfiniBand Subnet Manager
- Utilities
  - Diagnostic tools
  - Performance tests
  - Sysinfo (see Sysinfo User Manual)
- Firmware tools (MFT)
- Source code for all the OFED software modules (for use under the conditions mentioned in the modules' LICENSE files)
- Documentation

# 全景图

![](../images/OFED-xmind.jpg)

广义的Verbs API主要由两大部分组成**IB_VERBS**和**RDMA_CM**：

rdma_cm 和ib_verbs配合使用，rdma_cm主要用于管理连接（建立和断开）、vbers用于管理数据的收发。

## IB_VERBS

接口以*ibv_xx*（用户态）或者*ib_xx*（内核态）作为前缀，是最基础的编程接口，使用IB_VERBS就足够编写RDMA应用了。

![verbs-api](../images/verbs-api.jpg)

代码目录：rdma-core/libibverbs
头文件：infiniband/verbs.h

## RDMA_CM

![rdma-cm-api](../images/rdma-cm-api.jpg)

代码目录：rdma-core/librdmacm
头文件：
rdma/rdma_cma.h // RDMA_CM CMA 头文件 用于CM建链
rdma/rdma_verbs.h> // RDMA_CM VERBS 头文件 用于使用基于CM的Verbs接口

接口以*rdma_xx*为前缀，主要分为两个功能：

一：CMA（Connection Management Abstraction）：建连连接管理

在Socket和Verbs API基础上实现的，用于CM建链并交换信息的一组接口。CM建链是在Socket基础上封装为QP实现，从用户的角度来看，是在通过QP交换之后数据交换所需要的QPN，Key等信息。

二：CM VERBS：收发数据

RDMA_CM也可以用于数据交换 (收发数据），相当于在verbs API上又封装了一套数据交换接口。

**MAD层**：MAD全称为Management Datagram，即管理数据报，其实MAD层就是基于IB传输层的UD服务类型实现的。也就是说，它在UD服务类型的报文的基础上，利用其Payload部分又实现了一层协议。
**MPA层**：MPA层的全称是Marker PDU Aligned framing，它是iWARP协议族的最底层，向下基于TCP层，向上为DDP层提供服务。

版权声明：本文为CSDN博主「bandaoyu」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：<https://blog.csdn.net/bandaoyu/article/details/112860396>

# RDMA软件协议栈

RDMA之用户态与内核态交互 - Savir的文章 - 知乎
<https://zhuanlan.zhihu.com/p/346708569>

![RDMA-architecture](../images/RDMA-architecture.jpg)

首先从上往下看：

**用户空间**

- Application
各种RDMA应用程序（比如perftest）、中间件（比如UCX）等。
- libibverbs.so
RDMA软件栈用户态核心动态链接库，作用：

1. 实现并且向上层应用提供各种Verbs API
2. 在各种Verbs API的逻辑中调用到各厂商驱动注册的钩子函数
3. 提供进入内核态的接口

- libmlx5.so
Mellanox ConnectX-5网卡的用户态驱动，也是个动态链接库，实现厂商的驱动逻辑。

**内核**

- 中间交互模块
  负责通过ABI来处理用户态的系统调用请求，用户态verbs陷入内核，需要通过这一层的ib_uverbs模块来解析命令；另外右侧的xxx.ko指的是使用内核verbs接口（比如ib_post_send）的上层应用程序所需要的处理系统调用的自定义模块。
- ib_core.ko
内核RDMA子系统核心模块，作用：

1. 向使用内核态Verbs的应用程序提供内核态Verbs API
2. 在各种Verbs API的逻辑中调用到各厂商驱动注册的钩子函数
3. 管理各种RDMA资源，为用户态提供服务

- mlx5_ib.ko
Mellanox ConnectX-5网卡的内核态驱动模块，负责直接和硬件交互。
- 硬件
指Mellanox ConnectX-5网卡。

**rdma-core代码目录介绍**

- libibverbs：ibv_*等可执行程序和 libibverbs.so动态链接库
  - examples，生成ibv_devices，ibv_devinfo等ibv_*可执行程序
  - man，接口文档
  - libibverbs.map.in，导出符号
- librdmacm：rdma_*等可执行程序 librdmacm.so 和 librspreload.so
  - example，rdma_*等可执行程序，具体看cmakefile
  - man：接口文档
  - librdmacm.map: 导出符号
  - librspreload.map：导出符号
- libibumad: mad层，libibumad.so
  - libibumad.map: 导出符号
- infiniband-diags：在mad层上提供的工具
- providers：各厂家用户态驱动
  - mlx5：libmlx5.so mellanox网卡用户态驱动
    - libmlx5.map：导出符号
    - man：接口文档
- kernel-headers：内核头文件，需要与内核源码include/uapi下一致
- debian: 通过apt安装的配置
  - control：定义apt安装的各个package，比如rdma-core，ibverbs-providers，ibverbs-utils等
  - *.install: 各个package安装的文件，可以通过dpkg -L rdma-core查看

**RDMA内核代码目录**：
drivers\infiniband\core: ib_core, ib_cm, iw_cm, infiniband, ib_umand, ib_uverbs, rdma_ucm
drivers\infiniband\hw\mlx5: mlx5_ib
drivers\net\ethernet\mellanox: mlx5_core

## 与spdk关系

scripts/rpc.py nvmf_create_transport -h

spdk中直接调用rdma-core中的verbs接口来支持RDMA, 而不是通过DPDK来使用
lib/nvmf/rdma.c有
nvmf_rdma_qpair_from_wc
test/unit/lib/nvmf/transport.c 有rdma的配置
struct spdk_nvmf_transport_opts g_rdma_ut_transport_opts = {
 .max_queue_depth = SPDK_NVMF_RDMA_DEFAULT_MAX_QUEUE_DEPTH,
 .max_qpairs_per_ctrlr = SPDK_NVMF_RDMA_DEFAULT_MAX_QPAIRS_PER_CTRLR,
 .in_capsule_data_size = SPDK_NVMF_RDMA_DEFAULT_IN_CAPSULE_DATA_SIZE,
 .max_io_size = (SPDK_NVMF_RDMA_MIN_IO_BUFFER_SIZE * RDMA_UT_UNITS_IN_MAX_IO),
 .io_unit_size = SPDK_NVMF_RDMA_MIN_IO_BUFFER_SIZE,
 .max_aq_depth = SPDK_NVMF_RDMA_DEFAULT_AQ_DEPTH,
 .num_shared_buffers = SPDK_NVMF_RDMA_DEFAULT_NUM_SHARED_BUFFERS,
 .opts_size = sizeof(g_rdma_ut_transport_opts)
};

nvmf_rdma_create(struct spdk_nvmf_transport_opts *opts) 创建rdma

以spdk_rdma开头的函数，对verbs重新封装
{
 global:

 spdk_rdma_srq_create;
 spdk_rdma_srq_destroy;
 spdk_rdma_srq_queue_recv_wrs;
 spdk_rdma_srq_flush_recv_wrs;
 spdk_rdma_qp_create;
 spdk_rdma_qp_accept;
 spdk_rdma_qp_complete_connect;
 spdk_rdma_qp_destroy;
 spdk_rdma_qp_disconnect;
 spdk_rdma_qp_queue_send_wrs;
 spdk_rdma_qp_flush_send_wrs;
 spdk_rdma_create_mem_map;
 spdk_rdma_free_mem_map;
 spdk_rdma_get_translation;
 spdk_rdma_qp_queue_recv_wrs;
 spdk_rdma_qp_flush_recv_wrs;

 local: *;
};

## 与dpdk关系

dpdk中的Poll Mode Driver驱动模块：librte_common_mlx5和librte_net_mlx5
依赖模块：libibverbs libmlx5, 内核中的mlx5_core, mlx5_ib, ib_uverbs
代码目录：
drivers/common/mlx5
drivers/common/net/mlx5
文档：
<https://doc.dpdk.org/guides/nics/mlx5.html>
<https://doc.dpdk.org/guides/platform/mlx5.html>

————————————————
版权声明：本文为CSDN博主「bandaoyu」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：<https://blog.csdn.net/bandaoyu/article/details/113125473>
