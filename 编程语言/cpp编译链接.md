# 一：软件包介绍

glibc : libc, ld.so, ldd, ldconfig 等

binutils : ar, as, ld ,nm, objcopy, objdump, readelf, size, strip

gcc : c++, cc, cpp, libgcc, libstdc++, gcc, gccbug, gcc 相关

# 二：gcc 头文件搜索目录

打印具体编译器程序位置
gcc -print-prog-name=cc1 # 打印c编译器，c++换成 cc1plus

c 头文件内置搜索目录：`gcc -print-prog-name=cc1` -v   !!!!注意：不是单引号，是 tab 按键上面的按键

c++头文件内置搜索目录：`gcc --print-prog-name=cpp` -v

寻找策略：

1：从-I 开始

2：从环境变量 C_INCLUDE_PATH 等

3：内定目录（使用-nostdinc 关闭默认路径）

# 二：连接器搜索库目录：ld --verbose | grep SEARCH

寻找策略：

1：从-L 开始

2：环境变量 LIBRARY_PATH

3：内定目录（使用-nostdlib 关闭默认库）

打印连接库的具体路径

```bash
gcc -print-file-name=libc.a
/usr/lib/gcc/x86_64-linux-gnu/4.6/../../../x86_64-linux-gnu/libc.a
```

# 三：gcc 预处理器 cpp 的内置宏

```bash
gcc -dM -E - < /dev/null

gcc -posix -E -dM - </dev/null 或者 cpp -dM < /dev/null
```

注意：不包含**LINE**这些。

# 四：运行时动态链接库查找目录

1：编译时指定

2：LD_LIBRARY_PATH

3：/etc/ld.so.conf 指定

4：默认动态库查找路径/lib, /usr/lib.....

# 五：gcc 环境变量

C_INCLUDE_PATH：c 程序查找头文件

CPATH：c/c++/obj-c 头文件查找

CPLUS_INCLUDE_PATH：c++头文件查找

LIBRARY_PATH：连接器 ld 查找库文件

LANG：字符集

LD_LIBRARY_PATH：运行时查找动态库

# 六：ld 标准连接器设置入口地址方法

1：ld 命令行-e 选项

2：链接脚本 ENTRY()命令

3：如定义 start 符号，使用 start

4：存在 text section 使用 text section

5：使用 0 值

ld --vervose 内置连接器脚本
