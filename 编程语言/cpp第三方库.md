使用到的第三方库

通信：
zeromq/4.3.4_linux
libzmq/3.2.5_linux
fast-dds/2.1.1_linux 通信中间件，类似ros

日志
glog/0.3.3_linux
fmt/5.3.0_linux 字符串格式化
hlog/0.3.11_linux

单元测试

数据格式
yaml-cpp/0.6.1_linux
jsoncpp
protobuf/2.6.1_linux

矩阵计算
eigen/3.3.4_linux

pack-sdk/0.1.42_linux

# protobuff

类似xml，json，数据交换格式，二进制格式
优点是效率高，占用空间小

流程

1. 编写.proto文件
2. 编译.proto，生成h和cpp
3. 业务代码
一，编写.proto文件

```
message Example1 {
    optional string stringVal = 1;
    optional bytes bytesVal = 2;
    message EmbeddedMessage {
        optional int32 int32Val = 1;
        optional string stringVal = 2;
    }
    optional EmbeddedMessage embeddedExample1 = 3;
    repeated int32 repeatedInt32Val = 4;
    repeated string repeatedStringVal = 5;
}
```

二：编译.proto，生成h和cpp
protoc -I=$SRC_DIR $SRC_DIR/xxx.proto --cpp_out=$DST_DIR

$SRC_DIR 表示 .proto文件所在的源目录；
$DST_DIR 表示生成目标语言代码的目标目录；
xxx.proto 表示要对哪个.proto文件进行解析；
--cpp_out 表示生成C++代码。

三：使用

```c++
//
// Created by yue on 18-7-21.
//
#include <iostream>
#include <fstream>
#include <string>
#include "test.pb.h"

int main() {
    Example1 example1;
    example1.set_stringval("hello,world");
    example1.set_bytesval("are you ok?");

    Example1_EmbeddedMessage *embeddedExample2 = new Example1_EmbeddedMessage();

    embeddedExample2->set_int32val(1);
    embeddedExample2->set_stringval("embeddedInfo");
    example1.set_allocated_embeddedexample1(embeddedExample2);

    example1.add_repeatedint32val(2);
    example1.add_repeatedint32val(3);
    example1.add_repeatedstringval("repeated1");
    example1.add_repeatedstringval("repeated2");

    std::string filename = "single_length_delimited_all_example1_val_result";
    std::fstream output(filename, std::ios::out | std::ios::trunc | std::ios::binary);
    if (!example1.SerializeToOstream(&output)) {
        std::cerr << "Failed to write example1." << std::endl;
        exit(-1);
    }

    return 0;
}
```

g++ test.cc test.pb.cc -o test --std=c++11 -I /home/zyy-1604/.gradle/caches/modules-2/files-2.1/com.hobot.native.hobot-adas.stable.3rd-party/protobuf/2.6.1_linux_ubuntu_gcc5.4.0/files/protobuf/include -L /home/zyy-1604/.gradle/caches/modules-2/files-2.1/com.hobot.native.hobot-adas.stable.3rd-party/protobuf/2.6.1_linux_ubuntu_gcc5.4.0/files/protobuf/lib -lprotobuf

# zeromq

通信中间件，类似ros

<https://www.cnblogs.com/leijiangtao/p/12016200.html>

zeromq源码分析笔记之架构（1） <https://www.cnblogs.com/zengzy/p/5122634.html>

如果我们使用so_type+ip地址+端口号实例一个socket

当调用listen后，内核就会建立两个队列，一个SYN队列，表示接受到请求，但未完成三次握手的连接；另一个是ACCEPT队列，表示已经完成了三次握手的队列

api <http://api.zeromq.org/4-3:_start>

ZeroMQ通过各种传输（TCP，进程内，进程间，多播，WebSocket等）支持通用消息传递模式（发布/订阅，请求/答复，客户端/服务器等），使进程间消息传递变得简单作为线程间消息传递。这样可以使您的代码清晰，模块化并且易于扩展。

ZeroMQ将消息通信分成4种模型，分别是一对一结对模型（Exclusive-Pair）、请求回应模型（Request-Reply）、发布订阅模型（Publish-Subscribe）、推拉模型（Push-Pull）。

Request-reply，它将一组客户端连接到一组服务。这是一个远程过程调用和任务分配模式。
Pub-sub，它将一组发布者连接到一组订阅者。这是一种数据分发模式。
Pipeline，以扇出/扇入模式连接节点，该模式可以具有多个步骤和循环。这是并行的任务分配和收集模式。
Exclusive pair, 专门连接两个Socket。这是用于在一个进程中连接两个线程的模式。

int zmq_bind (void *socket, const char*endpoint);

**通信协议**：提供进程内、进程间、机器间、广播等四种通信协议。
通信协议配置简单，用类似于URL形式的字符串指定即可，格式分别为inproc://、ipc://、tcp://、pgm://。
tcp unicast transport using TCP, see zmq_tcp(7)
ipc local inter-process communication transport, see zmq_ipc(7)
inproc local in-process (inter-thread) communication transport, see zmq_inproc(7)
pgm, epgm reliable multicast transport using PGM, see zmq_pgm(7)
vmci virtual machine communications interface

编译 Hello World server

```c++
//  Hello World server

#include <zmq.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>

int main (void)
{

    int major, minor, patch;
    zmq_version(&major, &minor, &patch); // 获取当前使用的ZeroMQ的版本号
    printf("Current ZeroMQ version is %d.%d.%d\n", major, minor, patch);

    ///  1.创建上下文
    void *context = zmq_ctx_new ();   // 创建 0MQ上下文  线程安全的

    /// // 2.创建、绑定套接字
    // ZMQ_REP 服务使用ZMQ_REP类型的套接字来接收来自客户端的请求并向客户端发送回复
    void *responder = zmq_socket (context, ZMQ_REP);
    int rc = zmq_bind (responder, "tcp://*:5555");
    assert (rc == 0);

    /// 3.循环接收数据、发送数据
    while (1) {
        char buffer [10];
        /// 4.接收数据
        zmq_recv (responder, buffer, 10, 0);
        printf ("Received Hello\n");
        sleep (1);
        /// 5.回送数据
        zmq_send (responder, "World", 5, 0);
    }
    return 0;
}


```

g++ server.cpp -o server --std=c++11 -I zeromq/include/zmq -L zeromq/lib -lzmq -lpthread

客户端

```c++
//  Hello World client
#include <zmq.h>
#include <stdio.h>

int main (void)
{
    printf ("Connecting to hello world server...\n");
    /// 1、创建上下文
    void *context = zmq_ctx_new ();   // 创建 0MQ上下文  线程安全的

    /// 2.创建、绑定套接字
    // ZMQ_REQ  客户端使用ZMQ_REQ类型的套接字向服务发送请求并从服务接收答复
    void *requester = zmq_socket (context, ZMQ_REQ);
    zmq_connect (requester, "tcp://localhost:5555");

    ///3.循环发送数据、接收数据
    int request_nbr;
    for (request_nbr = 0; request_nbr != 10; request_nbr++) {
        char buffer [10];
        printf ("Sending Hello %d...\n", request_nbr);
        /// 4.发送数据
        zmq_send (requester, "Hello", 5, 0);
        // 5.接收回复数据
        zmq_recv (requester, buffer, 10, 0);
        printf ("Received World %d\n", request_nbr);
    }
    /// 6.关闭套接字、销毁上下文
    zmq_close (requester);
    zmq_ctx_destroy (context);
    return 0;
}

```

g++ client.cpp -o client --std=c++11 -I zeromq/include/zmq -L zeromq/lib -lzmq -lpthread
