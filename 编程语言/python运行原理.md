# 目录

- [目录](#目录)
- [参考资料](#参考资料)
- [概览](#概览)
- [词法分析](#词法分析)
- [解析树](#解析树)
- [抽象语法树](#抽象语法树)
  - [打印AST](#打印ast)
  - [渲染AST](#渲染ast)
- [字节码](#字节码)
- [语义分析](#语义分析)

# 参考资料

官方文档: [开发者指南](https://devguide.python.org/)

关于编译过程的几个章节：

1. <https://devguide.python.org/exploring/>
2. <https://devguide.python.org/grammar/>
3. <https://devguide.python.org/parser/>
4. <https://devguide.python.org/compiler/>

代码分析的文章：

[A guide from parser to objects, observed using GDB](https://hackmd.io/s/ByMHBMjFe)

[CPython-Internals](https://github.com/zpoint/CPython-Internals#Interpreter)

[**过期：**PEP 339 -- Design of the CPython Compiler](https://www.python.org/dev/peps/pep-0339/#id12)

# 概览

Python 源码到机器码的过程，以 CPython 为例，编译过程如下：

将源代码解析为解析树 (Parser/pgen.c)
将解析树转换为抽象语法树 (Python/ast.c)
将抽象语法树转换到控制流图 (Python/compile.c)
根据控制流图发送字节码 (Python/compile.c)

可以使用以下模块进行操作：
ast 模块可以控制抽象语法树的生成和编译
py-compile 模块能够将源码换成字节码（编译），保存在 __pycache__ 文件夹，以 .pyc 结尾（不可读）
dis 模块通过反汇编支持对字节码的分析（可读）

即实际python代码的处理过程如下：

- 解析树
  - [EBNF完整语法](https://docs.python.org/zh-cn/3/reference/grammar.html)
  - 词法分析
    - [打印词法示例](#词法分析)
  - 语法分析
    - [打印CST解析树](#解析树)
- 抽象语法树(AST)
  - [抽象文法](https://docs.python.org/zh-cn/3.9/library/ast.html)
  - [打印AST示例](#抽象语法树)
  - [在线查看AST](https://python-ast-explorer.com/)
  - [图形显示AST](#图形化AST)
- 控制流程图
- 字节码
  - [打印字节码](#字节码)

# 词法分析

```bash
# 查看使用方法
PS E:\test> python -m tokenize -h
usage: python -m tokenize [-h] [-e] [filename.py]

PS E:\test> cat .\example.py
def testfuc():
    i = 4 + 10
PS E:\test> python -m tokenize -e .\example.py  
0,0-0,0:            ENCODING       'utf-8'        
1,0-1,3:            NAME           'def'
1,4-1,11:           NAME           'testfuc'      
1,11-1,12:          LPAR           '('
1,12-1,13:          RPAR           ')'
1,13-1,14:          COLON          ':'
1,14-1,16:          NEWLINE        '\r\n'
2,0-2,4:            INDENT         '    '
2,4-2,5:            NAME           'i'
2,6-2,7:            EQUAL          '='
2,8-2,9:            NUMBER         '4'
2,10-2,11:          PLUS           '+'
2,12-2,14:          NUMBER         '10'
2,14-2,15:          NEWLINE        ''
3,0-3,0:            DEDENT         ''
3,0-3,0:            ENDMARKER      ''
```

# 解析树

```python
PS E:\csttest> cat .\pythontest.py  
import parser, symbol, token
from pprint import pprint

def experiment(block):
    """experimenting, like the man said
    """

    cst = parser.suite(block)
    cst_list = cst.tolist()
    return cst_list

def process(cst_list):
    """recursively convert a raw parse tree into something readable
    """
    for node in cst_list:
        if type(node) is type([]):
            process(node)
        else:
            cst_list[cst_list.index(node)] = interp(node)

def interp(x):
    """given an int or a string, return a symbol, token, or other
    string
    """
    if type(x) is type(0):
        if x < 256:
            return token.tok_name[x]
        else:
            return symbol.sym_name[x]
    return x

cst = experiment("""\
def test():
    x = 9+10
""")

process(cst)
pprint(cst)

PS E:\csttest> python .\pythontest.py
['file_input',
 ['stmt',
  ['compound_stmt',
   ['funcdef',
    ['NAME', 'def'],
    ['NAME', 'test'],
    ['parameters', ['LPAR', '('], ['RPAR', ')']],
    ['COLON', ':'],
    ['suite',
     ['NEWLINE', ''],
     ['INDENT', ''],
     ['stmt',
      ['simple_stmt',
       ['small_stmt',
        ['expr_stmt',
         ['testlist_star_expr',
          ['test',
           ['or_test',
            ['and_test',
             ['not_test',
              ['comparison',
               ['expr',
                ['xor_expr',
                 ['and_expr',
                  ['shift_expr',
                   ['arith_expr',
                    ['term',
                     ['factor',
                      ['power',
                       ['atom_expr', ['atom', ['NAME', 'x']]]]]]]]]]]]]]]]],
         ['EQUAL', '='],
         ['testlist_star_expr',
          ['test',
           ['or_test',
            ['and_test',
             ['not_test',
              ['comparison',
               ['expr',
                ['xor_expr',
                 ['and_expr',
                  ['shift_expr',
                   ['arith_expr',
                    ['term',
                     ['factor',
                      ['power', ['atom_expr', ['atom', ['NUMBER', '9']]]]]],
                    ['PLUS', '+'],
                    ['term',
                     ['factor',
                      ['power',
                       ['atom_expr',
                        ['atom', ['NUMBER', '10']]]]]]]]]]]]]]]]]]],
       ['NEWLINE', '']]],
     ['DEDENT', '']]]]],
 ['NEWLINE', ''],
 ['ENDMARKER', '']]
```

# 抽象语法树

抽象语法树，是通过asdl语言定义：[Python.asdl](https://docs.python.org/zh-cn/3/library/ast.html)
ASDL官网和语法说明：[ASDL官网](http://asdl.sourceforge.net/)
通过运行Parser/asdl_c.py重新生成Include/Python-ast.h 和 Python/Python-ast.c
解析树转为抽象语法树，Python/ast.c 中的 PyAST_FromNode().

抽象语法树文档除了官方文档，还有
<https://greentreesnakes.readthedocs.io/en/latest/>

----

pypy的解释器，AST实现参考：<https://doc.pypy.org/en/latest/parser.html>

## 打印AST

```bash
PS E:\asttest> python -m ast -h
usage: python -m ast [-h] [-m {exec,single,eval,func_type}] [--no-type-comments] [-a] [-i INDENT] [infile]

PS E:\asttest> python -m ast .\example.py
Module(
   body=[
      FunctionDef(      
         name='testfuc',
         args=arguments(
            posonlyargs=[],
            args=[],
            kwonlyargs=[],
            kw_defaults=[],
            defaults=[]),
         body=[
            Assign(
               targets=[
                  Name(id='i', ctx=Store())],
               value=BinOp(
                  left=Constant(value=4),
                  op=Add(),
                  right=Constant(value=10)))],
         decorator_list=[])],
   type_ignores=[])
```

## 渲染AST

- [在线查看AST](https://python-ast-explorer.com/)
- 代码渲染

```python
import os
import ast
import graphviz

def visit(node, nodes, pindex, g):
    name = str(type(node).__name__)
    index = len(nodes)
    nodes.append(index)
    g.node(str(index), name)
    if index != pindex:
        g.edge(str(index), str(pindex))
    for n in ast.iter_child_nodes(node):
        visit(n, nodes, index, g)
    
graph = graphviz.Digraph(format="png")


# 读取源文件
code = ""
with open("example.py") as f:
    code = f.read()

tree = ast.parse(code)
print(ast.dump(tree))
visit(tree, [], 0, graph)
graph.render("ast")
```

# 字节码

```bash
PS E:\dis> cat .\pythontest.py
import dis

def test():
    x = 10

dis.dis(test)
PS E:\dis> python .\pythontest.py
  4           0 LOAD_CONST               1 (10)
              2 STORE_FAST               0 (x) 
              4 LOAD_CONST               0 (None)
              6 RETURN_VALUE
```

# 语义分析
