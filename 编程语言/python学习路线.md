
# 学习路线

1. 语法练习，核心是会用，方法是看**官方语法文档+编程练习题**
   笨办法学python,有点太简单，做到练习35，**放弃**
   **python 基础版**：有些题目比较好，有针对性，**练完了**<https://blog.csdn.net/include_ice/category_7392475.html>
   廖雪峰python教程：**粗略过了一遍**<https://www.liaoxuefeng.com/wiki/1016959663602400>
   还是没找到符合要求的资料，以动手编程练习为主的语法学习资料
   python面试题，参考 : [python面试题](./python面试题.md)
   **备用**
   Python-100-Days：**有时间过最好**，讲解多，练习题少，**没做**，**有面试题整理**<https://github.com/jackfrued/Python-100-Days>
   牛客网 **有练习题**
2. 最佳实践，包括代码风格，经典代码片段
   python3-cookbook
   Effective Python之编写高质量Python代码的59个有效方法
3. 语言背后的原理，流畅的python
4. 阅读语言源码

# 书籍

**笨办法学python3**

英文官网： <http://learnpythonthehardway.org/book/> （第3版，可能需要翻墙）

中文翻译：<https://www.bookstack.cn/read/LearnPython3TheHardWay/spilt.1.learn-py3.md>
其他备份网址：<https://www.zybuluo.com/Standalone/note/1677177>
作者书本代码仓库：<https://github.com/zedshaw/learn-python3-thw-code>
附加题答案：<https://blog.csdn.net/xiebin6163/category_6875779.html>

**笨办法学python-进阶**
中文翻译：<https://www.bookstack.cn/read/lmpythw-zh/README.md>
官方英语：<https://learncodethehardway.org/more-python-book/>
作者答案仓库：<https://github.com/zedshaw/learn-more-python-the-hard-way-solutions>

**笨办法学python**
Python 2.6.5
笨办法学python(python2.6) pdf 下载：<https://blog.jkxuexi.com/262.html>
在线中文：<https://wizardforcel.gitbooks.io/lpthw/content/>

**点评**：learn by doing it，好的学习方法

**python3-cookbook**
<https://python3-cookbook.readthedocs.io/zh_CN/latest/index.html>
点评：很多具体问题的解决方案

**think python**：How to Think Like a Computer Scientist
点评：适合完全没有编程基础的人，不适合我
pdf下载：<https://greenteapress.com/wp/think-python-2e/>
代码：<https://github.com/AllenDowney/ThinkPython2>

<http://openbookproject.net/thinkcs/python/english3e/>
最新版，质量可以·

**Effective Python之编写高质量Python代码的59个有效方法**

<https://weread.qq.com/web/reader/c2932f9072620d81c29c1edkc81322c012c81e728d9d180>

<https://www.cnblogs.com/xiaobingqianrui/p/10167398.html>

书单推荐：<https://www.zhihu.com/question/265662120>
书单推荐：<https://www.jianshu.com/p/ac1ac8cb0973>

python书籍：<https://wiki.python.org/moin/PythonBooks>
python pdf：<https://www.cnblogs.com/jikexianfeng/p/6149765.html>

python免费书籍：<https://itsmycode.com/list-of-free-python-books/>
101-Free-Python-Books：<https://blog.finxter.com/wp-content/uploads/2020/03/101-Free-Python-Books.pdf>

# 博客资源

python 基础版：<https://blog.csdn.net/include_ice/category_7392475.html>

# 在线编程

Online exercicescodingbatpyschoolscodecademyProblem Solving with Algorithms and Data StructuresHow To Think Like a Computer ScientistCode School: Try PythonPython Online Tutorial | Code Schoolprogramming challengesThe Python Challenge (solve each level through programming)CheckiO (game world)Project Euler (math heavy)CodeEval (unlock job offers)/r/dailyprogrammer

作者：胡阳
链接：<https://www.zhihu.com/question/265662120/answer/298510159>
来源：知乎
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

<https://exercism.org/tracks/python/exercises>

<http://www.pyschools.com/>

<https://checkio.org/>

<https://brilliant.org/>
