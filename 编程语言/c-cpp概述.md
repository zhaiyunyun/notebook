c/c++的学习分为两个部分

# 一、语言标准

语言标准定义了功能特性和标准库两部分。
功能特性由编译器负责具体实现，比如 linux 下 gcc，windows 下 Visual Studio
标准库实现依赖于具体平台，比如 linux 下 c 标准库是 glibc，windows 下的 MSVCRT.DLL。

c/c++是有国际标准化组织 ISO 制定标准，网址为：<https://www.iso.org>

c11 标准参考：<https://www.iso.org/standard/57853.html>

c++2017 参考：<https://www.iso.org/standard/68564.html>

以上是需要收费才能下载，我的百度网盘中可下载：<https://pan.baidu.com/s/1Dc6lyRPryC9ShbPsZ1U5uQ，提取码：c8kw>

其他可以免费查看的网站：

各种语言或软件 API 文档：<https://tool.oschina.net/apidocs>

c/c++语言参考：<https://zh.cppreference.com>

除了参考语言标准，也可对照具体实现进行学习

glibc 官方文档：<https://www.gnu.org/software/libc/manual/html_node/index.html>

# 二、运行环境

c/c++程序生成的可执行程序，由操作系统加载运行。因此这部分的学习主要学会使用操作系统相关 API，比如文件操作，进程线程相关，网络通信等。

应用程序可通过系统调用与操作系统进行交互。系统调用需要操作寄存器等，所以 glibc 封装了这部分操作，提供了简单的 c 语言接口

linux 下系统调用在内核代码中 sys_call_table 定义。

windows 下有 kernel32.dll、user32.dll、gdi32.dll 实现了系统 API。

**总结：**

学习 c/c++除了学习基本语法，标准库和系统 API 的学习将是重点。

linux 系统调用列表官方列表：<http://man7.org/linux/man-pages/dir_section_2.html>

<https://www.ibm.com/developerworks/cn/linux/kernel/syscall/part1/appendix.html>

部分内容参考自：<https://www.cnblogs.com/findumars/p/9000371.html>
