# 类型相关

1. 基本类型：char、short、int、long
   数值极限头文件<limits.h>和<float.h>

- CHAR_MIN、CHAR_MAX：char 类型极限
- INT_MIN、INT_MAX：int 类型极限
- FLT_MIN、FLT_MAX：float 类型极限

2. 附加基本类型：size_t、ptrdiff_t、NULL 等，定义于<stddef.h>
   极限头文件：<stdint.h>

- SIZE_MAX：size_t 类型对象的最大值

3. 定宽整数类型：定义于头文件<stdint.h>

- int8_t：8 位有符号整数类型
- uint8_t：8 位无符号整数类型
- INT8_MIN：int8_t 类型最小值
- INT8_MAX：int8_t 类型最大值

# 内存管理

```C
void* malloc( size_t size );
// 分配size个字节的未初始化的内存，失败返回空指针

void* calloc( size_t num, size_t size );
// 分配num*size个字节的内存，并初始化为0，失败返回空指针


void *realloc( void *ptr, size_t new_size );
// 如果ptr=NULL，size不为0，相当于malloc
// 如果ptr不为NULL，size=0，相当于free
// size比原内存大，返回结果有2种情况，1.返回结果与ptr相等，2.返回新地址，ptr被释放
// size比原内存小，原内存内容被截断，返回原地址
// 失败时返回空指针，原地址ptr保持有效
int *p = malloc(sizeof(int));
if(!p){
  return;
}
int *q = realloc(p, 5 * sizeof(int));
if(q){
  if(p != q){
    // p 已经被释放
    p = NULL;
  }
  free(q);
} else {
  free(p);
}
void free( void* ptr );
// ptr = NULL 时，无操作
```

# 字符串库

可参考我的文章：字符集和编码

宽字符串：每个字符的字节数固定的，通常是 Unicode 编码，也就是 Unicode 字节集中的序号
窄字符串：也叫多字节字符串，每个字符占用的字节个数不同，比如 UTF-8、GB2312、GBK 等；

宽字符和窄字符相互转换：
由于宽字符串是 Unicode 编码，所以跟环境无关
窄字符串的编码是可变的，比如 UTF-8、GB2312、GBK 等
所以窄字符串和宽字符串在转换过程中，会根据当前活动的 locale 的字符编码来识别窄字符串，然后进行转换

C 语言字符串操作函数分为 3 类：单字节字符串、多字节字符串和宽字符串

# 输入输出库

将字符串打印到控制台时，字符串的编码格式需要跟终端的一致
