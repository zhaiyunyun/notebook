# itertools

itertools用于高效循环的迭代函数集合
Python itertools模块详解 <https://www.cnblogs.com/fengshuihuan/p/7105545.html>

## islice切片迭代器

itertools.islice(iterable, start, stop[, step])
创建一个迭代器，大部分用生成器实现，功能类似于切片
不过内置的切片操作，只针对于序列类型(list,tuple,range,str)，返回也是序列

# math库

**基本概念**
**弧度**
定义：弧长等于半径的弧，其所对的圆心角为1弧度。
单位：rad
特殊值：0°=0 90°=π/2, 180°=π, 360°=2π

math库中角度和弧度互转函数
degrees: 弧度转角度
radians: 角度转弧度

三角函数： 输入弧度，输出比值
反三角函数：输入比例，输出弧度

# 生成随机数

Python随机数小结——random和np.random的区别与联系：<https://blog.csdn.net/qq_20011607/article/details/82288561>

在python中，有两个模块可以产生随机数：

1. python自带random包： 提供一些基本的随机数产生函数，可满足基本需要
2. numpy.random：提供一些产生随机数的高级函数，满足高级需求

一些概念：
随机数：真随机数和伪随机数：<https://blog.csdn.net/czc1997/article/details/78167705>
真随机：利用物理现象产生
伪随机：利用算法模拟，在一定周期会出现重复
一般都用伪随机，因为周期比较长才出现重复

# 文件和目录

## 文件操作

<https://docs.python.org/zh-cn/3/tutorial/inputoutput.html#reading-and-writing-files>

<https://docs.python.org/zh-cn/3/library/io.html>

io模块：io.open，是内置open函数的别名

主要方法：
f.read(size) 可用于读取文件内容，它会读取一些数据，并返回字符串（文本模式）
f.readline() 从文件中读取单行数据；
f.readlines() 以列表形式读取文件中的所有行，可以用 list(f) 或 f.readlines()。

## 目录操作

- 工作目录（CWD）通过os.getcwd()获得，**启动解释器的目录**，跟被执行脚本文件位置无关，os.path.exists或FileStream都会依赖此目录
- 导包路径，参考[模块搜索路径](#模块搜索路径)，**默认会把当前执行的脚本文件目录，添加到PYTHONPATH的第一项**，所以被执行脚本文件的目录下的模块和包都可以直接import

windows和linux文件路径区别
在Windows中，**路径分隔采用反斜杠"\"**，比如"C:\Windows\System"
在Unix/Linux中，**路径的分隔采用正斜杠"/"**，比如"/home/hutaow"

**转义字符**：反斜杠开始的字符，比如"/n": 换行，"//":反斜杠
所以windows下的文件路径有两种方法：
**双反斜杠的字符串**表示："E:\\SelfDriveGuard\\XX"
**字符串前加r使转义无效**：r"E:\SelfDriveGuard\XX"

win和linux路径通用解决方案：**统一使用linux的正斜杠**

### 面向对象的文件系统路径pathlib

Python 3.4之前，和路径相关操作函数都放在os模块里面，尤其是**os.path**这个子模块
现在使用pathlib的优势：

1. 之前路径操作主要依赖，字符串拼接，现在更方便
2. 之前代码在不同操作系统间可移植性差

官方文档：<https://docs.python.org/zh-cn/3/library/pathlib.html#concrete-paths>

### 核心API

![](../images/pathlib-api.jpg)

**路径生成和拼接**
p = Path('/etc')
q = p / 'init.d' / 'reboot'
z = p.joinpath('init.d')

**核心方法**

iterdir：列出子目录
glob('*.txt')：正则查询
home()：家目录
cwd()：
resolve(**file**)：获取绝对路径

**路径组成**
.name 文件名，包含后缀名，如果是目录则获取目录名。
.stem 文件名，不包含后缀。
.suffix 后缀，比如 .txt, .png。
.parent 父级目录，相当于 cd ..
.anchor 锚，目录前面的部分 C:\ 或者 /。

# 并发编程

参见：[并发编程](../未分类/并发编程)

**多进程**：
（1）fork子进程：只能在linux平台
（2）采用 multiprocessing 这个库创建子进程，跨平台

**多线程**：_thread和threading，_thread是低级模块，threading是高级模块
python的**多线程是操作系统的原生线程**，在不同的操作系统的原生线程之上，python提供了一套统一的抽象，封装为threading和_thread模块

python**多线程缺陷**：由于CPython解释器的GIL（全局解释器锁），
为什么要GIL？cpython使用引用计数进行内存管理，为了避免多线程带来的复杂的竞争风险问题
运行过程：每一个 Python 线程，都会先锁住自己，以阻止别的线程执行。
**结果**：线程是原生线程，但只能交替执行，伪并发

**协程**：
协程是可以并发执行的函数，协程相对进程线程切换效率极高，加上异步IO的支持，就可以把耗时IO操作放在协程中，而不阻塞其他任务的执行，从而达到高并发
python中的协程发展三个阶段：
一：python2.x：**yield + send**
利用生成器实现协程：通过send给其他协程发送启动执行命令，通过yield返回结果，从而实现多个函数相互协作
x = yield y: y是返回值返回，x是外部send发送的值
二：Python3.x：**asyncio + yield from**
yield from gen(n): 调用子生成器产生返回值
asyncio：异步IO，主要提供Eventloop

```python
import asyncio

@asyncio.coroutine
def hello():
    print("Hello world!")
    # 异步调用asyncio.sleep(1):
    r = yield from asyncio.sleep(1)
    print("Hello again!")

# 获取EventLoop:
loop = asyncio.get_event_loop()
# 执行coroutine
loop.run_until_complete(hello())
loop.close()
```

三：python3.5：**asyncio + async/await**

请注意，async和await是针对coroutine的新语法，要使用新的语法，只需要做两步简单的替换：

把@asyncio.coroutine替换为async；
把yield from替换为await。

```python
async def hello():
    print("Hello world!")
    r = await asyncio.sleep(1)
    print("Hello again!")
```

**协程总结**
目前 Python 语言的协程从实现来说可分为两类：

- （Python 3.10 中移除）一种是基于传统生成器的协程，叫做 generator-based coroutines，通过包装 generator 对象实现。
- 另一种在 Python 3.5 版本 PEP 492 诞生，叫做 native coroutines，即通过使用 async 语法来声明的协程。

# 正则表达式

python正则表达式从字符串中提取数字：<https://blog.csdn.net/u010412858/article/details/83062200>

Python正则表达式，这一篇就够了！<https://zhuanlan.zhihu.com/p/127807805>

匹配大括号，中括号：<https://blog.csdn.net/weixin_40962462/article/details/87968161>

# 垃圾回收

在Python中，主要通过引用计数进行垃圾回收；通过 “标记-清除” 解决容器对象可能产生的循环引用问题；通过 “分代回收” 以空间换时间的方法提高垃圾回收效率。

## 引用计数

```python
a = 234
```

`234`, python对象，每一个对象的核心就是一个结构体PyObject，它的内部有一个引用计数器（ob_refcnt）
`a`, 变量名，相当于cpp中的shared_ptr
通过赋值操作，建立变量和对象的引用关系
以下情况是导致**引用计数加一**的情况:

- 对象被创建，例如a=2
- 对象被引用，b=a
- 对象被作为参数，传入到一个函数中
- 对象作为一个元素，存储在容器中

下面的情况则会导致**引用计数减一**:

- 对象别名被显示销毁 del
- 对象别名被赋予新的对象
- 一个对象离开他的作用域
- 对象所在的容器被销毁或者是从容器中删除对象

获取对象引用计数： sys.getrefcount(a)

**循环引用**。这将是引用计数的致命伤，引用计数对此是无解的，因此必须要使用其它的垃圾回收算法对其进行补充。

```python
# 循环引用代码
a = [i for i in range(1000000)]
b = [j for j in range(1000000)]
a.append(b)
b.append(a)
```

## 标记清除解决循环引用

TODO

## 分代回收

TODO

## 内存泄漏排查方法

Python 内存泄漏问题排查
<https://blog.csdn.net/qq_16681169/article/details/113804000>

常用的内存分析工具:
objgraph: 可以打印类型对象的数目, 绘制有关objs的引用图 link
tracemalloc: Python3内置库，非常轻量和强大，可以用于追踪内存的使用情况，查看对象和调研栈（Python2如果安装的话需要编译) link
pympler: 可以统计内存里边各种类型的使用, 获取对象的大小 link
pyrasite: 内存注入工具的第三方库, 可以渗透进入正在运行的python进程动态修改里边的数据和代码 link
guppy: 可以对堆里边的对象进行统计 link

# 单元测试

内置unittest，第三方pytest

单元测试目录结构如下：

```bash
PS E:\SelfDriveGuard\cartel> tree tests
E:\SELFDRIVEGUARD\CARTEL\TESTS
├─cartel
│  └─ast
├─cartel_stdlib
└─tools
```

**运行所有单元测试用例**
python -m unittest discover -v -s tests

**注意**：使用discover模式时，tests下所有目录下必须得有__init__.py，否则找不到

**单独运行某个模块或方法**, 比如
python -m unittest -v tests/tools/test_cartel_helper.py

[python unittest TestCase间共享数据（全局变量的使用）](https://blog.csdn.net/xiaojing0511/article/details/95665117)

## unittest mock

utittest和pytest中mock的使用详细介绍 ： <https://www.cnblogs.com/goldsunshine/p/15265187.html>

控制你的数据——Python mock的基本使用  ： <https://www.sohu.com/a/372643983_216613>

### 自己总结

首先确定测试对象，一般被测对象就是函数或方法
然后分析被测函数中，哪些对象需要mock掉，被测函数中，调用的无非就是，
数据：外部变量，内部变量(属于被测逻辑，不能mock)
     外部变量，全局变量，类属性，直接修改，`test2.Cls.aaa = 9`
函数：调用的其他函数或者方法
     函数：直接使用`test2.test3.fuc = mock.Mock(return_value=44)`
     类方法：`test2.test3.Cls.class_method = mock.Mock(return_value=11)`
     实例方法：同上

**test.py**

```python
from unittest import mock
import test2

test2.fun3 = mock.Mock(return_value=44)
v = test2.fun2()
print(v)
```

**test2.py**

```python
from test3 import fun3
def fun2():
    n = 3
    t3 = fun3()
    return n+t3
```

**test3.py**

```python
def fun3():
    return 3
```

这种使用方法，没法控制作用域，所以需要用mock.patch

### 外部资料

【Python】模拟对象模块unittest.mock
<https://blog.csdn.net/zzy979481894/article/details/122142829>

这个帖子写的好：
核心是Mock, MagicMock两个类以及patch()函数

Mock类，访问Mock类属性或方法，不存在的话则创建，创建的还是Mock对象，Mock类是可调用的，调用的结果是return_value或side_effect
MagicMock类，将一些魔术方法定义为mock对象
patch() 函数装饰器、类装饰器或上下文管理器，用于将指定的类替换为一个Mock对象, patch()不仅可以用于模块中的类，可也以用于模块中的函数和变量

patch多个类时，注意参数顺序

```python
@patch("aa.bb", aa)
@patch("cc.dd", cc)
def test(cc_mock, aa_mock): # patch最下面的是第一个参数
    pass
```

mock.patch.object() 用于mock一个实例

### 参考

见： Mocking a class: Mock() or patch()? ： <https://stackoverflow.com/questions/8180769/mocking-a-class-mock-or-patch>

<https://hakin9.org/what-the-mock%e2%80%8a-%e2%80%8aa-cheatsheet-for-mocking-in-python-by-yeray-diaz/>

<https://www.cnblogs.com/guyuyun/p/14880885.html>
<https://www.cnblogs.com/fnng/p/5648247.html>

<https://www.cnblogs.com/ajianbeyourself/p/8795441.html>

An Introduction to Mocking in Python: <https://www.toptal.com/python/an-introduction-to-mocking-in-python>
