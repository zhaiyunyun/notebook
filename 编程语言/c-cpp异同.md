根据关键字比较：

# 基本类型

共有部分：double,char,float,int,long,short,signed,void,union,unsigned,enum,struct
c++中特有：class,bool,true,false
wchar_t
char8_t (C++20 起)
char16_t (C++11 起)
char32_t (C++11 起)

类型限定符：const,restrict (C99 起),volatile
c++中特有：mutable

强制类型转换
const_cast
dynamic_cast
reinterpret_cast
static_cast

# 存储类指定符

auto
extern
static
register
thread_local

# 面向对象

friend
private
protected
public
virtual
operator
typeid
this

# 模板编程

template
typename

# 命名空间

namespace
using

# 异常处理

throw
try
catch
noexcept (C++11 起)

# 动态内存分配

delete(1)
new

# 协程

co_await (C++20 起)
co_return (C++20 起)
co_yield (C++20 起)

# 模块

export(1)(3)
import (C++20)

# 代用记号

&&替代 and，&=替代 and_eq，等等
