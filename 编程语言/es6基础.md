# ES6介绍

ECMAScript 是语言标准，javascript 是对 ECMAScript 的实现，js 还包括 BOM，DOM 部分

es6 是 ECMAScript2015，标准文档：<http://www.ecma-international.org/ecma-262/6.0/index.html>

可通过以下网站查看各宿主环境对 es6 的支持情况：

<https://kangax.github.io/compat-table/es6/>

<http://ruanyf.github.io/es-checker/index.cn.html>
