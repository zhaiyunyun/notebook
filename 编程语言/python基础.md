# 目录

- [目录](#目录)
- [个人理解](#个人理解)
- [核心概念](#核心概念)
  - [可变与不可变对象](#可变与不可变对象)
  - [命名空间和作用域](#命名空间和作用域)
    - [命名空间](#命名空间)
- [属性访问](#属性访问)
  - [属性查找](#属性查找)
  - [描述符](#描述符)
- [内置类型](#内置类型)
  - [数字类型](#数字类型)
  - [序列类型](#序列类型)
    - [列表](#列表)
    - [元组](#元组)
    - [range对象](#range对象)
    - [文本序列类型](#文本序列类型)
    - [二进制序列类型](#二进制序列类型)
  - [推导式](#推导式)
  - [字符串](#字符串)
- [类型判断](#类型判断)
- [注解和类型提示](#注解和类型提示)
  - [函数注解：PEP3107](#函数注解pep3107)
  - [常用方法](#常用方法)
  - [常用库的类型提示](#常用库的类型提示)
  - [静态检查工具](#静态检查工具)
- [内置运算](#内置运算)
  - [切片](#切片)
  - [三目运算符](#三目运算符)
  - [逻辑布尔运算](#逻辑布尔运算)
- [内置函数](#内置函数)
- [解包与组包](#解包与组包)
  - [解包](#解包)
    - [解包应用](#解包应用)
- [python函数参数](#python函数参数)
- [with-as上下文管理器](#with-as上下文管理器)
  - [为什么要with-as？](#为什么要with-as)
  - [如何实现？](#如何实现)
- [闭包](#闭包)
  - [实现原理](#实现原理)
  - [闭包应用：装饰器](#闭包应用装饰器)
- [迭代器与生成器](#迭代器与生成器)
  - [迭代器](#迭代器)
  - [生成器](#生成器)
- [面向对象](#面向对象)
  - [Class 对象](#class-对象)
  - [访问限制](#访问限制)
  - [实例对象](#实例对象)
  - [类和实例变量](#类和实例变量)
  - [函数和方法的区别](#函数和方法的区别)
  - [类中的特殊方法](#类中的特殊方法)
  - [类中的装饰器](#类中的装饰器)
  - [元类](#元类)
  - [多重继承](#多重继承)
- [抽象基类](#抽象基类)
  - [自定义抽象基类](#自定义抽象基类)
- [多版本和虚拟环境](#多版本和虚拟环境)
  - [虚拟环境](#虚拟环境)
  - [多版本管理](#多版本管理)
- [模块](#模块)
  - [包](#包)
- [模块导入](#模块导入)
  - [导入流程](#导入流程)
  - [import hook](#import-hook)
  - [导入方法](#导入方法)
  - [三种导入方式](#三种导入方式)
  - [模块搜索路径](#模块搜索路径)
  - [相对导入和绝对导入](#相对导入和绝对导入)
  - [循环导入问题](#循环导入问题)
- [包管理](#包管理)
  - [打包和分发工具](#打包和分发工具)
  - [setup.py参数](#setuppy参数)
  - [包安装工具](#包安装工具)
- [其他](#其他)

# 个人理解

闭包：具有执行环境的函数，内层函数实现功能，内层函数外部是执行环境，闭包是简化的对象
应用场景：装饰器，装饰器是相对被装饰对象而言，函数的装饰器无非就是对函数的输入和输出做修改
对函数输入修改，比如函数需要3个参数，但前两个是固定的，这时候就可以用装饰器装饰下这个函数，然后就可以用一个参数调用了，类似c++的bind绑定部分参数创建新函数

生成器->迭代器(_iter_,_next_)->可迭代对象(_iter_)
全部用for in遍历

# 核心概念

Python彻底搞懂：变量、对象、赋值、引用、拷贝：<https://zhuanlan.zhihu.com/p/265179766>

对象：对象是分配的一块**内存空间**，对象三要素：地址（通过**id函数**获取），类型（通过**type函数**获取），Value（对象的值）
变量：对象的**名字**
引用：变量和对象的关系为引用，通过赋值操作建立引用关系，`a = 40 # a为变量，40为对象`

区分几个概念：名字、对象、类、实例，python中一切都是对象，类也是对象，类实例也是对象，名字是对象的引用
对象使用`__dict__`字典来存储所以属性和方法，类和类实例各自有自己的属性字典

**特殊属性**：
`object.__dict__`：一个字典或其他类型的映射对象，用于存储对象的（可写）属性。
`instance.__class__`: 类实例所属的类。
`class.__bases__`: 由类对象的基类所组成的元组。

**值可以改变的对象**被称为 可变的；值不可以改变的对象就被称为 不可变的

## 可变与不可变对象

Python - 可变和不可变对象 ： <https://www.cnblogs.com/poloyy/p/15073168.html>

**python内置的一些类型**中
可变对象：list dict set
不可变对象：tuple string int float bool

**含义**：
在python中，一切皆对象，对象必有的三个属性：**地址、类型、值**
可变对象与不可变对象的区别在于**对象本身的值是否可变**。

**对不可变对象的修改**，解释器会自动copy一份副本进行修改，原来的会被垃圾回收
特别注意：参数传递过程中，对不可变参数的修改，函数外的变量不会随之改变，因为会生成新的副本

可变包含不可变对象引用：

```python
a = 10
b = 20
c = [a]
a = 15
print(c) #[10]
```

不可变包含可变对象引用：

```python
a = [543]
s = (a, 543,'564')
a.append(54)
print(s) # ([543, 54], 543, '564')
```

```python
# 函数
def test_no_define(age, name):
    age = 123
    name = "poloyy"
    print(age, name)


age = 1
name = "yy"
print(age, name) # 1 yy

test_no_define(age, name) # 123 poloyy
print(age, name) # 1 yy
```

```python
a = 123
b = a
print(id(a))
print(id(b))
print(a, b)

a += 2

print(id(a))
print(id(b))
print(a, b)

# 输出结果
4473956912 
4473956912 # a、b指向同一块地址
123 123
4473956976 # a指向新的地址
4473956912 # b还指向原来地址
125 123
```

不可变对象作用：
它们在需要常量哈希值的地方起着重要作用，例如作为字典中的键

可哈希（不可哈希）对象
参考官方文档的解释：

如果一个对象在其生命周期内有一个固定不变的哈希值 (这需要__hash__()方法) 且可以与其他对象进行比较操作 (这需要__eq__()方法) ，那么这个对象就是可哈希对象 (hashable) 。可哈希对象必须有相同的哈希值才算作相等。
由于字典 (dict) 的键 (key) 和集合 (set) 内部使用到了哈希值，所以只有可哈希 (hashable) 对象才能被用作字典的键和集合的元素。
所有python内置的不可变对象都是可哈希的，同时，可变容器 (比如：列表 (list) 或者字典 (dict) ) 都是不可哈希的。用户自定义的类的实例默认情况下都是可哈希的；它们跟其它对象都不相等 (除了它们自己) ，它们的哈希值来自id()方法。

## 命名空间和作用域

[Python:作用域与命名空间](https://zhuanlan.zhihu.com/p/53361675)
[python 作用域 命名空间区别_python 命名空间和作用域](https://blog.csdn.net/weixin_35693911/article/details/114913344)
[Python3 命名空间和作用域](https://www.runoob.com/python3/python3-namespace-scope.html)

<https://docs.python.org/zh-cn/3/tutorial/classes.html#python-scopes-and-namespaces>

Python NameSpace（命名空间）：<https://www.jianshu.com/p/4e02388c16bf>

**命名空间(namespace)**：是name到object的映射的集合，在Python中是基于字典实现。
当一段代码在Python中执行时，它存在四个命名空间：local，nonlocal，global和built-in。

各命名空间创建顺序：
python解释器启动 ->创建内建命名空间 -> 加载模块 -> 创建全局命名空间 ->函数被调用 ->创建局部命名空间

**作用域(scope)**：是Python程序的文本区域。在该区域，某个命名空间中的名字可以被直接引用。

1. 最内层作用域，可以访问local命名空间的代码区域
2. 外层函数的局部作用域，可以访问nonlocal空间，包含了外层函数的所有局部命名。(not global, not local)
3. 包含模块中所有全局命名的作用域。访问global空间
4. 最外层的作用域是包含了内置命名的命名空间。

只有模块（module），类（class）以及函数（def、lambda）才会**引入新的作用域**，其它的代码块（如if、try、for等）是不会引入新的作用域的

**命名空间与作用域区别**
命名空间定义了在某个作用域内变量名和绑定值之间的对应关系，命名空间是键值对的集合，变量名与值是一一对应关系。作用域定义了命名空间中的变量能够在多大范围内起作用。

命名空间在python解释器中是以字典的形式存在的，是以一种可以**看得见摸得着**的实体存在的。作用域是python解释器定义的一种规则，该规则确定了运行时变量查找的顺序，是一种**形而上的虚的规定**。

golbal： 用于声明名称属于全局命名空间
nolocal ：用于声明名称属于外层函数命名空间

python的 局部变量和全局变量:<https://blog.csdn.net/Vincent_ceso/article/details/76462320>
<https://docs.python.org/zh-cn/3/reference/executionmodel.html?highlight=%E7%BB%91%E5%AE%9A#naming-and-binding>
名称绑定：赋值操作，会在当前命名空间定义新的名称
**易错点**：先使用，后赋值(定义新名称)，会出现矛盾

```python
a = 1
def test():
  print(a)
  a = 10 # UnboundLocalError: local variable 'a' referenced before assignment
```

### 命名空间

- 每个函数function 有自己的命名空间，称local namespace，记录函数的变量
- 每个模块module 有自己的命名空间，称global namespace，记录模块的变量，包括functions、classes、导入的modules、module级别的变量和常量

  ```python
  # 通过globals()内置函数可以打印模块的变量
  {'__name__': '__main__', '__doc__': None, '__package__': None, 
  '__loader__': <_frozen_importlib_external.SourceFileLoader object at 0x7f5724dd6370>, 
  '__spec__': None, '__annotations__': {}, 
  '__builtins__': <module 'builtins' (built-in)>, 
  '__file__': 'test.py', '__cached__': None, 
  'os': <module 'os' from '/usr/lib/python3.8/os.py'>, 
  'zz': <module 'zz' from '/home/zyy/code/python-test/zz.py'>, 
  'x': 7, 'ghh': <function ghh at 0x7f5724dc71f0>}
  ```

- build-in命名空间，它包含build-in function和exceptions，可被任意模块访问

内置函数 dir() 用于打印模块定义的名称。返回结果是经过排序的字符串列表：

```bash
>>> import fibo, sys
>>> dir(fibo)
['__name__', 'fib', 'fib2']
>>> dir(sys)  
['__breakpointhook__', 等

>>> import builtins
>>> dir(builtins)  
```

# 属性访问

## 属性查找

属性引用操作符: a.x，查找对象a中的属性b，查找顺序是先调用`__getattribute__`，当属性不存在时调用`__getattr__`
`__getattribute__`在object基类中有默认实现，`__getattr__`没有
`__getattribute__`默认实现原理：

1. 属性是否是描述符
2. 查找实例空间: `a.__dict__['x']`, 绑定到self上的属性
3. 查找类空间: `type(a).__dict__['x']`，通过`instance.__class__`获取实例所属的类
4. 查找每个父类空间，通过`class.__bases__`获取父类
5. 调用`__getattr__`

属性访问相关：
`__getattribute__`：object基类有默认实现，所有`a.b`访问对象属性，第一步会调用这个
`__get__`：描述符对象，当对象属性时描述符时调用，见：[描述符](#描述符)
`__getattr__()`：访问属性不存在时，调用此方法

总结一下属性查找的顺序：
`__getattribute__()`， 无条件调用
数据描述符（优先于实例属性）
实例对象的字典（与描述符属性同名时，会被覆盖哦）
非数据描述符（只有__get__()方法）或者类属性
`__getattr__()` 方法（属性不存在时调用）

属性相关操作：
`dir()` 内置函数，查询对象所有属性，比`object.__dict__`更多
hasattr、getattr: 会查询类实例属性和类属性
setattr、delattr：**只设置**和删除实例属性

## 描述符

描述符：任何定义了 `__get__ __set__ __delete__`方法的对象，作为类属性时，
目的：用描述符为了控制类属性的行为
应用：classmethod、staticmethod、property内置类
参考：[Python进阶——什么是描述符？](https://zhuanlan.zhihu.com/p/336926012)

# 内置类型

内置类型有
一： **公开可直接使用**：int,float,bool,bytes, dict、list、set 和 tuple
二： 还有些类型比如函数类型，方法类型等，需要**导入types**使用

```python
import types
def f():
  pass
print(isinstance(f, FunctionType))
# 可以通过FunctionType来创建函数
def foobar():
    return "foobar"

dynamic = types.FunctionType(foobar.__code__, {})

# MethodType动态的给对象添加实例方法
import types
class Foo:
    def run(self):
        return None

def bark(self):
    print('i am barking')

a = Foo()
a.bark = types.MethodType(bark, a)
a.bark()
```

三： 还有一些内置类型的替代类型，在**collections**中定义
当需要对内置数据类型进行拓展, 并且想要覆写其某些标准功能能时，
应该使用**UserDict, UserList**, 和UserString,而不是直接继承dict,list, str.

```python
    'ChainMap',
    'Counter',
    'OrderedDict',
    'UserDict',
    'UserList',
    'UserString',
    'defaultdict',
    'deque',
    'namedtuple',
```

Python中的collections模块(四) UserString，UserList，和UserDict
<https://blog.csdn.net/be5yond/article/details/119279799>

四：内置注解类型
类型注解的内置类型为 Generic Alias 和 Union。
typing定义了很多注解类型，请看[注解和类型提示](#注解和类型提示)

```python

# 数字
print("数字")
print("int = ", type(78))
print("float = ", type(float(78.9)))
print("bool = ",type(True))

# 字符串
print("字符串")
print("str = ", type("str")) # 字符串
print("str = ", type(str("fdsa")))

# 列表
print("列表")
print("list [] = ", type([3,3,3]))
print("list list() = ", type(list())) # 参数只能为迭代器

# 元组
print("元组")
print("tuple () = ", type(()))
print("tuple (2,2,2) = ", type((2,2,2)))
print("tuple tuple() = ", type(tuple())) # 参数可以为迭代器

# 字典
print("字典")
print("dict {} = ", type({1:3,4:5}))
print("dict dict = ", type(dict()))

# 集合
print("集合")
print("dict {2} = ", type({2})) # 空的{}就是字典了，至少有一个元素
print("dict set = ", type(set())) # 参数可以为迭代器
```

主要内置类型有数字、序列、映射、类、实例和异常。

<https://docs.python.org/zh-cn/3/library/stdtypes.html#textseq>

## 数字类型

数字类型 --- int, float, complex

## 序列类型

有三种基本序列类型：list, tuple 和 range 对象
专有序列类型：文本字符串、二进制数据

**通用序列操作**

包括：成员检测，[not] in
切片操作：s[i], s[s:j], s[i:j:k]
常用方法：len(s), min(s), man(s), s.index(x), s.count(x)

**不可变序列操作**

与可变序列唯一区别是：支持hash()函数
所以不可变序列可作为dict的键

**可变序列操作**

包括：增删改查之类

### 列表

列表实现了所有 **一般** 和 **可变** 序列的操作。 列表还额外提供了以下方法

### 元组

元组实现了所有 **一般** 序列的操作

### range对象

range 类型表示**不可变的数字序列**，通常用于在 for 循环中循环指定的次数。

range 对象实现了 **一般** 序列的所有操作，但拼接和重复除外

**注意**：range对象不是迭代器，它只实现了`__iter__`，只是可迭代对象，跟其他容器类型一样，参见[迭代器与生成器](#迭代器与生成器)

### 文本序列类型

字符串实现了所有 **一般** 序列的操作，还额外提供了以下列出的一些附加方法

### 二进制序列类型

操作二进制数据的核心内置类型是 **bytes 和 bytearray。** 它们由 memoryview 提供支持，该对象使用 缓冲区协议 来访问其他二进制对象所在内存，不需要创建对象的副本。

## 推导式

为了构建列表、集合或字典，Python 提供了名为“显示”的特殊句法，每个类型各有两种形式:
第一种是显式地列出容器内容，每一项用逗号分隔
第二种是通过一组循环和筛选指令计算出来，称为 **推导式**。

因此推导式又可细分为列表推导式、元组推导式、字典推导式以及集合推导式。

**列表推导式**
语法：[表达式 for 迭代变量 in 可迭代对象 [if 条件表达式] ]

```python
e_list = [[x, y, z] for x in range(5) for y in range(4) for z in range(6)]
# e_list列表包含120个元素
print(e_list)
```

**元组推导式**

```python
a = (x for x in range(1,10))
print(a)
<generator object <genexpr> at 0x0000020BAD136620>
```

**注意**：元组推导式，生成的并不是一个元组，而是一个生成器对象，可以使用**tuple() 函数**转换为元组

## 字符串

python字符串有单引号、双引号、三引号，使用场景是：
当字符串中有单引号时，可以使用双引号包裹，比如I'm boy，就可以写成"I'm boy"
当字符串中有双引号时，可以使用单引号包裹，比如"text"，写成'"text"'
三引号字符串，中间的字符串在输出时保持原来的格式，可以包含单引号双引号，换行tab等 """ I'M boy, "test" """,注意：最后加空格，不能连一起

字符串操作符：+（连接），*（重复），切片[start:stop:step]（截取），in（判断），not in

**字符串格式化**方法：

一：**%格式字符串**
print "%d to hex is %x" %(num, num)
print "%d to hex is %X" %(num, num)
print "%d to hex is %#x" %(num, num)
print "%d to hex is %#X" %(num, num)

二：**格式化函数**，str.format()，python2.6之后
有三种用法：

```python
# 1. 指定位置
>>>"{} {}".format("hello", "world")    # 不设置指定位置，按默认顺序
'hello world'
 
>>> "{0} {1}".format("hello", "world")  # 设置指定位置
'hello world'
 
>>> "{1} {0} {1}".format("hello", "world")  # 设置指定位置
'world hello world'

# 2. 命名参数
print("网站名：{name}, 地址 {url}".format(name="菜鸟教程", url="www.runoob.com"))
 
# 通过字典设置参数
site = {"name": "菜鸟教程", "url": "www.runoob.com"}
print("网站名：{name}, 地址 {url}".format(**site))
 
# 通过列表索引设置参数
my_list = ['菜鸟教程', 'www.runoob.com']
print("网站名：{0[0]}, 地址 {0[1]}".format(my_list))  # "0" 是必须的

# 3. 传入对象
class AssignValue(object):
    def __init__(self, value):
        self.value = value
my_value = AssignValue(6)
print('value 为: {0.value}'.format(my_value))  # "0" 是可选的

```

三： f-string：是 python3.6 之后版本添加的，称之为**字面量格式化字符串**，是新的格式化字符串的语法。

```python
>>> name = 'Runoob'
>>> f'Hello {name}'  # 替换变量
'Hello Runoob'
>>> f'{1+2}'         # 使用表达式
'3'

>>> w = {'name': 'Runoob', 'url': 'www.runoob.com'}
>>> f'{w["name"]}: {w["url"]}'
'Runoob: www.runoob.com'
```

3. 字符串内建函数

# 类型判断

[Python issubclass和isinstance函数：检查类型](http://c.biancheng.net/view/2298.html)
Python 提供了如下两个函数来检查类型：
issubclass(cls, class_or_tuple)：检查 cls 是否为后一个类或元组包含的多个类中任意类的子类。
isinstance(obj, class_or_tuple)：检查 obj 是否为后一个类或元组包含的多个类中任意类的对象。

**总结**：

1. issubclass，用于判断**类和类的父子关系**
2. isinstance, 用于判断**对象实例和类**之间的关系
3. type，跟isinstance作用一样，但isinstance考虑继承关系，type只能判断**实例是否由某个类创建**

```python
class A:
    pass
class B(A):
    pass
a = A()
b = B()

print(isinstance(b, A)) # True
print(isinstance(b, B)) # True

print(type(b) is A) # False
print(type(b) is B) # False

```

# 注解和类型提示

浅谈Python中的注解和类型提示：<https://blog.csdn.net/weixin_37780776/article/details/106631045>

Python 的类型系统: <https://zhuanlan.zhihu.com/p/150432111>

typing: <https://docs.python.org/zh-cn/3/library/typing.html>

【Python】关于 Type Hints 你应该知道这些
<https://xie.infoq.cn/article/31aefb89726ce82dc4b5d94af>

上述的 Any、Union、List、Tuple 和 Dict 是 Type，不是运行时的 Class，这也是初学者最容易误解的地方。Type 只在静态类型检查时可用，在运行时没有意义，不可以继承与实例化，也就是说不存在一个叫 Any 的类，下面的代码将会报错：

**函数注解**：<https://www.python.org/dev/peps/pep-3107/>
**类型提示**：<https://www.python.org/dev/peps/pep-0484/>
**变量注解**：<https://www.python.org/dev/peps/pep-0526/>

**发展历程**：

## 函数注解：PEP3107

从**PEP 3107**中，**注解（annotation）被引入**，关于注解被引入的缘由，PEP 3107中写得很清楚：

因为在Python 2.x中缺乏一种对函数2参数和返回值进行注解的统一方式，所以这就催生了各种不同的工具和类库以弥补这一空档。
一些人使用PEP 318中引入的装饰器来实现注解的功能，而另外一些人通过查看函数的docstring来查找注解。

**语法形式**

```python
# 参数注解
def foo(a: expression, b: expression = 5):
    pass
# 返回值注解
def sum() -> expression:
    pass
```

**访问函数注解**：
注解存储在对象的__annotations__属性里

```python
def foo(a: 'x', b: 5 + 6, c: list) -> max(2, 9):
    ...

# 注解__annotations__结果为
{'a': 'x',
 'b': 11,
 'c': list,
 'return': 9}
```

**变量注解：PEP526**

差异是并不是给实例添加一个 **annotations** 成员，而是将变量的 annotations 信息存放在上下文变量 **annotations** 之中。

```python
from typing import List, ClassVar, Dict

# int 变量，默认值为 0
num: int = 0

# bool 变量，默认值为 True
bool_var: bool = True

# 字典变量，默认为空
dict_var: Dict = {}

# 列表变量，且列表元素为 int
primes: List[int] = []


class Starship:
    # 类变量,字典类型,键-字符串,值-整型
    stats: ClassVar[Dict[str, int]] = {}

    # 实例变量，标注了是一个整型
    num: int

# 打印变量注解
# print(__annotations__) 
# {'users': typing.List[int]}
```

类型提示和变量注解参考：<https://www.cnblogs.com/poloyy/p/15145380.html>

## 常用方法

**常用类型提示**
int,long,float: 整型,长整形,浮点型;
bool,str: 布尔型，字符串类型；
List, Tuple, Dict, Set:列表，元组，字典, 集合;
Iterable,Iterator:可迭代类型，迭代器类型；
Generator：生成器类型；

```python
# 函数返回值指定为字符串
def greeting(name: str) -> str:
    return "hello"
```

## 常用库的类型提示

在开发中不可避免的要使用各种库，为了兼容性考虑，这些库大部分是没有 type hints 的，为此 Python 提供了 Library stub 机制来弥补这个空白（PEP 561）。Library stub 为库的公共接口定义类型注解，使静态检查器可以覆盖到对库的使用。

另外 Python 提供了一个独立的项目 typeshed（地址见附录），包含了 Python 标准库及一些第三方库（如 requests、tornado、protobuf 等）的 Library stubs。另外一些第三方库自己提供了 stubs，比如 django-stubs（<https://pypi.org/project/django-stubs/>）

下面是标准库中 json.dumps 的 stub 定义：

## 静态检查工具

Mypy 是独立运行的工具，你可以用它做静态检查，遵从它的提示，

# 内置运算

## 切片

[切片完全指南(语法篇)](https://zhuanlan.zhihu.com/p/79541418)

切片就是在序列对象（字符串、元组或列表，range对象）中选择某个范围内的项。返回也是序列类型
切片操作封装了索引操作
语法：a[start:stop:step]
其行为是得到下标在这样一个**前闭后开区间范围内**的元素

切片可被用作表达式以及赋值或 del 语句的目标。

```python
l = [1,2,3,4,5]
del l[2:]
l[2:] = 'A'
print(l)
```

**step为正**：

缺省：start最小，stop最大，所以切片index，**不会出现index越界的错误**
这是没有step的默认情况

```python
 >>> a
 [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
 >>> a[0:6:2]
 [0, 2, 4]
 >>> a[::2]
 [0, 2, 4, 6, 8]
 >>> a[:-2:2]
 [0, 2, 4, 6]
 >>> a[4::2]
 [4, 6, 8]
```

**step为负**：
缺省：start最大，stop最小

```python
>>> a
 [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
 >>> a[5::-1]
 [5, 4, 3, 2, 1, 0]
 >>> a[:4:-2]
 [9, 7, 5]
 >>> a[::-1]
 [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
```

## 三目运算符

c语言中的：x = x%2==1 ? x+1:x;
python：为真时的结果 if 判断条件 else 为假时的结果（注意，没有冒号）

## 逻辑布尔运算

**算术运算**：
*, @, /, //, % 乘，矩阵乘，除，整除，取余
+, - 加和减

**关系运算**：运算结果为布尔值（True或False）
in, not in, 成员检测
is, is not, 标识号检测，同一对象，一个对象的标识号可使用 id() 函数来确定
<, <=, >, >=, !=, ==. 比较运算

**布尔运算**： 运算结果不限制为返回的值和类型必须为 False 和 True
布尔逻辑非 NOT, 返回True或False
布尔逻辑与 AND, x and y, x == False，则返回x的值，比如[] and True, 返回[]
布尔逻辑或 OR, x or y, x == True, 则返回y的值

**布尔运算规则**：

1. False, None, 所有类型的数字零，以及空字符串和空容器（包括字符串、元组、列表、字典、集合与冻结集合）。会被解析为False
2. 所有其他值都会被解析为真值True
3. 表达式 x and y 首先对 x 求值；如果 x 为假则返回该值；否则对 y 求值并返回其结果值。
4. 表达式 x or y 首先对 x 求值；如果 x 为真则返回该值；否则对 y 求值并返回其结果值。

# 内置函数

print(*objects, sep=' ', end='\n', file=sys.stdout, flush=False)

input([prompt])
其接收任意任性输入，将所有输入默认为字符串处理，并返回字符串类型。

zip打包函数
range，enumerate等

一些内置高阶函数
map，reduce，filter，sorted等
练习题：<https://blog.csdn.net/include_ice/article/details/96456065>

any(iterator):判断迭代器中，全false，返回false，有一个true，返回ture
all(iterator):所有true，返回true

# 解包与组包

[Python中的组包与解包](https://www.jianshu.com/p/cdcde927def0)

**组包**：在Python中，组包的操作是自动完成的，将**多个值同时赋给一个变量**时，解释器会进行**自动组包**操作。
组包：自动操作，当等号右边有多个数据的时候，会自动包装成为元组。

```python
a = 10, 20, 30
print(a)
# (10, 20, 30)
```

**解包**：解包就是把一个容器拆开、分解，在Python中的解包是自动完成的。
变量 = 容器(元素，会进行逐一赋值)

**解包操作符**：星号*

## 解包

详解python解包:<https://www.cnblogs.com/hls-code/p/15227567.html>

解包：就是把容器里的元素取出来，任何**可迭代对象都支持解包**，包括元组、字典、集合、字符串、生成器等实现了__next__方法的一切对象。

```
# 对列表、元祖等解包操作
>>> [*[1,2,3]]
[1, 2, 3]

# 对字典解包操作
>>> {**{'a':1}, **{'b':2}}
{'a': 1, 'b': 2}
```

**总结**：
一个星号可作用于所有的可迭代对象，称为**迭代器解包操作**，作为位置参数传递给函数
两个星号只能作用于字典对象，称之为**字典解包操作**，作为关键字参数传递给函数。

### 解包应用

**多变量赋值**, 本质上也是自动解包过程

```python
>>> name, age, date = ['Bob', 20, '2018-1-1']
>>> name
'Bob'
>>> age
20
>>> date
'2018-1-1'
```

在 Python2 中，如果等号**左边变量的个数不等于右边可迭代对象中元素的个数**，是不允许解包的。
python3 中，使用*号支持

```python
>>> a, *b, c = [1,2,3,4]
>>> a
1
>>> b
[2, 3]
>>> c
4
```

**_的用法**，当一些元素不用时，用_表示是更好的写法，可以让读代码的人知道这个元素是不要的

```python
>>> person = ('Bob', 20, 50, (11, 20, 2000))
>>> name, *_, (*_, year) = person
>>> name
'Bob'
>>> year
2000
```

**函数调用**：

```python
def func(a, b, c):
    print(a, b, c)

func(1, 2, 3)
# 1 2 3
func(*[1, 2, 3])
# 1 2 3
func(*(1, 2, 3))
# 1 2 3
func(*"abc")
# a b c
func(*{"a": 1,"b": 2,"c": 3})
# a b c
func(**{"a": 1,"b": 2,"c": 3})
# 1 2 3
```

# python函数参数

参考地址：<https://cloud.tencent.com/developer/article/1037232>

1. 位置参数
   位置(参数positional arguments)就是其他语言的参数，其他语言没有分参数的种类是因为只有这一种参数，所有参数都遵循按位置一一对应的原则
2. 默认参数
   形式：参数名 = 默认值
   **注意**：默认参数必须在最右端

   ```python
   def personinfo(name, age, gender = 'Female'):
      print(name, age, gender)

  ```
3. 可变参数
   Python函数提供了可变参数，来方便进行参数个数未知时的调用。可变参数将以元祖形式传递。
   格式: *参数 （即在参数前加*号）
   ```python
   def getsum(*num):
      sum = 0
      for n in num:
          sum += n
      return sum

    list = [2, 3, 4]

    print(getsum(1, 2, 3))
    print(getsum(*list))
    #结果：6 9
   ```

4. 关键字参数
   Python的可变参数以tuple形式传递，而关键字参数则是以**dict形式**传递。
   即可变参数传递的是参数值，关键字参数传递的是参数名:参数值键值对。
   **形式**：**kw 这是惯用写法，建议使用，容易被理解

 ```python
  def personinfo(name, age, **kw):
    print('name:', name, 'age:', age, 'ps:', kw)
  personinfo('Steve', 22)
  personinfo('Lily', 23, city = 'Shanghai')
  personinfo('Leo', 23, gender = 'male',city = 'Shanghai')
  # 运行结果
  name: Steve age:22 ps:{}
  name: Lily age:23 ps:{'city':'Shangehai'}
  name: Leo age:23 ps:{'gender':'male', 'city': 'Shanghai'}
  ```

5. 命名关键字参数
  Python的命名关键字参数对传入的关键字参数做了进一步的限制。
   **格式**：在关键字参数前增加一个”*”。

   ```python
   def personinfo(name, age, *, gender, city): #只能传递gender和city参数
    print(name, age, gender, city)
   personinfo('Steve', 22, gender = 'male', city = 'shanghai')
   Steve 22 male shanghai
   ```

6. 参数组合
   参数定义的顺序必须是：必选参数、默认参数、可变参数/命名关键字参数和关键字参数。

   ```python
   def function(a, b, c=0, *, d, **kw):
    print('a =', a, 'b =', b, 'c =', c, 'd =', d, 'kw =', kw)
   ```

**小结**
Python的函数具有非常灵活的参数形态，既可以实现简单的调用，又可以传入非常复杂的参数。

默认参数一定要用不可变对象，如果是可变对象，程序运行时会有逻辑错误！

要注意定义可变参数和关键字参数的语法：

*args是可变参数，args接收的是一个tuple；

**kw是关键字参数，kw接收的是一个dict。

以及调用函数时如何传入可变参数和关键字参数的语法：

可变参数既可以直接传入：func(1, 2, 3)，又可以先组装list或tuple，再通过_args传入：func(_(1, 2, 3))；

关键字参数既可以直接传入：func(a=1, b=2)，又可以先组装dict，再通过**kw传入：func(**{'a': 1, 'b': 2})。

使用*args和**kw是Python的习惯写法，当然也可以用其他参数名，但最好使用习惯用法。

命名的关键字参数是为了限制调用者可以传入的参数名，同时可以提供默认值。

定义命名的关键字参数在没有可变参数的情况下不要忘了写分隔符*，否则定义的将是位置参数。

# with-as上下文管理器

参考：
Python中的with-as用法：<https://www.jianshu.com/p/c00df845323c>
<https://docs.python.org/zh-cn/3/reference/compound_stmts.html#the-with-statement>

## 为什么要with-as？

以文件操作为例：

```python
file = open("/tmp/foo.txt")
data = file.read()
file.close()
```

**问题**：1. 忘记close，2. 没有处理异常
**解决**：

```python
file = open("/tmp/foo.txt")
try:
    data = file.read()
finally:
    file.close()
```

**问题**：代码冗长
**解决**：使用with-as语句

```python
with open("/tmp/foo.txt") as f
    data = f.read()
```

## 如何实现？

通过 _上下文管理器_ 对象

```python
with EXPRESSION as TARGET:
    SUITE
```

_EXPRESSION_ 语句返回 _上下文管理器_ 对象
此对象定义__enter__和__exit__方法，
当SUITE执行完毕，会执行__exit__方法，关闭文件清理资源

总结：

1. 上下文管理器 对象包装了资源获取和释放，类似c++构造函数和析构函数
2. with-as简化了try...except...finally使用

# 闭包

定义：在内部函数**引用外部函数中的变量**，就是闭包函数
一个闭包必须满足以下几点:

1. 必须有一个内嵌函数
2. 内嵌函数必须引用外部函数中的变量
3. 外部函数的返回值必须是内嵌函数

判断方法：函数名.**closure** 在函数是闭包函数时，返回一个cell元素；不是闭包时，返回None。

理解：闭包就是一个具有执行环境的函数。
基于上面的介绍，不知道读者有没有感觉这个东西和类有点相似，相似点在于他们都提供了对数据的封装。不同的是闭包本身就是个方法。和类一样，我们在编程时经常会把通用的东西抽象成类，（当然，还有对现实世界——业务的建模），以复用通用的功能。闭包也是一样，当我们需要函数粒度的抽象时，闭包就是一个很好的选择。

在这点上闭包可以被理解为一个只读的对象，你可以给他传递一个属性，但它只能提供给你一个执行的接口。因此在程序中我们经常需要这样的一个函数对象——闭包，来帮我们完成一个通用的功能，比如后面会提到的——装饰器。

```python
# fun2是闭包函数
def fun1():
    n = 1
    def fun2():
        print(n)
    print(fun2.__closure__) # (<cell at 0x00000000027D1708: int object at 0x000007FEDC7ED420>,)
    return fun2
aa = fun1()
print(aa)
# fun2不是闭包
n = 1
def fun1():
    def fun2():
        print(n)
    print(fun2.__closure__) # None
    return fun2
aa = fun1()
print(aa)
```

## 实现原理

[深入理解Python内部函数和闭包，和3个应用场景【Python进阶】](https://zhuanlan.zhihu.com/p/353846221)

答案：**__closure__属性**。Python给内部函数添加了这个属性来携带内部函数用到的外部函数中的变量。

```python
import random
def create_room():
    room_no = random.randint(1, 100)
    print(f'我创建了房间号：{room_no}')

    def toilet():
        print(f'我是{room_no}的内部厕所')
    return toilet


print('调用外部函数')
toilet = create_room()

print('打印一下toilet函数的变量，其中有一个是__closure__')
print(dir(toilet))
print('__closure__是一个包含它携带的变量的元组')
print(toilet.__closure__)
print('__closure__元组里是cell，通过cell_contents可以访问所携带的变量值')
print(toilet.__closure__[0].cell_contents)
```

## 闭包应用：装饰器

官网文档：<https://www.python.org/dev/peps/pep-0318/>

参考：<https://www.liaoxuefeng.com/wiki/1016959663602400/1017451662295584>
<https://zhuanlan.zhihu.com/p/305604008>

**语法**：将@装饰器名，放在被装饰对象上面。

**本质上**，返回值为另一个函数的函数，通常使用 @wrapper 语法形式来进行函数变换

示例：

```python
# 装饰器函数
def log(func):
    def wrapper(*args, **kw):
        print('call %s():' % func.__name__)
        return func(*args, **kw)
    return wrapper

@log
def now():
    print('2015-3-25')

# 调用
now()
```

解释：@log，相当于now = log(now)，即把变量名指向自己定义的装饰器函数
由此可知：装饰器可以，在函数前后添加调用代码，修改入参，和返回结果。

# 迭代器与生成器

参考：<https://www.liaoxuefeng.com/wiki/1016959663602400/1017323698112640>

一文彻底搞懂Python可迭代(Iterable)、迭代器(Iterator)和生成器(Generator)的概念:<https://cloud.tencent.com/developer/article/1478515>

**可迭代对象**: 他的特点其实就是我的序列的大小长度已经确定了(list,tuple,dict,string等)。他遵循可迭代的协议。

> 含__iter__()方法。且可迭代对象中的__iter__()方法返回的是一个对应的迭代器。(如list对应的迭代器就是list_iterator)

**迭代器对象**:他的特点就是他不知道要执行多少次，所以可以理解不知道有多少个元素，每调用一次__next__()方法，就会往下走一步，当然是用__next__()方法只能往下走，不会回退。是惰性的。这样我可以存很大很大的数据，即使是整个自然数，也可以很轻松的用迭代器来表示出来。他满足的是迭代器协议。

> 含__iter__()方法。且方法返回的Iterator对象本身
> 含__next__()方法，每当__next__()方法被调用，返回下一个值，直到没有值可以访问，这个时候会抛出stopinteration的异常。

**联系**：Iterator是Iterable的子类，迭代器都是可迭代的
迭代器是一次性的，比如enumerate对象是迭代器

```python
seasons = ['Spring']
ee = enumerate(seasons)
print(list(ee)) #[(0, 'Spring')]
print(list(ee)) #[]
```

可迭代对象可重复使用，比如range对象是可迭代对象

```python
r = range(10)
print(list(r)) #[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
print(list(r)) #[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```

**for循环原理**：

```python
for x in [1,2,3,4,5]:
    pass
# 等价于

#先获取iterator对象
it = iter([1,2,3,4,5])
while True:
    try:
        #获取下一个值
        x = next(it);
    except StopIteration:
        # 遇到StopIteration就退出循环
        break
```

**内置iter函数过程**

1. 检查对象是否实现了`__iter__()` 方法，如果实现了就调用它（也就是我们偶尔用到的特殊方法重载），获取一个迭代器。
2. 如果没有实现iter()方法， 但是实现了 [`__getitem__(self, key)`](#特殊方法) 方法，iter函数会把此对象转换为迭代器对象
3. 如果都没有则报错

可遍历对象：
`__getitem__(self, key)`：对序列（字符串、元组或列表）或映射（字典）对象的抽取操作 `a[b]` 通常就是从相应的**多项集中选择一项**

所以for循环，可用于两类对象，一种是迭代器(重载iter和next方法)，另外一种是重载了getitem方法的对象

![迭代器生成器关系](../images/迭代器生成器关系.jpeg)

## 迭代器

个人理解：迭代器类似c++中的指针，next()相当于++，并通过next取值
迭代器封装了遍历访问的操作

参考：[轻松搞懂Python中Iterable与Iterator](https://cloud.tencent.com/developer/article/1613017)

**创建迭代器对象**：

```python
list=[1,2,3,4]
it = iter(list)    # 创建迭代器对象
for x in it:
    print (x, end=" ")
```

**自定义迭代器**：

```python
class MyNumbers:
  def __iter__(self):
    self.a = 1
    return self
 
  def __next__(self):
    if self.a <= 20:
      x = self.a
      self.a += 1
      return x
    else:
      raise StopIteration
 
myclass = MyNumbers()
myiter = iter(myclass)
 
for x in myiter:
  print(x)

```

## 生成器

**生成器对象**：Generator是Iterator的子类，创建生成器对象的两种方式：

1. 通生成器函数创建

```python
import sys
 
def fibonacci(n): # 生成器函数 - 斐波那契
    a, b, counter = 0, 1, 0
    while True:
        if (counter > n): 
            return
        yield a
        a, b = b, a + b
        counter += 1
f = fibonacci(10) # f 是一个迭代器，由生成器函数返回生成
 
while True:
    try:
        print (next(f), end=" ")
    except StopIteration:
        break # 退出循环

while True:
    a = next(f, None)
    if a == None:
        break
# 结果
0 1 1 2 3 5 8 13 21 34 55
```

2. 通过生成器表达式创建

```python
#列表推导式
lis = [x*x for x in range(10)]
print(lis)
#生成器表达式
generator_ex = (x*x for x in range(10))
print(isinstance(generator_ex, Iterator))
 
结果：
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
True
```

原理：**生成器对象，包含一个代码对象，一个堆栈对象**，堆栈帧有个 “last instruction”指针，指向代码对象最近执行的那条指令
由于代码对象和堆栈对象，都分配在堆内存上，所以不会自动释放，由于这个特性，可以用生成器来实现协程。

why？
通过列表推导式，我们可以直接创建一个列表，但是，受到内存限制，列表容量肯定是有限的，而且创建一个包含100万个元素的列表，不仅占用很大的存储空间，如果我们仅仅需要访问前面几个元素，那后面绝大多数元素占用的空间都白白浪费了。

所以，如果列表元素可以按照某种算法推算出来，那我们是否可以在循环的过程中不断推算出后续的元素呢？这样就不必创建完整的list，从而节省大量的空间，在Python中，**这种一边循环一边计算的机制，称为生成器**：generator

所以生成器，**返回的是一个数据结构，但保存的是创建数据结构的算法**

参考：python 生成器和迭代器有这篇就够了：<https://www.cnblogs.com/wj-1314/p/8490822.html>

Python 生成器原理详解：<https://blog.csdn.net/iodjSVf8U1J7KYc/article/details/78337057>

# 面向对象

python中一切皆对象，类型也是对象

## Class 对象

```python
class MyClass:
    """A simple example class"""
    i = 12345

    def f(self):
        return 'hello world'
```

类对象支持两种操作：**属性引用和实例化**。
属性引用：类中定义的变量和函数，都会绑定到MyClass上，当然也可以动态增加和删除
实例对象：MyClass(),类型后面加括号，生成一个实例对象

## 访问限制

`__xx__` ：魔术方法，Python内部的名字，用来区别其他用户自定义的命名，以防冲突。Python不建议将自己命名的方法写为这种形式。
`__xx`: 双下划线开头，表示为**私有成员**，只允许类本身访问，子类也不行。
`_x`: 以单下划线开头，表示这是一个**保护成员**，只有类对象和子类对象自己能访问到这些变量。以单下划线开头的变量和函数被默认当作是内部函数，使用from module improt *时不会被获取，但是使用import module可以获取。
`x_`: 以单下划线结尾仅仅是为了区别该名称与关键词。

————————————————
版权声明：本文为CSDN博主「Spade_」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：<https://blog.csdn.net/Spade>_/article/details/107924585

## 实例对象

实例对象唯一操作是属性引用。 有两种有效的属性名称：**数据属性和方法**。

## 类和实例变量

一般来说，实例变量（绑定在self上的变量）用于每个实例的唯一数据，而类变量（类中定义的）用于类的所有实例共享的属性和方法:

名字查找规则：优先查找实例变量

## 函数和方法的区别

与类和实例无绑定关系的function都属于函数（function）；
与类和实例有绑定关系的function都属于方法（method）。

**函数（FunctionType）**：函数是封装了一些独立的功能，**可以直接调用**

**方法（MethodType）**：但是方法是只能**依靠类或者对象来调用**的，表示针对性的操作。
方法中的数据self和cls是隐式传递的，即方法的调用者；
方法可以操作类内部的数据

**本质区别**：函数的 **传参都是显式传递的**
方法则方法中 传参往往都会有**隐式传递**的，具体根据于调用方。例如示例中的 C().f 通过实例调用的方式会隐式传递 self 数据。

## 类中的特殊方法

**slot**:Python允许在定义class的时候，定义一个特殊的__slots__变量，来限制该class实例能添加的属性：

<https://www.liaoxuefeng.com/wiki/1016959663602400/1017590712115904>

## 类中的装饰器

**@property**：@property装饰器就是负责把一个方法变成属性调用的

对属性的访问，会变为方法调用

```python
class Student(object):

    @property
    def score(self):
        return self._score #实例变量与方法名，不能重名

    @score.setter
    def score(self, value):
        if not isinstance(value, int):
            raise ValueError('score must be an integer!')
        if value < 0 or value > 100:
            raise ValueError('score must between 0 ~ 100!')
        self._score = value
>>> s = Student()
>>> s.score = 60 # OK，实际转化为s.set_score(60)
>>> s.score # OK，实际转化为s.get_score()
60
>>> s.score = 9999
```

**@staticmethod 和 @classmethod**: 静态方法和类方法

参考：[Python 中 staticmethod 和 classmethod 原理探究](https://zhuanlan.zhihu.com/p/142560233)

> 注意：self，cls都不是关键字，用其他的xxx都可以

```python
class A(object):
    # 实例方法：第一个参数为self
    def m1(self, n):
        print("self:", self)
    # 类方法：第一个参数为cls
    @classmethod
    def m2(cls, n):
        print("cls:", cls)
    # 静态方法
    @staticmethod
    def m3(n):
        pass

a = A()
a.m1(1) # self: <__main__.A object at 0x000001E596E41A90>
A.m2(1) # cls: <class '__main__.A'>
A.m3(1)

```

使用场景：

1. 静态方法：不需要使用类变量或实例变量，比如工具函数
2. 类方法：不需要实例化对象，使用类变量，不使用实例对象的变量，比如单例模式
3. 实例方法：使用实例变量

## 元类

<<https://www.liaoxuefeng.com/wiki/1016959663602400/101759244937107>  2>
<https://www.jianshu.com/p/c1ca0b9c777d> Python 元类

python一切皆对象，类也不例外，类是对象，也可以作为参数传递，设置属性等
元类生成类，类生成实例
定义类的2种方式，1是class关键字，2是type
type是内置的元类，
元类的主要目的就是为了当创建类时能够自动地改变类。
就元类本身而言，它们其实是很简单的：
拦截类的创建
修改类
返回修改之后的类

```python
MyClass = MetaClass() # 使用元类创建出一个对象，这个对象称为“类”
my_object = MyClass() # 使用“类”来创建出实例对象
MyClass = type('MyClass', (), {}) # type实际上是一个元类
```

## 多重继承

```python
class DerivedClassName(Base1, Base2, Base3):
    <statement-1>
    <statement-N>
```

# 抽象基类

python定义了一些抽象基类，而很多内置类型就继承自这些抽象基类

比如：

```python
# 字符串类
class str(Sequence[str]):
# 集合
class set(MutableSet[_T], Generic[_T]):
```

内置的抽象基类，大部分定义在：**collections.abc**模块中

## 自定义抽象基类

Python之抽象类、抽象方法
<https://www.cnblogs.com/FG123/p/9463673.html>

python抽象基类
<https://blog.csdn.net/q1403539144/article/details/91389740>

```python
import abc


class CopyBase(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def save(self):
        pass


class CopyPaper(CopyBase):
    def __init__(self):
        pass

    def save(self):
        print("copy paper")


class CopyQuestion(CopyBase):
    def __init__(self):
        pass


copy_paper = CopyPaper()
copy_paper.save()
copy_question = CopyQuestion()
copy_question.save()


result:
copy paper
Traceback (most recent call last):
    copy_question = CopyQuestion()
TypeError: Can't instantiate abstract class CopyQuestion with abstract methods save

```

# 多版本和虚拟环境

## 虚拟环境

虚拟环境,Anaconda,virtualenv、pyenv，<https://zhuanlan.zhihu.com/p/216157886>

虚拟环境，用于解决，**多个应用之间**环境冲突问题，类似于docker

**创建虚拟环境**
python3.3以上自带：python3 -m venv /path/to/new/virtual/environment

**进入虚拟环境**

linux下：source /venv/bin/active
windows下：.\venv_py38_carla_0.9.10\Scripts\Activate.ps1

win下需要开启powershell脚本运行权限
管理员运行powershell，执行：Set-ExecutionPolicy Bypass

**退出虚拟环境**：
虚拟环境中运行：deactivate

**添加环境变量**

1. linux中：
虚拟环境中临时添加环境变量export xxx=xxx
永久修改：在建好的虚拟环境的 venv/bin/active 文件中，写入需要的环境变量，再进入虚拟环境； 如 配置文件路径

2. windows中：
Scripts\Activate.ps1中添加环境变量
$env:CARLA_ROOT="E:/carla_python_api/PythonAPI_0.9.10"
$env:PYTHONPATH="$env:CARLA_ROOT/PythonAPI/carla/dist/carla-0.9.10-py3.7-win-amd64.egg;$env:CARLA_ROOT/PythonAPI/carla/agents;$env:CARLA_ROOT/PythonAPI/carla:$env:CARLA_ROOT/PythonAPI"

ps中查看环境变量：ls env:

## 多版本管理

**多版本管理**工具（只有linux版本）：<https://github.com/pyenv/pyenv>
官方自带的：
linux中
python2   -m pip install SomePackage  # default Python 2
python2.7 -m pip install SomePackage  # specifically Python 2.7
python3   -m pip install SomePackage  # default Python 3
python3.4 -m pip install SomePackage  # specifically Python 3.4
在 Windows 中，使用 py Python 启动器命令配合 -m 开关选项:
py -2   -m pip install SomePackage  # default Python 2
py -2.7 -m pip install SomePackage  # specifically Python 2.7
py -3   -m pip install SomePackage  # default Python 3
py -3.4 -m pip install SomePackage  # specifically Python 3.4

# 模块

一个py文件就是一个模块

模块内置属性

`__name__` 直接运行本模块，值为 `__main__`，被导入时，值为模块名称

`__file__` 直接运行本模块，值为文件名，被导入时，职位文件绝对路径

## 包

当一个文件夹下有 `__init__.py` 时，该文件夹是一个包（package）

包只是一种特殊的模块。 特别地，任何具有 `__path__` 属性的模块都会被当作是包。该属性初始化为在包的 `__init__.py` 文件中的代码执行前所在的目录名列表

`__init__.py`主要控制包的导入行为

若想使用from pacakge import *这种形式的写法，需在 `__init__.py` 中加上： `__all__ = [‘file_a’, ‘file_b’]`

python package
如果你需要 python 将一个文件夹作为 Package 执行，那么这个文件夹中必须包含一个名为 `__main__.py` 的文件。

# 模块导入

Python创建包，导入包（入门必读）:<http://c.biancheng.net/view/4669.html>

## 导入流程

Python 导入机制 - import hook ： <https://blog.csdn.net/u010786109/article/details/52038443>

深入探讨 Python 的 import 机制并实现远程导入模块 : <https://blog.51cto.com/u_15494922/5045883>

Python 导入机制 - import hook： <https://blog.csdn.net/cpxsxn/article/details/108754707>

**模块查找流程**：

1. 检查 **sys.modules** 列表 (保存了之前import的类库的缓存），如果module被找到，则⾛到第二步。
2. 检查 **sys.meta_path** 列表。meta_path 是一个 list，⾥面保存着一些 finder 对象，如果找到该module的话，就会返回一个finder对象。
3. 检查某些**隐式的finder对象**，不同的python实现有不同的隐式finder，但是都会有 **sys.path_hooks**, sys.path_importer_cache 以及**sys.path**。
4. 抛出 ImportError

**隐式的finder对象**：

```python
>>> import sys
>>> sys.meta_path
[<class '_frozen_importlib.BuiltinImporter'>,  # 知道如何导入内置模块
<class '_frozen_importlib.FrozenImporter'>,    # 知道如何导入冻结模块
<class '_frozen_importlib_external.PathFinder'>] # 知道如何导入来自 import path 的模块 (即 path based finder)。
```

_frozen_importlib_external.PathFinder, 这个对象调用了sys.path以及sys.path_hooks。代码如下

```python
for path in sys.path:
    for hook in sys.path_hooks:
        try:
            importer = hook(path)
        except ImportError:
            # ImportError, so try the other path hooks
            pass
        else:
            loader = importer.find_module(fullname)
            <module> = loader.load_module(fullname)
```

## import hook

**查找器（finder）**： 决定自己是否能够通过运用其所知的任何策略找到相应的模块。在 Python2 中，finder 对象必须实现
find_module() 方法，在 Python3 中必须要实现 find_module() 或者 find_loader（)
方法。如果 finder 可以查找到模块，则会返回一个 loader 对象(在 Python 3.4中，修改为返回一个模块分支module
specs，加载器在导入中仍被使用，但几乎没有责任)，没有找到则返回 None。
**加载器（loader）**： 负责加载模块，它必须实现一个 load_module() 的方法
**导入器（importer）**： 实现了 finder 和 loader 这两个接口的对象称为导入器

我们需要实现一个import hook，只需要实现一个finder对象和loader对象对应的find_module方法和load_module方法。要让Python解释器import 模块的 时候触发这个finder对象，只需要将这个对象的实例插入到sys.metapath列表中。

```python
import sys
import unittest

import carla_mock.carla as carla_mock

class CarlaLoader:
    def load_module(self, fullname):
        sys.modules[fullname] = carla_mock

class CarlaFinder:
    @classmethod
    def find_module(cls, name, path, target=None):
        if name == "carla":
            print("use carla mock server")
            return CarlaLoader()
        return None

sys.meta_path.insert(0, CarlaFinder)

# 以上必须放在开始才生效
```

**简便方法**：如果要用自己的模块，替换第三方模块，直接

```python
import carla_mock.carla as carla_mock
sys.modules[0] = carla_mock
```

## 导入方法

导入方法可归结为以下 3 种：

1. import 包名[.模块名 [as 别名]]
   1. import 包名，本质上是导入包下面的__init__.py模块，__init__如果是空，那就啥都没导入
   2. import 包名.模块名，跟上面一样会运行__init__.py
   3. 不用as的话，只能通过 包名.模块名 来访问
2. from 包名 import 模块名 [as 别名]
   1. from 包名 import *: 导入的是包里的__init__.py里有__all__=['aa','bb']
   2. 包里的__init__py会运行
   3. 不用as的话，只能通过 模块名 来访问
3. from 包名.模块名 import 成员名 [as 别名]
   1. 通过该方式导入的变量（函数、类），在使用时可以直接使用变量名（函数名、类名）调用

总结：import只能接包或模块，from可以import模块中的函数变量等

## 三种导入方式

1. import 关键字
2. 内置的 `__import__()` 等函数
3. importlib.import_module()

import 首先调用内置 `__import__()` 函数进行模块搜索，然后进行返回值名称绑定到当前作用域

**import 语句**

- import module（package）：只能跟模块名或包名，导入后保留原有的命名空间，故得用module.X方式访问其函数或变量
- from module（package） import X：from/import后都可跟模块名或包名，import后还可跟变量类名等。导入后名称绑定到当前命名空间，无须用module.X访问

## 模块搜索路径

Python导入时，模块搜索顺序，可通过sys.path查看搜索路径，默认顺序：

1. 输入脚本的目录（或未指定文件时的当前目录）
2. PYTHONPATH （目录列表，与 shell 变量 PATH 的语法一样）
3. 默认安装目录

## 相对导入和绝对导入

无论是绝对导入还是相对导入，都需要一个参照物，不然「绝对」与「相对」的概念就无从谈起。

**绝对导入：**
绝对导入的参照物是，输入脚本的目录（或当前目录）

绝对导入要求我们必须从最顶层的文件夹（输入脚本目录）开始，为每个包或每个模块提供出完整详细的导入路径

**相对导入**

相对导入使用前缀点号，相对导入只能使用 from <> import <> 语法

相对导入的问题：

1. 主模块中使用相对导入报错

   ```bash
   File "zz.py", line 7, in <module>
   from .test import main1
   ImportError: attempted relative import with no known parent package
   ```

   原因：相对导入基于当前模块名（模块名一般是从顶层目录比如：x.y.module），因为主模块名是 "**main**" ，找不到父包（父包就是：x.y）出错。所以 Python 程序的主模块必须始终使用绝对导入。
2. 相对导入，不能导入顶层包所在目录的其他模块或者包

```bash
zyy@zyy-dell:~/code/python-test/test$ tree
.
├── main.py # import sub1.sub11
├── sub1
│   └── sub11.py  # from ..sub2 import sub21, print(__name__)
└── sub2
    └── sub21.py

zyy@zyy-dell:~/code/python-test/test$ python main.py 
sub1.sub11
Traceback (most recent call last):
  File "main.py", line 1, in <module>
    import sub1.sub11
  File "/home/zyy/code/python-test/test/sub1/sub11.py", line 2, in <module>
    from ..sub2 import sub21
ValueError: attempted relative import beyond top-level package
```

运行可知sub11的 **name** 名为 sub1.sub11，当相对导入时，一个点代表sub1，两个..从模块名上取不到包名了，所以出错

## 循环导入问题

a.py

```python
from B import b
def a():
 pass
# some codes
```

b.py

```python
from A import a
def b():
 a.a()
# some codes
```

这样就产生了循环import的问题。

解决循环import的方法主要有几种。
上策，拆成不循环引用的
中策，把import放到最后
下册，在函数里面import

1.延迟导入(lazy import)
即把import语句写在方法或函数里面，将它的作用域限制在局部。
这种方法的缺点就是会有性能问题。

2.将from xxx import yyy改成import xxx;xxx.yyy来访问的形式

3.组织代码
出现循环import的问题往往意味着代码的布局有问题。
可以合并或者分离竞争资源。
合并的话就是都写到一个文件里面去。
分离的话就是把需要import的资源提取到一个第三方文件去。
总之就是 将循环变成单向。

# 包管理

参考资料：

[花了两天，终于把 Python 的 setup.py 给整明白了](https://zhuanlan.zhihu.com/p/276461821)
[Python Packaging User Guide](https://packaging.python.org/en/latest/)

## 打包和分发工具

**参考**：<https://packaging.python.org/en/latest/tutorials/packaging-projects/>

**1. distutils**

标准库自带，所有后续的打包工具，全部都是基于它进行开发的。
distutils 的精髓在于编写 **setup.py**

**2. setuptools**

setuptools 是一个对于 distutils 的增强选项

目前只需要**关注setuptools**就可以

一个标准目录：

```python
# 示例目录
packaging_tutorial/
├── LICENSE
├── pyproject.toml
├── README.md
├── setup.cfg
├── src/
│   └── example_package/
│       ├── __init__.py
│       └── example.py
└── tests/
```

python包格式：
源码包：zip等，缺点是需要编译安装慢
二进制包：**egg和wheel格式**，egg是setuptools自己的格式，被wheel格式替代

## setup.py参数

python3 setup.py install

setup.py的常用参数

entry_points可以定义启动入口，也就是可以直接./flask运行，比如flask的setup.py
entry_points={"console_scripts": ["flask = flask.cli:main"]},

## 包安装工具

参考：<https://packaging.python.org/en/latest/tutorials/installing-packages/>

easy_install: setuptools自带的安装工具，已经被pip替代
pip可以直接安装源码包和wheel格式二进制包
安装egg方式：python -m easy_install xxx.egg

查看第三方包安装目录：

```python
python -m site
```

# 其他

jupyter 在线python

**魔术方法**：

<https://docs.python.org/zh-cn/3/reference/datamodel.html?highlight>=**init**#object.**init**
<https://zhuanlan.zhihu.com/p/66645590>
TODO:装饰器中的__get__作用
官方特殊方法：<https://docs.python.org/zh-cn/3/reference/datamodel.html#special-method-names>

<https://pycoders-weekly-chinese.readthedocs.io/en/latest/issue6/a-guide-to-pythons-magic-methods.html>

**Python文档docstring**
通过注释，生成文档
代码写完，注释写完，help函数直接输出一份有格式的文档

**官方文档**：<https://www.python.org/dev/peps/pep-0257/>

**语法**：三个引号的字符串"""quotes""", 如果有转义字符用 r"""quotes"""，Unicode用u"""Unicode strings""".
**位置**：模块、函数、类、成员定义的第一句，包的在__init__的开头

**如何查看**：最终会存储在对象的__doc__ 属性
通过help函数查看, help(object)

**代码风格**

官方建议：[Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/)
