# STL组成

6大组件+13个头文件
容器：`<vector> <deque> <list> <queue> <stack> <set> <map>`
迭代器：`<iterator>`
算法：`<algorithm> <numeric>`
函数对象：`<functional>`
适配器：
内存分配器：
c++ 常用头文件: <https://www.cnblogs.com/Yanfang20180701/p/10606258.html>

# 字符串库

头文件：`<string>`
**构造**：

```c++
string s("aaa");
string s = "aaa";
```

**访问**：

```c++
c = str.at(i);
c = str[i];
front(), back()
```

**插入**：

```c++
str.push_back(char c); insert(char); 
str.append(string s);
```

**删除**：

```c++
pop_back();
erase();
```

## 字符操作函数

子串：返回子串，string substr(pos, count)根据位置返回子串
    查找子串：find find_first_of

C字符数组互转
string.c_str(), 获取不能修改的c字符串
string.copy(char* d, cout, pos) 拷贝指定部分到c字符数组，不会自动加结束符'/0'

替换：replace(pos, count, "aa") 指定位置长度，替换为指定c字符串

## 字符串转换

字符串与数值转换，有以下几种方法
除了 C++ I/O 库、 C I/O 库、 C++ 字符串转换函数和 C 字符串转换函数提供的复杂的本地环境依赖的分析器和格式化器外，头文件 <charconv> 提供对于算术类型的轻量、不依赖本地环境、不分配、不抛出的分析器和格式化器。

C++中二进制、字符串、十六进制、十进制之间的转换 <https://blog.csdn.net/MOU_IT/article/details/89060249>

转换为二进制，由于流中只有dec,oct,hex，没有支持bin二进制，所以使用bitset来存储二进制

bitset<8 * sizeof(i)> bin(0x5A);

### C++ I/O 库

```c++
// 十六进制 转换为 八进制
int i = 0;
stringstream ss("0xa");
ss >> std::hex >> i; // 16进制转为10进制数字

ss.str("");
ss.clear();  //注意 stringstream 
ss << std::oct << std::showbase << i; // 10进制转为8进制
cout << ss.str() << endl;
```

### C++ 字符串转换函数

转换字符串为有符号整数： stoi stol stoll
转换字符串为无符号整数： stoul stoull
转换字符串为浮点值： stof stod stold
转换整数或浮点值为字符串: to_string

```c++
// stoi（字符串，起始位置，n进制），将 n 进制的字符串转化为十进制
string str = "0x12";
int i = stoi(str,0,0); // base=0 会自动判断
```

注意：程序中的数字类型都是十进制，所有base都是用来指定字符串中的进制
想要把十进制数字，转换为其他进制的字符串，只能使用stringstream

### C I/O 库

scanf，printf

### C 字符串转换函数

转换字节字符串为浮点值 atof
转换字节字符串为整数值 atoi atol atoll
转换字节字符串为整数值 strtol strtoll
转换字节字符串为无符号整数值 strtoul strtoull
转换字节字符串为浮点值 strtof strtod strtold

字符串练习： TODO:C++ string中的find()函数
<https://www.cnblogs.com/wkfvawl/p/9429128.html>
<https://www.cnblogs.com/wkfvawl/p/9229758.html>

字面量：
转义字符：`\0 空字符(NULL) 000 \n 换行(LF) 010`
data[3] = "aa"; // 字符串字面量后面自带结束符\0
**注意**：空格：`char c = ' ' = 32`，32，空字符：`char c = '\0' = 0;`

c++ string类并不会以\0为结束符，c语言字符串必须是空字符结束

<https://zh.cppreference.com/w/cpp/string/byte>

头文件：`<cctype>`

```c++
isalpha // 是否字母
isalnum // 是否字母或数字
```

# 正则表达式

```c++
string target = "zyy name zyy";

regex patten("zyy", std::regex_constants::ECMAScript | std::regex_constants::icase); //icase忽略大小写，默认ECMAScript文法

// 匹配（Match），target和patten完全匹配， 返回bool
cout << boolalpha << regex_match(target, patten) << endl; // false

// 搜索（Search）, target的某个子序列，是否匹配，返回bool，匹配到立马返回
cout << boolalpha << regex_search(target, patten) << endl; // true
std::smatch sm; // 匹配结果
while(regex_search(target, sm, patten)) // 重复搜索（参阅 std::regex_iterator ）
{
    std::cout << sm.str() << '\n';
    target = sm.suffix(); //第一个匹配结果后面的字符串
}

auto words_begin = 
    std::sregex_iterator(target.begin(), target.end(), patten); // sregex_iterator使用
auto words_end = std::sregex_iterator();

for (std::sregex_iterator i = words_begin; i != words_end; ++i) {
    std::smatch match = *i;                                                 
    std::string match_str = match.str(); 
    std::cout << match_str << '\n';
} 

// 替换（Replace），返回新字符串
cout  << regex_replace(target, patten, "who") << endl; 
```

## 正则文法

std::regex默认使用是ECMAScript文法，这种文法比较好用，且威力强大，常用符号的意义如下：

| 符号 | 意义 |
| --- | --- |
| ^ | 匹配行的开头 |
| $ | 匹配行的结尾 |
| . | 匹配任意单个字符 |
| […] | 匹配[]中的任意一个字符 |
| (…) | 设定分组 |
| \ | 转义字符 |
| \d | 匹配数字[0-9] |
| \D | \d 取反 |
| \w | 匹配字母[a-z]，数字，下划线 |
| \W | \w 取反 |
| \s | 匹配空格 |
| \S | \s 取反 |
| + | 前面的元素重复1次或多次 |
| * | 前面的元素重复任意次 |
| ? | 前面的元素重复0次或1次 |
| {n} | 前面的元素重复n次 |
| {n,} | 前面的元素重复至少n次 |
| {n,m} | 前面的元素重复至少n次，至多m次 |
| `|` | 逻辑或 |

# 输入输出库

1. `<iostream>`: 处理控制台IO，`<fstream>`: 处理命名文件IO，`<sstream>`: 处理内存string的IO
2. IO对象不能拷贝和赋值，对io对象的读写会改变状态，所以传递的流对象引用不能为const
3. 刷新输出缓冲区，endl（换行并刷新）、flush（只刷新）、ends（空字符并刷新）

`cout << unitbuf`无缓冲模式， nounitbuf，正常有缓冲方式

## 输入输出方法

**无格式输入输出**
std::getline(std::cin, string, delim); // 模板函数
cin.getline( char_type*s, count, delim); // 流的成员函数
char c1 = s1.get(); // 读取'H'
char str[5];
s1.get(str, 5);     // 读取 "llo,"

无格式输出：put('a'), write(char*)
**有格式输入输出**
跳过前导空白符,以空白符作为分隔
支持：int, long, double, void*, bool, string, bitset, char*
int n;float f;bool b;
cin >> n >> f >> std::boolalpha >> b;
有格式输出：cout << n << f;

## 输入流

**回车键的作用**：用户键入输入之后按下Enter键，便会将所有刚刚用户输入的一次性全送到缓冲区，而cin便会从输入缓冲区中读取数据。
如果数据不够，则会等待用户继续输入；
如果数据有多余，则将多余的数据存储在输入流缓冲区中（包括回车符），供下次使用。
参考：<https://blog.csdn.net/imkelt/article/details/51925479>

控制台结束输入：
windows下，在输入的空行位置，按ctrl+z可产生EOF
linux下，在输入时，按ctrl+d可产生EOF

cin的成员函数：
**有格式输入**：
`operator>>` ： 提取带格式数据（比如int long等），提取数据时会跳过输入流中的空格、tab键、换行符等空白字符
要提取空格回车等字符，要使用无格式输入
**无格式输入**：
`get`: 读并取走一个字符
`cin.getline(str,n,ch)`: 读一行或读到指定界定符，或全局函数`getline(cin, str)`, 返回是cin
字符串转整数：`sum += std::stoi(line);`

```c++
string line;
while (getline(cin, line)) {
    stringstream ss(line);
    unsigned int sum = 0, num = 0;
    while (ss >> num) {
        sum += num;
    }
    cout << sum << endl;
```

易错点：
`operator>>` 与 getline 混用出错问题：
参加：C++中的cin, cin.getline, getline等混合使用时不能输入直接执行下一行的问题
<https://blog.csdn.net/leowinbow/article/details/82190631>
现象：输入一样后，后面的直接执行了

```c++
#include <iostream>
#include <sstream>
#include <string>
using namespace std;
int main()
{
    int t = 0;
    cin >> t;
    while (t > 0) {
        string line;
        if (getline(cin, line)) {
            stringstream ss(line);
            unsigned a = 0, b = 0;
            ss >> a >> b;
            cout << a + b << endl;
        }
        --t;
    }
    return 0;
}
```

`cin >> a`， cin会在缓存区把回车符保留下来，导致getline读到回车了
解决办法：cin后加一句，cin.get(), 或cin.ignore()，把回车符消耗掉就可以

**对流进行条件判断**
对cin进行条件判断，是判断cin的流状态
当遇到文件结束符和无效输入时，流变无效
windows的文件结束符，ctrl+z

# 容器库

**顺序容器**:
**array**：**底层数组**，固定大小
**vector**：**底层数组**，可动态增大，内存扩展方式：当超过capacity，重新分配更大空间，释放原空间，**导致迭代等失效**
**list**：**双向链表**，插入删除快，不能随机访问
forward_list: **单向链表**
deque：双端队列，**底层双向数组**

**关联容器**:**底层是红黑树**
**map**: 键值对集合，按键排序，键是唯一的。，搜索、移除和插入操作拥有对数复杂度
**set**: 键的集合，同上
multiset, multimap，区别是，可以有多个相同的key

**无序关联容器**: 关联容器前加 unordered，**底层是散列表**

vector和deque的使用场景
<https://blog.csdn.net/sanqima/article/details/103761280>

## vector

```c++
// 创建
vector<int> v(10); // 10 个默认int
vector<int> v(10, 2); // 10 个2
vector<int> v{1,2,4,2};
```

## map

## 容器操作

元素访问：vector，array，map，都支持 [] 和 at
插入：都支持insert和erase，vector支持push_back，string有append(string),和push_back(char);
**关联容器特有的查找**（map set等）：支持 find、count和contains，以及一些二分查找算法，比如 lower_bound，
性能问题：关联容器自带查找函数性能是：性能O(logn)等，算法库提供的性能是O(n)，原因是算法库是顺序查找，容器自带的是二分查找，见：<https://www.cnblogs.com/shangguanpengfei/p/10483347.html>
**字符串string特有的查找**：find(相当于算法库中的search，**单个字符或子串**)  find_first_of（跟算法中的一样，查找任意一个**单个字符**）
以及find_first_not_of， find_last_of, find_last_not_of

考点：
插入：iter = insert(pos, val), 在pos之前插入，返回新插入元素迭代器，map的insert返回的是pair类型
删除：iter = erase(iterator), 删除迭代器指向元素或迭代器的范围，返回pos之后元素的迭代器

vector: 元素访问：at，[]，back(), front()
修改：insert，push_back(),pop_back(),erase

```c++
std::vector<int> vec(3,100);
auto it = vec.begin();
// 元素访问
demo.at(i) // 有越界检查，抛出 std::out_of_range 类型
demo[i] // 没越界检查
auto &item = front(); back(); // 返回最后或最前元素的引用，注意item需要定义为引用类型

// 增加
push_back(); pop_back(); 
emplace_back(); // 原位构造，直接传构造函数的参数
it = vec.insert(it, 200); emplace(); // 在 pos 前插入 value，返回新元素的迭代器，插入会让插入元素后所有迭代器失效

// 删除
iterator erase (pos); // 返回一个指向删除元素所在位置下一个位置的迭代器
auto iter = demo.erase(demo.begin()+1, demo.end() - 2); // 删除一个范围元素，被删除区域下一个位置元素的迭代器
```

map: 元素访问：at(key)，[]，find(key), at(key)key不存在则报错，find是key不存在，返回end迭代器
结论：首选at，安全且高效，map成员函数find和std::find，性能差别大
插入：insert和map[key] = value;区别，insert是没有插入，有就插入失败，[]时没有插入，有就更新。
insert({key,val})，返回一个pair<iter, sucess>, sucess表示插入是否成功，iter表示插入成功的迭代器
map[key]; key不存在则插入，没有给value，则依然插入，使用value的默认构造函数构造
key存在，只是返回对应value的值

erase，2中，参数是迭代器，返回下一个元素迭代器，参数是key，返回被删除元素个数

c++17 结构化绑定： auto [iter, sucess] = map.insert({'c',6})

# 迭代器

基本迭代器：
输入迭代器: 支持：*p, ++p, p++, p!=q, p == q 解引用读取值，不能修改，可赋值给其他变量：`int a = *iter;`
输出迭代器: 支持：同上，唯一区别是：*p可以被修改 `*iter = a;`
正向迭代器：支持：同上，既可以读取也可以修改
双向迭代器：支持：增加--p，p--，可读可改
随机迭代器：支持：增加 算术运算(p+=n, p-=n) 比较运算(`a< b` `a>b`) 随机访问(p[n])

从上向下，功能增强，下面的包含上面的功能
c++ iterator(迭代器)分类及其使用 ： <https://www.cnblogs.com/LonelyEnvoy/p/5909495.html>

各个容器迭代器类型
vector： 随机迭代器，因为是底层是数组
list：双向迭代器，因为是双向链表
deque：随机迭代器，因为底层是数组
map：双向迭代器，因为是二叉树

比如sort的参数，是随机迭代器类型，所以不能用sort对map进行排序

输入输出迭代器：C++输入输出迭代器：<https://blog.csdn.net/weixin_36796040/article/details/79269811>
示例：

```c++
string str("aaabbbbcccccc");
auto iter = ostream_iterator<char>(cout, ";");
copy(str.begin(), str.end(), iter);
```

迭代器分类：<https://www.cnblogs.com/VIPler/p/4272514.html>

vector。deque：随机迭代器
list、set、map：双向迭代器，所以**不支持比较运算**

指针和迭代器一样，支持迭代器大部分操作
比如：*（解引用）、++、==和!=、+n和-n、相减
指针相减结果为：ptrdiff_t 类型
**不支持的**：迭代器或指针相加
**少部分支持**：比较运算

所以：遍历时，尽量使用begin!=end

## 获取迭代器

std::begin(),std::end(),std::size(),std::empty()

## 迭代器操作

void advance(iter, 2) 令迭代器前进给定的距离，直接修改iter，**没有返回值**
iter = distance(iter，iter2) 返回两个迭代器间的距离
iter2 = next(iter) 返回迭代器的下一个，输入不变，返回值iter2指向下一个
prev 令迭代器自减

## 流迭代器

定义于头文件 <iterator>

```c++
std::istringstream str("0.1 0.2 0.3 0.4");
std::partial_sum(std::istream_iterator<double>(str),
                    std::istream_iterator<double>(),
                    std::ostream_iterator<double>(std::cout, " "));
```

尖括号里面是，格式化输入输出的类型，

## 迭代器适配器

<http://c.biancheng.net/stl/iterator_adaptor/>

本章将介绍 5 种迭代器适配器，分别是反向迭代器适配器、插入型迭代器适配器、流迭代器适配器、流缓冲区迭代器适配器、移动迭代器适配器。

初学者完全可以将迭代器适配器视为普通迭代器。之所以称为迭代器适配器，是因为这些迭代器是在输入迭代器、输出迭代器、正向迭代器、双向迭代器或者随机访问迭代器这些基础迭代器的基础上实现的。也就是说，使用迭代器适配器的过程中，其本质就是在操作某种基础迭代器。

不同的迭代器适配器，其功能大不相同，这些知识都会在本章中做详细讲解。
一：插入迭代器适配器
back_insert_iterator 迭代器的应用 ： <https://www.cnblogs.com/luck3368/p/13780600.html>

3个函数模板： back_inserter front_inserter  inserter

```c++
template< class Container >
std::back_insert_iterator<Container> back_inserter( Container& c )
{
    return std::back_insert_iterator<Container>(c);
}

template< class Container >
std::front_insert_iterator<Container> front_inserter( Container& c )
{
    return std::front_insert_iterator<Container>(c);
}
template< class Container >
std::insert_iterator<Container> inserter( Container& c, typename Container::iterator i )
{
    return std::insert_iterator<Container>(c, i);
}
```

示例：

```c++
int main()
{
    std::vector<int> v1{ 1, 2, 3, 4, 5, 6 };
    auto b_inserter = std::back_inserter(v1);
    *b_inserter = 10;

    // 始终在插入点前插入
    vector<int> vec{1,2,3};
    auto iter = inserter(vec, next(vec.begin()));
    *iter = 4;
    *iter = 5;
    for_each(vec.begin(), vec.end(), [](auto i){cout << i;});
    // 14523
}
```

back_insert_iterator 在指定容器的尾部插入新元素，但前提必须是提供有 push_back() 成员方法的容器（包括 vector、deque 和 list）。
front_insert_iterator 在指定容器的头部插入新元素，但前提必须是提供有 push_front() 成员方法的容器（包括 list、deque 和 forward_list）。
insert_iterator 在容器的指定位置之前插入新元素，前提是该容器必须提供有 insert() 成员方法。

实现原理：重载了赋值操作符，对迭代器的赋值，就是调用容器的对应方法，如back_insert_iterator的具体实现如下：

```c++
std::back_inserter
  back_insert_iterator<Container>& operator= (typename Container::value_type&& value)
    { container->push_back(std::move(value)); return *this; }
```

实例：

```c++
#include <iostream>
#include <iterator>
#include <vector>
using namespace std;
int main() {
    //创建一个 vector 容器
    std::vector<int> foo;
    //创建一个可向 foo 容器尾部添加新元素的迭代器
    std::back_insert_iterator< std::vector<int> > back_it(foo);
    //将 5 插入到 foo 的末尾
    back_it = 5;
    //将 4 插入到当前 foo 的末尾
    back_it = 4;
    //将 3 插入到当前 foo 的末尾
    back_it = 3;
    //将 6 插入到当前 foo 的末尾
    back_it = 6;
    //输出 foo 容器中的元素
    for (std::vector<int>::iterator it = foo.begin(); it != foo.end(); ++it)
        std::cout << *it << ' ';
    return 0;
    // 结果： 5 4 3 6
}
```

std::back_inserter(v), v提供push_back操作
std::front_inserter(v), v提供push_front操作

## 迭代器失效

| 容器底层数据结构类型 | 容器 | 内存特点 | 插入 | 删除 | 解法方法 |
| ----- |  ----- |  ----- |  ----- |  ----- |  ----- |
| 数组型 | string，vector，deque，array |  连续内存  |  插入后重新分配空间，则全部失效；不重新分配，则插入点之后的全部失效 |  删除点之后的全部失效 |  insert返回被插入元素迭代器，erase返回删除元素下一个迭代器，string按照index插入返回的是新字符串 |
| 链表型 |  list，forward_list |  内存不连续 |  不失效 |  被删除元素迭代器失效，其他ok |  ----- |
| 树形 |  map，set |  红黑树 |  不失效 |  被删除元素迭代器失效，其他ok |  ----- |
| 哈希 |  unordered_map/set |  哈希表 |  不失效 |  被删除元素迭代器失效，其他ok |  ----- |
| 堆栈 |  stack，queue |  vector或list |  不支持迭代器 |  不支持迭代器 |  ----- |

迭代器失效分三种情况考虑，也是分三种数据结构考虑，分别为数组型，链表型，树型数据结构。

数组型数据结构（string,vector,array,deque）：该数据结构的元素是分配在连续的内存中，insert和erase操作，都会使得删除点和插入点之后的元素挪位置，所以，插入点和删除掉之后的迭代器全部失效，也就是说insert(*iter)(或erase(*iter))，然后在iter++，是没有意义的。解决方法：erase(*iter)的返回值是下一个有效迭代器的值。 iter =cont.erase(iter);

链表型数据结构(list)：对于list型的数据结构，使用了不连续分配的内存，删除运算使指向删除位置的迭代器失效，但是不会失效其他迭代器.解决办法两种，erase(*iter)会返回下一个有效迭代器的值，或者erase(iter++).

树形数据结构(map)： 使用红黑树来存储数据，插入不会使得任何迭代器失效；删除运算使指向删除位置的迭代器失效，但是不会失效其他迭代器.erase迭代器只是被删元素的迭代器失效，但是返回值为void，所以要采用erase(iter++)的方式删除迭代器。

注意：经过erase(iter)之后的迭代器完全失效，该迭代器iter不能参与任何运算，包括iter++,*ite

<https://github.com/huihut/interview/tree/master/STL#array>

## 编写通用算法

如何利用迭代器编写通用算法？

C++ STL 迭代器Iterator、五种迭代器类别 <https://blog.csdn.net/Johnsonjjj/article/details/107783210>

C++自定义迭代器（STL自定义迭代器)的实现详解 <http://c.biancheng.net/view/471.html>

要通过迭代器获取类型，通过stl萃取

```c++
#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>
#include <forward_list>
 
template <class ForwardIt>
 void quicksort(ForwardIt first, ForwardIt last)
 {
    if(first == last) return;
    auto pivot = *std::next(first, std::distance(first,last)/2);
    ForwardIt middle1 = std::partition(first, last, 
                         [pivot](const auto& em){ return em < pivot; });
    ForwardIt middle2 = std::partition(middle1, last, 
                         [pivot](const auto& em){ return !(pivot < em); });
    quicksort(first, middle1);
    quicksort(middle2, last);
 }
 
int main()
{
    std::vector<int> v = {0,1,2,3,4,5,6,7,8,9};
    std::cout << "Original vector:\n    ";
    for(int elem : v) std::cout << elem << ' ';
 
    auto it = std::partition(v.begin(), v.end(), [](int i){return i % 2 == 0;});
 
    std::cout << "\nPartitioned vector:\n    ";
    std::copy(std::begin(v), it, std::ostream_iterator<int>(std::cout, " "));
    std::cout << " * " " ";
    std::copy(it, std::end(v), std::ostream_iterator<int>(std::cout, " "));
 
    std::forward_list<int> fl = {1, 30, -4, 3, 5, -4, 1, 6, -8, 2, -5, 64, 1, 92};
    std::cout << "\nUnsorted list:\n    ";
    for(int n : fl) std::cout << n << ' ';
    std::cout << '\n';  
 
    quicksort(std::begin(fl), std::end(fl));
    std::cout << "Sorted using quicksort:\n    ";
    for(int fi : fl) std::cout << fi << ' ';
    std::cout << '\n';
}
```

# 算法库

`#include<algorithm>`：algorithm意为"算法",是C++的标准模版库（STL）中最重要的头文件之一，提供了**大量基于迭代器**的非成员模版函数。
stl常用算法：<http://c.biancheng.net/stl/algorithms/>

算法要注意算法使用条件：输入参数的迭代器类型
要求的迭代器类型，跟算法实现密切相关，
sort partial_sort stable_sort，需要随机迭代器，因为sort快速排序

算法名称中的后缀
后缀：n：限制第一个范围，两种方式，first和end，或first+n
后缀：if，需要添加谓词，一般作用于第一个范围
后缀：copy，一般是把处理结果复制到新的地方

erase，remove* unique 区别 C++之erase、remove 、remove_if的区别
erase是真删除，remove只是移动元素，返回新范围的尾后迭代器，remove完还需要erase
unique去除相邻重复元素，也是返回尾后迭代器，一般sort，unique，erase，unique+erase可以用unique_copy实现
<https://blog.csdn.net/aixiaodeshushu/article/details/85041599>

## 按功能分

1，遍历
for_each
for_each_n (C++17)

2，查找
**顺序查找**分为三个功能
1，查找单个元素，find, find_if, find_if_not
2, 查找多个元素中任意一个，find_first_of
3, 查找一个子序列， search ， search_n 第一次出现的位置, find_end 最后一次出现的位置,
比如：
序列 A：5,8,1,2,3,4,5,1,2,3,4,5
序列 B：1,2,3
B序列，在A序列中出现2次，用search返回位置2，用find_end返回5迭代器

对于字符串自带的查找算法
单个字符或子串查找，都用find, rfind反向查找
多个字符任意一个，find_first_of，find_last_of

adjacent_find: 在范围 [first, last) 中搜索二个相继的相等元素。

**二分查找**：基于已排序的查找

顺序查找是找某一确定元素或多个元素，二分查找可以找大于等于小于某个元素的迭代器
其次，性能更好
1， 大于等于给定值，lower_bound ,返回第一个大于等于的迭代器
2， 大于给定值，upper_bound ， 返回第一个大于的迭代器
3， 等于给定值，equal_range(value) 等于value的范围，返回pair，first指向大于等于，second是大于
4， 给定值是否存在，binary_search

3，统计
返回范围 [first, last) 中满足特定判别标准的元素数。
count(first,last,value)
count_if(first,last,comp) (在ｃｏｍｐ为ｔｒｕｅ的情况下计数）

4，划分，排序, 合并

各种排序算法
sort : 快速排序，不稳定，**要求随机迭代器**
partial_sort : 部分元素排序， partial_sort_copy,返回新范围的尾后迭代器
stable_sort : 稳定排序，归并排序

合并两个已排序序列到新范围： merge

6，拷贝与替换

拷贝: 复制到指定新位置 copy, copy_if, copy_n, copy_backward(倒着拷贝)
变换拷贝：std::transform 应用给定的函数到范围并存储结果于始于 d_first 的另一范围。
这两个都需要一个std::back_inserter(vector)

删除：  remove(s,e,value) 把==value的移到最后，返回新范围的尾后迭代器，remove_if
remove_copy(s,e,d,value),拷贝[s,e)到d开始的范围，跳过等于value的
remove_copy 功能上跟copy_if一样，都实现按条件拷贝，但是copy_if功能更强大，remove_copy只能跳过特定值
去重：unique，原地去重相邻元素，范围新范围的尾后迭代器，unique_copy, 去重后拷贝到新位置，就不用erase了

rlavue = move(lvalue): 将一个左值转换为右值引用
移动：move(first,last,dfirst)，将一个范围内的元素移动到新位置，move_backword, 从后向前移动
交换：swap交换2个值，iter_swap 交换两个迭代器所指向的元素，swap_ranges(first,last,dfirst)交换2个范围

更新： replace,replace_if和replace_copy函数用法详解

反转顺序：std::reverse， reverse_copy ，反转后放到新位置

赋值：
fill， fill_n，在给定序列上，赋值**给定值**
generate， generate_n，在给定序列上，赋值**函数对象生成**的值

7，算术生成

递增填充 ： std::iota
对序列求和(或应用二元谓词比如乘)： std::accumulate，返回值

8，集合运算

差集：set_difference
并集：set_union
交集：set_intersection
子序列：includes

9， 比较两个序列

C++ equal(InputIt1 first1, InputIt1 last1, InputIt2 first2)， 用于比较两个范围是否相同，比较last1-first1次
C++ mismatch, 返回两个范围中首个不相等的元素迭代器，auto [iter1, iter2] = mismatch()
C++(STL) all_of、any_of及none_of算法详解,

10 最大最小操作
max(value1,value2), 返回几个值或初始化列表中的最大的
max_elment(begin,end) 返回一个范围中的最大的
minmax_element 返回pair，first指向min，second指向max
min minmax max_elment min_element minmax_element

## 按实现分

**在一个范围上遍历**
输入，first，last
for (; first != last;) {}
输入，first，n
for (int i = 0; i < n; ++i, ++first) {}
adjacent_find，找第一个相邻相等元素的首元素
做法：双指针，next，prev，循环next，prev跟着走，++next; while(next!=last) if (*next ==*first) break;
reverse：范围逆序，对撞指针，while(first < last) iter_swap(first,last); ++first;--last
**在两个范围遍历**
输入，first1，last1，first2，last2
同步增长，比如mismatch算法，找到第一个不匹配的元素
while(first1 != last1 && first2 != last2) first1++; first2++;

不同步，比如search，找到范围1中找到第一个子范围的首元素
一般使用2层for循环，每取出一个元素，然后再范围2中同步比较
find_end，找到范围1中找到最后一个子范围的首元素
while(true) 循环调用search，不断修改first

# 智能指针

现代 C++：一文读懂智能指针
https://zhuanlan.zhihu.com/p/150555165

静态内存：局部static对象，类static数据成员，和所有函数外部变量
栈内存：函数内部非static变量
堆内存：动态分配的对象，new，malloc对象

静态内存和栈内存对象，由编译器自动创建和销毁
堆内存对象，由程序员创建销毁

动态内存极易出现的问题：
1.内存泄漏，new完，忘记delete
2.野指针，没有初始化，指向随机内存地址
3.悬空指针，delete完，没有赋值为空

如何避免：
1.使用前ptr=nullptr
2.delete完，再次ptr=nullptr

智能指针是类，利用栈退出时临时变量自动销毁的原理，在析构函数中释放资源，避免内存泄漏

1. unique_ptr：没有复制和赋值构造函数，同一时间只有一个智能指针指向对象
2. shared_ptr：采用引用计数，当计数为 0，销毁
3. weak_ptr：解决 shared_ptr 循环引用导致资源泄漏，weak_ptr 由 shared_ptr 创建，但是不影响引用计数
4. auto_ptr(C++17 中移除)

## shared_ptr

**使用make_shared来生成智能指针**
注意：内置指针不能转换为指针指针

```c++
shared_ptr<int> p = new int(12); //错误，必须直接初始化
shared_ptr<int> getp() {
    return new int(12); // 错误
}
```

```c++
// 空构造，内部是nullptr, p==nullptr
shared_ptr<T> p;
shared_ptr<T> p(nullptr);
// 构造
shared_ptr<Test> p = make_shared<Test>(args); // args 是Test类构造函数参数
shared_ptr<Test> p2(new Test()); // 管理普通指针
shared_ptr<Test> p3(p2); // 拷贝构造， p2引用计数+1
// 指向其他对象
p3 = p; // 赋值操作，p3的引用计数--， p引用计数++
p3.reset(); // p3指向的对象引用计数-1，且 p3=nullptr; 
P3.reset(new Test()); //指向新对象，跟赋值操作区别是，reset的参数只能是内置指针
```

使用引用计数管理内容导致的循环引用，造成内存泄漏

```c++
#include <iostream>
#include <memory>
#include <vector>
using namespace std;

class ClassB;

class ClassA
{
public:
    ClassA() { cout << "ClassA Constructor..." << endl; }
    ~ClassA() { cout << "ClassA Destructor..." << endl; }
    shared_ptr<ClassB> pb;  // 在A中引用B
};

class ClassB
{
public:
    ClassB() { cout << "ClassB Constructor..." << endl; }
    ~ClassB() { cout << "ClassB Destructor..." << endl; }
    shared_ptr<ClassA> pa;  // 在B中引用A
};

int main() {
    shared_ptr<ClassA> spa = make_shared<ClassA>();
    shared_ptr<ClassB> spb = make_shared<ClassB>();
    spa->pb = spb;
    spb->pa = spa;
    // 函数结束，思考一下：spa和spb会释放资源么？
}
```

输出：
ClassA Constructor...
ClassB Constructor...
Program ended with exit code: 0

为了解决类似这样的问题，C++11引入了weak_ptr，来打破这种循环引用。

## weak_ptr

weak_ptr 是 shared_ptr 的查看器，我们可以获取 shared_ptr 指针的一些状态信息，比如有多少指向相同的 shared_ptr 指针、shared_ptr 指针指向的堆内存是否已经被释放等等。

没有重载 * 和 -> 运算符， weak_ptr 类型指针只能访问所指的堆内存，而无法修改它

```c++
// 构造
weak_ptr<Test> p; // 空指针
shared_ptr<Test> s = make_shared<Test>();
weak_ptr<Test> p2(p); // 从其他weak_ptr 构造
weak_ptr<Test> p3(s); // 从其他shared_ptr 构造
// 查看状态
int cnt = p.use_count(); 
p.expired(); // 是否被释放
// 创建管理被引用的对象的shared_ptr
if (shared_ptr<A> pa = wp.lock())
{
    cout << pa->a << endl;
}
else
{
    cout << "wp指向对象为空" << endl;
}
```

## unique_ptr

独占控制对象，引用计数是0或1
区别是**不支持拷贝和赋值操作**（显式删除其左值复制构造函数和左值赋值运算符）
但是，unique_ptr 的赋值运算符只接受典型地由 std::move 生成的右值

```c++
unique_ptr<Test> p(new Test());
unique_ptr<Test> p2 = make_unique<Test>(12,12);
// 指向其他对象
p2 = move(p); // 支持右值赋值运算符
p2.reset(new Test());
p2.reset(p.release()); // p=nullptr
```

# 并发编程基础

参考：<https://www.jianshu.com/p/e70f240a46f6>
<https://www.sohu.com/a/461156788_355142>

为了充分利用多核多cpu，可以利用多线程技术进行并发编程提高性能
但是多线程会带来如下问题

一：并发问题：安全性问题
解决方法：
    一： 线程之间不使用共享变量
    二： 共享变量，不可修改
    三： 使用同步机制访问共享资源
前两个从根本上避免了并发问题，第三个通过同步机制解决并发问题
解决并发问题关注三个方面：
    一：原子性，一个或多个操作，要么全执行，要么都不执行，执行过程不被打断
        比如 ： i++; 就不是原子性，包含三个操作，读取i的值，将i加1，将值赋给i；都可能被其他线程打断
    二：可见性，一个线程修改了变量的值，其他线程可以立即看到
    三：有序性，不会因为重排序的问题导致结果不可预知

二： 可见性问题：死锁，活锁，饥饿，
死锁：比如，A线程拥有a锁，请求b锁，B线程拥有b锁，请求a锁，相互等待
或，在unlock前抛出异常导致永远没有unlock，其他线程死等
三： 性能问题：线程切换或不正确加锁

# 多线程

请参考：[linux进程和线程](../Linux开发/linux进程和线程.md)

## 线程

```c++
#include <thread>
void fun(){}
void fun2(int n, int &n2){}
int main()
{
    int n = 0;
    thread thd(fun); // 没有参数
    thread thd2(fun2, n, ref(n)); // 两种传参方式，值传递，和引用传递(必须用ref包装)
    thd2.join(); // 当前线程阻塞，等待thd2线程执行完成
    thd2.detach(); // 让thd2在后台独立运行，主线程结束也不影响
    std::this_thread::sleep_for(200ms); // 休眠200ms
}
```

## mutex

**原理**：mutex本质分为两个部分：
一：原子的 "compare and set" 操作
锁本质就是一个普通的内存变量，关键在于对这个变量的"判断并置位"的原子性
实现方法：cpu硬件指令，锁总线，关中断
二：根据锁是否获得，后续策略
没获得锁，挂起等待，或者反复检查(自旋锁)

**涉及的角色**：
一：共享资源，比如变量，打印机等
二：mutex，互斥锁
三：操作共享资源的线程，流程为
获取锁、资源操作、释放锁

**mutex有两大类**
mutex ： 其他有timed_mutex， recursive_mutex recursive_timed_mutex
shared_mutex ： 其他有 shared_timed_mutex， 增加了lock_shared函数

**成员函数**
try_lock： 1. 没lock，则lock后继续执行。2. 被其他线程lock，则返回false。3. 被自己lock，则产生死锁(deadlock)
lock : 1. 没lock，则lock后继续执行。2. 被其他线程lock，则当前线程阻塞等待。3. 被自己lock，产生死锁(deadlock)

共同的缺点：当自己重复lock时，会死锁
解决办法：使用recursive_mutex，比如保护类中的共享状态，而类的成员函数可能相互调用

**更方便的mutex包装器**

在使用锁时应避免发生死锁(Deadlock)
死锁发生情况一：unlock前抛出异常或者某一分支返回，则永远没有释放，其他线程会死等
解决办法：使用RAII管理互斥对象，析构时自动unlock

BasicLockable基本可锁定：提供lock，unlock方法
Lockable ： 增加 try_lock
TimedLockable： try_lock_for try_lock_until

c++ 11： lock_guard/unique_lock详解 ： <https://blog.csdn.net/zzhongcy/article/details/85230200>

**lock_guard**：对BasicLockable最简单的包装，只有lock，unlock，try_lock方法

```c++
mutex m;
lock_guard<std::mutex> lock(m); //构造函数中自动lock
m.lock();
lock_guard<mutex> lock1( m, adopt_lock ); // adopt_lock表示m已lock
```

**unique_lock**：功能更丰富
上锁/解锁操作：lock，try_lock，try_lock_for，try_lock_until 和unlock
获得属性：owns_lock、operator bool ，判断是否获得了锁

```c++
mutex m;
unique_lock<std::mutex> lock(m); //构造函数中自动lock
m.lock();
unique_lock<mutex> lock1( m, adopt_lock ); // adopt_lock表示m已lock
```

**scoped_lock**
**shared_lock**

**对多个互斥对象加锁**

某些情况需要lock两个以上互斥对象，如果加锁顺序有问题，可能发生死锁
解决办法：
std::lock
std::try_lock

**总结**：
C++里各种mutex与lock： <https://zhuanlan.zhihu.com/p/347576523>

## 条件变量

参考：C++同步机制之条件变量（std::condition_variable）
<https://blog.csdn.net/wxj1992/article/details/116888582?spm=1001.2014.3001.5502>

**涉及的角色**：
一：条件，普通bool变量，线程根据条件是否满足，决定等待还是进行相关处理
二：condition_variable变量，线程等待和通知的媒介
三：wait线程流程，加锁，判断条件，不满足时需要在condition_variable等待（通过wait传一个谓词或while循环测试+wait），满足则继续执行
四：notify线程，加锁，修改条件变量，通过condition_variable通知等待线程
五：mutex互斥锁，判断条件是否满足并等待，需要通过mutex保证原子性

条件变量wait必须跟unique_lock一起使用

C++11:为什么 std::condition_variable 使用 std::unique_lock？
<https://www.coder.work/article/12943>

wait函数 ： wait 、 wait_for ， wait_until
notify函数 ： notify_one 、 notify_all

## 异步结果

C++并发与多线程学习笔记--future成员函数、shared_future、atomic
<https://www.1024sou.com/article/878408.html>

# 多线程编程练习

## 1114. 按序打印

题目链接： <https://leetcode-cn.com/problems/print-in-order/>

给你一个类：

public class Foo {
  public void first() { print("first"); }
  public void second() { print("second"); }
  public void third() { print("third"); }
}
三个不同的线程 A、B、C 将会共用一个 Foo 实例。

线程 A 将会调用 first() 方法
线程 B 将会调用 second() 方法
线程 C 将会调用 third() 方法
请设计修改程序，以确保 second() 方法在 first() 方法之后被执行，third() 方法在 second() 方法之后被执行。

提示：

尽管输入中的数字似乎暗示了顺序，但是我们并不保证线程在操作系统中的调度顺序。
你看到的输入格式主要是为了确保测试的全面性。

输入：nums = [1,2,3]
输出："firstsecondthird"
解释：
有三个线程会被异步启动。输入 [1,2,3] 表示线程 A 将会调用 first() 方法，线程 B 将会调用 second() 方法，线程 C 将会调用 third() 方法。正确的输出是 "firstsecondthird"。

输入：nums = [1,3,2]
输出："firstsecondthird"
解释：
输入 [1,3,2] 表示线程 A 将会调用 first() 方法，线程 B 将会调用 third() 方法，线程 C 将会调用 second() 方法。正确的输出是 "firstsecondthird"。

实现思路：条件变量
条件：int=1 线程1运行，其他等待，执行结束int+1，int=2,2打印，其他等待

```c++
class Foo {
public:
    int condition = 1;
    condition_variable cv;
    mutex m;
    Foo() {
    }

    void first(function<void()> printFirst) {
        unique_lock<mutex> lock(m);
        cv.wait(lock, [this](){return this->condition == 1;});

        // printFirst() outputs "first". Do not change or remove this line.
        printFirst();
        this->condition += 1;
        lock.unlock();
        cv.notify_all();
    }

    void second(function<void()> printSecond) {
        
        unique_lock<mutex> lock(m);
        cv.wait(lock, [this](){return this->condition == 2;});

        // printFirst() outputs "first". Do not change or remove this line.
        printSecond();
        this->condition += 1;
        lock.unlock();
        cv.notify_all();
    }

    void third(function<void()> printThird) {
        
        unique_lock<mutex> lock(m);
        cv.wait(lock, [this](){return this->condition == 3;});

        // printFirst() outputs "first". Do not change or remove this line.
        printThird();
        this->condition = 1;
        lock.unlock();
        cv.notify_all();
    }
};
```

## 1115. 交替打印 FooBar

题目链接： <https://leetcode-cn.com/problems/print-foobar-alternately/>

给你一个类：

class FooBar {
  public void foo() {
    for (int i = 0; i < n; i++) {
      print("foo");
    }
  }

  public void bar() {
    for (int i = 0; i < n; i++) {
      print("bar");
    }
  }
}
两个不同的线程将会共用一个 FooBar 实例：

线程 A 将会调用 foo() 方法，而
线程 B 将会调用 bar() 方法
请设计修改程序，以确保 "foobar" 被输出 n 次。

输入：n = 1
输出："foobar"
解释：这里有两个线程被异步启动。其中一个调用 foo() 方法, 另一个调用 bar() 方法，"foobar" 将被输出一次。

输入：n = 2
输出："foobarfoobar"
解释："foobar" 将被输出两次。

```c++
class FooBar {
private:
    int n;
    int condition = 1;
    condition_variable cv;
    mutex m;

public:
    FooBar(int n) {
        this->n = n;
    }

    void foo(function<void()> printFoo) {
        
        for (int i = 0; i < n; i++) {
            
         // printFoo() outputs "foo". Do not change or remove this line.
            unique_lock<mutex> lk(m);
            cv.wait(lk, [this](){return this->condition == 1;});
         printFoo();
            this->condition = 2;
            lk.unlock();
            cv.notify_all();
        }
    }

    void bar(function<void()> printBar) {
        
        for (int i = 0; i < n; i++) {
            
         // printBar() outputs "bar". Do not change or remove this line.
         // printBar();

            unique_lock<mutex> lk(m);
            cv.wait(lk, [this](){return this->condition == 2;});
         printBar();
            this->condition = 1;
            lk.unlock();
            cv.notify_all();
        }
    }
};

void printFoo()
{
    cout << "foo";
}
void printBar()
{
    cout << "bar" << endl;;
}

int main()
{
    vector<thread> t_vec;
    FooBar data(100);
    t_vec.push_back(thread(&FooBar::foo, &data, &printFoo));
    t_vec.push_back(thread(&FooBar::bar, &data, &printBar));
    t_vec[0].join();
    t_vec[1].join();
    return 0;
}

```

# 牛客网在线编程

## 输入输出专练

在线编程输入输出练习：<https://ac.nowcoder.com/acm/contest/5657#question>

总结：

1. getline用法，读到换行符，或者指定的界定符

用于一行一行读，或者用特定界定符分割字符串，比如`a,f,fdsa,hfda`，使用`getline(ss, str, ',')`分割

注意有2个getline
一。`<string>`, 函数模板，std::getline(cin, string)，可以**接受string类**
<https://qingcms.gitee.io/cppreference/20210212/zh/cpp/string/basic_string/getline.html>
二。`basic_istream` 输入流的成员函数,cin.getline(char *, size);只能接受**原生字符数组**

1. 字符串流stringstream，`stringstream ss(str);`,然后就跟cin一样格式化读取或getline
2. sort排序函数

TODO:
c++中字符串分割方法
<https://blog.csdn.net/birenxiaofeigg/article/details/115464934>
<https://www.jianshu.com/p/5876a9f49413>

## STL

<https://www.nowcoder.com/exam/oj?tab=%E8%AF%AD%E6%B3%95%E7%AF%87&topicId=225>
algorithm 算法总结：
一：遍历算法，`std::for_each()` 可以代替for循环
二：set。map遍历，迭代器不能用`<`，只能用`!=`，因为set map的迭代是双向迭代器（双向迭代器不支持比较，只有随机迭代器可以）
三：set map只有insert，没有push_back ,
四，map 的 [key] ， 当key不存在时，value进行默认初始化，比如`++map[key];` 实现插入并增加个数
五。set 自带的 lower_bound 和 upper_bound 的时间复杂度为 O(logn)。 algorithm 库中的 lower_bound 和 upper_bound 函数对 set 中的元素进行查询，时间复杂度为 0(n)

但使用 algorithm 库中的 lower_bound 和 upper_bound 函数对 set 中的元素进行查询，时间复杂度为 0(n)。

总结：对于可随机访问的有序容器使用 algorithm 库中的 lower_bound 和 upper_bound 函数时间复杂度为O(logn)，

但对于set，multiset这种不能随机访问的有序容器，要用其自带的 lower_bound 和 upper_bound 的时间复杂度才为 O(logn)。
————————————————
版权声明：本文为CSDN博主「华-山」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：<https://blog.csdn.net/mountain_hua/article/details/115790311>
六。sort排序，默认升序，即`[](auto v1, auto v2){return v1 < v2;}`，v1是前一个元素，v2是后一元素
unique在排序好上面，去重，去掉相邻相同的元素

七。进制转换，使用c++流控制
<https://blog.csdn.net/vir_lee/article/details/80645066>

# STL知识点练习

C++知识总结——STL容器的常用用法（持续更新）
<https://zhuanlan.zhihu.com/p/112810801>

TODO
多线程
线程同步，锁
单例模式
