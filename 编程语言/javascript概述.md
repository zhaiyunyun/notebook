# javascript 介绍

javascript 语言特性：动态语言，动态类型，解释执行

1. 数据类型
   原始类型：boolean、number、string、undefined、null
   引用类型：object 和（es6 新增类型）symbol、bigint
   注：引用类型变量里存储的是指针

2. 执行环境和作用域
   js 函数执行环境是一个对象，此对象包含了这个函数中声明的变量或函数，函数的执行环境串联起来组成了类似 c 语言中的调用堆栈。
   理解执行环境，也便理解变量作用域，也就明白所谓变量或者函数提升。
   在浏览器中，最外层的执行环境就是 windows 对象

3. new 和 this
   this 是动态变化的，始终指向调用此函数的对象
   new 先创建一个空对象，然后在空对象上调用构造函数

   ```javascript
   function test() {
     console.log(this);
   }
   var t2 = new test();
   t2.aa = 3;
   test();
   ```

   调用 test()，在全局对象上，所以 this 指向 windows 对象
   而使用 new test()，在新创建的 t2 对象，所以 this 指向 t2 对象

4. 构造函数，原型对象，实例的关系
   通过 new 调用的函数就是构造函数，如果没有通过 new 操作符来调用的，就是普通函数
   原型对象和实例的关系如图，此图来自于 JavaScript 高级编程
   ![JavaScript原型对象关系](../images/JavaScript原型对象关系.png)
   Person 是定义的函数对象，默认有一个原型对象，所有通过 new Person 构造的对象实例内部也有原型对象，共享 Person 函数的原型对象。

5. 两个操作符
   typeof 操作符，区别原生类型或者 Object 类型，但如果想具体分辨是 Array 还是 Number 等引用类型，则需要 instanceof 操作符。
   instanceof 原理：

   ```javascript
   function _instanceof(L, R) {
     //L为instanceof左表达式，R为右表达式
     let Ro = R.prototype; //原型
     L = L.__proto__; //隐式原型
     while (true) {
       if (L === null) {
         //当到达L原型链顶端还未匹配，返回false
         return false;
       }
       if (L === Ro) {
         //全等时，返回true
         return true;
       }
       L = L.__proto__;
     }
   }
   ```

js 手册：
[MDN 手册](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript)
