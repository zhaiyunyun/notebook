# 区别

**变量含义**
[python基本概念](./python基础.md#核心概念)
python，变量是指针，指向一个对象
c++，变量定义分配存储空间

TODO: c++和python 变量定义的语法形式

**作用域**
[python作用域](./python基础.md#命名空间和作用域)
[c++名字查找和命名空间](./cpp语言核心.md#名字查找和命名空间)

c++中有**块作用域**（符合语句和大括号中），python没有

# 语法区别

## c++常用语句

**类型定义**

```c++

// 基本类型，包括算术类型(bool, 字符类型char，整数类型int)、浮点类型（double）
char a = 'f';
int i = 10;
double = d = 12.32;

// 复合类型
// 函数类型
return_type function_name( parameter list ){}

// 数组类型
type arrayName[size];

// 引用类型，对象别名
std::string &r1 = "sss"; //左值引用
std::string &&r2 = r1 + "222"; //右值引用

// 指针类型（包括对象指针，函数指针，成员函数指针，数据成员指针），nullptr：空指针字面量
int n;
int* np = &n; // 指向 int 的指针
void f(int);
void (*p1)(int) = &f; // 函数指针

// 类类型
class Dog : public Animal {
    // bark() 函数
};

// 枚举和联合体
enum Color { red, green, blue };

```

**控制语句**

```c++
// 范围for语句
for (auto &&i : v)
{
    
}
while (/* condition */)
{
    /* code */
}
do {

} while()
for (size_t i = 0; i < count; i++)
{
    /* code */
}
```

**标准库STL**

```c++
// 字符串类，替代原生字符串
std::string s3("valuee");

// array，静态的连续数组
std::array<double, 10> values {0.5,1.0,1.5,,2.0};

// vector，动态的连续数组
std::vector<int> v = {7, 5, 16, 8};

// map, 
std::map<std::string, int>myMap{std::make_pair("C语言教程",10),std::make_pair("STL教程",20)};
```

## python常用语句

**基本类型**

```python
n = 10 #数字
s = 'sss' #字符串
l = [1, 2] #列表
t1, t2 = (12,), tuple() #元组
s1, s2 = set(iterator), {value01,value02,...} # 集合
m1, m2 = {"a":2, "b":3}, dict() #字典
```

**控制语句**

```python
if condition_1: # 一般不加括号，跟c++不一样
    statement_block_1
elif condition_2:
    statement_block_2
else:
    statement_block_3

while 判断条件(condition)：
    执行语句(statements)……
for <variable> in <sequence>:
    <statements>
else:
    <statements>
```

# 内置函数

**lambda**:
c++ : `[](auto &x){return x++;}`
python: `lamba x: x+=1`

**map**:
c++: `std::transform(v.begin(), v.end(), [](auto &n){return ++n;})`
python: `map(lambda n: n+=1, v)`

**reduce**:
c++: ``
python: `reduce(lambda x, y: x+y, [1,2,3,4,5])`

**sort**:
c++: `std::sort(s.begin(), s.end(), [](int a, int b) {return b < a;});`
python: `b = sorted([2,3,5,3,2,3,5,3,2])  # 保留原列表`

**filter**:
c++: ``
python: `new_data = filter(lambda x: x % 2 == 0, range(10))`

总结：
python map filter sorted返回新列表，不修改原数据
c++ sort会修改原数组

C++里没有map，filter，reduce，这是python的名字
C++的等价物是transform、copy_if、accumulate
