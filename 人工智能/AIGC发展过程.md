快速发展阶段（2010s-至今） ，深度学习模型不断迭代，AIGC 取得突破性进展。尤其在 2022 年，算法获得井喷式发展，底层技术的突破也使得 AIGC 商业落地成为可能。下面列举了一些迄今为止具有代表性的算法模型，其中主要集中在 AI 绘画领域：

2014 年 6 月，生成式对抗网络（Generative Adversarial Network， GAN ）被提出。
2021 年 2 月，openAI 推出了 CLIP （Contrastive Language-Image Pre-Training）多模态预训练模型。
2022 年，扩散模型 Diffusion Model 逐渐替代 GAN。
上述三个算法是当前 AIGC 的技术核心，下述算法模型大部分都是以此为基础。

2018 年 12 月，NVIDIA 推出 StyIeGAN ，可以自动生成高分辨率图片。目前已升级到第四代模型。
2019 年 7 月，DeepMind 推出 DVD-GAN ，可以生成连续视频。
2021 年 1 月，OpenAI 推出 DALL-E ，是首个引起公众广泛关注的文本生成图像的模型之一。
2022 年 2 月，开源 AI 绘画工具 Disco Diffusion 发布。
2022 年 3 月，Meta 推出 Make-A-Scene 这一 AI 图像生成工具。
2022 年 4 月，OpenAI 推出了 DALL-E-2 ，在图像分辨率、真实感和新功能上进行了升级。
2022 年 4 月，AI 绘画工具 MidJourney 发布。
2022 年 5 月，Google 推出 Imagen ，同样是文本生成图像的模型。
2022 年 6 月，Google 推出 parti ，与 Imagen 功能相同，但在模型算法、模型参数和图像效果等方面做了升级。
2022 年 7 月，开源 AI 绘画工具 Stable Diffusion 发布。
2022 年 9 月，Meta 推出 Make-A-Video ，可以从文字生成视频。
2022 年 10 月，Google 提出 Imagen video ，同样是文字生成视频的模型。
2022 年 11 月，Stable Diffusion 2.0 发布，在模型算法、图像质量和内容过滤等方面做了升级。
2022 年 11 月，openAI 推出 AI 聊天机器人 chatGPT 。
关于上述算法模型的具体原理和分析，可以期待我们的下一篇文章哦