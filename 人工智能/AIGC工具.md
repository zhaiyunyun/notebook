# 文本生成

典型的产品或算法模型包括 JasperAI、Copy.ai、彩云小梦、AI dungeon、ChatGPT 等。

## AI辅助编程

典型的产品或算法模型包括 GitHub Copilot、Replit、CodeGeeX、Mintlify 等。

[CodeGeeX](https://codegeex.cn/zh-CN): **免费**的AI编程助手
功能：代码生成，补全，翻译

[GitHub Copilot]:

Github Copilot、

Codeium、

Tabnine、

Replit Ghostwriter

和Amazon CodeWhisperer

# 图片生成

典型的产品或算法模型包括 EditGAN、Deepfake、DALL-E、MidJourney、Stable Diffusion、文心·一格等。

# 视频生成

典型的产品或算法模型包括 Deepfake、videoGPT、Gliacloud、Make-A-Video、Imagen video 等。

# 音频生成

典型的产品或算法模型包括 DeepMusic、WaveNet、Deep Voice、MusicAutoBot 等。

# 3D生成

# 文字生成图片

cogview

<https://open.bigmodel.cn/>

# 文字生成视频

cogvideo

<https://open.bigmodel.cn/>

# AIGC学习资源

https://www.learnprompt.pro/
