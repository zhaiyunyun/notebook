# Pylot编译安装及运行

根据本地测试，以docker方式运行会出现初始化时间过长等问题。通过本地编译的方式部署的Pylot可以正常运行。

## 环境配置

本地开发环境：

- GPU: Nvidia RTX 3080
- Driver Version: 460.73.01
- CUDA Version: 11.1
- Python version: 3.6.9（必须）

> 安装过程中需要从国际互联网下载很多文件，请保证已连接国际互联网

## 安装过程

根据官方文档操作：<https://pylot.readthedocs.io/en/latest/manual_installation.html>

克隆Pylot项目

```
git clone https://github.com/erdos-project/pylot
```

配置环境变量

```
cd pylot
export PYLOT_HOME=`pwd`/
```

安装torch依赖（有些模块的编译要用到）

```
pip install torchvision 
```

运行自动安装脚本

```
./install.sh
```

> ***注意 1***:该安装脚本实际上在下载各种依赖并根据需要编译。请保证国际互联网网络连接正常。

> ***注意 2***:如果本地已有CARLA（目前的环境要求为CARLA 0.9.10.1），不想重复下载，可以将脚本末尾的CARLA相关部分注释掉。

> ***注意 3***:该脚本缺乏整体的异常处理机制，如果没有信心一次性安装成功，可以手动执行里面每个模块的命令。在前文所述的开发环境下，DCNv2无法成功编译，不过不影响没有用到该模块的测试运行。

检查并安装python依赖（确保当前Python版本为3.6.9）

```
pip install -e ./
```

## 运行测试

如果使用本地的CARLA 0.9.10.1,则需要下文中的CARLA_HOME替换为CARLA所在的本地路径。
打开两个terminal。

Terminal 1：启动模拟器

```
cd $CARLA_HOME

./CarlaUE4.sh -opengl -windowed -ResX=800 -ResY=600 -carla-server  -benchmark -fps=20 -quality-level=Epic
```

Terminal 2:运行pylot

先进入pylot的根目录，设置环境变量

```
export PYLOT_HOME=`pwd`/

export CARLA_HOME=$PYLOT_HOME/dependencies/CARLA_0.9.10.1/

cd $PYLOT_HOME/scripts/

source ./set_pythonpath.sh
```

再运行测试（这里的示例为运行detection模块并可视化显示）

```
cd  $PYLOT_HOME/

python3 pylot.py --flagfile=configs/detection.conf --visualize_detected_obstacles
```

## 运行Pylot

安装相关Python依赖：

```
pip install websockets
```

确保本机9088端口能够使用，随后在Pylot项目根目录运行：

```
python server/local_controller.py 
```

即可启动Pylot服务。
