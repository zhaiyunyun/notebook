# 简介

[代码仓库](https://github.com/BerkeleyLearnVerify/Scenic)

scenic是伯克利开发的场景描述语言，2020年12月发布2.0版本，目前已经基本停止更新维护了（21年提交9次更新），但它设计的behavior，action和Mutations概念，以及对不同模拟器的抽象的架构设计，对场景描述语言的设计有借鉴意义。

需要注意的是：scenic语言本质上是扩展了python的语法，scenic会翻译为python代码后执行

# 主要文件

```bash
├── core
│   ├── scenarios.py
│   ├── simulators.py
├── domains
│   ├── driving
│   │   ├── actions.py
│   │   ├── behaviors.scenic
│   │   ├── controllers.py
│   │   ├── model.scenic
│   │   ├── roads.py
│   │   ├── simulators.py
│   │   └── workspace.py
├── simulators
│   ├── carla
│   │   ├── actions.py
│   │   ├── behaviors.scenic
│   │   ├── blueprints.scenic
│   │   ├── controller.py
│   │   ├── model.scenic
│   │   ├── simulator.py
└── syntax
    ├── translator.py
    └── veneer.py  #scenic语言实现的重点
```

examples/carla/trafficLights.scenic

会默认加载

/scenic/simulators/carla/model.scenic
/scenic/simulators/carla/behaviors.scenic
/scenic/simulators/carla/blueprints.scenic
/scenic/domains/driving/model.scenic
/scenic/domains/driving/behaviors.scenic

# 主流程

[主流程](../images/scenic主流程.dio ':include :type=code')

德萨  FDS

[filename](https://cdn.jsdelivr.net/npm/docsify-drawio/test.drawio ':include :type=code')

# 编译过程

```bash
# 使用--dump-python参数，可以查看scenic编译后生成的python代码
scenic --dump-python -S examples/carla/trafficLights.scenic
```

[编译过程](../images/scenic编译.dio)

# CarlaSimulator类图

[CarlaSimulator类图](../images/scneic-CarlaSimulator类图.dio)

**TODO**

behavior，action实现流程
Mutations实现
