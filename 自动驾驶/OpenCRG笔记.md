# OpenCRG笔记

OpenCRG属于OpenX系列，是描述道路表面质量的文件标准格式。官方标准文件可见[用户指南](https://www.asam.net/index.php?eID=dumpFile&t=f&f=3950&token=21a7ae456ec0eb0f9ec3aee5bae3e8c9ebaea140)。

## OpenCRG工具

**1.** 官方提供了C-API和MATLAB-API两种方式，下载方式如下：

​ 1.1 Open <https://www.asam.net/standards/detail/opencrg/>.

​ 1.2 Navigate to the *Download* area.

​ 1.3 Follow the instructions to download the software package.

​ 1.4 Unpack the archive into a local directory.

**2.** 以MATLAB为例，调用API生成OpenCRG文件：[MATLAB-API使用方法](https://www.asam.net/index.php?eID=dumpFile&t=f&f=3950&token=21a7ae456ec0eb0f9ec3aee5bae3e8c9ebaea140#_usage_of_the_matlab_tools)

​ 2.1 运行`OpenCRG/matlab/crg_init.m`将程序目录添加到路径中

​ 2.2 调用API描述路面质量，输出.crg文件，demo如下：

```
% clear enviroment
clear all;
close all

% display results
dispRes = 0;

% build minimum crg-struct
uinc = 0.01;
vinc = 0.01;
nv = 201;
nu = 5*nv;

z = 0.01*rand(nv,nv);
nvt = nv-1;
for i=2*nvt/5:4*nvt/5
 for j = 1:nvt/5
        z(i,2*nvt/5+j)=0.01*(1-j);
        z(i,4*nvt/5-j)=0.01*(1-j);
 end   
end

z = repmat(z, nu/nv, 1);

%% Demo1: crg defined by z matrix and scalar u and v specs

data.u = (nu-1)*uinc;
data.v = (nv-1)*vinc/2;
data.z = z
data.ct{1} = 'CRG defined by z matrix';

%% write demo1.crg file
crg_write(crg_single(data), 'demo1.crg');

%% load demo road
dat = crg_read('demo1.crg');

%% visualize road
dat = crg_show(dat);
```

​ 其中，`u`可理解为沿着道路延申的方向，`v`垂直于道路延申方向，`z`是道路的高程。上述demo主要通过构造`z`矩阵，描述道路表面突起和凹陷的情况。
