网络问题：

1. github访问慢，`github.com`修改为：`github.com.cnpmjs.org`

2. pip安装慢，`pip install -i https://pypi.tuna.tsinghua.edu.cn/simple some-package`

环境要求：

1. carla 0.9.9 下载地址：`https://carla-releases.s3.eu-west-3.amazonaws.com/Linux/CARLA_0.9.9.tar.gz`

2. python 3.8

# 一：运行carla客户端示例
创建客户端运行的虚拟环境

选择要在虚拟环境使用的python版本，比如python3.7等

`python3.7 -m venv carla_0_9_9_python_3_7  // env-dir表示存储你的虚拟环境的目录`

`source carla_0_9_9_python_3_7/bin/activate  // 进入到虚拟环境`

`pip install pygame numpy networkx // 安装carla python api运行依赖包`

`cd PythonAPI/examples`

`python automatic_control.py  // 运行carla示例`

# 二：安装scenic
方式一：在python3.8的虚拟环境中：
```pip install scenic```

方式二：源码安装
```
git clone https://github.com.cnpmjs.org/BerkeleyLearnVerify/Scenic.git

pip install poetry  // 安装poetry

export PATH=/home/ubuntu/.local/bin/:$PATH // 将poetry加入到系统路径

cd Scenic

poetry install // 利用poetry安装scenic依赖

poetry shell // 进入到scenic自动创建的虚拟环境

// 添加carla客户端的库到PYTHONPATH，或者可以easy_install /carla/dist/carla-0.9.9-py3.7-linux-x86_64.egg

export PYTHONPATH=/home/ubuntu/yunyun/PythonAPI/carla/dist/carla-0.9.9-py3.7-linux-x86_64.egg:/home/ubuntu/yunyun/PythonAPI/carla/

scenic examples/driving/badlyParkedCarPullingIn.scenic --simulate --model scenic.simulators.carla.model --time 200
```


