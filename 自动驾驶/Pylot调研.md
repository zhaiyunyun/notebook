# Pylot调研

Github项目：<https://github.com/erdos-project/pylot>

官方文档：<https://pylot.readthedocs.io/en/latest/>

## 1. 安装

可以通过编译安装和拉取Docker镜像两种方式获取Pylot。
使用Docker的具体步骤参考官方文档的[Quick Start](https://pylot.readthedocs.io/en/latest/quick_start.html)部分。

根据本地测试，以docker方式运行会出现初始化时间过长等问题。通过本地编译的方式部署的Pylot可以正常运行。编译安装的具体方式参考文档[Pylot编译](./Pylot编译安装及运行.md)。

## 2. 运行

### 2.1 容器内部运行

按照官方文档的[Quick Start](https://pylot.readthedocs.io/en/latest/quick_start.html)部分，容器内部已经包含了Carla 0.9.10.1版本，可以在容器内部启动模拟器可Pylot主程序进行测试。

如果要观看模拟器界面和可视化图像，需要通过SSH连接容器。
这种运行方式适合部署环境，开发时稍有不便。

### 2.2 基于容器外模拟器运行

在本地开发时，可以在本地启动Carla 0.9.10.1服务：

```
./CarlaUE4.sh -opengl -windowed -ResX=800 -ResY=600 -carla-server  -benchmark -fps=20 -quality-level=Epic
```

随后使用host的网络参数启动Pylot容器，直接运行pylot主程序即可。

## 3. 分模块测试

Pylot主要分为
Perception 感知、
Prediction 预测、
Planning 规划和
Control 控制四个大模块。

### 3.1 Perception

#### 3.1.1 Depth estimation

- 输入：cameras提供的图像
- 模型：AnyNet neural network
- 输出：深度估计的数值

#### 3.1.2 Detection

##### a) Obstacle detection

- 输入：cameras提供的图像
- 模型：

1. 符合Tensorflow object detection model zoo的任何模型。Pylot默认提供faster-rcnn、ssd-mobilenet-fpn-640和ssdlit-mobilenet-v2
2. 在COCO数据集上训练的EfficientDet

- 输出：检测物体的轮廓及标签

![](https://pylot.readthedocs.io/en/latest/_images/pylot-obstacle-detection.png)

##### b) Traffic light detection

- 输入：cameras提供的图像
- 模型：Faster RCNN（在1280x720分辨率下效果最好）
- 输出：检测物体的轮廓及标签

![](https://pylot.readthedocs.io/en/latest/_images/pylot-traffic-light-detection.png)

##### c) Lane detection

- 输入：cameras提供的图像
- 模型：Canny Edge, Lanenet
- 输出：道路轮廓

![](https://pylot.readthedocs.io/en/latest/_images/pylot-lane-detection.png)

#### 3.1.3 Segmentation

- 输入：cameras提供的图像
- 模型：DRN neural network
- 输出：语义分割后的图像

![](https://pylot.readthedocs.io/en/latest/_images/pylot-segmentation.png)

> 当前的语义分割模型不是在Carla数据上训练的，该模型输出的数据不会被Pylot的其他模块所用

#### 3.1.4 Obstacle tracking

- 输入：cameras提供的图像
- 模型：DaSiamRPN, SORT, Deep SORT
- 输出：持续追踪检测到的障碍物的轮廓示意及标签

### 3.2 Prediction

- 输入：模拟器提供的真实感知数据
- 模型：Linear predictor, R2P2
- 输出：对其他交通参与者的行动轨迹预测

![](https://pylot.readthedocs.io/en/latest/_images/world-visualization.png)

### 3.3 Planning

- 输入：模拟器提供的真实感知数据
- 模型： Frenet optimal trajectory, RRT*, and Hybrid A*
- 输出：Ego车辆后续的行驶路线上的waypoint

![](https://pylot.readthedocs.io/en/latest/_images/pylot-planning.png)

### 3.4 Control

- 输入：模拟器提供的真实感知数据、Planning模块的规划数据
- 算法：MPC、PID
- 输出：使Ego车辆按照规划路径行驶的控制指令

## 4. 已知Bug

### 4.1 行人生成点错误

在需要生成行人的场景，有时会出现行人生成失败导致程序中断的问题。需要修改/home/erdos/workspace/pylot/pylot/simulation/utils.py 243行，添加

```
# change z value
spawn_point.location.z += 1
```
