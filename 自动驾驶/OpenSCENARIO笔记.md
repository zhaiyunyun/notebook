# 目录

- [ASAM与OpenX标准体系](../ASAM标准.md)
- [OSC标准文档](#标准文档)
- [OSC标准解读](#标准解读)
  - [场景定义](#场景)
  - [OpenSCENARIO定义](#OpenSCENARIO)
  - [抽象架构](#场景)
  - [场景组成结构](#场景组成结构)
  - [执行流程](#执行流程)
  - [示例演示](#示例演示)
  - [OSC2.0规划](#OSC2.0规划)
- [测试工具：esmin](#测试工具：esmin)
- [参考引用](#参考引用)

# 标准文档

[用户指南](https://www.asam.net/index.php?eID=dumpFile&t=f&f=4092&token=d3b6a55e911b22179e3c0895fe2caae8f5492467)

[模型文档](https://www.asam.net/static_downloads/modelDocumentation/index.html)

[中文翻译](https://www.asam.net/index.php?eID=dumpFile&t=f&f=3769&token=92f25a7707bead3ac650df003bfcc89ab945287b)

[文档和代码下载](https://guard-strike.oss-cn-shanghai.aliyuncs.com/ADTest/openscenario.zip)

# 标准解读

## 场景

包括静态和动态两个方面

静态环境描述：包括路网结构和道路设施（红绿灯，交通标志灯）

动态内容描述：动态变化内容，包括天气变化，灯光变化，交通信号灯状态变化，动态实体（比如车辆，人，障碍我）互动的完整过程，动态实体的行为模型（可选）

## OpenSCENARIO

OpenSCENARIO主要用于描述复杂的，涉及多个交通参与者的同步动作，场景的描述一般基于驾驶行为，比如变道，超车，按导航行驶等。

**不包括**：测试语言，测试验证，驾驶员模型，动力学模型，环境模型

## 抽象架构

![抽象架构图](../images/OpenSCENARIO抽象架构.png)

OSC Model：场景设计文件，符合osc规范
Road Network：路网结构，opendrive或NDS文件
openCRG：道路表面描述文件

OSC Director：场景解析执行器，包含以下元素

- Storyboard (1 per scenario)
- Story instances (0..* per Storyboard)
- Act instances (1..* per Story)
- ManeuverGroup instances (1..* per Act)
- Maneuver instances (0..* per ManeuverGroup)
- Event instances (1..* per Maneuver)
- Action instances (1..* per Event)

Simulator Core：模拟器，包含以下元素

- 实体，如车辆行人等交通参与者.
- 环境参数，如时间，天气，道路条件.ditions.
- 交通信号控制器和交通信号
- 交通对象，如车流等
- 控制器，默认控制器，用户自定义控制器如模拟驾驶员或真实司机
- 控制策略，由实体的动作引发的控制指令
- 用户参数

## 场景组成结构

![OpenSCENARIO组成结构](../images/OpenSCENARIO组成结构.png)

四个主要组件：

1. Road network: 逻辑路网（opendrive），三维环境模型（osgb），交通信号灯
2. Entities: 道路使用者，包括车辆，行人，其他物体
3. Actions: 定义实体的动态行为，比如加减速，变道等
4. Triggers: 触发器

## 控制器，实体，控制策略

**Action动作**

动作规定了对象的行为或者设定对象状态
有三个动作类型：

1. 专属动作，属于实体的动作，比如交通参与者的动作
2. 全局动作，非实体动作，比如时间，天气，交通信号状态
3. 自定义动作，用于在场景设计者和模拟实现者之间约定的动作

专属动作的类型：

1. LongitudinalAction：纵向动作，包括SpeedAction和LongitudinalDistanceAction
2. LateralAction：横向动作，LateralDistanceAction，LaneOffsetAction，LaneChangeAction
3. VisibilityAction：可视性动作
4. SynchronizeAction：同步动作
5. ActivateControllerAction：激活控制器动作
6. ControllerAction：控制器动作，比如给车辆分配驾驶员模型
7. TeleportAction：传送动作
8. RoutingAction：路径选择动作

全局动作类型：

1. EnvironmentAction：设置天气事件等
2. EntityAction：添加删除实体
3. ParameterAction：设置修改参数
4. InfrastructureAction：设置修改交通信号灯状态或者交通信号灯控制器
5. TrafficAction：交通流操作

用户自定义动作：

1. CustomCommandAction：自定义命令

此外，标准还规定了动作冲突解决方案，动作完成的条件，机动组（一个动作同时控制多个实体）

**控制策略**

控制策略规定了动作的具体行为，OSC Director负责将动作映射到模拟器中
>所有实体任何时间都有横向和纵向的控制策略，如果没有指定，则使用默认控制策略，横向默认控制策略是保持车道和车道内偏移，纵向默认控制策略是保持当前车速

**控制器**

控制器用来执行控制策略，OpenSCENARIO并不规定控制器的具体实现

控制器的目的：

1. 指定车辆由待测系统（autoware等）控制
2. 智能控制模型算法控制，人类驾驶行为建模
3. 由人控制

有两种类型

1. 默认控制器，严格执行默认控制策略
2. 自定义控制器，控制策略的解释执行，所以只有默认控制器的场景是可以回放的

激活控制器，并不能影响分配在实体上的控制策略以及action对应的控制策略

控制器、控制策略、动作，三者关系

## 实体

![抽象架构图](../images/OpenSCENARIO实体类图.png)

ScenarioObject 表示单一实体，EntitySelection 可以表示多个实体
ScenarioObject 目前分为：Vehicle，Pedestrian，MiscObject，ExternalObjectReference

注意：每个ScenarioObject都可以ObjectController属性指定自己的控制器，model3d可以指定fbx等三位模型

## 执行流程

元素状态：completeState，runningState

启动完成条件：
|  元素   | 启动条件  | 完成条件  |
|  ----  | ----  | ----  |
| storyboard  | init结束 | stop trigger |
| story  | storyboard进入runningState | 1. storyboard触发停止，2. Acts执行结束 |
| Act  | start trigger | 1. stop trigger，2. story被停止 |
| ManeuverGroup  | Act进入runningState | 所有maneuver结束 |
| maneuver  | ManeuverGroup进入runningState | 所有Event结束 |
| event  | start trigger | act结束或者被优先级更好事件覆盖 |
| action  | event进入runningState | 动作完成或者被停止 |

总结：

1. 程序启动，storyboard，story进入运行状态
2. Act的start trigger触发，ManeuverGroup，maneuver进入运行状态
3. event的start trigger触发，action开始执行
4. 场景的动态推进是有Act和event的trigger触发的

## 示例演示

![超车示意图](./SimpleOvertake示意图.png)

SimpleOvertake.xosc

## OSC2.0规划

项目状态更新：<https://www.asam.net/active-projects/development-update-openscenario-v20/#42LanguageConcepts>

时间节点：2021年7月发布初稿

主要的几个小组

1. DSL，领域专用语言，类似scenic，M-SDL
2. 测试评价标准，包括成功标准，和覆盖率（功能覆盖率）
3. 特性集，工具实现支持的特性，比如回放，测试评价标准等
4. 领域模型，比如Vehicles​，Vehicle Actions​，Pedestrians​，Pedestrian Actions​，Environmental Conditions​，Coordinate Systems​，Road Abstractions​，Traffic Signals & Communication​

# 测试工具：esmin

## 功能介绍

esmini（Environment Simulator Minimalistic） is a basic OpenSCENARIO player，[github地址](https://github.com/esmini/esmini)

主要包含两个库：

- RoadManager (esminiRMLib)，解析OpenDRIVE格式地图
- ScenarioEngine (esminiLib)，提供OpenSCENARIO场景解析和显示

这样可以很方便的集成到其他的应用中

一些工具：

- esmini. scenario播放器，静态链接方式
- esmini-dyn. 动态链接方式的esmini
- odrplot. 导出OpenDRIVE地图数据导csv格式供python使用
- odrviewer. OpenDRIVE可视化
- replayer. Re-play previously executed scenarios.
- osireceiver. A simple application receiving OSI messages from esmini over UDP

外部工具：OpenScenarioEditor

OpenSCENARIO 可视化的编辑器，依赖esmini，[github地址](https://github.com/ebadi/OpenScenarioEditor)

示例中的osgb模型都是通过：[VIRES Road Network Editor](https://vires.mscsoftware.com/solutions/3d-environment-road-network/) 生成的

# 参考引用

## 逻辑路网

1. OpenDRIVE
2. NDS(Navigation Data Standard): [官网](https://nds-association.org/)

## 3D 模型

1. CityGML
2. OpenSceneGraph，[官网](http://www.openscenegraph.org/)，[github地址](https://github.com/openscenegraph/OpenSceneGraph)
3. FBX (Autodesk)
4. 3ds (Autodesk)

## xml和xsd

xsd:all 所有子元素都必须出现，顺序无关
xsd:sequence 子元素出现顺序固定
xsd:choice 只有一个子元素才能出现
