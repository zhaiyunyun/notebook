# 目录

- [目录](#目录)
- [Carla API](#carla-api)
- [scenario_runner](#scenario_runner)
  - [控制器](#控制器)
  - [原子动作](#原子动作)
  - [触发条件](#触发条件)
  - [状态监控](#状态监控)
  - [Scenario](#scenario)

# Carla API

1. 车辆控制：carla.VehicleControl（throttle=0.0, steer=0.0, brake=0.0, hand_brake=False, reverse=False, manual_gear_shift=False, gear=0）
2. PID控制器：VehiclePIDController包含横向和纵向控制
纵向控制根据目标速度计算加速度，进而计算油门throttle和刹车brake
横向控制根据目标waypoint计算转角steer
代码在：carla pythonapi agents的navigation下
3. planner规划器：
根据当前位置自动生成waypoint列表（利用了carla waypoint next函数），多条路径随机选择（比如十字路口），每次step利用VehiclePIDController根据下一个waypoint的目标速度和位置生成控制指令
set_global_plan函数，可以用外部生成的waypoint替换自动生成
carla pythonapi下
4. 代理
class Agent(object):
class BasicAgent(Agent): 利用规划器规划路径到目的地，具有遵守红绿灯，避障功能
class BehaviorAgent(Agent): 到最短路径到目的地，具有交通规则，变道超车，保持安全距离等
class RoamingAgent(Agent):
carla pythonapi下

carla ros-bridge
<https://carla.readthedocs.io/projects/ros-bridge/en/latest/>
Features
Provide Sensor Data (Lidar, Semantic lidar, Cameras (depth, segmentation, rgb, dvs), GNSS, Radar, IMU)
Provide Object Data (Transforms (via tf), Traffic light status, Visualization markers, Collision, Lane invasion)
Control AD Agents (Steer/Throttle/Brake)
Control CARLA (Play/pause simulation, Set simulation parameters)

# scenario_runner

[仓库](https://github.com/carla-simulator/scenario_runner)
[文档](https://carla-scenariorunner.readthedocs.io/en/latest/)

## 控制器

控制器，功能上跟自驾系统类似，不过这个非常简单，只提供规划和pid控制器接口来控制车辆

class ActorControl(object): This class provides a wrapper (access mechanism) for user-defined actor controls
class BasicControl(object): This class is the base class for user-defined actor controllers
class CarlaAutoPilotControl(BasicControl):
class ExternalControl(BasicControl):
class NpcVehicleControl(BasicControl):
class PedestrianControl(BasicControl):
class SimpleVehicleControl(BasicControl):
class VehicleLongitudinalControl(BasicControl):

## 原子动作

使用行为树实现，参见：[行为树](../_posts/机器学习之决策树.md#行为树)
class AtomicBehavior(py_trees.behaviour.Behaviour):
class ChangeActorLateralMotion(AtomicBehavior):

## 触发条件

class AtomicCondition(py_trees.behaviour.Behaviour):
class TriggerVelocity(AtomicCondition):

## 状态监控

在整个场景执行过程中的状态检测，判断执行成功还是失败
class Criterion(py_trees.behaviour.Behaviour):
class CollisionTest(Criterion):

## Scenario

class BasicScenario(object):
class OpenScenario(BasicScenario):
class RouteScenario(BasicScenario):

BasicScenario 使用原子动作中，直接对车辆进行控制，通过carla actor apply_control
OpenScenario 默认使用NpcVehicleControl和PedestrianControl进行控制，在行为树顶层添加**UpdateAllActorControls**节点进行外部控制调用更新控制
