# OpenDRIVE坐标系

opendrive坐标系：<https://blog.csdn.net/whuzhang16/article/details/110388309>

OpenDRIVE使用三种类型的坐标系，如下图所示：

惯性坐标系：x（东）/y（北）/z（上）
参考线坐标系：s/t/h
局部坐标系：u/v/z
与参考系相关联的还有航向角/偏航角（heading）、俯仰角（pitch）和横摆角/翻滚角（roll）

对**惯性坐标系**，一般使用WGS-84经纬度坐标系确定原点，然后使用UTM投影坐标系将三维坐标点转换为二维平面xy坐标。
在OpenDRIVE中使用的xy坐标，全部都是投影后的xy坐标

```xml
<geoReference>
           <![CDATA[+proj=utm +zone=32 +ellps=WGS84 +datum=WGS84 +units= m+no_defs]]> 
</geoReference> 
```

使用格式化为 “proj4”-字符串的投影定义对路网进行地理参照转化. PROJ 是一种通用坐标变换软件, 它将地理空间坐标从一个坐标参考系统 (CRS) 转换为另一个坐标参考系统。这包括制图投影和大地测量转换。 geoReference元素定义了该文件使用的投影坐标系，其中地理坐标系为WGS-84

[地理坐标投影坐标及转换](https://page.om.qq.com/page/Oea2CKzZ0MKCjfSUc1lLWU-A0)

对**参考线坐标系**位置与方向的设定则相对于惯性坐标系来开展
参考线总是被放置在由惯性坐标系定义的x/y平面里。
s ：坐标沿参考线，以[m]为单位，由道路参考线的起点开始测量，在xy平面中计算（也就是说，这里不考虑道路的高程剖面）；
t ：侧面，在惯性x/y平面里正向向左；
h ：在右手坐标系中垂直于st平面；

通过planview定义道路的参考线：

```xml
 <planView>
    <geometry s="0.0000000000000000e+00" x="-3.0024844302609563e+03" y="2.7962779217697680e+03" hdg="2.8046409307224991e+00" length="1.3647961224498889e+02">
        <arc curvature="-3.3912133325369736e-05" />
    </geometry>
    <geometry s="1.3647961224498889e+02" x="-3.1311844847723842e+03" y="2.8416976011684164e+03" hdg="2.7974752903867355e+00" length="9.9592435217850038e+01">
        <arc curvature="-1.2467775981482813e-03" />
    </geometry>
</planView>
```

geometry用来画参考线，xy是起点，hdg是角度，下面的arc或者line是曲线形状，这样曲线就能画出来了，
然后通过s（s-t参考系的起点）和length就可以取这个曲线上的某一段
下一个的s起点，就是上一个的s+length

对**局部坐标系**的查找与定位将相对于参考线坐标系来进行。
u ：向前匹配 s
v ：向左匹配 t
z ：向上匹配 h

[opendrive三大坐标系](../images/opendrive坐标系.jpg)

## carla和autoware中的坐标系

# 坐标系介绍

## 左右手坐标系

<https://zhuanlan.zhihu.com/p/64707259>

## 八大坐标系

参考：八大坐标系 & OpenDRIVE.docx
