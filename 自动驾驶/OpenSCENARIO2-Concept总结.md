# 为什么选择DSL

osc1.0 xml格式 更底层和具体，适合机器执行，但用户需要更抽象描述方式

三种方式:

1. 通用编程语言接口，
2. 文本化自然语言，
3. DSL

选择的核心因素：领域专家不借助外部工具进行场景编写和审查，并且保留形式化精确描述和可供机器执行的能力

所以选择DSL，需支持从很抽象到很具体的场景描述

# 与1.0的关系

2.0是1.0的超集
2.0 采用了1.0，osc1.0 事件驱动执行模型
新增更多的action和仿真模型属性
新增场景组合，场景参数化

2.0 发布计划：2021年11月
<https://www.asam.net/project-detail/asam-openscenario-v20-1/#backToFilters>

# 基本概念

actor（**参与者**）：包括物理对象、地图、环境，表示参与者
action（**动作**）：是actor的行为，不可再分，由领域模型提供，就是actor对象提供的方法
scenario（**场景**）：把action或其他scenario按照时序或逻辑组成和成场景，包括组合操作符（serial, parallel），时间指令（wait，util），事件（）
modifier（**修饰符**）：用来修改action或scenario行为，本质上封装了action或scenario的调用参数

# 方法论

+ 声明性DSL
+ DSL概念
  + SCENARIO，参与者行为序列
  + ACTOR，交通参与者
  + ACTION，actor的功能
  + PHASE，阶段，原子阶段，复合阶段，SCENARIO也是一个PHASE
  + TEMPORAL COMPOSITION OPERATORS，(e.g. serial, parallel)
  + CONSTRAINT/MODIFIER，约束和修改
  + COVERAGE，希望收集的数据

# 领域模型
