# ASAM（德国自动化及测量系统协会）

官网：<https://www.asam.net/>
<http://www.c-asam.net/?s=news-show-id-2.html>

## 仿真领域主要有5个部分

1. OpenDRIVE 对应静态地图场景，负责描述地图信息，包括高精地图信息；
2. OpenCRG 对应道路表面，专注于车辆动力学，以及车辆对路面信息的反馈；
3. OpenSCENARIO 对应动态行为场景，也就是描述自动驾驶的测试场景；
4. OSI(Open Simulation Interface)，OSI标准定义了一个通用接口，用来连接自动驾驶功能的开发和各种驾驶模拟框架；
5. OpenLABEL 是今年新开的一个项目，研究的是场景标签与传感器原始数据。
6. OpenODD概念项目阶段，定义设计运行域（ODDs）的标准化格式；
7. OpenOntology新标准制定中，通过一个由本体论表示的公共域模型将OpenX标准串联起来。

![OpenX仿真标准](../images/ASAM-OpenX标准.png)

## OpenX系列标准关系

![OpenX体系结构](../images/ASAM-OpenX标准体系.png)

## 国内研究

C-ASAM: C-ASAM由中汽数据与德国自动化及测量系统标准协会联合发起成立，协调ASAM中国会员，共同制定ASAM标准
官网：<http://www.c-asam.net/>

## OpenDRIVE总结

[笔记整理](./OpenDRIVE笔记.md)

## OpenSCENARIO总结

[笔记整理](./OpenSCENARIO笔记.md)

## OSI 研究

[笔记整理](./OSI笔记.md)
