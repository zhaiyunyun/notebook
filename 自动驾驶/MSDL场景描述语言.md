# 概念

+ SCENARIO - 交通参与者的行为随时间的变化
+ ACTOR - 交通参与者
+ ACTION - 原子动作，比如车辆drive，行人walk等
+ PHASE - 阶段
  + atomic phase，单一参与者单一动作：car1 drives with 40 to 50 mph on a two-lane road for 10 to 20 seconds
  + composite phase，单一参与者多个动作，时间操作符合成多个动作，串行，并行
  + SCENARIO，多个参与者多个动作
+ TEMPORAL COMPOSITION OPERATORS - 时间操作符
+ CONSTRAINT/MODIFIER - 限制和修改，constraint用来修改参数，修改用来影响行为
+ COVERAGE - cover，覆盖率，场景测试的充分程度，主要是关键参数的覆盖情况
+ KPI - record，实现对评价公式中参数的抽样

# 基本数据类型

+ Scalar types
  + bool
  + number
  + enum
+ list types: 同一类型多个值
+ String types
+ Resource types
+ Compound types
  + Scenarios
  + Actors
  + Structs

## 类型作用

1. 对现实世界的抽象
2. 作为外部模块的接口，通过external进行远程过程调用使用，调用接口协议作为单独标准出现

## 类型成员

1. 成员变量，如：car1(标识符): car(类型)
2. 约束，keep(car.color == green)
3. 通用成员，事件，外部方法声明等
4. 验证，cover
5. 场景调用

## 表达式

表达式嵌套在类型成员中，比如在约束中，表达式由原子和操作符构成，原子由标识符和字面量构成，操作符包含算术和关系运算符以及**领域特定运算符**（比如在车后多少米）

# 声明语句

声明语句：枚举类型声明，struct声明，actor声明，scenario声明，Scenario modifier 声明，扩展语句

struct声明

```python
struct storm_data:
    storm: storm_type
    wind: speed
```

actor声明

```python
actor my_car : car:
    keep(category==four_wheel_drive)
```

scenario声明

```python
scenario vehicle.drive:
    lights: light_kind
```

Scenario modifier 声明

```python
modifier car.speed_and_lane: s: speed
    speed(speed: s)
```

extend声明

```python
extend storm_data: 
    wind_velocity: speed
```

## 参数的意义

对scenarios, actors, models参数的意义：

1. 逻辑场景，通过参数空间表示大量具体场景
2. 排除无用参数
3. 通过制定概率分布方法，选择合适的参数

参数，参数约束，参数概率分布

三种参数类型：

1. Parameters, 保持不变的，比如车的颜色，大小等，用keep约束keep(v.color === green)
2. variables, 随时间变化的，车速，相对距离等，modifiers来影响，speed([20..80]kph)
3. observations，观测值，场景运行结果，持续时间，是否发生碰撞等，modifiers影响，max_duration(80s)

参数概率分布：

1. 均匀分布
2. 正态分布
3. 直方图

通过外部调用，指定概率模型：

```python
def sample_normal(normal: normal_p):real is only \
  external.python("numpy.random.normal",normal.mean, normal.sd)
```

# 事件

```python
event too_close is (distance_between(car1, car2) < 10m) # 定义
emit <event-path>[(<param>+)] # 触发事件
wait <qualified-event> # 等待事件
on @near_collision: # 监听事件
    log_info("Near collision occurred.")

```

# car.drive

车辆驾驶参数：

+ **time** is a single time value or a range with a time unit, such as [100..120]s.
+ **path** is the actual path (road) on which the drive is performed. This parameter is required.
+ **exactly** specifies whether to perform the drive from the start to the end of the <path> or just
+ **adjust** specifies whether to perform automatic adjustment to achieve the desired
+ **with-block** is a list of one or more keep() constraints or scenario modifiers

drive的modifiers

+ acceleration
+ avoid_collisions
+ change_lane
+ change_speed
+ keep_lane
+ keep_position
+ keep_speed
+ lane
+ lateral
+ position
+ speed

# 内置对象

+ top
  + builtin
  + av_sim_adapter，represents M-SDL’s simulator interface
  + map，represents the sets of paths traveled by actors.
  + env，represents cars, pedestrians and so on.
  + dut，represents the AV system or device under test.

top.main()是整个程序入口

每个actor，内置start（），lifetime（）的scenario

每个scenario 内置 events: start, end, fail.

# 语法总结

msdl程序结构

+ Statements，包括：Enum、Struct、Actor、Scenario、Scenario modifier、extend，import声明
  + Enum声明

    ```python
    # 语法
    type <type-name> : [<member>*] 
    # 示例
    type car_type: [sedan = 1, truck = 2, bus = 3] 
    ```

  + Struct声明

    ```python
    # 语法
    struct <type-name>[: <base-struct-type>[(<condition>)]] [:
      <member>+]
    # 示例
    struct storm_data:
      storm: storm_type
      wind: speed
    ```

  + Actor声明

    ```python
    # 语法
    actor <actor-name>[: <base-actor-type>[(<condition>)]] [:
      <member>+]
    # 示例
    actor my_vehicle:
      category: vehicle_category
      emergency_vehicle: bool
      track_kind: track_kind
    ```

  + Scenario声明

    ```python
    # 语法
    scenario <name>[: <base-scenario-type>(<condition>)] [:
      <member>+]
    # 示例
    scenario vehicle.drive:
      lights: light_kind
    ```

  + Scenario modifier声明

    ```python
    # 语法
    modifier <name>[:
      <member>+]
    # 示例
    modifier car.speed_and_lane: 
      s: speed
      l: lane_type
      speed(speed: s)
      lane(lane: 1)
    ```

  + extend声明

    ```python
    # 语法
    extend <type-name> :
      <member>+
    # 示例
    extend car.bar:
      do f3: foo(y: 10)
    ```

  + import声明

    ```python
    # 语法
    import <path-name>
    # 示例
    import cut_in_and_slow_top
    ```

+ Struct, actor or scenario members，包括Field declarations（name:type）、Field constraints（keep）、Cover definitions（cover、record）、Events、External method
  
  + field

    ```python
    # 语法
    [!]<field-name>: [ <type> ][= <sample>][ <with-block> ]
    # 示例1：标量
    actor my_car: # Current car speed
      !current_speed: speed
    # 示例2：列表
    extend traffic:
      my_cars: list of car
    # 示例3：字符串
    struct data:
      name: string with:
        keep(it == "John Smith")
    # 示例4：sample
    extend dut.cut_in: 
      !speed_car1_get_ahead_end := sample(car1.state.speed, @get_ahead.end) with:
        cover(it, text: "Speed of car1 at get_ahead end (in kph)", unit: kph, range: [10..130], every: 10)
    ```

  + external method declaration

    ```python
    # 语法
    def <msdl-method-name> (<param>*) [: <return-type>] [ is empty | is undefined | is [first|only|also]<bind-exp> ]
    # 示例
    extend car:
      def calculate_dist_to_other_car(other_car: car) is external.cpp("calculate_dist_to_other_car", "libexample.so")
    extend top.main:
      on @c.slow.start:
        call dut.car.calculate_dist_to_other_car(c.car1)
    set_map(name: "$FTX_QA/odr_maps/hooder.xodr")
    do c: cut_in_and_slow()
    ```

  + keep()
  
    **NOTE:** keep() is also allowed in the with block of field declarations.

    ```python
    # 语法
    keep([<constraint-type>] <constraint-boolean-exp>)
    # 示例
    keep(x <= 3 and x > y)
    ```

  + event

    ```python
    # 语法
    event <event-name> [(<param>+)] [ is <qualified-event> ]
    # 示例
    event near_collision is @dut.too_close =>col
    ```

  + cover()& record()

    ```python
    # 语法
    cover|record (<exp> [, <param>* ])
    # cover示例
    current_speed: speed
    cover(current_speed, unit: kph)
    # record示例
    record([ttc_at_end_of_change_lane, dut_v_cls], event: start,
      text: "Cross record of TTC and absolute DUT velocity")
    ```

+ Scenario members，包括Scenario modifier invocation、do (behavior definition)

  + do (behavior definition)

  ```python
  # 语法
  do [only] <scenario-invocation>
  # 示例
  extend top.main:
    car1: car
    set_map(name: "my map")
    do z: car1.zip()
  ```

  + Scenario modifier invocation

  ```python
  # 语法
  [<label>: ]<scenario-modifier>(<params>)
  # 示例
  do parallel:
    dut.car.drive(path) with:
      s1: speed([50..120]kph)
    car1.drive(path, adjust: true) with:
      p1: position(distance: [5..100]m, behind: dut.car, at: start)
      p2: position(distance: [5..15]m, ahead_of: dut.car, at: end)
  ```

+ Scenario invocations

  ```python
  # 语法
  [<label-name>:] <scenario-name>(<param>*) [<with-block>]
  # 示例
  ss2: some_scenario(z: 4) with:
    keep(ss2.ss.x==3)
    keep(ss2.ss.y==5)
  ```

+ Expressions，强类型，操作符

几种内置scenario

Operator scenarios
serial parallel repeat mix try match   multi_match first_of，[if else if else]

Event-related scenarios
emit、wait、wait_time

Zero-time scenarios
call, dut.error,end,fail,log_*

movement scenarios
car.drive

scenario modifier
in, on, synchronize, trace(), until
movement scenarios modifiers
acceleration, change_lane等
map scenarios
path_length,等

keyword：
any, properties, when （手册中没说明)
label（感觉没用）

通用：
false true null undefined empty（默认值）
and not or

列表操作：
shape: list of point

类型定义和扩展
类型：struct actor scenario modifier type（枚举）
扩展：extend import
其他语言：def external call
类型检测：is
类型转换：as    <path-to-object>.as(<typename>)

场景行为定义：
do [only] <scenario-invocation>

in <scenario-path> <with-block>
in ci.ga.car1 with: speed([50..75]kph); lane(1)

with:
<member>+
or
with: <member1> [; <member2>;...]

约束：
keep soft or default （约束强度）

抽样：
sample

with:
<member>+

Operator scenarios：场景操作符
serial parallel repeat mix try match   multi_match first_of，[if else if else]

事件：
event， emit，wait，on，until

cover：record
cover(<exp> [, <param>* ])

示例：

调用其他语言：
def <msdl-method-name> (<param>*) [: <return-type>] [ is empty | is undefined | is [first|only|also]<bind-exp> ]
external.[e | python | cpp | shell] ( <param>* )
def calculate_dist_to_other_car(other_car: car) is external.cpp("calculate_dist_to_other_car", "libexample.so")

事件：
on @near_collision:
log_info("Near collision occurred.")

约束：
keep([<constraint-type>] <constraint-boolean-exp>)
Is either soft or default. default is allowed only in scenario fields. If neither soft nor default is
specified, the constraint type is hard. S

sample：
sample( <exp>, <qualified-event> )

类型检测：is
<path-to-object>.is(<type-name>)

The following specify an extension to a previously declared method:
• is also appends the specified method to the previously declared method(s).
• is first prepends the specified method to the previously declared method(s).
• is only replaces the previously declared method(s) with the specified method.

**Predefined identifiers**
The following identifiers are predefined in some contexts:
• me refers to the current type (struct, actor or scenario).
• it exists in a with context, referring to the with subject.
• actor exists in scenario declarations, and refers to the related actor instance.
• outer exists in an in context, and refers to the type in which in is declared

**M-SDL operators** and special characters
M-SDL supports the use of the following operators in expressions.
Operator
type
Operators Description
Boolean
comparison
==, !=, <, <=, >,
>=
Compares two expressions and returns a Boolean
Boolean
compound
and, or, => Join two simple Boolean expressions
Boolean
negation
!, not Negate a Boolean expression
List index￾ing
[n] Reference an item in a list
Boolean
implication
<exp1> =>
<exp2>
Returns true when the first expression is false or when
the second expression is true. This construct is the same
as: (not exp1) or (exp2)
Range [range] Reference a range of values, any of the following or com￾bination of the following: [i..] [i..j] [..j]
M-SDL basics PDF last generated: July 10, 2020
Measurable Scenario Description Language Reference Page 29
Operator
type
Operators Description
If then else x ? y : z Select an expression
Arithmetic + - * / % Perform arithmetic operations
Type check is(<type>) Check whether an object is a specified type
Type cast <path-to-object>.as(<typename>)
