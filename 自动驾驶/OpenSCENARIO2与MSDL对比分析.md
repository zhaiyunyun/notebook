# 结论

osc2跟MSDL几乎完全一样，**红色字体是osc2特有的**

# 类型系统

静态强类型语言，类型包括：

- 原始类型：布尔、整型、浮点、字符串

- 枚举类型

- 物理类型

- 复合类型

  - 结构化类型

    - 结构体：包含字段和方法

    - 参与者：包含字段和方法、动作

    - 场景：包含字段和方法、行为（do）

    - 动作：domain model中的原子动作

    - 修饰符：修饰动作或场景

  - 聚合类型：目前只支持列表聚合类型

结构化类型共有成员：

- 字段：它们代表这些复合类型中的命名数据成员。

  - 参数：参数是在场景执行之前评估和修复的字段

  - 变量：变量是允许随时间变化的字段，关键字var

- 方法：成员函数（def）

- 事件：*事件*是命名实体，表示时间上的零时间发生

- 约束：keep关键字

- cover and record

# 表达式

osc2表达式：[**https://asam-ev.github.io/public_release_candidate/asam-openscenario/2.0.0/language-reference/expressions_main.html**](https://asam-ev.github.io/public_release_candidate/asam-openscenario/2.0.0/language-reference/expressions_main.html)

经过对比，osc2.0与msdl完全一致

# domain model

msdl中没有关于领域模型的描述，domain model可以作为实现的设计类图
