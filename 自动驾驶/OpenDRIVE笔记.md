# 目录

- [ASAM与OpenX标准体系](../ASAM标准.md)
- [ODR标准文档](#标准文档)
- [ODR标准解读](#标准解读)
- [测试工具](#测试工具)

# 标准文档

[用户指南](https://www.asam.net/index.php?eID=dumpFile&t=f&f=4089&token=deea5d707e2d0edeeb4fccd544a973de4bc46a09)

[中文翻译](https://www.asam.net/index.php?eID=dumpFile&t=f&f=3768&token=66f6524fbfcdb16cfb89aae7b6ad6c82cfc2c7f2)

# 标准解读

OpenDrive格式地图数据解析：<https://blog.51cto.com/u_15147256/2798621>
OpenDrive：<https://www.codenong.com/cs105975308/>

OpenDrive作用：定义道路的几何结构，车道，和其他比如交通标线，交通信号等静态属性

![ODR组成结构](../images/OpenDRIVE组成结构.png)

总结：opendrive地图由多个road组成，每个road包含多个lane，

road 和road之间通过普通连接和Junction进行连接，同时还要将road中的相关车道进行连接。
普通连接：两条road之间，通过前驱后继定义就可以
junction：有多个连接，三种道路，incoming road、connecting road、
概念：Junctions link in-coming roads via paths (connecting roads) to out-going roads.
每个road都有一个junction属性标示是否属于某个junction
junction节点定义了，道路的连接关系

对于road先确定reference line，有了reference line的几何形状和位置，然后再确定reference line左右的车道lane,车道lane又有实线和虚线等属性；

道路参考线：通过
opendrive定义：定义道路的几何结构，车道，和其他比如交通标线，交通信号

道路参考线：通过直线，弧线，螺旋线等生产道路走向作为参考

# 测试工具

## 在线工具

<http://opendrive.bimant.com/>

## odrviewer

官方提供的工具，但网上已经找不到了
[下载链接](https://guard-strike.oss-cn-shanghai.aliyuncs.com/ADTest/opendrive.zip)

## covise

<https://github.com/hlrs-vis/covise>

## OpenRoadEd

OpenRoadEd是一个二维的OpenDRIVE和OpenSceneGraph地图可视化和编辑工具

<https://gitlab.com/OpenRoadEd/OpenRoadEd/-/tree/master>

可直接解压执行的二进制文件下载地址：<https://gitlab.com/OpenRoadEd/archive>

## 微信王方浩的工具

<https://github.com/daohu527/imap>
